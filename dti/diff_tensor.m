function [D,e] = diff_tensor(adc,q,W)

if ndims(adc) < 3
	error('diff_tensor: 1st arg must be at least 3D [Ndirs Ny Nx (Nz ...)]');
end

if nargin < 4
	W = diag(ones(size(q,1),1));  % Default to weighting of one
end

A = diff_getA(q);
%iA = pinv(A); 
iA = inv(transpose(A)*W*A) * transpose(A) * W; 
dim = size(adc);
adc = reshape(adc,size(adc,1),[]);
D = iA * adc;

if nargout >= 2
  e = W * (adc - A*D)/sum(vec(W));
  e = reshape(e.*e,dim);
end

D = reshape(D,[6,dim(2:end)]); % size(D) = [6 Ny Nx Nz]

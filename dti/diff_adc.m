function adc = diff_adc(t2w,dwi,hinfo)

adcthresholdlevel = 0.3;

if isstruct(dwi)
	dim = size(dwi(1).V);
    if (ndims(dim)) == 2, dim(end+1:3) = 1; end
	dim(4) = numel(dwi);
else
	dim = size(dwi);
	dim(end+1:4) = 1;
end

if nargin < 3
	bvalue = 2000;
	warning('assuming b = 1000\n');
else
	bvalue = hinfo.bvalue;
    if (length(bvalue)==1)
        bvalue = ones(dim(4),1)*bvalue;
    end
end


% mean T2w
if isstruct(t2w(1))
	t2w_mean = zeros(dim(1:3),class(t2w));
	for t = 1:numel(t2w)
		t2w_mean = t2w_mean + t2w(t).V / numel(t2w);
	end
else
	t2w_mean = mean(t2w,4);
end
t2_mean = abs(t2w_mean);

msk = zeros(dim,class(t2w)); msk = smooth(t2w_mean,5); msk = msk > mean(msk(:)) * adcthresholdlevel;

% ADC
adc = zeros([dim(4) dim(1:3)],class(t2w));

for d = 1:dim(4)
	if isstruct(dwi(1))
		adc(d,:,:,:) = log(abs(t2w_mean) ./ abs(dwi(d).V))    /bvalue * 1e6 .* msk;
	else
		adc(d,:,:,:) = log(abs(t2w_mean) ./ abs(dwi(:,:,:,d)))/bvalue(d) * 1e6 .* msk ;
	end
end

% set NaN's, inf's, negative ADC values and all pixels outside 'msk' to zero
adc(isnan(adc)) = 0; adc(isinf(adc)) = 0; adc(adc<0) = 0;





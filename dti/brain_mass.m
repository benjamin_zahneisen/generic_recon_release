function m=brain_mass(head,threshold,plot_flag)

if nargin < 3
    plot_flag = 1;
end

if nargin < 2
    threshold = 3;
end

%normalise to one
head=head/max(head(:));
maske=head>0.05;

mean_signal = mean(head(maske));
mean_noise = mean(head(~maske));

snr = mean_signal/mean_noise;

maske=head>(snr/threshold)*mean_noise;

%SE = strel('ball', 4, 4, 0);
SE=strel('disk',4,0);

IM2 = imopen(maske,SE);

[L,num] = bwlabeln(IM2);
stats=regionprops(L,'Area');
area=[stats.Area];
biggest=find(area == max(area));

brain_mass = (L==biggest);
%fill in holes
for k=1:size(head,3); brain_mass(:,:,k)=imfill(brain_mass(:,:,k),'holes');end

%brain_mass=smooth3(brain_mass,'gaussian',0.5);
brain_mass=brain_mass>0;

m = brain_mass;

if plot_flag
 figure; imagesc(array2mosaic(head)); colormap gray
 figure; imagesc(array2mosaic(head.*brain_mass)); colormap gray
end


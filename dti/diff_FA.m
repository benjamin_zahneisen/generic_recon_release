function [FA,C,L1,L2,L3] = diff_FA(D)

dim = [size(D,2) size(D,3) size(D,4)];
FA = zeros(dim);
if nargout > 1
   C = zeros([dim 3]);
end
if nargout > 2
   L1 = zeros(dim);
   L2 = zeros(dim);
   L3 = zeros(dim);
end

for y = 1:dim(1)
	for x = 1:dim(2)
		for z = 1:dim(3)
			d = D(:,y,x,z); % Dxx Dyy Dxx Dxy Dxz Dyz
			if ~all(d == 0)
			   [e_vec,e_val] = eig([d(1) d(4) d(5); d(4) d(2) d(6); d(5) d(6) d(3)]);
			   e_val = diag(e_val);
         FA(y,x,z) = sqrt((3/2)*2*var(e_val)./sum(e_val.^2));
         if nargout > 1
           C(y,x,z,:) = e_vec(:,e_val == max(e_val));
         end
         if nargout > 2
           e_val_sort = sort(e_val);
           L1(y,x,z) = e_val_sort(3);
           L2(y,x,z) = e_val_sort(2);
           L3(y,x,z) = e_val_sort(1);
         end
			else
			   FA(y,x,z) = 0;
         if nargout > 1, C(y,x,z,:) = 0; end
         if nargout > 2, L1(y,x,z) = 0; L2(y,x,z) = 0; L3(y,x,z) = 0; end        
			end
		end
	end
end

#!/bin/bash

echo "Run post processing with FSL's topup for a single time frame of a blip up/down dataset"


motherdir=`pwd`/${motherdir}
echo "$motherdir"

echo "processing frame $1"

#input checking
tframe=$1
infolder=$2
infolder2=$3

if [ -z "$tframe" ]; then
tframe=0;
fi
if [ -z "$infolder" ]; then
infolder='./blipUp';
fi
if [ -z "$infolder2" ]; then
infolder2='./blipDown';
fi

#name of folder that contains corrected output
outfolder='dico'
echo "$outfolder"
[ ! -d $outfolder ] && mkdir -p $outfolder


#single topup call

    #generate names
    echo "Running topup."
    fmap_name="fmap_$tframe"
    Vin_name="V_$tframe"
    Vout_name="Vout_$tframe"
    Voutavg_name="Vavg_$tframe"
    echo "$fmap_name"
    
    #call topup
    echo "Running topup."
    topup --imain=./$outfolder/$Vin_name --datain=./$outfolder/acqp.txt --config=b02b0.cnf --out=distortion_field --iout=./$outfolder/$Vout_name --fout=./$outfolder/$fmap_name

    #average both corrected volumes
    fslmaths ./$outfolder/$Vout_name -Tmean ./$outfolder/$Vout_name

    #clean up
    #rm ./$outfolder/$mergeTmp_name.*
    #rm ./$outfolder/$Vout_name.*
    #rm ./$outfolder/$mergeTmp_name.topup_log






#------------

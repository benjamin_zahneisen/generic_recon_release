function [X, voxSize]=importNIFTIS(fname,type)

if nargin < 2
    type ='none';
end

%do some file type checking
[pathstr,name,ext]= fileparts(fname);
if(isempty(pathstr))
    pathstr = '.'; %current directory
end
    
if((strcmp(ext,'.gz')) && (exist(fname,'file') == 2)) 
    %file exists and is of type gz
    gz_flag = true;
end
if((strcmp(ext,'.gz')) && (exist(fname,'file') == 0)) %file exists and is of type gz
    if(exist([pathstr,'/',name],'file') == 2)
        %that means fname input wa .nii.gz but file is only nii
        gz_flag = false;
        fname = [pathstr,'/',name];
    else
        disp('file does not exist');
        return
    end
end
if((strcmp(ext,'.nii')) && (exist(fname,'file') == 2)) 
    %file exists and is of type nii
    gz_flag = false;
end
if((strcmp(ext,'.nii')) && (exist(fname,'file') == 0)) 
    %file does not exist
    gz_flag = false;
    if(exist([pathstr,'/',fname,'.gz'],'file') == 2)
        %that means fname input wa .nii.gz but file is only nii
        gz_flag = true;
        fname = [pathstr,'/',fname,'.gz'];
    else
        disp('file does not exist');
        return
    end
end


%re-run on true filename
[pathstr,name,ext]= fileparts(fname);
if(isempty(pathstr))
    pathstr = '.'; %current directory
end

isCompressed = false;
if(strcmp(ext,'.gz'))
    %unzip file
    unix(['gunzip ',fname]);
    fname = name;
    isCompressed = true;
end

nii = load_nii([pathstr,'/',fname]);

X = double(nii.img);
voxSize = nii.hdr.dime.pixdim(2:4);

if strcmp(type,'generic')
    disp('re-orient for internal use in generic');
    X = X(end:-1:1,:,:,:);
    
else
    disp('no re-orientation');
end

if isCompressed
    if isempty(pathstr)
        unix(['gzip ',fname]);
    else
        unix(['gzip ',pathstr,'/',fname]);
    end
end
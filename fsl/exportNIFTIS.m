function exportNIFTIS(V,filename,voxel,type)

if nargin < 3
    voxel = [3 3 3];
end
if nargin < 4
    type = 'none';
end

dim = size(V);


%reorient images (somewhat arbitrary and subject to change)
if strcmp(type,'dicom')
    for t=1:size(V,4)
        V(:,:,:,t)=imrotate3D(V(:,:,:,t),-90);
    end
elseif strcmp(type,'nufft')
    disp('match nufft recon and fsl.');
    for t=1:size(V,4)
        V(:,:,:,t)=imrotate3D(V(:,:,:,t),-90);
        V(:,:,:,t)=V(:,:,end:-1:1,t);
    end
elseif strcmp(type,'generic')
    disp('generic re-orientation');
    V = V(end:-1:1,:,:,:);

else
    disp('no reorientation.');
        
end

nii=make_nii(V,voxel);
save_nii(nii,filename);

% filename.nii.gz format
unix(['gzip -f ',filename]);
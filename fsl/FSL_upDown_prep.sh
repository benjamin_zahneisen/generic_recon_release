#!/bin/bash

echo "prepare dataset"


motherdir=`pwd`/${motherdir}
echo "$motherdir"


#input checking
infolder=$1
infolder2=$2
outfolder=$3

if [ -z "$tframe" ]; then
tframe=0;
fi
if [ -z "$infolder" ]; then
infolder='./blipUp';
fi
if [ -z "$infolder2" ]; then
infolder2='./blipDown';
fi
if [ -z "$outfolder" ]; then
outfolder='./dico';
fi

#name of folder that contains corrected output
#outfolder='dico'
echo "$outfolder"
[ ! -d $outfolder ] && mkdir -p $outfolder


#get acquisition time
read VAR1 VAR2 VAR3 effAcqTime < ./blipUp/acqp.txt

echo "effective acquisition time for 1 slice $effAcqTime [ms]"

#create new acqp-file
echo "0 -1 0 $effAcqTime" > "./$outfolder/acqp.txt"
echo "0 1 0 $effAcqTime" >> "./$outfolder/acqp.txt"


#------------

function [f_topup, Vtopup,movPar]=run_topup(V_up,V_down,vox,acqTime,configFile,plot_flag,cleanUp)

%returns fieldmap in [rad/s]

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 5
    configFile ='b02b0.cnf';
end
if nargin < 7
    plot_flag = 0;
end
if nargin < 7
    cleanUp = 0;
end

GENERIC_HOME=getenv('GENERIC_RECON_HOME');


%add dummy slices at top and bottom
tmp = V_up;
V_up = zeros([size(tmp,1) size(tmp,2) size(tmp,3)+2]);
V_up(:,:,2:end-1) = tmp;
tmp = V_down;
V_down = zeros([size(tmp,1) size(tmp,2) size(tmp,3)+2]);
V_down(:,:,2:end-1) = tmp;

exportNIFTIS(abs(V_up),'b0_up.nii',vox,'generic');
exportNIFTIS(abs(V_down),'b0_down.nii',vox,'generic');
fid=fopen('acqp.txt','w'); fprintf(fid,'0 -1 0 %f \n',acqTime);fprintf(fid,'0 1 0 %f \n',acqTime);fclose(fid);

%cmd ='LD_LIBRARY_PATH=/usr/lib/fsl/5.0;fslmerge -t UpDown b0_down b0_up';unix(cmd);
cmd = 'fslmerge -t UpDown b0_down b0_up';
call_fsl(cmd);

%copy the config files (use local config file)
%if(~strcmp(configFile,'b02b0.cnf'))
cmd= ['cp ',GENERIC_HOME,'/fsl/',configFile,' .'];
unix(cmd);
%end

%call topup
cmd = ['topup --imain=UpDown --datain=acqp.txt --config=',configFile,' --out=topup_results --iout=hifi_b0 --fout=fieldmap -v'];
call_fsl(cmd)

f_topup = importNIFTIS('fieldmap.nii.gz','generic');
f_topup = f_topup(:,:,2:end-1); %remove dummy slices
f_topup = -f_topup*2*pi; %if down=(0 -1 0) and up=(0 1 0) then we have to multiply by -1 here


%import motion parameters [tx ty tz rx ry rz] in [mm] & [rad]
movPar=importdata('topup_results_movpar.txt');
movPar(1,:)=[]; %contains only zeros

try
    Vtopup = importNIFTIS('hifi_b0.nii','generic');
catch
    try
        Vtopup = importNIFTIS('hifi_b0.nii.gz','generic');
    catch
        disp('cannot load corrected recon')
    end
end
    
if plot_flag
    figure; 
    imagesc(array2mosaic(f_topup/(2*pi)),[-200 200])
    title('Fieldmap [Hz]')
end

if cleanUp
    try
        unix('rm hifi_b0.nii.gz');
        unix('rm UpDown.nii.gz')
        unix('rm b0_up.nii.gz');
        unix('rm b0_down.nii.gz');
        unix('rm fieldmap.nii.gz');
        unix('rm topup_results_fieldcoef.nii.gz');
        unix('rm topup_results_movpar.txt');
        unix('rm acqp.txt');
    catch
        disp('error deleting topup files')
    end
end
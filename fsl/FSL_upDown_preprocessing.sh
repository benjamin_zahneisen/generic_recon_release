#!/bin/bash

echo "Run post processing with FSL's topup for a blip up/down dataset"


motherdir=`pwd`/${motherdir}
echo "$motherdir"

#input checking
infolder=$1
infolder2=$2

if [ -z "$infolder" ]; then
infolder='./blipUp';
fi
if [ -z "$infolder2" ]; then
infolder2='./blipDown';
fi

#name of folder that contains corrected output
outfolder='dico'
echo "$outfolder"
mkdir $outfolder

#get number of volumes
numvols=$(fslnvols ./blipUp/dti.nii)

echo "number of vols in timeseries $numvols"

#get acquisition time
read VAR1 VAR2 VAR3 effAcqTime < ./blipUp/acqp.txt

echo "effective acquisition time for 1 slice $effAcqTime [ms]"

#create new acqp-file
echo "0 -1 0 $effAcqTime" > "./$outfolder/acqp.txt"
echo "0 1 0 $effAcqTime" >> "./$outfolder/acqp.txt"


#loop over volumes
for ((i=0;i<$numvols;i++)); do

    echo "processing volume $i"
    #extract blip down scan
    fslroi ./blipUp/dti.nii ./$outfolder/tmpUp $i 1
    fslroi ./blipDown/dti.nii ./$outfolder/tmpDown $i 1
    fslmerge -t ./$outfolder/Vtmp ./$outfolder/tmpDown ./$outfolder/tmpUp

    #no brain extraction here

    #call topup
    echo "Running topup."
    fmap_name="fmap_$i"
    echo "$fmap_name"
    topup --imain=./$outfolder/Vtmp --datain=./$outfolder/acqp.txt --config=b02b0.cnf --out=distortion_field --iout=./$outfolder/Vout --fout=./$outfolder/$fmap_name

    #average both corrected volumes
    fslmaths ./$outfolder/Vout -Tmean ./$outfolder/V2

    #merge all volumes to one timeseries
    if [ "$i" -eq "0" ]; then
        cp ./$outfolder/V2.nii.gz ./$outfolder/dti.nii.gz
    else
        fslmerge -t ./$outfolder/dti.nii ./$outfolder/dti.nii ./$outfolder/V2
    fi

done


#------------

function y=sinc(x)
%SINC Sin(pi*x)/(pi*x) function.
%   SINC(X) returns a matrix whose elements are the sinc of the elements 
%   of X, i.e.
%        y = sin(pi*x)/(pi*x)    if x ~= 0
%          = 1                   if x == 0
%   where x is an element of the input matrix and y is the resultant
%   output element.
%
%   See also SQUARE, SIN, COS, CHIRP, DIRIC, GAUSPULS, PULSTRAN, RECTPULS,
%   and TRIPULS.

%   Author(s): T. Krauss, 1-14-93
%   Copyright 1988-2002 The MathWorks, Inc.
%       $Revision: 831 $  $Date: 2009-07-13 11:15:32 -0700 (Mon, 13 Jul 2009) $

y=ones(size(x));
i=find(x);
y(i)=sin(pi*x(i))./(pi*x(i));

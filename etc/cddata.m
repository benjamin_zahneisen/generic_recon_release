function h = cddata(mypath)




if strcmp(computer, 'PCWIN')
   h = fullfile('F:',mypath);
else
    [dummy,hostname] = system('hostname');
    if strncmp(hostname,'p060-laptop5',10)
        h = fullfile('/windows/E',lower(mypath));
    else
        h = fullfile('/home/stefan/data',mypath);
    end
end

if ~exist(h,'dir')
   error('cddata: could not enter directory ''%s''\n',h);   
end

eval(sprintf('cd %s', h));

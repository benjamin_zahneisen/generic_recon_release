function cmddir(mypath,mycmd,mytype,debug)
  
  if ~exist('mytype','var') || isempty(mytype)
    mytype = 'matlab';
  end
  if ~exist('debug','var') || isempty(debug)
    debug = 0;
  end
  
  
  PWD = pwd;
  
  D = ls2(mypath,'-d');
  

  if debug == 2
    for d = 1:numel(D)
      tmpdir = D{d};
      if isdir(tmpdir)
        fprintf('Starting job in directory: %s\n',tmpdir);
        dothejob(d,tmpdir,mycmd,mytype);
        drawnow;
      end
      cd(PWD);
    end

  else
    if matlabpool('size')
      matlabpool('close');
    end   
    num_workers = min([numel(D) 8]);
    matlabpool('open','local',num_workers);
    
    parfor d = 1:numel(D)
      tmpdir = D{d};
      if isdir(tmpdir)
        fprintf('Starting job in directory: %s\n',tmpdir);
        if debug
          dothejob(d,tmpdir,mycmd,mytype)
        else
          try
            dothejob(d,tmpdir,mycmd,mytype)
          catch
            fprintf('\n\n\ncmddir: ERROR occured for dir %s\n\n\n',tmpdir);
            cd(PWD);
          end
        end
      end
      cd(PWD);
    end
    
    matlabpool('close');
  end
  
end

function dothejob(d,tmpdir,mycmd,mytype)
  cd(tmpdir);
  if strcmp(mytype,'system')
    system(mycmd);
  else
    eval(mycmd);
  end
end

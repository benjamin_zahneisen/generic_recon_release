function V=imrotate3D(V,angle)

if nargin < 2
    angle = 90;
end

if length(size(V)) == 3
    
    for n=1:size(V,3)
        V(:,:,n)=imrotate(V(:,:,n),angle);
    end
elseif length(size(V)) ==4
    for n=1:size(V,3)
        for m=1:size(V,4)
            V(:,:,n,m)=imrotate(V(:,:,n,m),angle);
        end
    end
end
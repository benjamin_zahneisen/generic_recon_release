function V = smooth4D(V,kernel,width,mode)

if nargin < 4
    mode = '3d'
end

if nargin < 3
    width = 0.5;
end
if nargin < 2
    kernel = [3 3 3];
end

for t= 1:size(V,4)
    if(strcmp(mode,'3d'))
    V(:,:,:,t) = smooth3(V(:,:,:,t),'gaussian',kernel,width);
    elseif(strcmp(mode,'slice'))
        for sl=1:size(V,3)
           V(:,:,sl,t) = smooth2(V(:,:,sl,t),'gaussian',kernel(1:2),width);
        end
    end
end
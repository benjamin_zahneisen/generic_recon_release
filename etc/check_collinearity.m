GE_D = readtensordat('tensor.dat');
jones_D = readtensordat('jonestensor.dat');

schemetypes = {'jones','GE'};

for s = 1:length(schemetypes)
  
  eval(sprintf('D = %s_D;',schemetypes{s}));
  
  for ddir = [6 7:s:length(D)] % s => do all #dirs for jones but only odd for GE

    fprintf('#dirs=%d: ',ddir);

    neighborangle = 1000*ones(size(D{ddir},1),1);
    for k = 1:size(D{ddir},1)
      for m = 1:size(D{ddir},1)
	if m ~= k
	  dotproduct = abs(dot(D{ddir}(k,:),D{ddir}(m,:)));
	  curangle = acos(dotproduct / (norm(D{ddir}(k,:))*norm(D{ddir}(m,:))))*180/pi;
	  if curangle < neighborangle(k)
	    neighborangle(k) = curangle;
	  end
	  if dotproduct > 0.999 & m > k
	    fprintf('(%d,%d) ',k,m);
	  end
	end % for m
	meanangle = mean(neighborangle);
      end % for k
    end
    
    figure(1);clf;plot(neighborangle,'bo-');title(sprintf('%s diffusion scheme (i.e. #dirs): %d  |<vk,vm>|',schemetypes{s},ddir));axis([-inf inf 0 max(40,max(neighborangle)+5)]); grid on
    hold on; plot([1 length(neighborangle)], [1 1]*meanangle,'r');
    ylabel('spherical angle [degrees]');xlabel('direction index in diffusion scheme');
    drawnow
    eval(sprintf('print -dtiff %s_#%03d',schemetypes{s},ddir));

    fprintf('\n');
    
  end
  
end



% Collinear vectors for JONES and GE. Numbers indicate index # in the corresponding diffusion scheme
% >> check_collinearity

% JONES
% #dirs=6: 
% #dirs=7: 
% #dirs=8: 
% #dirs=9: 
% #dirs=10: 
% #dirs=11: 
% #dirs=12: 
% #dirs=13: 
% #dirs=14: 
% #dirs=15: 
% #dirs=16: 
% #dirs=17: 
% #dirs=18: 
% #dirs=19: 
% #dirs=20: 
% #dirs=21: 
% #dirs=22: 
% #dirs=23: 
% #dirs=24: 
% #dirs=25: 
% #dirs=26: 
% #dirs=27: 
% #dirs=28: 
% #dirs=29: 
% #dirs=30: 
% #dirs=31: 
% #dirs=32: 
% #dirs=33: 
% #dirs=34: 
% #dirs=35: 
% #dirs=36: 
% #dirs=37: 
% #dirs=38: 
% #dirs=39: 
% #dirs=40: 
% #dirs=41: 
% #dirs=42: 
% #dirs=43: 
% #dirs=44: 
% #dirs=45: 
% #dirs=46: 
% #dirs=47: 
% #dirs=48: 
% #dirs=49: 
% #dirs=50: 
% #dirs=51: 
% #dirs=52: 
% #dirs=53: 
% #dirs=54: 
% #dirs=55: 
% #dirs=56: 
% #dirs=57: 
% #dirs=58: 
% #dirs=59: 
% #dirs=60: 
% #dirs=61: 
% #dirs=62: 
% #dirs=63: 
% #dirs=64: 
% #dirs=65: 
% #dirs=66: 
% #dirs=67: 
% #dirs=68: 
% #dirs=69: 
% #dirs=70: 
% #dirs=71: 
% #dirs=72: 
% #dirs=73: 
% #dirs=74: 
% #dirs=75: 


% GE
% #dirs=6: 
% #dirs=7: 
% #dirs=9: 
% #dirs=11: (4,5) (6,8) 
% #dirs=13: 
% #dirs=15: 
% #dirs=17: (11,12) 
% #dirs=19: 
% #dirs=21: 
% #dirs=23: (6,11) 
% #dirs=25: 
% #dirs=27: (1,25) 
% #dirs=29: (11,20) 
% #dirs=31: (3,9) (8,31) (11,22) (13,14) (15,18) (16,29) 
% #dirs=33: (2,19) (3,33) (5,13) (7,16) (8,30) 
% #dirs=35: 
% #dirs=37: (20,22) 
% #dirs=39: 
% #dirs=41: (5,38) 
% #dirs=43: (2,5) (13,27) (15,41) (22,37) (24,30) 
% #dirs=45: (4,28) (10,25) (12,16) (13,44) (15,18) (37,39) 
% #dirs=47: (2,7) (9,28) 
% #dirs=49: 
% #dirs=51: 
% #dirs=53: (16,31) 
% #dirs=55: (1,48) 
% #dirs=57: 
% #dirs=59: (8,13) (18,28) (32,51) 
% #dirs=61: (2,36) (11,42) (15,54) (20,53) (27,30) (28,51) 
% #dirs=63: (4,44) (6,16) (28,42) 
% #dirs=65: 
% #dirs=67: (34,51) 
% #dirs=69: 
% #dirs=71: (13,71) 
% #dirs=73: (3,23) 
% #dirs=75: 
% #dirs=77: (3,57) (4,36) (6,72) (8,44) (11,65) (16,24) (22,41) (26,58) (28,60) (34,52) (35,67) (37,76) (39,71) (45,63) (47,50) (55,75) 
% #dirs=79: (2,29) (6,47) (8,43) (11,48) (12,56) (13,45) (15,49) (17,18) (19,63) (23,73) (24,41) (26,64) (30,69) (31,38) (33,60) (34,46) (35,61) (39,78) (44,71) (67,79) (76,77) 
% #dirs=81: (5,12) (15,65) (21,74) 
% #dirs=83: (21,53) (25,65) (40,45) 
% #dirs=85: 
% #dirs=87: 
% #dirs=89: (11,78) (25,81) 
% #dirs=91: 
% #dirs=93: (2,33) (21,70) (59,61) (87,90) 
% #dirs=95: (7,50) (8,17) (14,87) (15,66) (31,76) (32,86) (49,59) (54,61) (72,89) 
% #dirs=97: (1,6) (4,15) (12,44) (18,77) (23,24) (27,95) (29,48) (39,85) (52,67) (56,88) (58,97) (62,64) (65,81) (72,93) 
% #dirs=99: (6,88) (12,95) (16,34) (20,23) (21,75) (33,37) (43,51) (55,63) (74,96) 
% #dirs=101: (18,64) (62,92) (75,93) (81,85) 
% #dirs=103: (3,12) (4,36) (34,70) 
% #dirs=105: 
% #dirs=107: 
% #dirs=109: (7,16) (34,83) 
% #dirs=111: 
% #dirs=113: (1,83) (9,50) (13,62) (14,34) (15,113) (18,64) (42,82) (70,74) (72,107) (109,111) 
% #dirs=115: (7,26) (77,78) (87,88) 
% #dirs=117: (17,27) (29,46) (30,43) (36,68) (67,71) 
% #dirs=119: (1,5) (2,33) (8,14) (15,80) (17,108) (21,109) (23,105) (26,65) (34,55) (36,107) (44,104) (46,116) (48,75) (51,89) (58,70) (61,64) (62,114) (66,92) (67,95) (68,85) (79,99) (93,100) 
% #dirs=121: (2,114) (3,104) (4,43) (6,87) (7,48) (11,74) (12,46) (19,50) (20,98) (22,42) (23,32) (25,82) (26,112) (28,47) (29,77) (30,60) (33,120) (34,116) (35,36) (37,51) (39,90) (40,84) (44,86) (45,70) (52,100) (56,67) (58,93) (59,92) (61,68) (62,76) (63,78) (71,118) (73,94) (75,109) (79,106) (83,121) (88,119) (95,117) (103,111) 
% #dirs=123: (1,71) (2,61) (3,109) (5,106) (6,16) (7,43) (8,21) (9,108) (10,102) (11,33) (13,50) (14,85) (18,66) (20,103) (22,41) (23,63) (24,47) (26,84) (27,74) (28,105) (29,73) (31,54) (32,87) (35,42) (36,118) (37,55) (38,44) (45,57) (46,112) (48,72) (49,76) (51,91) (52,67) (53,75) (56,120) (58,122) (62,81) (65,114) (68,82) (69,115) (77,90) (79,123) (88,93) (94,99) (97,98) (100,101) (104,107) (117,119) 
% #dirs=125: (19,25) (20,68) (30,55) (72,74) 
% #dirs=127: (4,19) (10,62) (24,113) (30,124) (32,63) (64,115) 
% #dirs=129: (2,119) (11,116) 

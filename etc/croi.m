function [roi_result, origin, radius,xroi] = croi (x, origin, radius)

xsize = size(x);

if length (xsize) == 3
  nreps = xsize (3);
elseif length(xsize) == 2
	nreps = 1;
else
  fprintf (['Input argument 1 must be a MxN matrix or MxNxt matrix.' ...
			' Press CTRL-C\n']);
end


clf;
x([1:5 (end-4):end],:,:) = 0;

if nargin < 3
  figure(100);
  imagesc(x(:,:,1));
  colormap gray;
  axis image;
end

if ~exist ('origin') | ~exist('radius')
	idata = ginput(2);
	origin = idata (1,:);
	tmp =  idata (2,:);
	radius = sqrt((tmp(1) - origin(1))^2 + (tmp(2) - origin(2))^2);
end

%if nargin < 3
%  fprintf ('origin = [%g %g]; radius = %g;\n', origin(1), origin(2), radius);
%end

xroimask = zeros (xsize(1), xsize(2));

for row = 1:xsize(1)
  for col = 1:xsize(2)
	if sqrt((row - origin(2))^2 + (col - origin(1))^2) < radius
	  xroimask(row,col) = 1;
	end
  end
end

npixels = sum (sum(xroimask));

for rep = 1:nreps

  xroi = x(:,:,rep) .* xroimask;
  
  if rep == 1 & nargin < 3
	colormap gray;
	subplot (1,2,1);
	imagesc(xroi);
	axis image off;
	subplot (1,2,2);
	xroinot = x(:,:,rep) .* ~xroimask;
	imagesc(xroinot);
	axis image off;
  end
  tmp = reshape (xroi(xroimask == 1), npixels, 1);
  roi_result (1,rep) = mean(tmp);
%  roi_result (2,rep) = std(tmp);
%  roi_result (3,rep) = max(tmp);
%  roi_result (4,rep) = min(tmp);
  
end

if nargout > 3
   xroimask = repmat(xroimask,[1 1 nreps]);
   xroi = x(xroimask > 0);
 end

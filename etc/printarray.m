function printarray(prefix, x, suffix,crstr)
if nargin == 1
  x = prefix;
else
  fprintf('%s ',prefix);
end
fprintf('%.2f ',x);

if nargin > 2
  fprintf('%s',suffix);  
end

if ~(nargin == 4 && strcmp(crstr,'nocr'))
	fprintf('\n');
end

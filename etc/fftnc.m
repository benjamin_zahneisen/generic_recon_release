function im = fftnc(d,dim)
% im = fftnc(d,dim)
%
% fftnc perfomrs a centered fftn
%

if nargin > 1
    im = fftshift(fft(fftshift(d,dim),[],dim),dim);
else    
    im = fftshift(fftn(fftshift(d)));
end
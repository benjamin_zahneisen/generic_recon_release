%changed by danielk1

function clean = strip_nonhex(dirty,sep)

if(numel(dirty)>2)
if strcmpi(dirty(1:2),'0x'), dirty = dirty(3:end); end

clean = dirty(regexp(dirty,'[a-f,A-F,0-9]'));
else
   disp 'Warning: strip_nonhex:  numel(dirty) is < 2 : returning dirty value'
   clean=dirty;
end
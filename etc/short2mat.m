function [x] = short2mat(fileptrn)

  pxlsize = 2;
  
  outputformat = 'vol';
  BigEndian = 0;
  
  if ~exist('fileptrn','var')
    fileptrn = 'I*';
  end
  
  files = ls2(fileptrn);
  
  
  for k = 1:numel(files)
    
    if BigEndian
      f = fopen(files{k},'r','b');
    else
      f = fopen(files{k},'r','l');
    end
    fseek(f,0,'eof');
    bytes = ftell(f);
    fseek(f,0,'bof');
    
    if (bytes < 32*32*pxlsize)
      fprintf('File too small: %s\n',files(i).name);
    elseif (bytes < 8192)
      res = 32;
      headersize = bytes - 32*32*pxlsize;
    elseif (bytes < 32768)
      res = 64;
      headersize = bytes - 64*64*pxlsize;
    elseif (bytes < 131072)
      res = 128;
      headersize = bytes - 128*128*pxlsize;
    elseif (bytes < 524288)
      res = 256;
      headersize = bytes - 256*256*pxlsize;
    elseif (bytes < 512*512*pxlsize + 20000)
      % 20000 is maximum allowed header size here
      %to allow some filter against huge files
      res = 512;
      headersize = bytes - 512*512*pxlsize;
    else
      fprintf('File too big: \n',files{k}.name);
      x = [];
      break;
    end
    
    if k == 1
      x = zeros(res,res,size(files,1));
    end
    
    
    fseek(f, headersize, 'bof');
    x(:,:,k) =  fread(f, [res,res], 'short')';
    fclose(f);
    
    if size(files,1) > 1
      fprintf(' %02d %%\b\b\b\b\b',round(100*k/size(files,1)));
    end
    
  end
  
  if size(files,1) > 1
    fprintf('\n');
  end
  

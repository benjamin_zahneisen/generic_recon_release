function B = fullpath(A)
%FULLPATH  Generates a full path from a partial or relative path
%
% Examples:
%   fp = fullpath(pp)
%
% This function generates a full path to a file or directory, given a
% partial path with respect to the current directory
%
%IN:
%   pp - String containing a path to a file or directory (which need not
%        exist) from the current directory. A full path is also valid.
%
%OUT:
%   fp - String containing the full path.

% $Id: fullpath.m,v 1.1 2009/04/08 22:15:37 ojw Exp $

if nargin == 0
  A = '';
end

if iscell(A)
  for p = 1:numel(A)
    B{p} = rel2full(A{p});
  end
else
  B = rel2full(A);
end

end


function B1 = rel2full(A1);
  
  [d f e] = fileparts(A1);
  if isempty(d)
    d = cd;
  else
    d = cd(cd(d));
  end
  B1 = fullfile(d, [f e]);
end

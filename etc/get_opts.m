% function get_opts(varargin_cell_arr,opts_def)
%
% given a cell of command line arguments in format 'str1', val1, 'str2',
% val2... and default options, returns the options to be used. This
% function is useful for converting optional arguments to a structure.

function opts=get_opts(varargin_cell_arr,opts_def)

%getting the structure corresponding to the optional arguments
if ~isempty(varargin_cell_arr)
    if isstruct(varargin_cell_arr{1})
        opts=varargin_cell_arr{1};
    else
        opts=cmdargs2struct2(varargin_cell_arr);
    end
else
    opts=struct;
end

if ~isempty(opts_def)
    names=fieldnames(opts_def);
    for i=1:numel(names)
        if ~isfield(opts,names{i})
            opts=setfield(opts,names{i},getfield(opts_def,names{i}));
        end
    end
end

end


% function args_struct=cmdargs2struct2(args_cell) takes the arguments in
% args_cell cell array and places them in the structure args_struct. The 
% odd numbered entries in the cell array are considered as field names 
% whereas the even numbered entries are considered as values. The numeric 
% strings are also converted to numbers. This function is especially useful
% for converting from command line arguments to a function to a struct 
%  
% Example :
%
% function y=myFunction(x,z,varargin)
%     opts=cmdargs2struct2(varargin);
%     .....
% end
%
%
% >> y=myFunction(2,4,'opt1',opt1value,'opt2',opt2value;
% >> y=myFunction 2 4 opt1 opt1value opt2 opt2value

function args_struct=cmdargs2struct2(args_cell)

    if ~exist('args_cell','var') || isempty(args_cell)
        args_struct=struct;
        return;
    end
          
    % converting to struct
    args_struct=cell2struct(vec(args_cell(2:2:end)),vec(args_cell(1:2:end)),1);
    args_struct=orderfields(args_struct);
    
    %converting tha numeric strings into numbers
    fields=fieldnames(args_struct);
    for i=1:length(fields)
        eval(sprintf('val=args_struct.%s;',fields{i}));
        if ischar(val)
            if ~isnan(str2double(val))
                eval(sprintf('args_struct.%s=str2double(val);',fields{i}));
            end
        end
    end                                
end
% mov=mat2mov(A,'filename',filename,'n',n,'fps',fps,'compression',compression)

function mov=mat2mov(A,varargin)

if ndims(A)==4 && size(A,4)~=3
    error('for 4D array, the last dimension must be 3 corresponding to RGB channels');
elseif ndims(A) == 1 || ndims(A)>4
    error('input array must have dimensions M x N x Nd x (1 or 3)');
end
    
%[y,m,d,h,mi,s] =datevec(now);
%dat=[num2str(y) '_' num2str(m,'%02d') '_' num2str(d,'%02d') '_' num2str(h,'%02d') '_' num2str(m,'%02d') '_' num2str(round(s),'%02d')]; 

A=abs(A);

%opts_def.filename=[dat '.avi'];
opts_def.filename=[];
opts_def.n=[size(A,1) size(A,2)];
opts_def.fps=4;
opts_def.compression='None';
opts_def.rewind=1; %for movie playback only
opts_def.minval=min(A(:));
opts_def.maxval=max(A(:));

opts=get_opts(varargin,opts_def);

minval=opts.minval;
maxval=opts.maxval;
disp(['minval : ' num2str(minval) ' maxval : ' num2str(maxval)]);

if (minval ~=0 || maxval ~=0)       
    A=(A-minval)/(maxval-minval);
end

if ~isempty(opts.filename)
    aviobj = avifile(opts.filename,'fps',opts.fps,'compression',opts.compression);        
    for i=1:size(A,3)
        
        if minval==0 && maxval==0
            minval2=min(vec(A(:,:,i)));
            maxval2=max(vec(A(:,:,i)));
            A(:,:,i)=(A(:,:,i)-minval2)/(maxval2-minval2);
        end
                    
        if ndims(A)==4
            if size(A,4) ~= 3
                error('last dimension has to be 3');
            end
            
            
            tmp=cat(3,imresize(A(:,:,i,1),opts.n),...
                      imresize(A(:,:,i,2),opts.n),...
                      imresize(A(:,:,i,3),opts.n));
            
            aviobj = addframe(aviobj,tmp);
        else
            tmp=repmat(imresize(A(:,:,i),opts.n),[1 1 3]);                      
            aviobj = addframe(aviobj,tmp);                       
        end        
    end
    aviobj=close(aviobj);
else         
    figure;
    if opts.rewind
        counter_end=Inf;
    else
        counter_end=size(A,3);
    end
    for i=1:counter_end
                        
        j=mod(i-1,size(A,3))+1;
        if ndims(A)==4
            A2=mean(squeeze(A(:,:,j,:)),3);            
        else
            A2=A(:,:,j);
        end
        
        if minval==0 && maxval==0
            minval2=min(vec(A2));
            maxval2=max(vec(A2));
            A2=(A2-minval2)/(maxval2-minval2);
        end
                
        A2=imresize(A2,opts.n);
        imshow(A2);
        title(['Slice ' num2str(j)]);
        drawnow;
        pause(1/opts.fps);
    end    
    
end
     

end
function newlockflag = lockmatfile(fname)
  %
  % Usage: lockmatfile myfile.mat
  % Creates empty file ".myfile.lock" if and only if myfile.mat exists
  % Outputs 1 if the function created a new lock, 0 if old lock, and -1 if the mat file didn't exist
  
  % See also unlockmatfile, islocked, waitonlock
  %
  
  newlockflag = -1;
  
  [mypth,myfile] = fileparts(fname);
  matfname = fullfile(mypth,[myfile '.mat']);
  if exist(matfname,'file')
    lockfname = fullfile(mypth,['.' myfile '.lock']);
    if ~exist(lockfname,'file')
      fclose(fopen(lockfname,'w'));
      newlockflag = true;
    else
      newlockflag = false;
    end
  end
  
end

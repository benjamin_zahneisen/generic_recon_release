function [Pfiles, rcnparm] = parse_rcnargs(varargin)
% function [Pfiles,rcnparm] = parse_rcnargs(varargin)
%
% Takes a cell array which is assumed to have the following properties
% 1. In any cell, in any order, strings with pattern P[0-9][0-9][0-9][0-9][0-9].7 are extracted into output cell array Pfiles
% 2. All remaining cells must be command line arguments of type strings and come in pairs, with
%    first the field name and then its value
%
% Example:
% [Pfiles, rcnparm] = parse_rcnargs('-pc','0.5','-gw','0','P00512.7','P00519.7','-outpath','/home/stefan/data','P00522.7')
%
% -----------------------------------------------------------------------------
% Stefan Skare, Stanford University, March 2009
% -----------------------------------------------------------------------------

if nargin == 1 && iscell(varargin{1})
  varargin = varargin{1};
end

cmdargs = []; Pfiles = []; pf = 0; argi = 0;
for c = 1:numel(varargin)
  if ischar(varargin{c}) && ~isempty(regexp(varargin{c},'P[0-9][0-9][0-9][0-9][0-9]\.7', 'once'))
    pf = pf + 1;
    Pfiles{pf} = varargin{c};
  elseif iscell(varargin{c}) && ischar(varargin{c}{1}) && all(cell2mat(regexp(varargin{c},'P[0-9][0-9][0-9][0-9][0-9]\.7')))
    for pp = 1:numel(varargin{c})
      pf = pf + 1;
      Pfiles{pf} = varargin{c}{pp};
    end
  else
    argi = argi + 1;
    cmdargs{argi} = varargin{c};
  end
end

if ~isempty(cmdargs)
  if mod(numel(cmdargs),2)
    fprintf('\n\n');
    for j = 1:numel(cmdargs)
      fprintf('parse_rcnargs - arg %02d: %s\n',j,cmdargs{j});
    end
    fprintf('\n\n');
    error('parse_rcnargs: number of command line arguments (excl. Pfiles) must be even');
  end
  for c = 1:2:numel(cmdargs)
    if cmdargs{c}(1) == '-'
      cmdargs{c} = cmdargs{c}(2:end);
    end
  end
  rcnparm = cmdargs2struct(cmdargs);
else
  rcnparm = [];
end
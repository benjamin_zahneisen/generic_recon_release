% vol2fileseries(A,'minval',minval,...
%                   'maxval',maxval,...
%                   'prefix',prefix,...
%                   'outpath',outpath);

function vol2fileseries(A,varargin)

ss=size(A);
if ndims(A) < 2
    error('Input dimension wrong');
end
if ndims(A) > 3
    A=reshape(A,ss(1),ss(2),prod(ss(3:end)));
end

opts_def.minval=min(A(:));
opts_def.maxval=max(A(:));
opts_def.prefix='im';
opts_def.outpath='.';

opts=get_opts(varargin,opts_def);

disp(['minval : ' num2str(opts.minval) 'maxval : ' num2str(opts.maxval)]);

for sl=1:size(A,3)
    if opts.minval==0 && opts.maxval == 0
        minval=min(vec(A(:,:,sl)));
        maxval=max(vec(A(:,:,sl)));
    else
        minval=opts.minval;
        maxval=opts.maxval;
    end
    imwrite((A(:,:,sl)-minval)/(maxval-minval),...
        fullfile(opts.outpath,[opts.prefix 'sl' num2str(sl,'%03d') '.tif']))        
end

end
function D = readDiffusionTable(fname)

fileID = fopen(fname)
% Ndir = textscan(fileID,'# Set of %f directions, radial sampling half sphere: fdsi_3_1c_1c, 59 dirs')
% C = textscan(fileID,'%s')
% C= textscan(fileID,'%s')
% C= textscan(fileID,'%s')
% 
% textscan(fileID,'Vector[%f] = ( %f, %f, %f )')
C = textscan(fileID,'%f %f %f')
fclose(fileID)
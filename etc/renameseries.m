function renameseries(examdir,digits)

if nargin < 1
    examdir = './';
end
if nargin < 2
    digits = 4;
end

d = dir(examdir);
if strcmp(d(2).name,'..')
    start = 3;
else
    start = 1;
end


for di = start:length(d)
    if ~isempty(d(di).name) && d(di).isdir
        % dicominfo does not work with some GE product sequences.
        indx = 1;
        imfile = sprintf(sprintf('I%%0%dd.dcm',digits),indx);
        while ~exist(fullfile(examdir,d(di).name, imfile),'file')
          indx = indx + 1;
          imfile = sprintf(sprintf('I%%0%dd.dcm',digits),indx);
          if indx > 30
            break;
          end
        end
        if exist(fullfile(examdir,d(di).name, imfile),'file')
          [info img] = dicomread_v2(fullfile(examdir,d(di).name, imfile));
          if ~isnumeric(info.StudyID) info.StudyID = str2num(info.StudyID); end
          if ~isnumeric(info.SeriesNumber) info.SeriesNumber = str2num(info.SeriesNumber); end
          newname = sprintf('e%ds%03d_%s',info.StudyID, info.SeriesNumber,strip_unwanted(info.SeriesDescription,'_'));
          if ~strcmp(newname,d(di).name)
            eval(sprintf('movefile %s %s',d(di).name,newname));
          end
        end
    end
end

function im = ifftnc(d,dim)
% im = ifftnc(d)
%
% ifftnc perfomrs a centered ifftn

if nargin > 1
    im = ifftshift(ifft(ifftshift(d,dim),[],dim),dim);
else    
    im = ifftshift(ifftn(ifftshift(d)));
end


end

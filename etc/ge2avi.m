function mov = ge2avi(fps)


files = spm_get(inf,'I.???','V�lj GE Signabilder (ALL = alla, DONE = klar)',pwd);
outputdir = fileparts(files(1,:));
nim = size(files,1);

for k = 1:nim
  x(:,:,1,k) = short2mat(files(k,:),[],'b');
end

% Skala om bildens intensitet
x = x*255/max(x(:));

while ~exist('filmnamn') | isempty(filmnamn)
    filmnamn = input('Namn pa filmen [ej mellanslag]: ','s');
end
filmnamn = fullfile(outputdir,filmnamn);

graylevels = 999;
while graylevels < 10 | graylevels > 256
    graylevels = str2num(input('�vre graskalegrans [mkt ljus=10, optimal=256, tryck ENTER f�r 256]: ','s'));
    if isempty(graylevels)
       graylevels = 256;
    end
end

figure(1);clf;
mov = immovie(x,colormap(gray(graylevels)));

fps =  str2num(input('Uppspelningshastighet [tryck ENTER f�r 15/s]: ','s'));
if ~exist('fps') | isempty(fps)
    movie2avi(mov,filmnamn,'colormap',colormap(gray(256)));
else
    movie2avi(mov,filmnamn,'colormap',colormap(gray(256)),'fps',fps);
end

fprintf('Filmen ligger nu h�r: %s\n', filmnamn);

function [x] = float2mat(files,res,outputformat,BigEndian);

pxlsize = 4;

if ~exist('outputformat') | isempty(outputformat)
  outputformat = 'vol';
end
if ~exist('BigEndian') % Read little Endian
  BigEndian = 0;
end

if exist('files') & size(files,1) == 1 % Expand wildcards if necessary
  path = fileparts(files(1,:));
  files = dir(files);
  if (size(files,1) == 0)
      fprintf('File(s) not found.\n');
      x = 0 ;
      return;
  end
  for k = 1:size(files,1)
	  files(k,1).name = fullfile(path,files(k,1).name);
  end
else
  if ~exist('files') | isempty(files) % Get files interactively
    tmp = spm_get(inf,'I*',pwd);
  else
    tmp = files;
  end
  clear files;
  path = fileparts(tmp(1,:));
  for k = 1:size(tmp,1)
    files(k,1) = dir(strrep(tmp(k,:),' ',''));
    files(k,1).name = fullfile(path,files(k,1).name);
  end
end
  

for k = 1:size(files,1)
  if ~exist('res')
    if (files(k).bytes < 32*32*pxlsize)
        fprintf('File too small: %s\n',files(i).name);
    elseif (files(k).bytes < 8192)
        res = [32 32];
        headersize = files(k).bytes - 32*32*pxlsize;
    elseif (files(k).bytes < 32768)
        res = [64 64];
        headersize = files(k).bytes - 64*64*pxlsize;
    elseif (files(k).bytes < 131072)
        res = [128 128];
        headersize = files(k).bytes - 128*128*pxlsize;
    elseif (files(k).bytes < 524288)
        res = [256 256];
        headersize = files(k).bytes - 256*256*pxlsize;
    elseif (files(k).bytes < 512*512*pxlsize + 10000)
        % 10000 is maximum allowed header size here
        %to allow some filter against huge files
        res = [512 512];
        headersize = files(k).bytes - 512*512*pxlsize;
    else
        fprintf('File %s too big for auto-size detection, try passing in a file resolution, ala x=float2mat(filenames,[1024 1024])\n',files(k).name);
        x = [];
        break;
    end
  else 
      % res was passed in.
      headersize=files(k).bytes - (prod(res)*pxlsize);
  end
    if k == 1
      x = zeros([size(files,1) prod(res)]);
    end
    
    if BigEndian
      f = fopen(files(k).name,'r','b');
    else
      f = fopen(files(k).name,'r','l');
    end
    fseek(f, headersize, 'bof');
    if strcmp(outputformat,'vol')
      if ndims(res) == 2,       x(k,:) =  fread(f, prod(res), 'float=>float')' ;
      else,        x(k,:) =  fread(f, prod(res), 'float=>float') ;
      end
    elseif strcmp(outputformat,'struct')
      if ndims(res) == 2,      files(k).data(:) = reshape( fread(f, prod(res), 'float=>float')', res);
      else, files(k).data(:) = reshape( fread(f, prod(res), 'float=>float'), res);
      end
    end
    fclose(f);

    if size(files,1) > 1
      fprintf(' %02d %%\b\b\b\b\b',round(100*k/size(files,1)));
    end
    
end

if size(files,1) > 1
  fprintf('\n');
end


if ~strcmp(outputformat,'vol')
  x = files;
else
  x = reshape(x , [size(files,1) res]);
  x = permute(x,[2:ndims(x) 1]);
end

function plotrods(Q,c)

if nargin < 2
c = 'k';
end

hold on
for k = 1:size(Q,1)
	plot3([0 Q(k,1)],[0 Q(k,2)],[0 Q(k,3)],sprintf('bo-',c),'linewidth',2,'markersize',5);
	plot3([-Q(k,1) 0],[-Q(k,2) 0],[-Q(k,3) 0],sprintf('bo-',c),'linewidth',2,'markersize',5);
end
camproj perspective
box on
axis tight
axis square

v = [ 0.9336    0.3584    0.0000   -0.6460 ;...
	-0.1627    0.4238    0.8910   -0.5761 ; ...
	-0.3193    0.8318   -0.4540    8.6310 ; ...
	0         0         0    1.0000];
	 
view(v);
		 
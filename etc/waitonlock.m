function waitonlock(fname)
  % Usage: waitonlock <matlabfile>.mat
  
  [mypth,myfile] = fileparts(fname);
  matfname = fullfile(mypth,[myfile '.mat']);

  if exist(matfname,'file')
    [mypth,myfile] = fileparts(fname);
    lockfname = fullfile(mypth,['.' myfile '.lock']);
    while exist(lockfname,'file')
      pause(1);
    end
  end
  
end

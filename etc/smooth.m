function sI = smooth(I,f);

G = gausswin(f)*gausswin(f)';
sI = imfilter(abs(I),G,'conv');


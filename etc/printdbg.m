function printdbg(varargin)


global VerboseFlag;

if ~isempty(VerboseFlag) && VerboseFlag > 0
   fprintf(varargin{:});
end

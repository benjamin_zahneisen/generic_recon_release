function [P,Pre,Pim] = mat2spm(I,vox,fov,off,prefix,bladeangle)
  
  if ~exist('prefix','var') || isempty(prefix)
    prefix = 'v';
  end
  
  if exist('bladeangle','var')
    hinfo.shortaxistype = 'propeller';
    hinfo.bladeangles = -bladeangle;
    rcnparm.curr_vol = 1;
    prefix = sprintf('%s_blade%03d',prefix,bladeangle);
    hinfo.shortaxistype = 'propeller';
  else
    hinfo.shortaxistype = 'cartesian';
  end
  
  if ~exist('fov','var') || isempty(fov) 
    fov = size(I(1:2));
  elseif numel(fov) == 1
    fov = fov*[1 1];
  end
  
  if ~exist('vox','var') || isempty(vox)
    vox = [1 1 1];
  end
    
  rcnparm.orient.rotate = 0;
  hinfo.ny = size(I,1);
  hinfo.nx = size(I,2);
  hinfo.nz = size(I,3);
  hinfo.fFOV = fov(1);
  hinfo.pFOV = fov(2);
  hinfo.off = [0 0 0];
  if exist('off','var') && ~isempty(off)
    hinfo.fovoffset(1:numel(off)) = off;
    prefix = [prefix '_' strip_unwanted(num2str(off))];
  end
  hinfo.xmm = vox(1);
  hinfo.ymm = vox(2);
  hinfo.zmm = vox(3);
  hinfo.spacing = 0;
  hinfo.nechoes = 1;
  hinfo.user(15) = 1;
  
  rcnparm.outprefix = prefix;
  rcnparm.hinfo = hinfo;
  [P Pre Pim] = lx2spm(rcnparm,size(I));
  
  spm_write_vol(P,abs(I));
  if ~isreal(I)
    spm_write_vol(Pre,real(I));
    spm_write_vol(Pim,imag(I));
  end
  
   P.pinfo = [1 0 0]';
 
  
end

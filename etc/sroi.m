function [roi_result, origin, radius,xroi] = sroi(x,R)

xsize = size(x);

if length (xsize) == 3
  nreps = xsize (3);
elseif length(xsize) == 2
	nreps = 1;
else
  fprintf('Input argument 1 must be a MxN matrix or MxNxt matrix.Press CTRL-C\n');
end


clf;
x([1:5 (end-4):end],:,:) = 0;

if nargin == 1
  figure(1);
  imagesc(x(:,:,1));
  colormap gray;
  axis image;
end

if ~exist ('R')
  R = ginput(2);
end

R = round(R);

xroimask = zeros(xsize(1),xsize(2));
xroimask(R(1,2):R(2,2),R(1,1):R(2,1)) = 1;
npixels = sum(xroimask(:));

for rep = 1:nreps 

  xroi = x(:,:,rep) .* xroimask;
  
  if rep == 1 & nargin == 1
	colormap gray;
	subplot (1,2,1);
	imagesc(xroi);
	axis image off;
	subplot (1,2,2);
	xroinot = x(:,:,rep) .* ~xroimask;
	imagesc(xroinot);
	axis image off;
  end
  tmp = reshape (xroi(xroimask == 1), npixels, 1);
  roi_result (1,rep) = mean(tmp);
  roi_result (2,rep) = std(tmp);
  roi_result (3,rep) = max(tmp);
  roi_result (4,rep) = min(tmp);
  
end


function result = ls2(ptrn,opts)
  
  
  if ~exist('ptrn','var')
    ptrn = '';
  end
  if ~exist('opts','var')
   opts = '';
  end
 
  PWD = pwd;
  
  k = 1;
  if strcmpi(computer,'pcwin') % needs update!!
    tmp = dir(ptrn);
    for j = 1:numel(tmp)
      if ~dironlyflag || (dironlyflag && isdir(tmp{j}))
        result{k} = tmp(j).name; k = k + 1;
      end
    end
  else    
     
    [dummy,result] = unix(['ls -1 --color=never' opts ' ' ptrn]); % suppressing color output of ls
    if (dummy == 1)
        [dummy,result] = unix(['ls ' opts ' ' ptrn]);
    end
    tmp = regexp(result,'\n','split'); result = [];
    for j = 1:numel(tmp)
      if ~isempty(tmp{j})  && ~strcmp(tmp{j},'.') && ~strcmp(tmp{j},'..')
        result{k} = tmp{j};
        %fprintf('%s\n',result{k});
        k = k + 1;
   
      end
    end
  end
  
  cd(PWD)
  
end

function X = maxdistance(n,printc2filename)
  % Usage: x = maxdistance(n, filename)
  %
  % This function was written to generate a slice ordering scheme that maximizes the time between the acquistion of nearby
  % slices in MRI. Normally, in MRI, out of 'n' slices, the odd slices are acquired first
  % and then the even slices. This function recursively groups the odd slices into a new sub-odd and sub-even set
  % and so on until there are no more split-ups to be made.
  % The output x is an optimal 'MRI slice order' array of size 1xn
  %
  % n can be a scalar integer or integer 1D array. If 'n' is a an array, x is a matrix with each row corresponding to each element in 'n'
  % and will be zerofilled to produced a square matrix.
  %
  % If there is a second argument and this one is a string, then a c-style array is written to this file
  %
  % ---------------------------------------------
  % Stefan Skare, Stanford University, June 2010
  % ---------------------------------------------
  
  X = zeros(numel(n),max(n));
  
  for j = 1:numel(n)
    clear a;
    %a{1} = 1:n(j);
    a{1} = 0:(n(j)-1);
    while numel(a{1}) > 4
   % for m = 1:3
      clear b
      for g = 1:numel(a)
        tmp = a{g};
        b{2*g-1} = tmp(1:2:end);
        b{2*g} = tmp(2:2:end);
      end
      a = b;
    end
    x = [];
    for g = 1:numel(a)
      x = [x a{g}];  
    end
    X(j,1:numel(x)) = x;
  end
  
  
  % C code output to file
  if nargin >= 2 && ischar(printc2filename)
    f = fopen(printc2filename,'w');
    fprintf(f,'int iprop_sliceorder[%d][%d] = \\\n{', size(X,1),size(X,2));
    for j = 1:size(X,1);
      if j > 1
        fprintf(f,' ');
      end
      fprintf(f,'{');
      for k = 1:size(X,2)
        fprintf(f,'%2d',X(j,k));
        if k < size(X,2)
          fprintf(f,', ');
        else
          if j < size(X,1)
            fprintf(f,'}, \\\n');
          elseif j == size(X,1) && k == size(X,2)
            fprintf(f,'}');
          end
        end
      end
    end
    fprintf(f,'};\n');
    
    fclose(f);
  end
  
end

    
    
    
    
function result = multiload(varargin)
  
  ptrn = '';
  matfile = '';
  if nargin == 1
    matfile = varargin{1};
  elseif nargin >= 2
    ptrn = varargin{1};
    matfile = varargin{2};
  end
  
  dirs = ls2(ptrn,'-d');
  if isempty(dirs)
    dirs = {'.'};
  end
  
  for d = 1:numel(dirs)
    clear filename
    if isdir(dirs{d})
      filename = ls2(fullfile(dirs{d},[matfile '*.mat']));
      for j = 1:numel(filename)
        [tmppth,tmpname] = fileparts(filename{j});
        if ~strcmp(tmppth,'.')
          tmppth = strip_unwanted(tmppth);
        end         
        tmpname = strip_unwanted(tmpname);
        if numel(dirs) == 1 && strcmp(dirs{1},'.')
          thisfield = tmpname;
        else
          thisfield = [tmppth '_' tmpname];
        end
        if exist(filename{j},'file')
          result.(thisfield) = load(filename{j});
        end
      end
    end
  end

  if ~exist('result')
    result = [];
  end
    
  
end


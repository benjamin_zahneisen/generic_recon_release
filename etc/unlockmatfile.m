function unlockmatfile(fname)
  %
  % Usage: lockmatfile myfile.mat
  % Creates empty file ".myfile.lock" if and only if myfile.mat exists
  % 
  % See also unlockmatfile, waitonlock
  %
  
  [mypth,myfile] = fileparts(fname);
  matfname = fullfile(mypth,[myfile '.mat']);
  if exist(matfname,'file')
    lockfname = fullfile(mypth,['.' myfile '.lock']);
    delete(lockfname);
  end
  
end

function clean = strip_unwanted(dirty,sep)

  if isempty(dirty)
    clean = dirty;
    return
  end
  
if strcmpi(dirty(1:2),'0x'), dirty = dirty(3:end); end

if nargin > 1
  clean = strrep(dirty(regexp(dirty,'[a-zA-Z0-9_\ ]')),' ',sep);
else
  clean = dirty(regexp(dirty,'[a-zA-Z0-9_]'));
end

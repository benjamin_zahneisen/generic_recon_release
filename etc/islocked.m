function lockflag = islocked(fname)
  % Usage: islocked <matlabfile>.mat
  
  [mypth,myfile] = fileparts(fname);
  matfname = fullfile(mypth,[myfile '.mat']);

  if exist(matfname,'file')
    [mypth,myfile] = fileparts(fname);
    lockfname = fullfile(mypth,['.' myfile '.lock']);
    if exist(lockfname,'file')
      lockflag = true;
    else
      lockflag = false;
    end
  end
  
end

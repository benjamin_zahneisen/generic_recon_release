% function [I files T]=analyze75read_all(varargin)
% 
% I -> an array of output images
% files -> (optional) the names of the files that were read
% T -> (optional) transformation matrices of the SPM files

function [I files2 T]=analyze75read_all(varargin)

if numel(varargin) == 1
    fname=varargin{1};
else
    fname='.';
end


[pathstr name ext] = fileparts(fname);

%if fname is a path 
if exist(fname,'dir') == 7
    pathstr=fname;
    name='*';
    ext='img';
end

if isempty(pathstr)
    pathstr='.';
end

if isempty(name)
    name='*';
end

if isempty(ext)
    ext='.img';
end

fname=fullfile(pathstr,[name ext]);

files=dir(fname);

if isempty(files)
    return;
end

temp=analyze75read(fullfile(pathstr,files(1).name));    
ss=size(temp);
nn=numel(temp);

I=zeros([ss numel(files)]);
T=zeros([4 4 numel(files)]);

files2=cell(numel(files),1);
for i=1:numel(files)    
    ind=(i-1)*nn+1:i*nn;
    files2{i}=fullfile(pathstr,files(i).name);
    hdr=spm_vol(files2{i});
    temp=spm_read_vols(hdr);    
    I(ind)=temp(:);    
    T(:,:,i)=hdr.mat;
end


end
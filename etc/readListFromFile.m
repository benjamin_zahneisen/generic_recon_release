function [filelist]=readListFromFile(filename)
%% --- function reads filenames from list in a text file
% input: filename of the file which contains the text list,
%        new-line separated
% output: list of filenames
    
    % open the file
    handle = fopen(filename, 'r');
    if handle == -1
        % if file cannot be opened, return -1
        fprintf('Can''t read file %s\n', filename);
        filelist = -1;
        return;
    end

    
    filelist=cell(1);
    index=0;
    % read the new-line separated list items until EOF
    while 1
        tline = fgetl(handle);
        if ~ischar(tline)
            break
        end
        index = index + 1;
        filelist{index} = tline;
    end
    fclose(handle);    
end
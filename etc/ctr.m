function range = ctr(width,strip)
%
% Usage: range = ctr(width, strip)
%
% This function returns an array of length 'strip', containing numbers
% that are centered around 'width/2'.
% Consider e.g. an image/k-space of width 512 and you want to cut out the
% most central 96 pixels. Instead of writing range = ((512-96)/2+1):((512+96)/2),
% you write range = ctr(512,96)
% --------------------------------------------------------------------------------
% Stefan Skare, Stanford University, No Rights Reserved. Feb 2009
%

% --------------------------------------------------------------------------------
% SVN information:
% $Revision: 745 $
% $Date: 2009-02-26 09:59:30 -0800 (Thu, 26 Feb 2009) $
% $Author: stefan $
% $HeadURL: svn+ssh://bzahneisen@metroid.stanford.edu/export/repos/matlab/trunk/matlab/etc/ctr.m $
% --------------------------------------------------------------------------------

if strip > width
  error('ctr: the 2nd argument must be less than the 1st');
else
  a = round((width - strip)/2+1);
  range = a:(a+strip-1);
end
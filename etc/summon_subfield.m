function result = summon_subfield(mystruct,subfieldname,fieldrange,returntype)
  
  f = fieldnames(mystruct);
  if nargin < 4 || isempty(returntype)
    returntype = 'num';
  end
  if nargin < 3 || isempty(fieldrange)
    fieldrange = 1:numel(f);
  else
    fieldrange = fieldrange(fieldrange <= numel(f));
  end
  
  onefield = getfield(mystruct,f{fieldrange(1)});
  onesubfield = eval(sprintf('onefield.%s',subfieldname));
  n = numel(onesubfield);
  
  if strcmp(returntype,'num')
    result = zeros([size(onesubfield) numel(fieldrange)],class(onesubfield));
  end
  
  printdbg('Merging the following fields:\n');
  for k = fieldrange
    onefield = getfield(mystruct,f{k});
    printdbg('%d: %s\n',k,f{k});
    onesubfield = eval(sprintf('onefield.%s',subfieldname));
    switch returntype
      case 'num'
        try
          eval(sprintf('result(1:%d,1:%d,%s%d) = onesubfield;', size(onesubfield,1),size(onesubfield,2),repmat(':,',1,ndims(onesubfield)-2),k-fieldrange(1)+1));
        catch
          error('summon_subfield: Error for field %s\n',f{k});
        end
      case 'cell'
        result{k-fieldrange(1)+1} = onesubfield;
    end
  end
  
  if strcmp(returntype,'num')
    result = squeeze(result);
  end
end

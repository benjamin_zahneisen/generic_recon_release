function dispstruct(h, name)
%DISPSTRUCT Display a complex structure
%
%  DISPSTRUCT(STRUCT) display the values in the structure STRUCT which
%  may have sub-structures, arrays of structures, etc.
%
%  DISPSTRUCT(STRUCT, NAME) uses NAME instead of 'struct' as the structure
%  name; ie, the structure fileds are printed are NAME.field = val.
%
%  DB Clayton - v1.1 2005/10/12


if (nargin < 2), name='struct'; end
print_struct(h, name);



%
%   subroutines
%


function print_struct(h, s)
tags = fieldnames(h);
for i=1:numel(tags)
  x = getfield(h, tags{i});
  tag = [s, '.', tags{i}];
  if (isstruct(x))
    if (isscalar(x))
      print_struct(x, tag);
    else
      print_struct_array(x, tag);
    end
  elseif (isscalar(x))
    print_scalar(x, tag);
  else
    print_array(x, tag);
  end
end


function print_array(h, s)
z = size(h);
if (isempty(h))
  disp(sprintf('%s = []', s));
elseif (ischar(h))
  disp(sprintf('%s = %s', s, h));
else
  if (numel(z) > 2)
    disp(sprintf('%s = <ARRAY %d DIMS>', s, numel(z)));
  elseif (z(1) == 1 || z(2) == 1)
    for i=1:numel(h)
      disp(sprintf('%s[%d] = %d', s, i, h(i)));
    end
  else
    for i=1:z(1)
      for j=1:z(2)
        disp(sprintf('%s[%d,%d] = %d', s, i, j, h(i,j)));
      end
    end
  end
end


function print_struct_array(h, s)
z = size(h);
if (numel(z) > 2)
  disp(sprintf('%s = <STRUCT ARRAY %d DIMD>', s, numel(z)));
elseif (z(1) == 1 || z(2) == 1)
  for i=1:numel(h)
    print_struct(h(i), sprintf('%s[%d]', s, i));
  end
else
  for i=1:z(1)
    for j=1:z(2)
      print_struct(h(i,j), sprintf('%s[%d,%d]', s, i, j));
    end
  end
end


function print_scalar(h, s)
if (ischar(h))
  disp(sprintf('%s = %s', s, h));
else
  disp(sprintf('%s = %d', s, h));
end


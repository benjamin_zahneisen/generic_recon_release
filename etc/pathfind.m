function fullpath = pathfind(file)
% PATHFIND Searches certain environment paths to find a file
%
%   FULLPATH = PATHFIND(FILE) returns the full pathname, FULLPATH,
%   of the given file, FILE, by searching the Matlab path (see
%   Matlab's PATH function) and, if the system is not PCWIN (see
%   Matlab's COMPUTER function), the environment variables PATH
%   and LD_LIBRARY_PATH (see Matlab's GETENV function).  The order
%   of search is LD_LIBRARY_PATH, MATLABPATH, and PATH.
%
%   DB Clayton - v1.0 2005/05/03


% platform-specific path elements and delimiters
matpath = path;
if (strcmp(computer, 'PCWIN'))
  envpath = '';
  ldpath = '';
  sep = ';';
  slash = '\';
else
  envpath = getenv('PATH');
  ldpath = getenv('LD_LIBRARY_PATH');
  sep = ':';
  slash = '/';
end

% full search path
p = ['.', sep, ldpath, sep, matpath, sep, envpath, sep];  % join path elements
p = regexprep(p, [sep,'+'], sep);  % replace multiple sep with single sep
dirs = regexp(p, ['(.*?)',sep], 'match');  % seperate path into cell array

% search in each directory of path
found = false;
for i=1:length(dirs)
  d = dirs{i};
  fullpath = [d(1:end-1), slash, file];  % full path name
  if (exist(fullpath, 'file') == 2), found = true; break; end
end

% error if file not found
if (~found), error('file not found: %s', file); end

ones(10,10);

homedir = pwd;

%make all dependencies are within one folder
display('stable generic reconstruction v2')

reconPath = fullfile(homedir);

addpath(genpath(reconPath));

%% configuration, data structures, ...
%set home directory
disp(['setting home directory [$GENERIC_RECON_HOME] to: ',homedir])
setenv('GENERIC_RECON_HOME',homedir)
setenv('GENERIC_HOME',homedir)
setenv('DICO_DIR','dico'); %directory with fieldmaps as separate niftis (output from topup)
setenv('FMAP_BASE' ,'fmap_');
setenv('OUTDIR_JOINT','joint');
setenv('RAWDIR','rawdata');
setenv('RAW_BASE','hybSENSEdat');
setenv('UP_DIR','blipUp');
setenv('DOWN_DIR','blipDown');
setenv('NIFTI_NAME','dti');

clear all;




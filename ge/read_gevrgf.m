function v = read_gevrgf(file, na, nr)
%READ_GEVRGF Read a GE vrgf.dat file for EPI gradient correction
%
%  VRGF = READ_GEVRGF(FILE, NA, NR) returns the VRGF correction data
%  stored in FILE where NA is the number of read-out points in the
%  acquired data set and NR is the number of read-out points after the
%  correction is applied.
%
%  DB Clayton - v1.0 2004/04/02


id = fopen(file, 'r', 'l');
if (id == -1)
  error('Can not open vrgf.dat file (%s)', file);
end

[v, n] = fread(id, 'float');  % read entire file into vector v

fclose(id);
if (n ~= na*nr)
  error('vrgf.dat file (%s): %d floats read, %d floats expected', file, n, na*nr);
end

v = reshape(v, na, nr);  % reshape vector v into na-by-nr matrix

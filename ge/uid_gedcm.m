function u = uid_gedcm(varargin)
%UID_GEDCM Generate a unique GE DICOM UID
%
% UID = uid_gedcm; (on unix, it will get process ID and hostID from system. on windows, random # will be used)
%
% UID = uid_gedcm(procID,hostID); (will be faster, thanks to avoidance of system calls)
%
% DB Clayton - v1.1 2005/09/28
% Stefan Skare, Matus Straka - v 2.0 2008/10/22
% Stanford University

% Initialize RAND to a different state each time.
rand('twister',sum(100*clock));
 
if nargin < 1 || isempty(varargin{1})
  if ~strcmpi(computer,'pcwin')
    [status,u1] = system('echo $$'); % process ID   
  else
    u1 = floor(1e3*rand(1));
  end
else
  u1 = varargin{1};
end
if ischar(u1)
  u1 = str2num(u1);
end

tmp = clock;
u2 = floor(1e6*tmp(6)); % time stamps 10^6 times the fraction of a second

if nargin < 2 || isempty(varargin{2})
  if ~strcmpi(computer,'pcwin')
    [status,hostid] = system('hostid'); % hostid
    if status % then something went wrong with hostid, use a random number
      u3 = floor(rand(1)*1e5);
    else
      u3 = hex2dec(strip_nonhex(hostid)) ;%  host id--must strip off leading 0x and trailing null
    end
  else
      u3 = floor(rand(1)*1e5);
  end
else
  u3 = hex2dec(strip_nonhex(varargin{2})) ;
end

% make sure u3 < 2e9, to avoid ...e+09 in the u string below
u3 = mod(u3,2e9);

u4 = floor(1e9*rand(1));  % random count
u5 = floor(1e3*rand(1));  % random count
u = sprintf('1.2.840.113619.2.156.%d.%d.%d.%d.%d', u1, u2, u3, u4, u5);

% HOSTIDs
% -------
% chmr: fc0a640a
% suhmr1: 41ab5382
% hospmr2: fc0af9b7
% zap: 0x7f0200

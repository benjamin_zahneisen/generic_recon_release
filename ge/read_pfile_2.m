% function data=read_pfile_2 reads the data from the pfile in the current
% directory without any f%$^ing reorientations whatsoever so that the first
% view is stored in the first index, the second stored in second, etc. data
% is complex array of size nk x ni x necho x nsl x nc, which is the
% ordering in the p-file itself
%
% options : frames : ni x 1 array. 

function data=read_pfile_2(varargin)

    
    
    if numel(varargin)==0
        pfilename=dir('P*.7');
        pfilename=pfilename(1).name;
        opts_start=1;
    elseif isnumeric(varargin{1})
        pfilename=['P' num2str(varargin{1}) '.7'];
        opts_start=2;
    elseif ischar(varargin{1})    
        pfilename=varargin{1};
        if ~isempty(str2num(pfilename))
            pfilename=['P' pfilename '.7'];
            opts_start=2;
        elseif ~isempty(regexp(pfilename,'P\d\d\d\d\d.7', 'once'))
            opts_start=2;
        else
            pfilename=dir('P*.7');
            pfilename=pfilename(1).name;
            opts_start=1;
        end
    end
    
    
    
    %reading the header
    hdr=read_gehdr(pfilename);
    
    %getting all pfiles with this tun number    
    [pathstr name ext]=fileparts(pfilename);
    allpfilesindir=dir(fullfile(pathstr,'*.7'));
    j=1;
    for i=1:numel(allpfilesindir)
        hdr2=read_gehdr(fullfile(pathstr,allpfilesindir(i).name));
        if hdr2.rdb.run_int==hdr.rdb.run_int
            allpfiles{j}=fullfile(pathstr,allpfilesindir(i).name);
            j=j+1;
        end
    end
    
    

    
    nk=hdr.rdb.frame_size;    
    ni=hdr.rdb.nframes;
    ne=hdr.rdb.nechoes;
    nsl=hdr.rdb.nslices;
    npasses=hdr.rdb.npasses;
    nsl_per_pfile=ceil(nsl/npasses);
    nc=hdr.rdb.dab_stop_rcv(1)-hdr.rdb.dab_start_rcv(1)+1;
    ptsize=hdr.rdb.point_size;
    
    opts_def.frames=[1:ni];
    opts=get_opts({varargin{opts_start:end}},opts_def);
        
    
    %creating data
    data=zeros(nk,numel(opts.frames),ne,nsl,nc);
    
    

               
    %reading data
    for passi=1:npasses
        fid=fopen(allpfiles{passi},'r');    
        for coili=1:nc
            for sli=1:nsl_per_pfile
                for echoi=1:ne
                    iii=1;
                    for ii=opts.frames
                        offset=hdr.rdb.off_data+...
                            (coili-1)*nsl_per_pfile*ne*(ni+1)*nk*ptsize*2+...
                            (sli-1)*ne*(ni+1)*nk*ptsize*2+...
                            (echoi-1)*(ni+1)*nk*ptsize*2+...
                            (ii)*nk*ptsize*2;
                        if fseek(fid,offset,'bof')==-1
                            fclose(fid);
                            error('error in fseek');
                        end
                        if ptsize==2
                            temp=fread(fid,2*nk,'int16');
                        elseif ptsize==4
                            temp=fread(fid,2*nk,'int32');
                        end
                        if numel(temp)~=2*nk
                            fclose(fid);
                            error('error in reading from file');
                        end
                        temp=temp(1:2:end)+temp(2:2:end)*sqrt(-1);
                        if (sli-1)*npasses+passi <= nsl
                            data(:,iii,echoi,(sli-1)*npasses+passi,coili)=temp;
                        end
                        iii=iii+1;
                    end
                end
            end
        end
        fclose(fid);
    end
               
    
end
function moveVols2Scanner(V,hdr,volnum)

% this function calls the script 'exportDCM2MRI.sh' on lcmr2.
% 'exportDMC2MRI.sh' copies the data folder over to either LC3T2 or LC3T3
% depending on information from the p-file header.

unix('mkdir dcmTmp');

prefix=generic_pathfileprefix(hdr,'volnum',volnum);

opts.dcm=1;
opts.mat=0;
opts.outpath = ['./dcmTmp/'];

P = generic_write_vol(V,hdr,'outpath',opts.outpath,'prefix',[prefix,''],'imnooffset',0);

%rename folder
unix('mv dcmTmp dcmexprt.sdcopen');

%copy dicom folder to lcmr2
unix(['scp -r dcmexprt.sdcopen bzahneisen@lcmr2:/home/bzahneisen']);
'done' 

%stores which scanner was used
if(strcmp(hdr.series.pr_sysid,'No2'))
    machineID = 2;
else
    machineID = 3;
end

%call script on lcmr2, ssh bzahneisen@lcmr2 'exportDCM2MRI.sh 2'
cmd = ['ssh bzahneisen@lcmr2 ','''','exportDCM2MRI.sh ',num2str(machineID),''''];
unix(cmd);
%delete folder on lcmr2 is handled by exportDCM2MRI.sh


%delete folder on nehalem
unix('rm -r ./dcmexprt.sdcopen');
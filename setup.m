%setup.m

%call setup.m to compile mex files and configure .bash_profile

%% export generic_recon home folder
% we assume we call setup from the 
generic_recon_home = pwd;
%cmd = echo 'export PATH=/usr/local/bin:$PATH' >>~/.bash_profile

%add to .bash_profile
current_GENERIC_RECON_HOME = getenv('GENERIC_RECON_HOME');
if isempty(current_GENERIC_RECON_HOME)
    cmd = ['echo ''export GENERIC_RECON_HOME=',generic_recon_home,'''',' >>~/.bash_profile'];
    unix(cmd)
else
   disp(['GENERIC_RECON_HOME already set to ',current_GENERIC_RECON_HOME]) 
end


%% MEX files are NOT used anymore
%% that makes sure that matlab finds a c-compiler (at least on a mac)
%mex -setup -v 

%cd generic/epic
%generic_make

%cd ../..

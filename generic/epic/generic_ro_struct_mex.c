#include <string.h>
#include "generic_readouts/generic_ro.c"


void mexFunction(int nlhs, mxArray *plhs[ ],int nrhs, const mxArray *prhs[ ]) 
{    
    generic_ro * w = (generic_ro*)malloc(sizeof(generic_ro));
    init_ro(w);
    plhs[0]=ro_c_to_matlab(w);
    free_w(w);
};

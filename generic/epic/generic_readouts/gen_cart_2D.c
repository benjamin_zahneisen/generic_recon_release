/*
 * gen_cart2D.c
 *
 *  Created on: Aug 17, 2015
 *      Author: bzahneisen
 */

int gen_cart_2D(generic_ro *w)
{
    /* for cartesian 2D, fn is used for partial echo and fn2 is for partial echo */
    int j,ni;
    float gamma = 2.0*M_PI*4.257e3;
    float dkx=MIN(2.0*M_PI/w->fovx*0.1,w->gmax*gamma*w->dts*0.1);
    float dky=2.0*M_PI/w->fovy/10.0;
    float gampx=dkx/gamma/w->dts*10.0;
    float kxmax=2.0*M_PI*(float)w->nx/w->fovx*0.1/2.0;
    float kymax=dky*(float)w->ny/2.0;
    float kystart;

    ni=2*(int)((float)w->ny*w->fn2/2.0+0.5); /*final number of interleaves*/
    kystart=-kymax+dky*0.5+dky*(w->ny-ni);

    w->ni=1; /*for now, I use a single line at the center, then shift it*/
    w->gx_res=(int)((1+w->fn)*kxmax/dkx+0.5);
    /* making it even */
    if (w->gx_res % 2) w->gx_res++;
    w->gy_res=w->gx_res;
    w->gz_res=w->gx_res;;

    /* creating the gradient waveforms trajectory */
    for (j=0;j<w->gx_res;j++)
    {
        w->gx[j]=gampx;
        w->gy[j]=0.0;
        w->gz[j]=0.0;
    }

    /* creating k-space */
    gen_k_from_g(w->gx,w->gx_res,w->dts,-kxmax,w->kx);
    gen_k_from_g(w->gy,w->gy_res,w->dts,kystart,w->ky);

    /*setting the start, end, center */
    w->gx_start=0;
    w->gy_start=0;
    w->gz_start=0.0;
    w->gx_end=w->gx_res-1;
    w->gy_end=w->gx_res-1;
    w->gz_end=w->gx_res-1;
    w->gx_ctr=(int)(w->gx_res/2.0-0.5);
    w->gy_ctr=w->gx_ctr;
    w->gz_ctr=w->gx_ctr;

    /* Transformation matrix of the lowermost line is identity */
    identity(w->T,0);
    for (j=0;j<12;j++) w->T[j]=0.0;
    w->T[0]=1.0;w->T[5]=1.0;w->T[10]=1.0;

    /* contains a single segment */
    w->nseg=1;
    w->seg_nodes[0]=0;
    w->seg_nodes[1]=w->gx_res;

    /* frame size */
    w->frsize=w->gx_end-w->gx_start+1;

    /* shift to generate all others*/
    shift(w,ni,dky,0.0,1);

    /* the line that crosses the 0 point */
    w->i_ctr=w->ni-w->ny/2;

    return 1;

};


int gen_cart_3D(generic_ro *w)
{
    int j;
    int ny2,nz2;
    float gamma = 2.0*M_PI*4.257e3;
    float dkx=MIN(2.0*M_PI/w->fovx*0.1,w->gmax*gamma*w->dts*0.1);
    float dky=2.0*M_PI/w->fovy/10.0;
    float dkz=2.0*M_PI/w->fovz/10.0;
    float gampx=dkx/gamma/w->dts*10.0;
    float kxmax=2.0*M_PI*(float)w->nx/w->fovx*0.1/2.0;
    float kymax=dky*(float)w->ny/2.0;
    float kzmax=dkz*(float)w->nz/2.0;
    float kystart, kzstart;
    int i_ctr_y, i_ctr_z;

    ny2=2*(int)((float)w->ny*w->fn2/2.0+0.5); /*actual ny phase encodes*/
    nz2=2*(int)((float)w->nz/2.0+0.5); /* actual nz phase encodes */

    w->ni=ny2*nz2;

    kystart=-kymax+dky*0.5+dky*(w->ny-ny2);
    kzstart=-kzmax+dkz*0.5+dkz*(w->nz-nz2);

    w->ni=1; /*for now, I use a single line at the center, then shift it*/
    w->gx_res=(int)((1.0+w->fn)*kxmax/dkx+0.5);
    /* making it even */
    if (w->gx_res % 2) w->gx_res++;
    w->gy_res=w->gx_res;
    w->gz_res=w->gx_res;

    /* creating the gradient waveforms trajectory */
    for (j=0;j<w->gx_res;j++)
    {
        w->gx[j]=gampx;
        w->gy[j]=0.0;
        w->gz[j]=0.0;
    }

    /* creating k-space */
    gen_k_from_g(w->gx,w->gx_res,w->dts,-kxmax,w->kx);
    gen_k_from_g(w->gy,w->gx_res,w->dts,kystart,w->ky);
    gen_k_from_g(w->gz,w->gx_res,w->dts,kzstart,w->kz);


    /*setting the start, end, center */
    w->gx_start=0;
    w->gy_start=0;
    w->gz_start=0;
    w->gx_end=w->gx_res-1;
    w->gy_end=w->gx_res-1;
    w->gz_end=w->gx_res-1;
    w->gx_ctr=(int)(w->gx_res/2.0-0.5);
    w->gy_ctr=w->gx_ctr;
    w->gz_ctr=w->gx_ctr;

    /* Transformation matrix of the lowermost line is identity */
    for (j=0;j<12;j++) w->T[j]=0.0;
    w->T[0]=1.0;w->T[5]=1.0;w->T[10]=1.0;

    /* contains a single segment */
    w->nseg=1;
    w->seg_nodes[0]=0;
    w->seg_nodes[1]=w->gx_res;

    /* frame size */
    w->frsize=w->gx_end-w->gx_start+1;

    /* shift along y*/
    shift(w,ny2,dky,0.0,1);
    /* shift along z*/
    shift(w,nz2,dkz,0.0,2);

    /* the line that crosses the 0 point */
    i_ctr_y=ny2-w->ny/2;
    i_ctr_z=nz2-w->nz/2;
    w->i_ctr=i_ctr_z*ny2+i_ctr_y;

    return 1;


};

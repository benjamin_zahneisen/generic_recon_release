/*
 * gen_rsepi.c
 *
 *  Created on: Aug 17, 2015
 *      Author: bzahneisen
 */


int gen_pepi(generic_ro *w )
{

    /* depending on nx and ny, this can create sapepi or propeller epi */
    /*generic_ro * wepi=(generic_ro*)malloc(sizeof(generic_ro));
    float c,s;*/


    /* generating EPI */
    w->ni=w->ni/w->pepi_nblades;
    gen_epi(w);
    if (w->epi_multiband_factor == 1)
        fill_gz_with_zeros(w);
    add_ramp_to_g(w);
    add_prewinder_to_g(w);
    if (w->rewind_readout)
        add_rewinder_to_g(w);

    /* generating pepi by rotating , how simple!!! */
    rotate(w,w->pepi_nblades,180.0/(float)w->pepi_nblades,0.0,2);


    return 1;

};

int gen_rsepi(generic_ro *w )
{
    float dkx;
    int ni_epi;
    int pepi_nblades_full;

    /* beware that pepi_nblades and ni are the number of blades and interleaves after fn2 is applied
     * this is designed this way to guarantee identity transform if the same
     * waveform is tried to be created twice
     */

    pepi_nblades_full=(int)(w->pepi_nblades/w->fn2+0.5);

    ni_epi=w->ni/w->pepi_nblades;
    w->ni=ni_epi;

    /* generating the EPI part */
    gen_epi(w);
    if (w->epi_multiband_factor == 1)
        fill_gz_with_zeros(w);

    /* the central interleaf that will be used for prescan.
     * this needs to be done according to the original value of pepi_nblades
     * here, I also assume that the last couple of blades will be cropped according to
     * fn2, and not the first ones
     */
    w->i_ctr=(int)((float)(pepi_nblades_full)/2.0-0.5)*ni_epi;

    /* shifting along x */
    dkx=(float)w->ny/(float)pepi_nblades_full*2.0*M_PI/w->fovx/10.0;

    /* rsepi is on of the sequences where I can apply partial fourier along the
     * interleaf dimension. Doing this here (fn2). I am cropping the last couple
     * of blades instead of the first ones
     */
    /*w->pepi_nblades=(int)((float)(w->pepi_nblades)*w->fn2+0.5);  */


    shift(w,w->pepi_nblades,dkx,-(float)pepi_nblades_full/2.0+0.5,0);

    return 1;
}

int gen_stack_of_rsepi(generic_ro * w)
{

    float dkz;

    w->ni=w->ni/w->nz;

    /* generating the rs-epi part */
    gen_rsepi(w);
    if (w->epi_multiband_factor == 1 )
        fill_gz_with_zeros(w);

    /* shifting along z */
    dkz=2.0*M_PI/w->fovz/10.0;
    shift(w,w->nz,dkz,-(int)((float)w->nz/2.0),2);

    w->i_ctr=w->ni/2; /*!!!! actually, this must be a function of pevious i_ctr since there can be pf along the blade dim
     * take care of this later
     */
    /*!!!! this is wrong, check the gen_rsepifunction above*/
    return 1;

}


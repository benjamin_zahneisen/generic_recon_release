/*
 * gen_base_ro.c
 *
 *  Created on: Aug 17, 2015
 *      Author: bzahneisen
 */

int gen_base_ro(generic_ro *w)
{

    /*if (check_w(w) !=1)
    return 0;  */

    /* preset ni for reduction */
    w->ni=w->ni*w->R;

    switch (w->ro_type)
    {
        case RO_TYPE_CART_2D:
            gen_cart_2D(w);
            fill_gz_with_zeros(w);
            add_ramp_to_g(w);
            add_prewinder_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);
            break;
        case RO_TYPE_CART_3D:
            gen_cart_3D(w);
            add_ramp_to_g(w);
            add_prewinder_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);
            break;
        case RO_TYPE_EPI:
            gen_epi(w);
            if ((w->epi_multiband_factor == 1) || (w->gz_res == 0) )
                fill_gz_with_zeros(w);

            add_ramp_to_g(w);
            add_prewinder_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);

            break;
        /*case RO_TYPE_SPIRAL:
            gen_spiral(w);
            fill_gz_with_zeros(w);
            add_ramp_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);

            break;*/
        /*case RO_TYPE_VDSPIRAL:
            gen_vd_spiral(w);
            fill_gz_with_zeros(w);
            add_ramp_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);

            break;*/
        case RO_TYPE_PEPI:
            gen_pepi(w);
            break;
        case RO_TYPE_RSEPI:
            gen_rsepi(w);
            if (w->epi_multiband_factor == 1)
                fill_gz_with_zeros(w);
            add_ramp_to_g(w);
            add_prewinder_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);
            break;
        case RO_TYPE_STACKOFEPI:
            gen_stack_of_epi(w);
            add_ramp_to_g(w);
            add_prewinder_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);
            break;
        /*case RO_TYPE_SPIRALRADIAL:
            gen_spiral_radial(w);
            break;*/
        /*case RO_TYPE_STACKOFSPIRALS:
            add_ramp_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);
            break;*/
        /*case RO_TYPE_TURBINE:
            gen_turbine(w);
            break;*/
        /*case RO_TYPE_3D_NAV_SOS:
            gen_3Dnav(w);
            break;*/
        /*case RO_TYPE_STACKOFRSEPI:
            gen_stack_of_rsepi(w);
            add_ramp_to_g(w);
            add_prewinder_to_g(w);
            if (w->rewind_readout)
                add_rewinder_to_g(w);
            break;*/

            /*ADD_NEW_RO_TYPE*/
    }

    /* apply the reduction factor */
    apply_reduction(w);

    /* turn of readouts */
    /*if(w->turnoff_readout==1){
    zero_w(w,w->turnoff_readout);
  }*/

    /* permute waveforms */
    permute(w);

    /* revert first */
    if(w->reverse_readout == 1)
        revert_w(w);

    return 1;

};

/*------------------------------------------------------------------------------*/
/* Include all the base functions that define a trajectory and
   create the corresponding gradient waveform                                   */
/*------------------------------------------------------------------------------*/
//#include "gen_cart_2D.c" /*contains 2D and 3D cartesian readout */
//#include "gen_epi.c"   /*contains both 2D-EPI and stack-of-EPI (3D-EPI)     */
//#include "gen_rsepi.c"
/*#include "gen_spiral.c"*/
/*#include "gen_turbine.c"*/
/*#include "gen_3Dnav.c"*/




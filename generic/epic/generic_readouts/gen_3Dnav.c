/*
 * gen_nav_3D.c
 *
 *  Created on: Aug 17, 2015
 *      Author: bzahneisen
 */


int gen_3Dnav(generic_ro * w)
{
    float gamma = 2.0*M_PI*4.257e3;
    float dt;
    float S0;
    int i, j, n, k, jj;
    float Ts, T, ts, t, theta, dthdt, thetas, x, y;
    float a1, a2, beta, Gmax, c, s;
    float gabs, gmax;
    float q;


    /* getting a copy of all variables necessary */
    float D = w->fovx;
    /*int N = w->nx;*/
    int N = w->nx;
    float dts=w->dts;
    int nl=1;/*w->ni;*/
    float gamp=w->gmax;
    float gslew = w->gslew;
    float Tmax = w->Tmax;


    /* arrays to hold bits of the readout */
    float gx_spi_in[ 2*MAXPTSPERINT], gy_spi_in[ 2*MAXPTSPERINT];
    float gx_spi_out[2*MAXPTSPERINT], gy_spi_out[2*MAXPTSPERINT];

    float gz_pre[2*MAXPTSPERINT], gz_step[2*MAXPTSPERINT];
    float  gz_re[2*MAXPTSPERINT];

    int start_ramp_down;
    int n_spiral;
    int n_blip;
    float target_area;
    float step_area;
    float pre_amp;
    float re_amp;
    float step_amp;
    float step_amp_half;


    int tbeg_refoPlat;
    float bx, by;

    int k_blip[256];

    int nramp_re_nav=20;

    q = 5;
    S0 = gslew*100;
    dt = dts*0.5;

    /*  slew-rate limited approximation  */

    Ts = 0.666667/(float)nl*sqrt(pow((double)(M_PI*N), (double)3.0)/(gamma*D*S0));
    if(Ts > Tmax) {
        printf("slew limited readout too long\n");
        return -1;
    }
    a2 = N*M_PI/(nl*pow(Ts, 0.666667));
    a1 = 1.5*S0/a2;
    beta = S0*gamma*D/nl;
    Gmax = a1*pow(Ts, 0.333333);
    gmax = 0;
    i = 0;
    for (t = 0; t<=Ts; t+=dt) {
        x = pow(t, 1.333333);
        theta = .5*beta*t*t/(q + .5*beta/a2*x);
        y = q+.5*beta/a2*x;
        dthdt = beta*t*(q+0.166667*beta/a2*x)/(y*y);
        c = cos(theta);
        s = sin(theta);
        w->gx[i] = nl/(D*gamma)*dthdt*(c - theta*s);
        w->gy[i] = nl/(D*gamma)*dthdt*(s + theta*c);
        gabs = hypot(w->gx[i], w->gy[i]);
        if(gabs>=gamp)  {
            if(gmax==0)  gmax = hypot(gamp/theta, gamp);
            if(gabs>gmax) break;
        }
        ts = t;
        thetas = theta;
        i++;
    }

    /*gmax limited approximation  */

    if(Gmax > gamp)  {
        T = ((M_PI*N/(float)nl)*(M_PI*N/(float)nl) - thetas*thetas)/(2*gamma*gamp*D/(float)nl) + ts;
        if(T > Tmax)  {
            printf("gmax limited readout too long\n");
            return -1;
        }
        for (t=ts+dt; t<=T; t+=dt)  {
            theta = sqrt(thetas*thetas + 2*gamma*gamp*D*(t-ts)/nl);
            c = cos(theta);
            s = sin(theta);
            w->gx[i] = gamp*(c/theta - s);
            w->gy[i++] = gamp*(s/theta + c);
        }
    }


    n=0;

    /*decimate by 2 to get back to 4us sampling */
    for (j=0; j<i; j+=2) {
        w->gx[n]   = LIMIT(w->gx[j], -gamp, gamp);
        w->gy[n++] = LIMIT(w->gy[j], -gamp, gamp);
    }


    start_ramp_down = n;


    /*  add ramp down to zero  */
    bx = w->gx[n-1];
    by = w->gy[n-1];
    for (j=0; j<nramp_re_nav; j++)
    {
        c = 1 - j/(float)nramp_re_nav;
        w->gx[n] = bx*c;
        w->gy[n] = by*c;
        n=n+1;
    }

    /* store the spiral in*/
    for (j=0; j<n; j++)  {
        gx_spi_out[j] = w->gx[j];
        gy_spi_out[j] = w->gy[j];
    }

    n_spiral = n-1;

    /* store the spiral out*/
    for (j=0; j<n; j++)  {
        gx_spi_in[n_spiral-j] = -w->gx[j];
        gy_spi_in[n_spiral-j] = -w->gy[j];
    }
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /* End the spiral     -----------------------------------------+*/
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /* Z phase encode */
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    target_area = (0.5 * (float)(N-1))/(D/* * 0.1*/) * 1.0e6 / 4257.59; /* [us*G/cm] */
    step_area   = ((float)(1)        )/(D/* * 0.1*/) * 1.0e6 / 4257.59;

    pre_amp       = target_area/(float)(4*(4 + nramp_re_nav));
    step_amp      =   step_area/(float)(4*(4 + nramp_re_nav));
    step_amp_half =   step_amp/2;

    pre_amp       = pre_amp-step_amp_half;
    re_amp       =  re_amp+step_amp_half;



    /*  balancer ramp up */
    jj=0;
    for(j=0; j<nramp_re_nav; j++){
        c = j/(float)nramp_re_nav;
        gz_pre[ jj] = pre_amp *c;
        gz_re[  jj] =  re_amp *c;
        gz_step[jj] = step_amp*c;
        jj=jj+1;
    }

    /*  balancer plateau  */
    tbeg_refoPlat = n;
    for (j=0; j<4; j++){

        gz_pre[ jj] = pre_amp;
        gz_re[  jj] =  re_amp;
        gz_step[jj] = step_amp;
        jj=jj+1;
    }

    /*  balancer ramp down */
    for(j=0; j<nramp_re_nav; j++){

        c = 1 - j/(float)nramp_re_nav;
        gz_pre[ jj] = pre_amp *c;
        gz_re[  jj] =  re_amp *c;
        gz_step[jj] = step_amp*c;
        jj=jj+1;

    }

    n_blip = 4 + nramp_re_nav*2;

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /* build grads : combine spirals for x and y grads , blips on z -----+*/
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    k = 0;

    /* z prephase */
    for (j=0; j<n_blip; j++)  {
        w->gx[k] = 0;
        w->gy[k] = 0;
        w->gz[k] = -gz_pre[j];
        k=k+1;
    }


    /* x/y spiral in out */
    for (i=0; i<ceil(N/2); i++)  {
        for (j=0; j<=n_spiral; j++)  {

            w->gx[k] = gx_spi_out[j];
            w->gy[k] = gy_spi_out[j];

            if(j==start_ramp_down){k_blip[i] = k;};
            w->gz[k] = 0;

            k=k+1;
        }

        for (j=0; j<=n_spiral; j++)  {

            w->gx[k] = gx_spi_in[j];
            w->gy[k] = gy_spi_in[j];
            w->gz[k] = 0;
            k=k+1;
        }
        /*z  even blips*/
        if(i<ceil(N/2)-1){
            for (j=0; j<n_blip; j++)  {
                w->gx[k] = 0;
                w->gy[k] = 0;
                w->gz[k] = gz_step[j];
                k=k+1;
            }
        }
    }

    /* z odd blips */
    for (i=0; i<(ceil(N/2)); i++)  {
        for (j=0; j<=n_blip; j++)  {

            w->gz[k_blip[i]+j] = gz_step[j];

        }

    }

    /* z rephase */
    for (j=0; j<n_blip; j++)
    {
        w->gx[k] = 0;
        w->gy[k] = 0;
        w->gz[k] = -gz_re[j];
        k=k+1;
    }
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    i = k-1;
    n = 0;


    /* making sure that the resolution is even */
    if (i % 2)
    {
        w->gx[i]=w->gx[i-1];
        w->gy[i]=w->gy[i-1];
        w->gz[i]=w->gz[i-1];
        i++;
    }

    n=0;
    /*decimate by 2 to get back to 4us sampling */
    /*!!!! I don't see any decimation here - murat */
    for (j=0; j<i; j++) {
        w->gx[n]   = LIMIT(w->gx[j], -gamp, gamp);
        w->gy[n]   = LIMIT(w->gy[j], -gamp, gamp);
        w->gz[n++] = LIMIT(w->gz[j], -gamp, gamp);
    }





    w->gx_ctr=0;
    w->gy_ctr=0;
    w->gz_ctr=0;
    w->gx_res=n;
    w->gy_res=n;
    w->gz_res=n;
    w->gx_start=0;
    w->gy_start=0;
    w->gz_start=0;
    w->gx_end=n-1;
    w->gy_end=n-1;
    w->gz_end=n-1;


    /* going from gradients to k-space, assuming spiral starts at 0 */
    gen_k_from_g(w->gx,w->gx_res,w->dts,0.0,w->kx);
    gen_k_from_g(w->gy,w->gy_res,w->dts,0.0,w->ky);
    gen_k_from_g(w->gz,w->gz_res,w->dts,0.0,w->kz);

    /* constructing all interleaves */
    /*for (i=0;i<nl;i++) {
     * theta = i*2.0*M_PI/(float)nl;
     * c = cos(theta);
     * s = sin(theta);
     * w->T[12*i+0] = c;
     * w->T[12*i+1] = -s;
     * w->T[12*i+2] = 0.0;
     * w->T[12*i+3] = 0.0;
     * w->T[12*i+4] = s;
     * w->T[12*i+5] = c;
     * w->T[12*i+6] = 0.0;
     * w->T[12*i+7] = 0.0;
     * w->T[12*i+8] = 0.0;
     * w->T[12*i+9] = 0.0;
     * w->T[12*i+10]= 1.0;
     * w->T[12*i+11]= 0.0;
     * }*/

    /* here the nav is the same for all shots */
    for (i=0;i<w->ni;i++) {

        w->T[12*i+0 ] = 1.0;
        w->T[12*i+1 ] = 0.0;
        w->T[12*i+2 ] = 0.0;
        w->T[12*i+3 ] = 0.0;
        w->T[12*i+4 ] = 0.0;
        w->T[12*i+5 ] = 1.0;
        w->T[12*i+6 ] = 0.0;
        w->T[12*i+7 ] = 0.0;
        w->T[12*i+8 ] = 0.0;
        w->T[12*i+9 ] = 0.0;
        w->T[12*i+10] = 1.0;
        w->T[12*i+11] = 0.0;
        w->T[12*i+12 ]= 0.0;
        w->T[12*i+13] = 0.0;
        w->T[12*i+14] = 0.0;
        w->T[12*i+15 ]= 1.0;
    }



    /* number of segments with different transformation matrices*/
    w->nseg=1;
    w->seg_nodes[0]=0;
    w->seg_nodes[1]=w->gx_res;

    /* number of segments with different WF_pulses */
    /*     w->n_psd_seg_gx=1; */
    /*     w->psd_segs_gx[0]=0; */
    /*     w->psd_segs_gx[1]=w->gx_res; */

    /*     w->n_psd_seg_gy=1; */
    /*     w->psd_segs_gy[0]=0; */
    /*     w->psd_segs_gy[1]=w->gy_res; */

    /*     w->n_psd_seg_gz=1; */
    /*     w->psd_segs_gz[0]=0; */
    /*     w->psd_segs_gz[1]=w->gz_res; */

    /* rhfrsize */
    w->frsize=w->gx_end-w->gx_start+1;



    return 1;

}


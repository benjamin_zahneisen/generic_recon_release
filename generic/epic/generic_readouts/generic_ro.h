/* generic_ro.h
 * Murat Aksoy aksoymurat@gmail.com
 * Rafael O'Halloran rafael.ohalloran@gmail.com
 * Stanford University, 2011
 */


/*
 *
 * Follow ADD_NEW_RO_TYPE to add a new readout type
 *
 * todo
 *
 * if 3d, define maxinter 32766, define maxptsperint 5000
 * if 2d, define maxinter bla bla
 * n_psd_seg_g? must be size 2*MAXSEG to be able to leave gaps in the waveform
 *
 */

#ifndef _GENERIC_RO_
#define _GENERIC_RO_

#ifdef COMPILE_FOR_PSD
/*#include <GElimits.h>
#include <slice_factor.h>
#include <usage_tag.h>
#include "pulsegen.h"
#include <entrypttab.h>*/
#endif


/*------------------------------------------------------------------------------*/
/* Define available readouts (=trajectories).                                   */
/*------------------------------------------------------------------------------*/
#define RO_TYPE_NONE           -1
#define RO_TYPE_CART_2D        0
#define RO_TYPE_CART_3D        1
#define RO_TYPE_EPI            2
#define RO_TYPE_SPIRAL         3
#define RO_TYPE_VDSPIRAL       4
#define RO_TYPE_PEPI           7
#define RO_TYPE_RSEPI          8
#define RO_TYPE_STACKOFEPI     9
#define RO_TYPE_SPIRALRADIAL   10
#define RO_TYPE_STACKOFSPIRALS 11
#define RO_TYPE_TURBINE        12
#define RO_TYPE_3D_NAV_SOS     13
#define RO_TYPE_STACKOFRSEPI   14

#define _XGRAD_ 1
#define _YGRAD_ 2
#define _ZGRAD_ 4

#ifndef MAXPTSPERINT
#define MAXPTSPERINT 32767
#endif
/* we cannot go above the maximum allowed readout length by GE */

/*#ifndef MAXPTS
 * #define MAXPTS (256*256*256)
 * #endif*/

/*!!!!! for now, this is valid for 2D only. will test 3D later */
#ifndef MAXINTER
#define MAXINTER 33000
#endif

#ifndef MAXPTSTOTAL
#define MAXPTSTOTAL 100000000
#endif

/* Golden angle stuff */
#define GOLDEN_1D       1.61803
#define GOLDEN_2Da      0.4656
#define GOLDEN_2Db      0.6823



/* maximum number of segments
 * with ref-lines, ramp up and down, prewinder and rewinder, this is maximum 5.
 *
 */
#define MAXSEG 5

/*#define MAX_PSD_SEGS_G 1024*/

#define MAX_PSD_ECHOES 8

/* maximum number of points of an acquisition window*/
#ifndef MAX_FILTER_POINTS
#define MAX_FILTER_POINTS 32767
#endif

#define MAXPTSPERWFPULSE 32767

#ifndef M_PI
#define M_PI 3.141592653589793
#endif

#ifndef LIMIT
#define LIMIT(x, xmin, xmax)   ( (x<xmax)? ((x>xmin)? x:xmin):xmax )
#endif

#ifndef MAX
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))
#endif

#define EPS_K (1.0e-10)

#include "stdlib.h"
#include "math.h"
#include "time.h"
#include "string.h"
#include "stdio.h"

/* if you decide to change this struct, you have to make changes in multiple (at least 4) other places */
typedef struct generic_ro {
    float gx[MAXPTSPERINT];
    float gy[MAXPTSPERINT];
    float gz[MAXPTSPERINT];
    float kx[MAXPTSPERINT];
    float ky[MAXPTSPERINT];
    float kz[MAXPTSPERINT];
    float gx_cur[MAXPTSPERINT];
    float gy_cur[MAXPTSPERINT];
    float gz_cur[MAXPTSPERINT];
    float kx_cur[MAXPTSPERINT];
    float ky_cur[MAXPTSPERINT];
    float kz_cur[MAXPTSPERINT];
    float tempmem[MAXPTSPERINT];
    float T[4*3*MAXINTER*MAXSEG];
    /*
   
  for (nseg)
     for (ni)
        for (12)
   
_      _      _               _  _       _  _  _
|kx_cur |    |  T0  T1  T2  T3 ||         || kx |
|ky_cur | =  |  T4  T5  T6  T7 ||         || ky |
|kz_cur |    |  T8  T9 T10 T11 || PERMUTE || kz |
|xxxxxx |    | T12 T13 T14 T15 ||         || xx |
|-     -|    |-               -||-       -||-  -|
     */
    
    int nseg;
    /* gradient waveform of segment i runs from seg_nodes[i] to seg_nodes[i+1]-1
     these are the segments with different transfer functions */
    int seg_nodes[MAXSEG+1];
    
#ifndef COMPILE_FOR_PSD
float kx_all[MAXPTSTOTAL];
float ky_all[MAXPTSTOTAL];
float kz_all[MAXPTSTOTAL];
#endif

/* the maximum length of gradient waveform and readout filter is limited.
 * WF_PULSE of segment i runs from psd_segs_g?[i] to psd_segs_g?[i+1]-1
 * This is why I divide it here
 */
/* int n_psd_seg_gx; */
/*   int psd_segs_gx[MAX_PSD_SEGS_G]; */
/*   int n_psd_seg_gy; */
/*   int psd_segs_gy[MAX_PSD_SEGS_G]; */
/*   int n_psd_seg_gz; */
/*   int psd_segs_gz[MAX_PSD_SEGS_G]; */
/* since echo lengths must be equal, I don't divide this using an array */
int frsize;

float fovx;
float fovy;
float fovz;
int gx_ctr;
int gy_ctr;
int gz_ctr;
int gx_res;
int gy_res;
int gz_res;
int gx_start;
int gy_start;
int gz_start;
int gx_end;
int gy_end;
int gz_end;
int ro_type;
int ni; /* number of interleaves */
int nx;
int ny;
int nz;
float fn; /* fractional nex */
float fn2; /* fractional nex in the second direction */
float dts; /*the gradient sampling time */
float dts_adc; /*the adc sampling time */
float gmax;
float gslew;
float Tmax;
float vdspiral_alpha;
/*int n_nav;*/
/*int spiralio_gres_nav;
 * int spiralio_gres_imag;*/
int epi_blip_res; /* resolution of the blip portion of epi */
int epi_flat_res; /* resolution of the flat portion of EPI */
int epi_ramp_res; /* resolution of the ramp portion of EPI */
int epi_rampsamp; /* ramp sampling for epi */
int epi_blip_up_down; /* use blip up and down for distortion correction */
int epi_multiband_factor; /* how many simultaneously excited slices */
float epi_multiband_fov; /* multi-band fov = mb_factor*slice separation */
float epi_multiband_R; /*defines caipi-blip pattern, no blips for epi_multiband_R = epi_multiband_factor */
int pepi_nblades; /* number of blades for pepi */
int spiral_radial_nspokes; /* number of radial arms for 3d spiral */
int turbine_radial_nspokes; /* number of radial arms for turbine */
int rewind_readout;      /* rewind the readout? 0=no 1=yes (only the imaging part, before adding nav)*/
int golden_angle;      /* */
int reverse_readout;   /* reverse the readout
 * the order is as follows:
 * 1) reverse 2) add ramp prewinder 3) add navigator
 */
/*int turnoff_readout;*/

int permute; /* permutations of the gradient waveforms.
 * x(0)  y(1)  z(2) -x(3) -y(4) -z(5)
 * Possible values are:
 * (default) standard axis - [012]
 * switch x and y -          [102]
 * switch x and z -          [210]
 * switch y and z -          [021]
 *                           [120]
 * etc...
 * 6-7-8-9 means no waveform on that axis, same as turn off waveform
 */

int i_ctr; /* this is the index for the interleaf that crosses the center of k-space
 * for spiral, EPI, SAP, etc. this is in general 0 because all
 * interleaves have similar signal. However, for readouts like RS (ro_type=8,14)
 * this is not 0 since the central blind is scanned later in sequence.
 * We will use this variable for performing prescan with the central blind.
 * see generic.e to see how this is used.
 */

int R; /* reduction factor. Even for, e.g., R=2, ni interleaves are produced. However, the trajectory
 * is desgined as if there are R*ni interleaves, and every R interleave is scanned
 */

int R_skip; /* R_skip=1, R=3
 * X--X--X--X--....
 *
 * R_skip=2, R=3
 * XX----XX----XX----
 *
 * R_skip is used for doing reduction in the second phase encode direction for
 * 3D acquisitions. e.g., for undersampling in the stack direction in STACKOFEPI
 */

/*ADD_NEW_RO_TYPE*/

} generic_ro;

#ifdef MATLAB_MEX_FILE
const char *generic_ro_field_names[] = {
    "gx",
    "gy",
    "gz",
    "kx",
    "ky",
    "kz",
    "kx_all",
    "ky_all",
    "kz_all",
    "T",
    "nseg",
    "seg_nodes",
    /*     "n_psd_seg_gx", */
    /*     "psd_segs_gx", */
    /*     "n_psd_seg_gy", */
    /*     "psd_segs_gy", */
    /*     "n_psd_seg_gz", */
    /*     "psd_segs_gz", */
    "frsize",
    "fovx",
    "fovy",
    "fovz",
    "gx_ctr",
    "gy_ctr",
    "gz_ctr",
    "gx_res",
    "gy_res",
    "gz_res",
    "gx_start",
    "gy_start",
    "gz_start",
    "gx_end",
    "gy_end",
    "gz_end",
    "ro_type",
    "ni",
    "nx",
    "ny",
    "nz",
    "fn",
    "fn2",
    "dts",
    "dts_adc",
    "vdspiral_alpha",
    "gmax",
    "gslew",
    "Tmax",
    "epi_blip_res",
    "epi_flat_res",
    "epi_ramp_res",
    "epi_rampsamp",
    "epi_blip_up_down",
    "epi_multiband_factor",
    "epi_multiband_fov",
    "epi_multiband_R",
    "pepi_nblades",
    "spiral_radial_nspokes",
    "turbine_radial_nspokes",
    "rewind_readout",
    "golden_angle",
    "reverse_readout",
    /*"turnoff_readout",*/
    "permute",
    "i_ctr",
    "R",
    "R_skip"
};
int generic_ro_nfields = 59;
/*ADD_NEW_RO_TYPE*/
#endif


void write_log(char *msg);
void write_log_3(generic_ro * w);
void write_kspace(generic_ro * w, char *fname);
void init_ro(generic_ro * w);
int gen_g_from_k(float * k,int kres,float dts, float * g);
int gen_k_from_g(float * g,int gres,float dts,float k0,float * k);

int revert_w(generic_ro * w);
int cat_w(generic_ro * w1, generic_ro * w2, int centerref);

int copy_w(generic_ro * w_src, generic_ro * w_dst);
void free_w(generic_ro * w);
float calc_g_area(float * g, int gres, float dts,float * tempmem);
int gen_ramp(float g1,
        float g2,
        float tr,
        float gslew,
        float dts,
        float *g,
        int * gres,
        float * gslew_used);
int gen_trap(float kmax,float gmax, float gslew, float dts, float * g, int * gres);

int gen_base_ro(generic_ro *w);
/* assumes that w->k exists */
int gen_cur_ro(generic_ro * w,int i);
#ifndef COMPILE_FOR_PSD
int gen_all_ro(generic_ro * w);
#endif

/* sets the base readout k* and g* to one of the interleaves
 * also sets the T correctly
 */
int set_base_ro(generic_ro *w, int ileaf);




/** 
 * create shifted copies of the waveform. This is done by adding the
 * necessary translations to the matrix on the old value of translation.
 * Be careful to apply this before applying any prewinder & rewinder
 * because this function will shift the while k-space
 * @param w the input and output waveform
 * @param n how many shifted copies will be created
 * @param d distance between shifted copies
 * @param start_index starting k-space is start_index*d
 * @axis the axis on which the shift will be done
 */
int shift(generic_ro * w,int n,float d,float start_index,int axis);


/*BZ shift a single interleave. Do not create copies of the waveform*/
int shift_ilv(generic_ro * w,int n,float d,int axis);

/*does what its name suggests */
int add_ramp_prewinder_to_g(generic_ro * w);


/** 
 * create rotated copies of the input waveform. This is done by 
 * left multiplying the transformation matrices. Most of the time, the
 * prewinders and rewinders are also rotated so you should be good applying
 * rotation after these.
 * @param w the input and output waveform
 * @param n how many rotated copies will be created
 * @param d angle between rotated copies (degrees)
 * @param start_index 0 to n-1 such that starting rotation angle is start_index*d
 * @axis the axis around which the rotation will be done
 */
int rotate(generic_ro * w,
        int n,
        float d,
        float start_index,
        int axis);

/**
 * replicate the waveforms with the same transformation matrices.
 * the new ni is n*ni, and the transformation matrices are replicated
 * in the rest of the waveform.
 * @param w the input and output waveform
 * @param n number of times the waveform is replicated.
 */
void rep_w(generic_ro * w,int n);

/**
 * add n interleaves to w. The added interleaves have the same number of 
 * segments as the input, but they are set to indentity
 */
/*void add_ileaf_to_w(generic_ro * w,int n);*/

/**
 * multiply the transformation matrices of an interleaf 
 * with the given transformation matrix
 */
void multiply_w(generic_ro * w, float * T, int leafi);

int apply_reduction(generic_ro *w);

int add_ramp_to_g(generic_ro * w);
int add_prewinder_to_g(generic_ro * w);

int add_rewinder_to_g(generic_ro * w);
int add_nav_before_g(generic_ro * w);
int fill_gz_with_zeros(generic_ro *w );

/**
 * return the golder angle (in radians)
 * @param idx interleave index
 * @param dim total number of dimensions for the golden angle calculation
 * @param dim_idx the dimension for which the golden angle is to be returned
 */
float golden_angle_coef(int idx, int dim, int dim_idx );


int permute(generic_ro * w); /* apply the w->permute to waveforms */

/**
 * multiply two matrices. All matrices are arrays with 12 elements
 * @param T1 first matrix
 * @param T2 second matrix
 * @param T preallocated result
 */
void multiply(float * T1, float * T2, float * T);



/**
 * Set the matrix to identity
 */
void identity(float * T, int n);

/**
 * invert the matrix 
 */
int gluInvertMatrix(double m[16], double invOut[16]);

/**
 * invert the matrix 
 */
void invert(float * T,float * Tinv);


/*------------------------------------------------------------------------------*/
/* Fucntion declarations for available readouts (=trajectories).                */
/* each readout is defined in a separate c-file                                 */
/*------------------------------------------------------------------------------*/
int gen_spiral(generic_ro * w);
int gen_3Dnav(generic_ro * w);
int gen_vd_spiral(generic_ro * w);
int gen_pepi(generic_ro *w);
int gen_cart_2D(generic_ro *w);                      /* #define RO_TYPE_EPI          0*/
int gen_cart_3D(generic_ro *w);
int gen_epi(generic_ro * w);                        /* #define RO_TYPE_EPI          2*/
int gen_spiral_radial(generic_ro * w);
int gen_rsepi(generic_ro *w );                      /* #define RO_TYPE_RSEPI        8*/
int gen_stack_of_rsepi(generic_ro * w);
int gen_stack_of_epi(generic_ro *w);                /* #define RO_TYPE_STACKOFEPI   9*/
int gen_turbine(generic_ro *w);



#endif

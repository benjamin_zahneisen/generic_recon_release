

% generic_make

function generic_make

temp=pwd;


%cd ~/generic_recon_release/generic
% I don't know if MATLAB_MEX_FILE preprocessor directive is defined by
% default by the mex command
mex -DMATLAB_MEX_FILE generic_gen_ro_mex.c
mex -DMATLAB_MEX_FILE generic_add_ramp_prewinder_to_g_mex.c
mex -DMATLAB_MEX_FILE generic_ro_struct_mex.c


cd(temp);
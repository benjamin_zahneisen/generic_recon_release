function generic_recon_remote(fullfilename)

%this wrapper function is called from lc3t2 via ssh
%fullfilename is the path and name of the p-file 
if nargin <1
    fullfilename = 'dummy';
end

display('hello from nehalem')
display(fullfilename)

try

	%set path to stable generic_recon release
	addpath(genpath('/home/bzahneisen/generic_recon_stable'))	
	
    %call generic_recon
    generic_recon_nyu(fullfilename);
    display('ready to quit.');
    quit;
catch
    display('failed attempt to call generic_recon_nyu.m')
end

%finally close matlab
display('quitting');
quit

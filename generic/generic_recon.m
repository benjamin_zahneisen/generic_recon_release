% function [rcn rcn2 w_imag w_nav]=generic_recon(varargin)
% 
% [rcn rcn2 w_imag w_nav]=generic_recon(pfilename)
% [[rcn rcn2 w_imag w_nav]=generic_recon(runno)
% [rcn w]=generic_recon
%
% rcn is size  nx ny nz nd nsl nc
%
% optional arguments
%
%   ts=[1:nd]
%   sls=[1:nsl] %processing only, memory is allocated for all slices anyway
%   epi_ref=[]
%   epi_ref_use_entropy
%   epi_ref_entropy_per_slice
%   outpath=pwd
%   reg=1
%   dosense=0
%   dograppa=0
%   grappa=[]
%   phasecorr=0
%   pocs=1 % do pocs (pocs=1) or zerofill (pocs=0)
%


function [rcn, rcn2, w_imag, w_nav, data_imag]=generic_recon(varargin)

[pfilename opts_start]=generic_getpfilename(varargin);

%getting header variables
%ADD_NEW_RO_TYPE
hdr=read_gehdr(pfilename);

if(strcmp(hdr.image.psd_iname,'generic_dPFG'))
    generic_exportHeader(hdr);
end


w_imag=generic_ro_struct_mex;
w_imag.ro_type=hdr.rdb.user0;
%w_imag.fovx=hdr.rdb.user1;
w_imag.fovx=hdr.image.dfov*0.1; % lets hope hdr.image.dfov*0.1 = hdr.rdb.user1
w_imag.fovy=hdr.image.dfov_rect*0.1;
w_imag.fovz=hdr.rdb.user14; % fovz can be different than excited slice thickness. 
                            % Keep as user14 and do not use hdr.image.slthick
w_imag.ni=hdr.rdb.user2;
w_imag.nx=hdr.rdb.user3;
w_imag.ny=hdr.rdb.user4;
w_imag.nz=hdr.rdb.user5;
w_imag.dts=hdr.rdb.user6;
w_imag.dts_adc=hdr.rdb.user9;
w_imag.gmax=hdr.rdb.user7;
w_imag.gslew=hdr.rdb.user8;
%w_imag.Tmax=hdr.rdb.user9;
w_imag.vdspiral_alpha=hdr.rdb.user11;
w_imag.slabthk=hdr.rdb.user14;
w_imag.pepi_nblades=5;
w_imag.spiral_radial_nspokes = hdr.rdb.user16;
%w_imag.fn=hdr.image.nex;
w_imag.fn=hdr.rdb.user34;
w_imag.fn2=hdr.rdb.user38;
w_imag.epi_rampsamp=hdr.rdb.user21;
w_imag.turbine_radial_nspokes=hdr.rdb.user22;
w_imag.rewind_readout=hdr.rdb.user23;
w_imag.nav_type=hdr.rdb.user24;
w_imag.golden_angle=hdr.rdb.user25;
w_imag.reverse_readout=hdr.rdb.user26;
w_imag.R=max(1,hdr.rdb.user38); %!!!!this is a dirty hack
w_imag.R_skip=w_imag.ni; %!!!!
w_imag.permute = hdr.rdb.user36;
w_imag.epi_blip_up_down = hdr.rdb.user42;

w_imag.epi_multiband_factor = hdr.rdb.user29;
w_imag.epi_multiband_fov = hdr.rdb.user30;
w_imag.epi_multiband_R = hdr.rdb.user32;


nav_pos=hdr.rdb.user27;
frsize=hdr.rdb.frame_size;
nd =  hdr.rdb.user12;
nc =  hdr.rdb.dab_stop_rcv(1)-hdr.rdb.dab_start_rcv(1)+1;           
nsl = hdr.image.slquant;
refscan=hdr.rdb.user31;


%% get pfile-header information
b0map               = hdr.rdb.b0map;
numdifdirs          = hdr.rdb.user17;  %opdifnumdirs
numB0               = hdr.rdb.user18;  %num_B0
pomp_calib          = 0; %this flag is set if we used the first n=multi-band factor time frames as a reference 
blipUpDown          = hdr.rdb.user44;
%blipUpDownIlv = w_imag.epi_blip_up_down;
if(strcmp(hdr.image.psdname,'generic_dPFG'))
    blipUpDown          = hdr.rdb.user44;%this might be different somewhere 
end
singleBandRef       = (hdr.rdb.user47 > 0);
singleBandRefDown   = (hdr.rdb.user47 > 1);

w_nav=generic_ro_struct_mex;
w_nav.ro_type=hdr.rdb.user24;
w_nav.fovx=hdr.image.dfov*0.1;
w_nav.fovy=hdr.image.dfov_rect*0.1;
w_nav.fovz=hdr.rdb.user14;
w_nav.ni=hdr.rdb.user35; 
w_nav.nx=hdr.rdb.user10; 
w_nav.ny=hdr.rdb.user33;
w_nav.nz=hdr.rdb.user10;
w_nav.dts=hdr.rdb.user6;
w_nav.gmax=hdr.rdb.user7;
w_nav.gslew=hdr.rdb.user8;
w_nav.Tmax=hdr.rdb.user9;
w_nav.vdspiral_alpha=hdr.rdb.user11;
w_nav.slabthk=hdr.rdb.user14;
w_nav.pepi_nblades=5;
%w_nav.spiral_radial_nspokes =1; %!!!!
w_nav.fn=hdr.rdb.user34;
w_nav.fn2=hdr.rdb.user37;
w_nav.epi_rampsamp=hdr.rdb.user21;
%w_nav.turbine_radial_nspokes=hdr.rdb.user22;
%w_nav.rewind_readout=hdr.rdb.user23;
%w_nav.golden_angle=hdr.rdb.user25;
w_nav.reverse_readout=hdr.rdb.user26;
w_nav.R=max(1,hdr.rdb.user39); %!!!!this is a dirty hack
w_nav.R_skip=max(1,hdr.rdb.user40); %!!!!
w_nav.permute = hdr.rdb.user36;
w_nav.epi_blip_up_down = hdr.rdb.user42;

if refscan
    w_epi_ref=w_imag;
    w_epi_ref.fn=1.0;
    w_epi_ref.fn2=1.0;
    w_epi_ref.ni=w_imag.ni/w_imag.nz;
    w_epi_ref.ny=6*w_epi_ref.ni; 
    w_epi_ref.nz=1;
    
end

opts_def.ts=1:nd;
opts_def.sls=[1:nsl];
opts_def.epi_ref=[];
opts_def.epi_ref_use_entropy=0;
opts_def.epi_ref_entropy_per_slice=0;
opts_def.outpath=pwd;
opts_def.reg=1;
opts_def.full =1;
opts_def.dosense=0; 
opts_def.dograppa=0; 
opts_def.grappa=[];
opts_def.phasecorr=0; 
opts_def.pocs=0;
opts_def.flavor ='sense';
opts_def.verbose = 0;

%opts_def.align_nav_imag=0;
opts=get_opts({varargin{opts_start:end}},opts_def);

% sanity checks here

%check grappa
if opts.dograppa == 1 && ...
        w_imag.ro_type ~= 0 && ...
        w_imag.ro_type ~= 1 && ...
        w_imag.ro_type ~= 2 && ...
        w_imag.ro_type ~= 7 && ...
        w_imag.ro_type ~= 8 && ...
        w_imag.ro_type ~= 9 && ...
        w_imag.ro_type ~= 11 && ...
        w_imag.ro_type ~= 12 && ...
        w_imag.ro_type ~= 14
    error('cannot do grappa with this trajectory... sorry....');
end




curpath=fileparts(pfilename);
%outpath=fullpath(opts.outpath)
% BZ set outpath to curpath
outpath = curpath;
opts.outpath = curpath;
cd(outpath) 

% If outpath does not exist
if ~exist(outpath,'dir')
    mkdir(outpath);
end
% Set folder properties to write
% fileattrib(outpath, '+w');

nd=numel(opts.ts);
nd_nav=nd*w_imag.ni/w_nav.ni;
w_imag=generic_gen_ro_mex(w_imag);
if w_nav.ro_type ~=-1
    w_nav=generic_gen_ro_mex(w_nav);
end
if refscan
    w_epi_ref=generic_gen_ro_mex(w_epi_ref);
end

%hack for now
if w_imag.epi_blip_up_down
   w_imag.ni = w_imag.ni/2;
   ni_true = hdr.rdb.user2; %that's the prescribed CV2
   ni = 2; %acquired number of interleaves
else
    ni =1;
end

frames_to_read=[];
for i=1:numel(opts.ts)
    frames_to_read=[frames_to_read (opts.ts(i)-1)*ni+1:opts.ts(i)*ni];
end

if ~isfield(opts, 'nr_pfiles')
    pfile_list = dir('P*.7');
    opts.nr_pfiles = length(pfile_list);
end

%extra header information after mex call
w_imag.slquant = hdr.image.slquant;
w_imag.slquant_eff = hdr.image.slquant/w_imag.epi_multiband_factor;
w_imag.slthick = hdr.image.slthick;
w_imag.R_skip = w_imag.ni;
w_imag.inplane_R= hdr.rdb.user48;
w_imag.pompCalib = 0;


%reading data
%data is size nk x ni x necho x nsl x nc
data=read_pfile_2(pfilename,'frames',frames_to_read, 'nr_pfiles', opts.nr_pfiles);

%append 0's to the data to make frame sizes between traj and data equal
data(end:max(w_imag.frsize,w_nav.frsize),:,:,:,:)=0.0;


%data_imag=data(:,:,1,:,:); %nk x ni x 1 x nsl x nc
%data_imag=generic_reorder_data(data_imag,w_imag.frsize); %!! is frsize == gx_end-gx_start+1???
data_imag=data(1:w_imag.frsize,:,1,:,:);
if w_nav.ro_type ~=-1
    %data_nav=data(:,:,2,:,:); %nk x ni x 1 x nsl x nc
    %data_nav=generic_reorder_data(data_nav,w_nav.frsize); %!! is frsize == gx_end-gx_start+1???    
    data_nav=data(1:w_nav.frsize,:,2,:,:);
else
    data_nav=[];
    rcn2=[];
end


%reading the refscan if there was one
%refscan is the last frame
refdata=[];
if refscan    
    %the refdata is always the 3rd echo
    refdata=data(:,:,3,:,:); % nk x (ni x nd) x 1 x nsl x nc
    %cropping refdata to the acutal size
    refdata_len=w_epi_ref.frsize;
    refdata=refdata(1:refdata_len,:,:,:,:);
end

%freeing memeory
clear data



% putting in the number of interleaves
% nk x ni x nd x 1 x nsl x nc
s=size(data_imag);
data_imag=reshape(data_imag,[s(1) ni nd s(3:end)]);
if w_nav.ro_type ~=-1    
    s=size(data_nav);
    data_nav=reshape(data_nav,[s(1) w_nav.ni nd_nav s(3:end)]);
end

if refscan   
    s=size(refdata);
    refdata=reshape(refdata,[s(1) 1 s(2:end)]);
end



%accounting for fov shifts (weird convention, should be fixed )
%hdr.rdb.xoff = w_imag.nx*(hdr.rdb.xoff/(w_imag.fovx*10));
%hdr.rdb.yoff = w_imag.ny*(hdr.rdb.yoff/(w_imag.fovy*10));
xoff = hdr.rdb.xoff; %store it
hdr.rdb.xoff = 0; %overwrite it because readout shifts are done with thetat wave 



%% slice re-ordering (linear/interleaved)
sliceOrderMode = hdr.rdb.user15;
if(sliceOrderMode == 1)
    slcIdxFull = 1:2:w_imag.slquant;
    slcIdxFull = [slcIdxFull (2:2:w_imag.slquant)];
    [~, slcIdxFull]=sort(slcIdxFull);
    slcIdx = 1:2:w_imag.slquant_eff;
    slcIdx = [slcIdx (2:2:w_imag.slquant_eff)];
    [~, slcIdx]=sort(slcIdx);
else
    slcIdxFull = 1:w_imag.slquant;
    slcIdx = 1:w_imag.slquant_eff;
end

%% save some memory
data_imag = single(data_imag);

%% single band, central region k-space in case of interleaved EPI(=inplane acceleration >1)
if(singleBandRef ==1)
    display('single band calibration')
    if(w_imag.ni>1)
        display('multiple shots=> generic acquires central k-space region in 1 shot only!')
    end
    data_ref_central = data_imag(:,1,1:w_imag.epi_multiband_factor,1,:,:);
    data_ref_central = permute(data_ref_central,[1 2 5 3 4 6]);
    data_ref_central = reshape(data_ref_central,[size(data_imag,1) 1 1 1 w_imag.slquant size(data_ref_central,6)]);
    
    %in-plane shifts with reduced gy gradient (only the first interleave) 
    data_ref_central=applyInplaneShifts(data_ref_central,hdr.rdb.xoff,hdr.rdb.yoff/w_imag.ni,w_imag,1);
    
    w_ref_central= w_imag;
    w_ref_central.epi_multiband_factor = 1;
    w_ref_central.epi_multiband_fov = -1;
    w_ref_central.slquant = w_imag.slquant;
    w_ref_central.slquant_eff = w_imag.slquant;
    w_ref_central.pompCalib = false;
    w_ref_central.ni = 1; %set interleaves to 1
    w_ref_central.ny = w_imag.ny/w_imag.ni; %reduce y resolution to central region
    w_ref_central.R_skip = 1; %we do no longer skip samples
    opts_ref = opts;
    opts_ref.doghost = 1;
    opts_ref.dosense = 0;
    data_ref_central = data_ref_central(:,:,:,:,slcIdxFull,:);
    [rcn_ref, GC,S,data_ref_central]=generic_recon_epi(data_ref_central,w_ref_central,opts_ref);
    
    %reset transformation to multi-band scan
    GC.isTrans = 0;
    
     %remove ref-data from data_imag
     data_imag = data_imag(:,:,w_imag.epi_multiband_factor+1:end,:,:,:);
     opts.ts = opts.ts(1:end-w_imag.epi_multiband_factor);
     
    %copy reconstructed single band reference data back to scanner
    try
        V = sos(rcn_ref);
        V=imresize3D(V,[w_imag.nx w_imag.ny size(V,3)]);
        %moveVols2Scanner(V,hdr,1);
    catch
       display('failed to export dicoms to scanner') 
    end
end


%% blip down single band scan
if(singleBandRefDown ==1)
    display('Blip down single band calibration')
    data_ref_centrald = data_imag(:,1,1:w_imag.epi_multiband_factor,1,:,:);
    data_ref_centrald = permute(data_ref_centrald,[1 2 5 3 4 6]);
    data_ref_centrald = reshape(data_ref_centrald,[size(data_imag,1) 1 1 1 w_imag.slquant size(data_ref_centrald,6)]);
    
    
    %in-plane shifts with reduced AND inverted gy gradient (only the first interleave) 
    data_ref_centrald=applyInplaneShifts(data_ref_centrald,hdr.rdb.xoff,hdr.rdb.yoff/w_imag.ni,w_imag,-1);
    
    data_ref_centrald = permute(data_ref_centrald,[1 2 5 3 4 6]);

    data_ref_centrald = reshape(data_ref_centrald,[size(data_imag,1) 1 1 1 w_imag.slquant size(data_ref_centrald,6)]);
    w_ref_central_down= w_ref_central;
    w_ref_central_down.reverse_readout = 1;
    opts_ref = opts;
    opts_ref.doghost = 1;
    opts_ref.dosense = 0;
    data_ref_centrald = data_ref_centrald(:,:,:,:,slcIdxFull,:);
    [rcn_ref_down, GC_down,S_down,data_ref_centrald]=generic_recon_epi(data_ref_centrald,w_ref_central_down,opts_ref);
    
    %undo flip
    data_ref_centrald=data_ref_centrald(:,end:-1:1,:,:,:,:,:);
    
    %reset transformation to multi-band scan
    GC.isTrans = 0;
    
    %remove ref-data from data_imag
    data_imag = data_imag(:,:,w_imag.epi_multiband_factor+1:end,:,:,:);
    opts.ts = opts.ts(1:end-w_imag.epi_multiband_factor);
    
    save('refScan','data_ref_central','data_ref_centrald','w_ref_central');
    
end



%% re-order slices prior to multi-band slice offset correction
data_imag = data_imag(:,:,:,:,slcIdx,:);


%which interleaves are used
if(blipUpDown); 
    if(hdr.rdb.user2 == 1);
        iv_ro = [1 2];
    else
        iv_ro = [1 4];
    end
else
   iv_ro = 1:ni; 
end


%% in case of blipped-EPI and multi-band excitation we also have to perform a
%slice dependent phase offset correction
if( w_imag.epi_multiband_factor > 1)    
    
    coverage=(w_imag.slthick + hdr.image.scanspacing)*w_imag.slquant;
    if(mod(w_imag.slquant,2)==0)
        slcPos1 = -coverage/2 + (w_imag.slthick + hdr.image.scanspacing)/2;
        slcPos = slcPos1:(w_imag.slthick + hdr.image.scanspacing):(coverage/2-w_imag.slthick/2);
        
        if(mod(w_imag.epi_multiband_factor,2)==0); %mb=2,4,6,...
            lowestSliceGroup = 1:(w_imag.slquant/w_imag.epi_multiband_factor):w_imag.slquant;
            lowestSliceOffset = slcPos(lowestSliceGroup(w_imag.epi_multiband_factor/2 + 1));
        else %3,5,...
            lowestSliceGroup = 1:(w_imag.slquant/w_imag.epi_multiband_factor):w_imag.slquant;
            lowestSliceOffset = slcPos(lowestSliceGroup(ceil(w_imag.epi_multiband_factor/2 )) );  
        end
        
    else
        display('not yet implemented.')
    end
    
    for sl=1:(nsl/w_imag.epi_multiband_factor)
        slcOffset = lowestSliceOffset + (sl-1)*(w_imag.slthick + hdr.image.scanspacing);
        phaseOffset(:,:,sl) = exp(1i*(w_imag.kz_all(w_imag.gz_start+1:w_imag.gz_end+1,iv_ro)*slcOffset));
    end
    phaseOffset = reshape(phaseOffset,[size(data_imag,1) size(data_imag,2) 1 1 size(data_imag,5) 1]);
    phaseOffset = repmat(phaseOffset,[1 1 size(data_imag,3) size(data_imag,4) 1 size(data_imag,6) ]);
    
    data_imag = data_imag.*phaseOffset;
end

time=clock;
fprintf('%.2d %.2d %.2d: Data reading completed...\n', time(4), time(5),time(6))



%% blip up down
if(blipUpDown==1)
     
    %make sure we pick the correct interleave(=waveform) from w_imag
    if(hdr.rdb.user2 == 1)
        iv_ro = [1 2];
    else
        iv_ro = [1 4];
    end
    
    %split data 
    dataUp = data_imag(:,1,:,:,:,:);
    dataUp = dataUp.* repmat(exp(-sqrt(-1)*(w_imag.kx_all(w_imag.gx_start+1:w_imag.gx_end+1,iv_ro(1))  *  hdr.rdb.xoff -w_imag.ky_all(w_imag.gy_start+1:w_imag.gy_end+1,iv_ro(1))  *  hdr.rdb.yoff )),[1 1 size(data_imag,3) s(3:end)]);
    
    %blip down
    dataDown = data_imag(:,2,:,:,:,:);
    dataDown = dataDown.* repmat(exp(-sqrt(-1)*(w_imag.kx_all(w_imag.gx_start+1:w_imag.gx_end+1,iv_ro(2))  *  hdr.rdb.xoff -w_imag.ky_all(w_imag.gy_start+1:w_imag.gy_end+1,iv_ro(2))  *  hdr.rdb.yoff )),[1 1 size(data_imag,3) s(3:end)]);

    %w_imag.inplane_R = 2;
    w_imag.ni = w_imag.ni/w_imag.inplane_R;
    %w_imag.R_skip =2;
    opts.GC = GC; %pass ghost correction
    opts.coil = S; %pass coil sensitivites
    opts.doghost = 0;
    opts.dosense = 1;
    opts.full = 0;
    
    %reconstruct blip up data
    [rcnUp,~,~,dataUp2]=generic_recon_epi(dataUp,w_imag,opts);
    
    %tell epi recon that we invert y
    w_imag.reverse_readout = 1;
    opts.coil = S_down;
    [rcnDown,~,~,dataDown]=generic_recon_epi(dataDown,w_imag,opts);%do not reverse readout here. let hybrid-SENSE take care of that
    %dataDown = dataDown(:,end:-1:1,:,:,:,:,:);%undo reverse data ordering
    
    %save('data.mat','dataUp','dataDown','w_imag','S','S_down','-v7.3');
    
    %assign up recon as output
    rcn = rcnUp;
    
else    %normal recon
    
    shots = w_imag.ni/w_imag.inplane_R;
    
    %in-plane shifts
    data_imag = data_imag.* repmat(exp(-sqrt(-1)*(w_imag.kx_all(w_imag.gx_start+1:w_imag.gx_end+1,1:shots)  *  hdr.rdb.xoff -w_imag.ky_all(w_imag.gy_start+1:w_imag.gy_end+1,1:shots)  *  hdr.rdb.yoff )),[1 1 size(data_imag,3) s(3:end)]);
    
    %configure and run the actual reconstruction
    opts.GC = GC; %pass ghost correction
    opts.coil = S; %pass coil sensitivites
    opts.doghost = 0;
    opts.dosense = 1;
    opts.full = 0;
    opts.gmap = 0;
    w_imag.pompCalib = 0;
    w_imag.ni = w_imag.ni/w_imag.inplane_R;
    
    %remove empty interleaves for accelerated scan
    data_imag = data_imag(:,1:w_imag.inplane_R:end,:,:,:,:);
    
    %reconstruct all time frames
    if((S.Nc >16) && size(data_imag,3)>128)
        tframes = size(data_imag,3);
        [rcn(:,:,:,1:round(tframes/2)),~,~,data]=generic_recon_epi(data_imag(:,:,1:round(tframes/2),:,:,:),w_imag,opts);
        [rcn(:,:,:,round(tframes/2)+1:tframes),~,~,~]=generic_recon_epi(data_imag(:,:,round(tframes/2)+1:end,:,:,:),w_imag,opts);
    else
        [rcn,~,~,data]=generic_recon_epi(data_imag,w_imag,opts);
    end
    
end


    
    
%%  blipUpDownILV
    
%reconstruct 2nd blip down scan only
if((numB0 >1) && (blipUpDown==0))
    w_imag.reverse_readout = 1;
    data_down = data_imag(:,:,2,:,:,:);
    data_down=data_down.* repmat(exp(-sqrt(-1)*(+w_imag.ky_all(w_imag.gy_start+1:w_imag.gy_end+1,1:w_imag.inplane_R:end))*  2*hdr.rdb.yoff ),[1 1 size(data_down,3) size(data_down,4) size(data_down,5) size(data_down,6)]);
    if(singleBandRefDown)
       opts.coil = S_down; 
    end
    [rcn_b02,~,~,data]=generic_recon_epi(data_down,w_imag,opts);
    w_imag.reverse_readout = 0;
        
    %replace 2nd time frame
    rcn(:,:,:,2) = rcn_b02;   
end
    
clear data_imag

time=clock;
fprintf('%.2d %.2d %.2d: Reconstruction complete. Writing output files...\n', time(4), time(5),time(6));

%% write volume niftis
    if(blipUpDown)
        generic_write_nii(abs(rcnUp),hdr,w_imag,'blipUp');
        generic_write_nii(abs(rcnDown),hdr,w_imag,'blipDown');
    else
        generic_write_nii(abs(rcn),hdr,w_imag,'nii');
    end


% If Write dicoms and spm files if requested
if isfield(opts, 'verbose') && opts.verbose==0
else
    time=clock;
    fprintf('%.2d %.2d %.2d: Reconstruction complete. Writing output files...\n', time(4), time(5),time(6));
    
    % the number of image volumes generated might be different from the one
    % that was given by numel(opts.ts). One way this can happen is with grappa,
    % where one file can be generated from each interleaf inside a volume
    nd_new=size(rcn,4);

    %rcntmp=permute(rcn,[1 2 3 5 4 6]);
    %rcntmp=reshape(rcntmp,size(rcntmp,1), size(rcntmp,2), size(rcntmp,3)*size(rcntmp,4),size(rcntmp,5),size(rcntmp,6));


    % writing dicoms and spm
    % important points here are:
    % 1) the scaling has to be accurate and consistent
    % 2) the naming has to be accurate
    % normally, satisfying these two points is simple but consider the case of
    % parallel reconstruction of multiple volumes on a multi-core cpu. Then, we
    % have to take into account opts.ts for naming and use a fixed scale
    % factor, which is, sadly, still an open problem...
    
    %since we do a SENSE reconstruction we just take the magnitude here
    %rcn_sos=sqrt(sum(abs(rcntmp).^2,5)); clear rcntmp

    rcn_sos = reshape(abs(rcn),[w_imag.nx w_imag.ny w_imag.slquant numel(opts.ts)]);
    if(size(rcn_sos,3)==1)
        %rcn_sos = smooth4D(abs(rcn_sos),[3 3 3],0.5);
    else
        rcn_sos = smooth4D(abs(rcn_sos),[3 3 3],0.25);
    end
    
    %no upsampling to 256x256
    upsampleDicoms = true;
    if(upsampleDicoms)
        if size(rcn_sos,1) < 256
            ss=size(rcn_sos);
            scalefact=256/ss(1);
            tmp=reshape(rcn_sos,ss(1),ss(2),prod(ss(3:end)));
            rcn_sos_2=zeros(256,2*ceil(ss(2)*scalefact/2),prod(ss(3:end)), 'single');
            for i=1:prod(ss(3:end))
                rcn_sos_2(:,:,i)=imresize(tmp(:,:,i),[256 size(rcn_sos_2,2)]);
            end
            rcn_sos_2=reshape(rcn_sos_2,[size(rcn_sos_2,1) size(rcn_sos_2,2) ss(3:end)]);
            %rcn_sos_2=smooth4D(abs(rcn_sos_2),[3 3 3],0.5);
        else
            %rcn_sos_2 = smooth4D(rcn_sos,[3 3 3],0.5);
        end
    else
        %rcn_sos = smooth4D(rcn_sos,[3 3 3],0.5);
    end

    if ~isempty(opts.outpath)
        mkdir dcm
        opts.outpath = [opts.outpath,'/dcm'];
        %rcn_sos = abs(squeeze(rcn));
        for i=1:numel(opts.ts)
            %read the comments above for the naming convention
            % assume that the reconstructed number of volumes is an integer 
            % multiple of the number of volumes is opts.ts. This is true in all
            % cases
            for j=1:nd_new/numel(opts.ts)
                volnumind=(i-1)*nd_new/numel(opts.ts)+j;
                volnumname=(opts.ts(i)-1)*nd_new/numel(opts.ts)+j;
                prefix=generic_pathfileprefix(hdr,'volnum',volnumname);
                P(volnumind,:)=generic_write_vol(permute(abs(rcn_sos_2(:,end:-1:1,:,volnumind)),[2 1 3 4]),hdr,'outpath',opts.outpath,'prefix',prefix,'imnooffset',(opts.ts(i)-1)*nsl,'dcm',0);       
            end
        end
        
        %try and see if we have a gmap
        try
            load([opts.outpath,'/../gmap.mat'])
            %generic_write_vol(permute(gmap(:,end:-1:1,:),[2 1 3 4]),hdr,'outpath',opts.outpath,'prefix','gmap','imnooffset',0,'dcm',0); 
        catch
        end
        

        
    end
    %clear rcn_sos
    
end


time=clock;
fprintf('%.2d %.2d %.2d: Reconstruction complete.\n', time(4), time(5), time(6))

end


function data=applyInplaneShifts(data,xoff,yoff,w,ypolarity)

    [nk, ni, nt, n, nsl, nc] = size(data);
    
    theta = exp(-sqrt(-1)*(w.kx_all(w.gx_start+1:w.gx_end+1,1:ni)*xoff  -ypolarity*w.ky_all(w.gy_start+1:w.gy_end+1,1:ni)*yoff ));
    data = data.*repmat(theta,[1 1 nt n nsl nc]);

end


%% rcn=generic_recon_2(data,w,opts)
%
% rcn is [nx ny nz nd nsl nc]
% discriminating between 2D and 3D acquisitions will help in some cases
function [rcn, rcn2]=generic_recon_2(data,w,opts)

nd=size(data,3);
nsl=size(data,5);
nc=size(data,6);

    

rcn2=[]; %empty by default

if opts.dosense && isempty(opts.coil) && (opts.full == 0)
    error('You must provide navigator images if you want to do SENSE');
end



switch w.ro_type
        
    case 0
        %cartesian 2d        
        rcn=zeros(w.nx,w.ny,1,nd,nsl,nc);
        for c=1:nc
            for sl=opts.sls
                for t=1:nd
                    if w.fn==1.0 && w.fn2==1.0
                        %fully sampled 2d matches epi orientation
                        temp=ifftnc(data(:,:,t,1,sl,c));
                        rcn(:,:,1,t,sl,c)=rcn_zpcrop(single(temp),single([w.nx w.ny]));
                        
                    elseif w.fn==1.0
                        opts2=opts;
                        opts2.sls=1;
                        %partial fourier, forcing pocs
                        temp=generic_recon_pocs(data(:,:,t,1,sl,c),w.nx,opts2);
                        temp=ifftnc(temp);
                        rcn(:,:,1,t,sl,c)=rcn_zpcrop_mex(single(temp),single([w.nx w.ny]));   
                    elseif w.fn2==1.0
                        opts2=opts;
                        opts2.sls=1;
                        %partial echo, forcing pocs
                        temp=generic_recon_pocs(data(:,:,t,1,sl,c),w.ny,opts2);
                        temp=ifftnc(temp);
                        rcn(:,:,1,t,sl,c)=rcn_zpcrop_mex(single(temp),single([w.nx w.ny]));
                    end
                end
            end
        end
        
        %get and save sensitivity information
        S=CoilSensitivity(squeeze(rcn(:,:,1,1,:,:)));
        S.fov = [10*w.fovx, 10*w.fovy, 3*nsl]; %fov in [mm]
        save('../Sgre.mat','S');               
        
    case 1
        %cartesian 3d        
        rcn=zeros(w.nx,w.ny,w.nz,nd,nsl,nc);
        data=reshape(data,[size(data,1) w.ny w.nz nd 1 nsl nc]);
        for c=1:nc
            for sl=opts.sls
                for t=1:nd
                    temp=ifftnc(data(:,:,:,t,1,sl,c));                    
                    rcn(:,:,:,t,sl,c)=rcn_zpcrop_mex(single(temp),single([w.nx w.ny w.nz]));
                end
            end
        end
        
        
    case 2
        if (strcmp(opts.flavor,'sense'))
            rcn=generic_recon_epi(data,w,opts);
        else
            rcn = generic_recon_CG(data,w,opts);
        end
    case {3,4,5,6}
        
        %move this to generic_recon_spiral.m
        if ~isempty(w.kx_all)
            kx=w.kx_all(w.gx_start+1:w.gx_start+size(data,1),:);
        end
        if ~isempty(w.ky_all)
            ky=w.ky_all(w.gy_start+1:w.gy_start+size(data,1),:);
        end
        %if ~isempty(w.kz_all)
        %    kz=w.kz_all(w.gz_start+1:w.gz_start+size(data,1),:);
        %end
        
        %%normal spiral
        kxmax=2.0*pi*w.nx/w.fovx*0.1/2.0; %[rad/mm]
        kx=w.kx_all(w.gx_start+1:w.gx_start+size(data,1),:)/kxmax*pi;
        ky=w.ky_all(w.gy_start+1:w.gy_start+size(data,1),:)/kxmax*pi;
        n=1;
        for d=0;
        kx2 = interp1(1:size(kx,1),kx,(1:size(kx,1))+d,'spline'); 
        ky2 = interp1(1:size(kx,1),ky,(1:size(kx,1))+d,'spline'); 
        
        K(:,1) = kx2(:);
        K(:,2) = ky2(:);
        A=nuFTOperator(K,[w.nx w.ny],[],2);
        
        %2D spiral
        dens=CalcDensComp(kx(:)*w.fovx*10/2/pi,ky(:)*w.fovy*10/2/pi);
        rcn=zeros([w.nx,w.ny,1,nd,nsl,nc], 'single');
        for t=1:nd
            for sl=opts.sls
                for c=1:nc                    
                    %rcn(:,:,1,t,sl,c)=rcn_EHEx_mex(single([kx(:)*w.fovx ky(:)*w.fovy]*10/2/pi),single(vec(data(:,:,t,1,sl,c))),single([w.nx w.ny]),'dens',single(dens(:)));
                    %reorienting
                    %rcn(:,:,1,t,sl,c)=flipdim(imrotate(rcn(:,:,1,t,sl,c),90),2);
                    
                    rcn(:,:,1,t,sl,c)=regularizedReconstruction(A,vec(data(1:end,:,1,1,1,c)),'maxit',30);
                end                
            end
        end
        recon = sos(squeeze(double(rcn)));
        recon = recon/max(recon(:));
        E(n) = entropy(recon);
        figure; imagesc(recon); colormap gray
        n=n+1;
        end
        
    case 7
        %propeller
        [rcn rcn2]=generic_recon_pepi(data,w,opts);
        
    case 8
        %RS        
        [rcn rcn2]=generic_recon_rsepi(data,w,opts);
    case 9
        %3d stack of epi
        rcn=generic_recon_CG_stackof(data,w,opts);
        
    case 10
        %spiral radial
        
    case 11
        %stack of spiral
        
    case 12
        %turbine        
        rcn=generic_recon_turbine(data,w);
    case 14
        %3drsepi        
        rcn=generic_recon_3drsepi(data,w);    
    case 15
        %vd-spiral with sinusoidal z-modulation
        rcn = generic_recon_vdspiral3D(data,w);
        
        % ADD_NEW_RO_TYPE
        
end

%combining z phase encode and slices to make rcn [nx ny nz*nsl nd nc]
%rcn=permute(rcn,[1 2 3 5 4 6]);
%rcn=reshape(rcn,size(rcn,1), size(rcn,2), size(rcn,3)*size(rcn,4),size(rcn,5),size(rcn,6));


end
 
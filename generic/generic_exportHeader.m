function generic_exportHeader(hdr,fname)
%exports p-file header parameters to text file

if nargin < 2
    fname = 'DPFG_parameters.txt';
end

dpfgMode ={'SSE','DSE','HSE','DTI'};

fid = fopen(fname,'w');

fprintf(fid,'# acquisition parameters # \n');
fprintf(fid,'tr =  %d \n',round(hdr.image.tr/1000));
fprintf(fid,'te =  %d \n',round(hdr.image.te/1000));
fprintf(fid,'Nslices =  %d \n',round(hdr.image.slquant));
fprintf(fid,'slthick =  %f \n',hdr.image.slthick);
fprintf(fid,'Nshots =  %d \n',round(hdr.rdb.user2));
fprintf(fid,'fov = [%d %d] \n',hdr.image.dfov*0.1,hdr.image.dfov_rect*0.1);
fprintf(fid,'res = [%d %d] \n',hdr.image.dfov*0.1/hdr.rdb.user3,hdr.image.dfov_rect*0.1/hdr.rdb.user4);

fprintf(fid,'# multi-band parameters # \n');
fprintf(fid,'mb =  %d \n',round(hdr.rdb.user29));
fprintf(fid,'caipi =  %d \n',round(hdr.rdb.user32));
fprintf(fid,'Ry =  %d \n',round(hdr.rdb.user48));
fprintf(fid,'PF =  %f \n',hdr.rdb.user34);

fprintf(fid,'# DWI parameters # \n');
fprintf(fid,'numB0 =  %d \n',round(hdr.rdb.user18));
fprintf(fid,'numDWI =  %d \n',round(hdr.rdb.user17));
fprintf(fid,'bval1 =  %d \n',round(hdr.rdb.user16));
fprintf(fid,'bval2 =  %d \n',round(hdr.rdb.user25));
fprintf(fid,'dte =  %d \n',round(hdr.rdb.user11));
tmp = dpfgMode{round(hdr.rdb.user22)+1};
fprintf(fid,'mode =  %s \n',tmp);
fprintf(fid,'gradfileID =  %d \n',round(hdr.rdb.user46));
fprintf(fid,'antiparallel = %d \n',round(hdr.rdb.user28));
fclose(fid);


%% function rcn=generic_recon_epi(data,w);
%
% reconstruct EPI data
%
% data : nk x ni x nd x necho x nsl x nc data array
%       ni = interleaves
%       nd = number of timepoints
%       necho = the number of echoes required to acquire a single EPI
%       train. Necessary due to sdf length limitations
%       nsl = number of slabs, possibly 1
%       nc = number of crazy pumpkins
% rcn : nx x ny x (nz=1) x nd x nsl x nc2
%       nd is the number of dynamic scans. If dograppa=1, each interleaf of
%       the epi  readout generates a separate image. In this case, 
%       nd = size(data,3)*w.ni and the interleaf is the inner index. 
%   
% optional parameters:
%   epi_ref
%   epi_ref_use_entropy
%   coil
%   dosense
%   outpath
%   refdata
%   

function [rcn, GC, S, data]=generic_recon_epi(data,w,varargin)

    opts_def.epi_ref=[];
    opts_def.dosense=0;
    opts_def.coil = [];
    opts_def.doghost = 1;
    opts_def.GC = []; 
    opts_def.gmap = 0;
    opts_def.outpath=[];
    opts_def.refdata=[];
    opts_def.nav=[];
    opts_def.dograppa=0; 
    opts_def.grappa=[];
    opts_def.phasecorr=0; 
    opts_def.pocs=1;
    opts_def.epi_ref_use_entropy=0;
    opts_def.epi_ref_entropy_per_slice=0;

    nd=size(data,3);
    nsl=size(data,5);
    nc=size(data,6);
    opts_def.sls=1:nsl;
    opts_def.ts=1:nd;

    opts=get_opts(varargin,opts_def);
    
    rcn=1;

    if opts.dosense && isempty(opts.coil) && opts.full ==0
        error('SENSE requires coil sensitivities as input');
    end


    % BZ slice reordering convention
    slcIdx=getSliceIndex(w.slquant,w.epi_multiband_factor);


    % reordering data 
    %after re-ordering the data is of the form:
    %data=[nptsperline nlines ni nz2 nframes nslices ncoils] = 7-dimensional
    [data, Kidx, Kidx_odd] = generic_recon_epi_reorder_data(data,w,opts);
    

    %rampsampling correction or taking the flat part only 
    data = generic_recon_epi_rampsamp_corr(data, w, opts);
    %after the gridding account for new read direction length
    Kidx=Kidx(1:w.nx,:,:);
    Kidx_odd = Kidx_odd(1:w.nx,:,:);
    
    %account for pomp k-space sampling [-kmax 0] compared to [-dk dk] for
    %blips (this seems to be true only for mb=2 !!!) ????
    if(w.pompCalib && (mod(w.epi_multiband_factor,2)==0))
        %do multiband slice shift
        [~, ~, Zramp]=ndgrid(ones(size(data,1),1),ones(size(data,2),1),linspace(0,1,size(data,3)));
        theta = exp(1i*Zramp*pi/2); %works for mb=2
        %theta = exp(1i*Zramp*pi/2); does it really work for mb=4
        theta = repmat(theta,[1 1 1 size(data,4) size(data,5) size(data,6) size(data,7)]);
        data = data.*theta;
    end
    
    %ghost correction in case of fully sampled data
    if opts.doghost

        GC=ghostCorrection([size(data,1) size(data,2) size(data,3)],Kidx_odd);
        GC.Nsl = size(data,6);
        GC.coilMode = 'coil';
        GC.sliceMode = 'central'; %'central'|slice-by-slice
        GC.debug = 0;
        %let ghost correction handle sum-of-squares combination
        dataTmp = reshape(data(:,:,:,1,1,ceil(w.slquant_eff/2),:),[size(data,1) size(data,2) size(data,3) size(data,7)]);
        
        %get inital values for fminsearch
        GC=GC.getStartParameters(dataTmp);

        GC=GC.doGhostCorrection(reshape(data(:,:,:,1,1,:,:),[size(data,1) size(data,2) size(data,3) size(data,6) size(data,7)]));
        
        GC = GC.interpolateGhostParams();
    
        cd(opts.outpath)
        if((w.pompCalib || w.golden_angle) && (w.epi_multiband_factor > 1))
            GC.isMultiband = true;
            save('GC_pomp.mat','GC')
        else
            save('GC.mat','GC')
        end
    else
        GC = opts.GC;
    end
    
    %make sure the ghost correction matches the actual data
    data = GC.applyPhaseCorrection(data,Kidx_odd);
      
    
    %flip read so that senseNd_operator convention matches epi recon!
    data = data(end:-1:1,:,:,:,:,:,:);
    

    % grappa
    if opts.dograppa
        data = generic_epi_grappa(data, w, opts);
    end
    
    
    % phase correction
    if opts.phasecorr
        warning('phase correction for epi not implemented yet');    
    end
    
    
    %partial fourier along z
    if w.fn2<1
        error('partial fourier along the RO dimension not implemented for EPI');
    end
    
    
    %partial fourier & sense matching
    [data,w] = generic_recon_epi_zerofill(data, w, opts);
    %if w.fn ~=1
        if opts.pocs
            data = generic_recon_pocs(data, w.ny, opts);
        else
            %oldLines = size(data,2);
            %[data,w] = generic_recon_epi_zerofill(data, w, opts);
            %newLines = size(data,2);
            
            Kidx = w.Kidx;
        end
    %end
    Kidx = w.Kidx;
    
    %temporary fix
    %KidxFromData = abs(data(:,:,1,1,1,1,1))>0;
    [a, centralLine]=find(abs(data(round(w.nx/2),:,1,1,1,1,1))>0,1,'first');
    while(Kidx(1,centralLine)==0)
       Kidx = circshift(Kidx,[0 1 0]);
    end
    
    %filter k-space to avoid ringing (only 2d)
    k_filter = generic_make_mask([w.nx w.ny_sense],'type','arctan');
    k_filter = repmat(k_filter,[1 1 size(data,3) size(data,4) size(data,5) size(data,6) size(data,7)]);
    data = data.*k_filter;
   
    
    % full recon if no sensitivites are provied
    if isempty(opts.coil)
        
        if(w.reverse_readout)
            display('reversing readout in generic_recon_epi_nyu')
            %overwrite senseOperator
            data = data(:,end:-1:1,:,:,:,:,:);
            Kidx = Kidx(:,end:-1:1,:);
        end                     
        
        %low resolution sensitivity maps
        if(w.nx > 64)
            Nfull(1) = 64;
            centerKx = ceil(w.nx/2+1)-32:ceil(w.nx/2+1)+31;
        else
            Nfull(1) = w.nx;
            centerKx = 1:w.nx;
        end
        if(w.ny > 64)
            Nfull(2) = 64;
            centerKy = ceil(w.ny/2+1)-32:ceil(w.ny/2+1)+31;
        else
            Nfull(2) = w.ny;
            centerKy = 1:w.ny;
        end
        recon = zeros([Nfull(1) Nfull(2) w.slquant size(data,7)]);
 
        for sl=1:size(data,6)
            for c=1:size(data,7)
                recon(:,:,slcIdx+sl-1,c) = ifff3d(data(centerKx,centerKy,:,1,1,sl,c));
            end
        end
        
        S=CoilSensitivities(single(recon),'espirit');
        %S=CoilSensitivities(single(recon),'adaptive');
        S.fov = [10*w.fovx 10*w.fovy w.slquant*w.slthick];
        S.type = 'epi';
        S.id = -1;
        
  
    else
        S = opts.coil; 
    end
    
    
    %only do this if flag was set
    if opts.dosense
    
        
        %get reference scan and adjust it in case of zero-filling, sense adjustements, etc
        % set up SENSE reconstruction
        S = opts.coil;
        S = S.resize([w.nx w.ny_sense w.slquant],[10*w.fovx 10*w.fovy S.fov(3)],'spline');
        S = S.applyMask();
        %B(1)=senseND_operator(S.getSubset(slcIdx),Kidx);
        NdummySlices = w.nmb_sense-w.epi_multiband_factor;
        
        if(w.reverse_readout)
            display('reversing readout in generic_recon_epi_nyu')
            data = data(:,end:-1:1,:,:,:,:,:);
            Kidx = Kidx(:,end:-1:1,:);
        end
        A = senseOperator(S.getSubset(slcIdx,NdummySlices),Kidx);
        
        if opts.gmap
            A.debug = true;           
            for sl=1:size(data,6)
                if(sl==1)
                    B(1)=senseOperator(S.getSubset(slcIdx+sl-1,NdummySlices),Kidx);
                    B(1).debug = true;
                end
                C = B(1).updateCoilSensitivities(S.getSubset(slcIdx+sl-1,NdummySlices),1);
                tmp = C.getgmap;
                gmap(:,:,slcIdx+sl-1)= tmp(:,:,NdummySlices+1:end);
              
            end
            gmap = imresize3D(gmap,[w.nx w.ny w.slquant]);
            save('gmap.mat','gmap')
            save('S.mat','S'); 
           
        end
        
        display('reconstructing all slices and time frames')
        tic
        rcn = single(zeros([w.nx w.ny_sense w.slquant size(data,5) 1])); 
        for sl=1:size(data,6)
            A = A.updateCoilSensitivities(S.getSubset(slcIdx+sl-1,NdummySlices),0);
            if (A.dim == 2)
                rcn(:,:,slcIdx+sl-1,:,1) = A'*permute(squeeze(data(:,:,1,1,:,sl,:)),[1 2 4 3]);
            else
                tmp = A'*permute(squeeze(data(:,:,:,1,:,sl,:)),[1 2 3 5 4]);
                rcn(:,:,slcIdx+sl-1,:,1) = tmp(:,:,NdummySlices+1:end,:);
            end
        end
        toc
        
        %re-sampling in case of zero filling
        if(w.ny_sense > w.ny)
            %rcn = rcn(:,3:102,:,:);
            rcn = imresize4D(rcn,[w.nx w.ny size(rcn,3)]); %phase discontinuities cause this step to fail
            %either fix phase or reshape magnitude images only
            
        end
        
        %end
    else %end of dosense
        try
            rcn = recon;
        catch
            rcn = [];
        end
    end



end
%End of main function


%% reordering data 
function [data_epi, Kidx, Kidx_odd]=generic_recon_epi_reorder_data(data,w,opts)

    nd=size(data,3);
    nsl=size(data,5);
    nc=size(data,6);
    ni=size(data,2);
    nframes = size(data,3);
    nptsperline=2*w.epi_ramp_res + w.epi_flat_res;
    nlines=(w.gx_end-w.gx_start+1)/nptsperline;
    
    %nlines in w structure is even
    ny_size = nlines*(w.R_skip); 

    data_epi = data;

    %echo index is faster since it divides the data in a single TR
    data_epi=permute(data_epi,[1 4 2 3 5 6]);
    %making data size nx x ny1 x ny2 x 1 x nd x nsl x nc 
    %by dividing each acquisition into individual lines
    data_epi=reshape(data_epi,nptsperline,nlines,ni,nd,1,nsl,nc);
    %reverting every other line
    data_epi(:,1:2:end,:,:,:,:,:)=data_epi(end:-1:1,1:2:end,:,:,:,:,:);

    % sort into 3D multi-band k-space if mb_factor >1
    %data = zeros(size(data_epi,1),ny_size,w.epi_multiband_factor,1,nframes,nsl,nc);
    
    zmax = w.epi_multiband_factor;
    zinc = ceil(w.epi_multiband_R);
    
    if(w.pompCalib)
        mode = 'pomp';
    else
        mode = 'blipped';
    end
    
    [data, Kidx2d, Kidx2d_odd]=generic_reorder_base(data_epi,ny_size,w.R_skip,w.epi_multiband_factor,zinc,w.ni,mode);
    
    
    data_epi  = data;
    
    Kidx2d_odd = sum(Kidx2d_odd,3);
    Kidx2d = sum(Kidx2d,3);
    Kidx_odd = logical(repmat(reshape(Kidx2d_odd,[1 ny_size zmax]),[size(data_epi,1) 1 1]));
    Kidx = logical(repmat(reshape(Kidx2d,[1 ny_size zmax]),[size(data_epi,1) 1 1]));

end



%% ramp sampling correction
function data=generic_recon_epi_rampsamp_corr(data,w,opts)

    nptsperline=2*w.epi_ramp_res + w.epi_flat_res;

    if w.epi_rampsamp == 0
        data=data(w.epi_ramp_res+1:w.epi_ramp_res +w.epi_flat_res,:,:,:,:,:,:);
    elseif w.epi_rampsamp==1
        ind_k=(w.gx_start+1+w.epi_blip_res/2):(w.gx_start+nptsperline);
        ind_data=(1+w.epi_blip_res/2):(nptsperline);
        data=interp1(w.kx(ind_k),...
            data(ind_data,:,:,:,:,:,:),....
            (-w.nx/2:w.nx/2-1)*2*pi/w.fovx/10,...
            'linear',...
            0);
    end

end

%% zero filling for partial fourier & SENSE and multi-band acceleration
function [data2,w]=generic_recon_epi_zerofill(data,w,opts)

    nlines_per_shot=(w.gx_end-w.gx_start+1)/(2*w.epi_ramp_res+w.epi_flat_res);
    nlines_total = nlines_per_shot*w.R_skip;
    %how many lines next to k=0
    nover=(nlines_total)-w.ny/2;
    data2=zeros([size(data,1),w.ny,w.epi_multiband_factor,size(data,4),size(data,5),size(data,6),size(data,7)], 'single');
    data2(:,w.ny/2-nover+1:end,:,:,:,:,:)=data;

    %check for mb-acceleration
    zInc = ceil(w.epi_multiband_R);
    w.nmb_sense = w.epi_multiband_factor;
    if(w.epi_multiband_factor >1)
        if(zInc ~= w.epi_multiband_R)
            for l=1:w.epi_multiband_factor
                w.nmb_sense = w.epi_multiband_factor + l;
                w.nmbR_sense = zInc;
                if (mod(w.nmb_sense,w.nmbR_sense) ==0)
                    break;
                end
            end
        end
    end
    
    %check for in-plane acceleration
    w.ny_sense = w.ny;
    effRy = (w.nmb_sense/w.epi_multiband_R)*w.inplane_R;
    if(mod(w.ny,effRy) ~= 0)
        for l=1:w.nmb_sense
            if (mod(w.ny+l,effRy) ==0)
                w.ny_sense = w.ny + l;
                break;
            end
        end
    end
    
    if(w.pompCalib)
        mode = 'pomp';
    else
        mode = 'blipped';
    end
    
    [~, Kidx2d, Kidx2d_odd]=generic_reorder_base([],w.ny_sense,w.R_skip,w.nmb_sense,zInc,w.ni,mode);


    data2(:,w.ny+1:w.ny_sense,w.epi_multiband_factor+1:w.nmb_sense,:,:,:,:)=0;
    w.Kidx2d_odd = sum(Kidx2d_odd,3);
    w.Kidx2d = sum(Kidx2d,3);
    w.Kidx_odd = logical(repmat(reshape(Kidx2d_odd,[1 w.ny_sense w.nmb_sense]),[size(data2,1) 1 1]));
    w.Kidx = logical(repmat(reshape(Kidx2d,[1 w.ny_sense w.nmb_sense]),[size(data2,1) 1 1]));
end


%% k-space re-ordering 
function [data2, Kidx2d, Kidx2d_odd]=generic_reorder_base(data,ny,yInc,nz,zInc,ni,mode)
%core re-ordering function called by generic_recon_epi_reorder_data() &
%generic_recon_epi_zerofill(). In case of zero filling no actual data
%re-ordering occurs

    %mode = 'blipped','pomp'

    if(isempty(data))
        fillData = false;
    else
        fillData = true;
        
    end

    Kidx2d = zeros(ny,nz);
    Kidx2d_odd = zeros(ny,nz);
    
    % sort into 3D multi-band k-space if mb_factor >1 data in = [nx ny nz
    % ni nt nsl nc]
    data2 = zeros(size(data,1),ny,nz,1,size(data,4),size(data,6),size(data,7));
    
    nframes = size(data,4);
    %zinc = ceil(w.epi_multiband_R);
    
    %this part should match reorder_data for blipped-EPI
    yi=1;
    z=1;
    for i=1:ni %interleaves can be in-plane and multiband slice encoding
        if(strcmp(mode,'blipped'))
            z=1; %reset z index only in case of blipped EPI
        end
        for y=1:ny
            y2 = yInc*(y-1) + yi;
            if (y2>ny)
                break;
            end
            Kidx2d(y2,z,i) = 1;
            Kidx2d_odd(y2,z,i) = mod(y+1,2);
            if fillData
                for t=1:nframes
                    data2(:,y2,z,1,t,:,:) = data(:,y,i,t,1,:,:);
                end    
            end
            if(strcmp(mode,'blipped')) %normal z-blip increment
                z=z+zInc;
                if z>nz
                    z=1; %reset blip counter
                end
            end
        end
        yi=yi+(yInc/ni); %increase interleave counter for blipped and pomp cases
        if strcmp(mode,'pomp') %z-encoding outside y-blip loop
            if yi>yInc; %prescribed as interleaves (slice loop AFTER in-plane loop)
                yi=1; 
                z=z+1;
            end
        end
    end
    
    %get rid of interleaves
    Kidx2d_odd = sum(Kidx2d_odd,3);
    Kidx2d = sum(Kidx2d,3);

end

%% ghost correction
function data=generic_epi_ghost_corr(data_epi,w,opts)

% if epi_ref_use_entropy is set, entropy based correction is used.
% if there's no refscan, entropy based ghost correction is used by default
% based ghost correction by default.
%
% if entropy based correction is used, epi_ref (the filename) is checked
% and used

s = size(data_epi);
nsl = size(data_epi,5);
nc = size(data_epi,6);
nd = size(data_epi,3);
nlines = size(data_epi,2);
nptsperline=size(data_epi,1);



if ~isempty(opts.refdata) && opts.epi_ref_use_entropy==0
    
    disp('using refscan');
    
    % there's refdata
    % the refdata comes from a short EPI scan right after the first 180
    % pulse
    
    
    % reordering data
    % from nk x (ni x nd) x 1 x nsl x nc to 
    % nptsperline x nlines(=6) x (ni x nd) x 1 x nsl x nc
    % I want to treat each interleave as a separate acquisition
    % this will make it easier to process the data since it will be
    % ordered
    
    w_tmp=opts.w_epi_ref;
    w_tmp.ni=1;
    refdata2=generic_recon_epi_reorder_data(opts.refdata,w_tmp,opts);    
    
    
 
    
    
    %refdata2 is nptsperline x (nlines=6) x (ni x nd) x 1 x nsl x nc        
    %fft_up=100;    
    refdata2=ifftnc(refdata2,1);
    %summing up to increase SNR
    tmp=zeros([nptsperline 2 w.ni*nd 1 nsl nc]);     
    %odd lines
    tmp(:,1,:,:,:,:)=sum(refdata2(:,1:2:end,:,:,:,:),2);   
    
    %even lines
    tmp(:,2,:,:,:,:)=sum(refdata2(:,2:2:end,:,:,:,:),2);    
    
    %reshaping to separate out the interleaves
    tmp=reshape(tmp,nptsperline,2*w.ni,nd,1,nsl,nc); 
    %ileaf_1_odd ileaf1_even ileaf2_odd ileaf2_even...
    
    %normalizing everything to first line of first interleaf
    %tmp=repmat(tmp(:,1,:,:,:,:),[1 2*w.ni 1 1 1 1]).*conj(tmp);
    
    %even odd mismatch
    tmp=reshape(tmp,nptsperline,2,w.ni,nd,1,nsl,nc);
    
    tmp=repmat(tmp(:,1,:,:,:,:,:),[1 2 1 1 1 1 1]).*conj(tmp);
    
    % the interleaf comes before the even-odd index
    tmp=reshape(tmp,nptsperline,2,w.ni,nd,1,nsl,nc); 
    tmp=permute(tmp,[1 3 2 4 5 6 7]);
    tmp=reshape(tmp,nptsperline,w.ni*2,nd,1,nsl,nc);
    
 
    %fitting a line
    disp('fitting line to phase');
    tmpmask=abs(tmp) > mean(abs(tmp(:))); 
    P=zeros(2,w.ni,nd,nsl,nc);
    for linei=(w.ni+1):w.ni*2
        for dyni=1:nd
            for sl=1:nsl
                for c=1:nc
                    tmp2=angle(tmp(:,linei,dyni,1,sl,c));
                    ind=find(tmpmask(:,linei,dyni,1,sl,c));
                    ptmp=polyfit(ind,tmp2(ind),1);
                    P(:,linei-w.ni,dyni,sl,c)=ptmp;
                    linphase=((1:nptsperline)*ptmp(1)+ptmp(2));
                    tmp(:,linei     ,dyni,1,sl,c)=exp(sqrt(-1)*linphase/2);
                    tmp(:,linei-w.ni,dyni,1,sl,c)=exp(-sqrt(-1)*linphase/2);                
                end
            end
        end
    end
    
    
    %the second index still goes ileaf_1_odd ileaf1_even ileaf2_odd ileaf2_even...
    %augmenting it to nlines numbers
    tmp=repmat(tmp,[1 nlines/2/w.ni 1 1 1 1]);
    
    
    phase=exp(sqrt(-1)*angle(tmp));
    
    %dirty hack to use a single phase
    %phase=repmat(phase(:,:,1,:,round(nsl/2),:),[1 1 nd 1 nsl 1]);
    
    clear tmp;   
    
else   
    
    % reference data file given (always absolute reference)
    if ~isempty(opts.epi_ref) && ...
            ischar(opts.epi_ref) && ...
            exist([opts.epi_ref '.mat'],'file')
        

        disp(['ghost correction file ' [opts.epi_ref '.mat'] ' being used as input']);

        ref=load(opts.epi_ref,'ref');
        ref=ref.ref; % ref is size 3x1
        
        
        
        if (size(ref,1) ==3 && size(ref,2) == 1) || ...
           (size(ref,1) ==1 && size(ref,2) == 3)
            phase=generic_epi_ghost_corr_ref2phase(ref,nptsperline,nlines,w.ni);
            phase= repmat(exp(1i*phase),[1 1 nd 1 nsl nc]);
            disp(['ghost correction single ref used']);
        elseif size(ref,1) == 3 && ...
               size(ref,2) == nd && ...
               size(ref,3) == nsl
            phase=zeros(nptsperline,nlines,nd,1,nsl);
            for dyni=1:nd
                for sl=1:nsl                    
                    phase(:,:,dyni,1,sl)=generic_epi_ghost_corr_ref2phase(ref(:,dyni,sl),size(data_epi,1),size(data_epi,2),w.ni);                
                end
            end
            phase= repmat(exp(1i*phase),[1 1 1 1 1 nc]);
            disp(['ghost correction one ref for each slice used']);
        else
            error('ref size is invalid');
        end
        
        
        %ref size is nptsperline x nlines x (nd or 1) x (nsl or 1) x (nc or 1)
        
        
    elseif ~isempty(opts.epi_ref) && ...
            isnumeric(opts.epi_ref)
        % ghost correction parameters are given directly
        ref=opts.epi_ref; %ref is 3x1 or 3 x nd x nsl if epi_ref_entropy_per_slice is 1
        
        disp(['ghost correction parameters ' opts.epi_ref 'being used as input']);
        if (size(ref,1) ==3 && size(ref,2) == 1) || ...
           (size(ref,1) ==1 && size(ref,2) == 3)
            disp(['ghost correction single ref used']);
            phase=generic_epi_ghost_corr_ref2phase(ref,nptsperline,nlines,w.ni);
            phase= repmat(exp(1i*phase),[1 1 nd 1 nsl nc]);
        elseif size(ref,1) == 3 && ...
               size(ref,2) == nd && ...
               size(ref,3) == nsl
            disp(['ghost correction one ref for each slice used']);
            phase=zeros(nptsperline,nlines,nd,1,nsl);
            for dyni=1:nd
                for sl=1:nsl                    
                    phase(:,:,dyni,1,sl)=generic_epi_ghost_corr_ref2phase(ref(:,dyni,sl),size(data_epi,1),size(data_epi,2),w.ni);                
                end
            end
            phase= repmat(exp(1i*phase),[1 1 1 1 1 nc]);
        else
            error('ref size is invalid');
        end
        
    elseif isempty(opts.epi_ref) || ...
            (~isempty(opts.epi_ref) && ...
            ischar(opts.epi_ref) && ...
            ~exist(opts.epi_ref,'file'))
        
        % no ref data or no file with the given ref data filename
        % calculate the ghost corr params and write to file
        
        disp(['using entropy based ghost correction'])
        
        % sanity check
        if w.R >1
            error('ghost calibration must be done on a fully sampled dataset');
        end

        if opts.epi_ref_entropy_per_slice==0
            disp(['ghost correction single ref used']);
            % choose first time point
            refdata=squeeze(data_epi(:,:,1,1,round(nsl/2),:));
            %epc wants the PE direction to be the first one.
            %refdata=permute(refdata,[2 1 3]);
            %refdata=ifftnc(ifftnc(refdata,1),2);
            %refdata=sumsq(refdata);
            %refdata=fftnc(fftnc(refdata,1),2);
            refparms.plotflag=0;
            refparms.fastguess=-1;
            ref=generic_epc(refdata,w.ni,refparms);
            phase=generic_epi_ghost_corr_ref2phase(ref,nptsperline,nlines,w.ni);
            phase= repmat(exp(1i*phase),[1 1 nd 1 nsl nc]);
        else
            disp(['ghost correction one ref for each slice used']);
            phase=zeros(nptsperline,nlines,nd,1,nsl);
            ref=zeros(3,nd,nsl);
            % choose first time point
            
            %epc wants the PE direction to be the first one.
            %refdata=permute(refdata,[2 1 3]);
            %refdata=ifftnc(ifftnc(refdata,1),2);
            %refdata=sumsq(refdata);
            %refdata=fftnc(fftnc(refdata,1),2);
            refparms.plotflag=0;
            refparms.fastguess=-1;
            for dyni=1:nd
                for sl=1:nsl
                    refdata=squeeze(data_epi(:,:,dyni,1,sl,:));
                    ref(:,dyni,sl)=generic_epc(refdata,w.ni,refparms);
                    phase(:,:,dyni,1,sl)=generic_epi_ghost_corr_ref2phase(ref(:,dyni,sl),size(data_epi,1),size(data_epi,2),w.ni);                
                end
            end
            phase=repmat(exp(1i*phase),[1 1 1 1 1 nc]);
            
        end
        
        %save the reference to file if a file is given
        if ~isempty(opts.epi_ref)
            disp(['saving ghost ref data to ' opts.epi_ref]);            
            save(opts.epi_ref,'ref');
        end
    end
    
    
    
    
end

data=fftnc(ifftnc(data_epi,1).*phase,1);

end 


%% grappa
function data=generic_epi_grappa(data_epi,w,opts)

%data_epi is size [nx ny nd (necho=1) nsl nc]
nsl=size(data_epi,5);
nc=size(data_epi,6);
nd=size(data_epi,3);
nx=size(data_epi,1);
ny=size(data_epi,2);



% grappa file given (always absolute reference)
if ~isempty(opts.grappa) && ...
   ischar(opts.grappa) && ...         
    exist(opts.grappa,'file')

    %save the grappa pars file if a file is given
    if ~isempty(opts.grappa)
        p=G_read_from_ascii(opts.grappa);
    end
    
% grappa correction parameters are given directly
elseif ~isempty(opts.grappa) && ...
        isnumeric(opts.grappa)
    p=opts.grappa;
% no grappa data or no file with the given grappa data filename
% calculate the grappa params and write to file   
elseif isempty(opts.grappa) || ...
        (~isempty(opts.grappa) && ...
        ischar(opts.grappa) && ...
        ~exist(opts.grappa,'file'))
    
    %use first time point
    cosfdata=permute(squeeze(data_epi(:,:,1,1,:,:)),[2 1 4 3]);
    p=G_pershot2(cosfdata,w.ni);
    clear cosfdata
        
    %save the grappa pars file if a file is given
    if ~isempty(opts.grappa)
        G_save_as_ascii(p,opts.grappa);
    end
    
    %if ~isempty(opts.grappa)
    %    save(opts.grappa,'p');        
    %end
end
    
%applying grappa weights
%data_epi is [nx ny nd (necho=1) nsl nc]

p.R_cv = w.R; 

data=reshape(data_epi,[nx ny nd nsl nc]);
clear data_epi
data=permute(data,[2 1 5 4 3]);
data=generic_G_pershot(data,p,0);

% 
% data=reshape(data_epi,[nx w.ni ny/w.ni nd nsl nc]);
% data=permute(data,[3 1 6 5 4 2]); 
% %data is size [ny/ni nx nc nsl ni nd]
% data2=zeros([ny nx nc nsl nd w.ni]);
% for i=1:w.ni
%     data2(1:w.ni:end,:,:,:,:,i)=data(:,:,:,:,:,i);
% end
% data2=reshape(data2,[ny nx nc nsl w.ni*nd]);
% data=Grappa(data2,p);

% reshape data

data=permute(data,[2 1 5 6 4 3]);

% final_ky = acquired ny * grappaR / undersampling_factor  -- Sjoerd's change
final_ky = ny * p.R / (p.R+1-w.R);
data=reshape(data,[nx final_ky w.ni*nd 1 nsl nc]);

end


%% Split up data if blip up and blip down was used in EPI readout
function [tmp_data tmp_w tmp_opts] = generic_epi_split_data(data, w, opts)

if w.epi_blip_up_down==1
    tmp_data{1} = data(:,1:w.ni/2,:,:,:,:);
    tmp_data{2} = data(:,w.ni/2+1:w.ni,:,:,:,:);
    
    w.ni = w.ni/2;
    tmp_w{1} = w;
    tmp_w{2} = w;    
else
    tmp_data{1} = data;
    tmp_w{1} = w;
end
clear data w

tmp_opts{1} = opts;
tmp_opts{2} = opts;
clear opts

permute_cv = sprintf('%.3i', tmp_w{1}.permute);


% Set epi_ref filenames separately for blip up and blip down
if ~isempty(tmp_opts{1}.epi_ref)
    [pathstr,name,ext] = fileparts(tmp_opts{1}.epi_ref);
    if strcmp(permute_cv(2), '1')
        tmp_opts{1}.epi_ref=fullfile(pathstr,[name '_pos_blip' ext]);
        tmp_opts{2}.epi_ref=fullfile(pathstr,[name '_neg_blip' ext]);
    else
        tmp_opts{1}.epi_ref=fullfile(pathstr,[name '_neg_blip' ext]);
        tmp_opts{2}.epi_ref=fullfile(pathstr,[name '_pos_blip' ext]);
    end
end

% Set grappa filenames separately for blip up and blip down
if ~isempty(tmp_opts{1}.grappa)
    [pathstr,name,ext] = fileparts(tmp_opts{1}.grappa);
    if strcmp(permute_cv(2), '1')
        tmp_opts{1}.grappa=fullfile(pathstr,[name '_pos_blip' ext]);
        tmp_opts{2}.grappa=fullfile(pathstr,[name '_neg_blip' ext]);
    else
        tmp_opts{1}.grappa=fullfile(pathstr,[name '_neg_blip' ext]);
        tmp_opts{2}.grappa=fullfile(pathstr,[name '_pos_blip' ext]);
    end
end
end
   

%% BZ multi band slice re-ordering
function slcIdx=getSliceIndex(Ntot,mb_factor,mode)

    if nargin < 3
        mode = 'sense';
    end

    if (mod(mb_factor,2)==1)
        %odd multiband factor
        sliceInc = Ntot/mb_factor;
        slcIdx = 1:sliceInc:Ntot;
    else
        sliceInc = Ntot/mb_factor;
        slcIdx = 1:sliceInc:Ntot;
        %if (strcmp(mode,'cg'))
        %    slcIdx = slcIdx(end:-1:1);
        %end
    end    
end
% mask=generic_make_mask(n,'im',im,'type',type,imthr,'imthr')
%
% types
%  none
%  arctan
%  fermi
%  hanning
%  raisedcosine
%  circular


function mask=generic_make_mask(n,varargin)

opts_def.im=[];
opts_def.type='none';
opts_def.imthr=0.5;

opts=get_opts(varargin,opts_def);

if numel(n)==1
    x=-n(1)/2:n(1)/2-1;
    r=x;
elseif numel(n)==2
    [x y]=ndgrid(-n(1)/2:n(1)/2-1,-n(2)/2:n(2)/2-1);
    r=sqrt((x/n(1)).^2+(y/n(2)).^2);
elseif numel(n)==3
    [x y z]=ndgrid(-n(1)/2:n(1)/2-1,...
        -n(2)/2:n(2)/2-1,...
        -n(3)/2:n(3)/2-1);
    r=sqrt((x/n(1)).^2+(y/n(2)).^2+(z/n(3)).^2);
end

switch opts.type
    case 'none'
        % no mask
        mask=ones(n);
        
    case 'arctan'
        % arctan      
        norm_diameter = 2*r;        
        arg  = ((1.0-8.0/n(1)) - norm_diameter) * 0.2 * n(1);
        mask = (pi/2+atan(arg))/pi;
        mask = mask .* (norm_diameter < 0.98);
        
    case 'fermi'
        %fermi
        mask=1.0./(1.0+5.0*exp(r*5));
        
    case 'hanning'
        %hanning
        if numel(n)==1
            mask=reshape(hanning(n(1)),n(1));        
        elseif numel(n)==2
            mask=repmat(reshape(hanning(n(1)),[n(1) 1]),[1 n(2)]).*...
                 repmat(reshape(hanning(n(2)),[1 n(2)]),[n(1) 1]);                    
        elseif numel(n)>=3
            mask=repmat(reshape(hanning(n(1)),[n(1) 1 1]),[1 n(2) n(3)]).*...
                 repmat(reshape(hanning(n(2)),[1 n(2) 1]),[n(1) 1 n(3)]).*...
                 repmat(reshape(hanning(n(3)),[1 1 n(3)]),[n(1) n(2) 1]);
        end
             
    case 'raisedcosine'
        % raised cosine
        tr1=0.8;
        tr2=1.0;
        norm_diameter = 2*r;
        mask=zeros(n);
        mask(norm_diameter <= tr1)=1.0;
        ind=find((norm_diameter > tr1) & (norm_diameter < tr2));
        mask(ind)=cos(pi*(norm_diameter(ind)-tr1)/(tr2-tr1) )/2.0+0.5;
        
    case 'circular'
        %circular        
        mask=(2*r < 1.0);
        
    otherwise
        error('unknown filter type');
        
        
        
end

 % now masking according to the image
 if ~isempty(opts.im)
     [mask_lb num]=bwlabeln(abs(opts.im.*mask) > opts.imthr*sum(abs(opts.im(:).*mask(:)))./sum(mask(:)));
     s=regionprops(mask_lb,'Area');
     areas=cat(1,s.Area);
     [dum maxAreaInd]=max(areas);
     mask2=(mask_lb == maxAreaInd);
 else
     mask2=mask;
 end
 
 mask=mask2;

            
end

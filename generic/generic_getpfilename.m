% [pfilename opts_start]=generic_getpfilename(varargin_cell_arr)
%
% look at the optional arguments and get the pfilename if it exists

function [pfilename, opts_start]=generic_getpfilename(varargin_cell_arr)

if ~exist('varargin_cell_arr','var') || numel(varargin_cell_arr)==0
    pfilename=dir('P*.7');
    if isempty(pfilename)
        pfilename=[];
        return;
    end
    pfilename=pfilename(1).name;
    opts_start=1;
elseif isnumeric(varargin_cell_arr{1})
    pfilename=['P' num2str(varargin_cell_arr{1}) '.7'];
    opts_start=2;
elseif ischar(varargin_cell_arr{1})    
    pfilename=varargin_cell_arr{1};
    if ~isempty(str2num(pfilename))
        pfilename=['P' pfilename '.7'];
        opts_start=2;
    elseif ~isempty(regexp(pfilename,'P\d\d\d\d\d.7', 'once'))
        opts_start=2;
    else
        pfilename=dir('P*.7');
        if isempty(pfilename)
            error('Pfile not found');
        end
        pfilename=pfilename(1).name;
        opts_start=1;
    end        
end

pfilename=fullpath(pfilename);
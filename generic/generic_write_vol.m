% P = generic_write_vol(rcn,hdr,varargin)
%
% rcn : nx x ny x nsl
%
% options:
%
% outpath
% prefix
% spm
% mat
% dcm
% imnooffset

function P = generic_write_vol(rcn,hdr,varargin)

opts_def.outpath=pwd;
opts_def.prefix='v';
opts_def.spm=0;
opts_def.mat=1;
opts_def.tif=0;
opts_def.dcm=0;
opts_def.nii = 0;
opts_def.imnooffset=0;
opts=get_opts(varargin,opts_def);
opts.outpath=fullpath(opts.outpath);

nsl=size(rcn,3);

vox=[hdr.image.dfov/size(rcn,1) hdr.image.dfov/size(rcn,2) hdr.image.slthick+hdr.image.scanspacing];
fov=[hdr.image.dfov hdr.image.dfov];

minval=min(rcn(:));
maxval=max(rcn(:));

if opts.dcm==1    
    for sl=1:nsl    
        hdr.image.im_no=sl+opts.imnooffset;
        write_gedcm((rcn(:,:,sl)-minval)/(maxval-minval)*32000, fullfile(opts.outpath,[opts.prefix 'sl' num2str(sl,'%03d') '.dcm']),hdr);
    end
     P=fullfile(opts.outpath,[opts.prefix '.dcm']);
end

if opts.tif==1
    for sl=1:nsl            
        imwrite((rcn(:,:,sl)-minval)/(maxval-minval),fullfile(opts.outpath,[opts.prefix 'sl' num2str(sl,'%03d') '.tif']))        
    end
end

if opts.spm==1
    generic_mat2spm(rcn,'vox',vox,'fov',fov,'outpath',opts.outpath,'prefix',opts.prefix);
    P=fullfile(opts.outpath,[opts.prefix '.img']);
end

if opts.mat==1
    save(fullfile(opts.outpath,[opts.prefix '.mat']),'rcn');
    P=fullfile(opts.outpath,[opts.prefix '.mat']);
end


end
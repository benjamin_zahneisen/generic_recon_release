function generic_write_nii(V,hdr,w_imag,foldername,dummySlices)

if nargin < 4
    foldername = 'nii';
end
if nargin < 5
   dummySlices = false; %add dummy empty dummy slices to bottom and top
end

%create directory
unix(['mkdir ',foldername]);


vox=[hdr.image.dfov/size(V,1) hdr.image.dfov/size(V,2) hdr.image.slthick+hdr.image.scanspacing];
fov=[hdr.image.dfov hdr.image.dfov];

%display('generic re-orientation')
V = V(end:-1:1,:,:,:);

if dummySlices
   tmp = V;
   V=zeros(size(tmp,1),size(tmp,2),size(tmp,3)+2,size(tmp,4));
   V(:,:,2:end-1,:) = tmp;
end

if exist('niftiwrite','file')
    filename = ['./',foldername,'/','dti.nii'];
    niftiwrite(V,filename,'Compressed',true);
    
    % load nifti info
    info = niftiinfo(strcat(filename,'.gz'));
    info.PixelDimensions = [vox 1];
    niftiwrite(V,filename,info,'Compressed',true);
    
else    
    % using an external nifti toolbox 
    nii = make_nii(V, vox, [0 0 0]);

    %prefix=generic_pathfileprefix(hdr);
    %save_nii(nii,['./nii/',prefix,'_',num2str(length(opts.ts)),'vols.nii']);
    save_nii(nii,['./',foldername,'/','dti.nii']);
    unix(['gzip -f ','./',foldername,'/','dti.nii']);
end

%% write some fsl stuff
if(exist('w_imag.dts_adc'))
    line_dwell = (w_imag.epi_flat_res + 2*w_imag.epi_ramp_res)*w_imag.dts_adc;
else
   line_dwell = (w_imag.epi_flat_res + 2*w_imag.epi_ramp_res)*w_imag.dts;%backward compatibility
end

%w_imag.
fid = fopen(['./',foldername,'/acqp.txt'],'w');
    fprintf(fid,'0 -1 0 %1.3f\n',line_dwell*w_imag.ny/w_imag.inplane_R);
    fprintf(fid,'0 1 0 %1.3f\n',line_dwell*w_imag.ny/w_imag.inplane_R);
fclose(fid);
        
fid = fopen(['./',foldername,'/index.txt'],'w');
    fprintf(fid,'1 ');
    fprintf(fid,'2 ');
    %for n=3:numB0
    %    fprintf(fid,'1 ');
    %end
    %for n=1:numdifdirs;
    %    fprintf(fid,'1 ');
    %end
fclose(fid);
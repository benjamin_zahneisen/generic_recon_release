% function  [fileprefix destexsubdir destsesubdir]=generic_pathfileprefix(hdr,varargin)
%
% get destination directory and file prefix for writing reconstructed files
%
% options : volnum :volume number of the file

function [fileprefix destexsubdir destsesubdir]=generic_pathfileprefix(hdr,varargin)
    
opts_def.volnum=[];
opts=get_opts(varargin,opts_def);

fileprefix=['e' num2str(hdr.exam.ex_no) 's' num2str(hdr.series.se_no,'%03d')];

if ~isempty(opts.volnum)
    fileprefix=[fileprefix 'v' num2str(opts.volnum,'%04d')];
end

if nargout >=2

    pat='[.,\s()\/\\\^]';
    patname=hdr.exam.patname;
    year=['20' hdr.rdb.scan_date(8:9)];
    month=hdr.rdb.scan_date(1:2);
    day=hdr.rdb.scan_date(4:5);
    exno=num2str(hdr.series.se_exno);
    seno=sprintf('%03d', hdr.series.se_no);
    exdesc=hdr.exam.ex_desc;
    sedesc=hdr.series.se_desc;

    exdesc(regexp(exdesc,pat))='_';
    sedesc(regexp(sedesc,pat))='_';
    patname(regexp(patname,pat))='_';

    destexsubdir=[year '_' month '_' day  '_e' exno '_' patname '_' exdesc];
    destsesubdir=['s' seno '_' sedesc];

end

end
function Z=imresize4D(A, dim, fov_ratio, method)
%loop over imresize3D

if nargin<=3 || isempty(method)
    method = 'spline';
end
if nargin<=2 || isempty(fov_ratio)
    fov_ratio = 1;
end
if isempty(dim)
    dim = size(A);
end

for n=1:size(A,4)
    Z(:,:,:,n)=imresize3D(A(:,:,:,n),dim,fov_ratio,method);
end
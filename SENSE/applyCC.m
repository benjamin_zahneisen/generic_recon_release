function [ data ] = applyCC( dataIn,S )
%APPPLYCC Summary of this function goes here
%   Detailed explanation goes here

% data = [Nx Ny Nz Nt 1 Ns Nc] = 7D (or 5d)

if(isempty(S.gccmtx))
    display('coil sensitivities are not compressed.')
    return;
end


dims = size(dataIn);
if (length(dims)==5)
    data = zeros([dims(1:4) S.Ncc]);
    for sl=1:size(dataIn,4)
        for t=1:size(dataIn,3)
            data(:,:,t,sl,:) = CC(squeeze(dataIn(:,:,t,sl,:)), S.gccmtx,1);
        end
    end
else
    data = zeros([dims(1:6) S.Ncc]);

    for sl=1:size(dataIn,6)
        for t=1:size(dataIn,4)
            data(:,:,:,t,1,sl,:) = CC(squeeze(dataIn(:,:,:,t,1,sl,:)), S.gccmtx,1);
        end
    end
end


end


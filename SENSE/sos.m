function out=sos(v,dim)

if nargin < 2
    dim = length(size(v));
end

out = sqrt(sum(abs(v).^2,dim));
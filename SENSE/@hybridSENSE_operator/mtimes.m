function Q = mtimes(A,B)


if isa(A,'hybridSENSE_operator')
    
% now B is the operator and A is the vector
else
    Q = mtimes(B',A')';
    
end
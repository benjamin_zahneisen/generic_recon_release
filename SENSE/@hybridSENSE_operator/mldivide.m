function Q = mldivide(A,B)

%perform the hybrid-SENSE reconstruction

if isa(A,'hybridSENSE_operator')
end

extra_dims = size(B);
extra_dims = extra_dims(A.dim+2:end);
if isempty(extra_dims)
    tframes = 1;
else
    tframes = prod(extra_dims);
end

%1d-fft to hybrid k-space
hybr = ifff(B,1);
hybr = reshape(hybr,[A.N(1) A.Ns*A.Nc tframes]);

Q=zeros(A.N(1),A.N(2)*A.N(3),tframes);

[~, msgid] = lastwarn;
msgid = 'MATLAB:rankDeficientMatrix';
warning('off',msgid);

%loop over lines
for x=1:A.N(1)
    %get sensitvity weighting
    sw = squeeze(A.C(x,:,:));
    
    if x==70
        a=1;
    end
        
    %get phase evolution due to off-resonance map
    if ~isempty(A.wmap)
        theta = ones([A.Ns,prod(A.N(2:3))]);
        for t=1:A.Ns 
            theta(t,:) = squeeze(exp(1i*A.wmap(x,:)*A.t_vec(t)));
        end 
        %theta2 = smooth2(theta(:,:),'gauss',[4 4],[0.2 2]);
        %theta = exp(1i*angle(theta2));
        %for n=1:96
        %    %spatial filtering
        %    theta(n,:) = exp(1i*angle(smooth(theta(n,:),4,'sgolay')));
        %end
        theta= repmat(theta,[A.Nc 1]);
    end

        
    %multiply k=-space encoding with sensitivity weighting
    E = A.E.*(sw.');
    if ~isempty(A.wmap)
        E = E.*theta;
    end
        
    Q(x,:,:) = E\reshape(hybr(x,:,:),[A.Ns*A.Nc tframes]);
    %Q(x,:,:) = pinv(E)*reshape(hybr(x,:,:),[A.Ns*A.Nc tframes]);
    
    [~, msgid] = lastwarn;
msgid = 'MATLAB:rankDeficientMatrix';
%warning('off',msgid);
end

Q = squeeze(reshape(Q,[A.N(1) A.N(2) A.N(3) extra_dims]));
% Definition of hybridSENSE operator
%
% 
% Author: Benjamin Zahneisen
% Last modified: 01 Nov 2015

classdef hybridSENSE_operator
    properties (SetAccess = public, GetAccess = public)
        N                   = [];
        dim                 = 3; % dim=2; for 2d slices; dim=3 for 3D reconstructions
        Nc                  = [];
        Ns                  = []; %number of k-space samples [-pi pi] 
        R                   = [1 1]; %acceleration factor along y and z
        Rtot                = 1; %total acceleration factor (=R(1)*R(2))

        r                   = []; %matrix of locations
        K                   = []; %2 x Ns matrix of k-space locations
        E                   = []; %encoding matrix
        smaps               = []; %coil sensitivities as [Nx Ny ... Nc]
        wmap                = []; %off-resonance map
        dwell               = []; %time between two EPI lines [s]
        t_vec               = []; %array of time points (can be used for partial fourier)
        C                   = []; %coil sensitivities as [A.Nc A.Rtot prod(A.ReducedFoVSize)]
        gmap                = []; 

        intTreshold         = (10^-5); %distinguish between noise floor and psf values
        adjoint             = 0;
        debug               = true;
        Anatom              = [];
        mask                = [];
        mode                = 'fast'; %'full','fast'
    end
   
    
    methods
        %-----------------------------------------------------------------+
        %% Constructor
        % Returns a hybrid-SENSE operator A that can be applied to undersampled
        % raw data in the form recon=A\s, where s is an n-dimensional
        % matrix of k-space data with zeros representing skipped samples.
        % N = input matrix
        % K = [samples x dim]
        %-----------------------------------------------------------------+
        function A=hybridSENSE_operator(N,Kyz,smaps,wmap,dwell)
            
            if nargin < 4
                wmap = [];
            else
                A.wmap = wmap;
                A.dwell = dwell;
            end
            
            if length(N)==2 || (N(3) == 1)
                N=[N(1) N(2) 1];
                A.dim = 2;
            end
            
            A.N = N;
            A.Ns = size(Kyz,1);
            A.K = Kyz.';
            
            %single coil case
            if (nargin < 3) || isempty(smaps)
               A.Nc = 1;
               A.C = ones(A.N);
            else
            
                if isa(smaps,'CoilSensitivity')
                    A.dim = smaps.dim;
                    A.Nc = smaps.Nc;
                    display([num2str(A.dim),'D imaging, ',num2str(A.dim-1),'D SENSE'])

                else
                    A.C = smaps;
                    A.Nc = size(smaps,4);
                end
            end
            
            %grid with spatial locations
            if A.N(3) == 1
                 [ygrid, zgrid]=ndgrid(-N(2)/2:N(2)/2-1,0);
            else
                [ygrid, zgrid]=ndgrid(-N(2)/2:N(2)/2-1,ceil(-((N(3)/2))):ceil(N(3)/2-1));
            end
            ygrid = ygrid(:);
            zgrid = zgrid(:);
            A.r(:,1) = ygrid(:);
            A.r(:,2) = zgrid(:);
            
            %pure k-space encoding (does not depend on x-location)
            A.E = exp(-1i*(A.r*A.K));
            
            A.E = A.E.';
            
            %replicate encoding matrix for coil weighting
            A.E = repmat(A.E,[A.Nc 1]);
            
            %reshape sensitvity weightings
            A.C = reshape(A.C,[N(1) N(2)*N(3) 1 A.Nc]); %[N(1) Ngrid 1 Nc]
            A.C = repmat(A.C,[1 1 A.Ns 1]);
            A.C = reshape(A.C,[N(1) N(2)*N(3) A.Ns*A.Nc]);
            
            %reshape off-resonance map
            if ~isempty(A.wmap)
                A.wmap = reshape(A.wmap,[N(1) N(2)*N(3)]);
                for t=1:A.Ns
                    A.t_vec(t) = (t-1)*dwell;
                end
            end

        
            %calculate total acceleration factor
            %A.Rtot = 1;
            %display(['Total acceleration factor: ',num2str(A.Rtot)])
        
            % call deriveSensePattern and return A (yields updated class A)

            
            %pre-calculate sense matrix inversions

         
        end
        % END of Constructor
        %/////////////////////////////////////////////////////////////////+
        
        

        
        %-----------------------------------------------------------------+
        %% getgmap()
        % compute gmap if not already performed 
        %-----------------------------------------------------------------+
        function gmap=getgmap(A)
            if isempty(A.gmap)
      
            else
                display('there is a gmap.')
            end
        end
        
        
        
        %% update coil sensitivity maps (the rest is unchanged)
        function A=updateCoilSensitivities(A,smaps,invFlag)
            
            if nargin < 3 || strcmp(A.mode,'fast')
                invFlag = false;
            end
            
            if (isa(smaps,'CoilSensitivity'))
               smaps = smaps.smaps; 
            end
            
            if all(size(smaps)==[A.N A.Nc])
                if A.debug
                    A.smaps = smaps;
                end
            else
                warning('coil sensitivity dimension mismatch !')
                return;
            end
            
            %update the pseudo inverse matrices based on new coil data
            if invFlag
                A=A.invertCoilSensitivities();
            else
                A.gmap = []; %gmap is no longer valid in that case
                CLin = reshape(smaps,[prod(A.N) A.Nc]);
                for n=1:size(A.AliasedVoxLinIdx,1)
                    A.C(:,:,n) = CLin(A.AliasedVoxLinIdx(n,:),:).';
                end
            end
            
        end
        
        
        %% local fft
        function x=fffnd(A,x,nd)
            for d=1:nd
                x=fff(x,d);
            end
        end
        function x=ifffnd(A,x,nd)
            for d=1:nd
                x=ifff(x,d);
            end
        end
        function out=fff3d(A,in)
            out = fff(in,1);
            out = fff(out,2);
            out = fff(out,3);
        end
        function out=ifff3d(A,in)
            out = ifff(in,1);
            out = ifff(out,2);
            out = ifff(out,3);
        end 
        function out=fff4d(A,in)
            out = fff(in,1);
            out = fff(out,2);
            out = fff(out,3);
            out = fff(out,4);
        end
        function out=ifff4d(A,in)
            out = ifff(in,1);
            out = ifff(out,2);
            out = ifff(out,3);
            out = ifff(out,4);
        end
        
     function A = setAnatomImage(A,Anatom)
    
        imSize = size(Anatom);
        if any(~(imSize == A.N))
            display('wrong size of anatomical image!')
        else
            A.Anatom = Anatom;
            
            %mask out zero intensities
            A.mask = Anatom > 1000;
            
        end
    
     end       
      
     function s = getSingleCoil(A,coiln)
         if A.dim == 3 
             s = A.smaps(:,:,:,coiln);
         elseif A.dim == 4
             s = A.smaps(:,:,:,:,coiln);
         elseif A.dim == 2
             s = A.smaps(:,:,coiln);
         end
     end
     
     
    %plot the gmaps
    function plotGmaps(A)
        if A.dim == 3
            if ~isempty(A.Anatom)
                %z= overlay(array2mosaic(A.Anatom),array2mosaic(A.gmap.*A.mask));
                %image(z)
                figure;
                imagesc(array2mosaic(A.gmap.*A.mask))
                
            else
                imagesc(array2mosaic(A.gmap))
            end
        elseif A.dim == 4
            maxG = max(A.gmap(A.mask))
            for n=1:A.N(4)
                figure;
                imagesc(array2mosaic(A.gmap(:,:,:,n).*A.mask(:,:,:,n)),[0 maxG])
            end
            
        end
        
    end
   
    
    end
    %End of methods

end
%END OF CLASSDEF


%% helper functions
function sIndices=cylicIdxShift(Indices,dI,N)
    
    %Indices is an Rtot x Nd matrix where each row stores the Nd pixel
    %locations
    sIndices = zeros(size(Indices));
    
    for l=1:size(Indices,1)
        for d=1:size(Indices,2)
            sIndices(l,d) = Indices(l,d)+dI(d);
            if sIndices(l,d) > N(d)
                sIndices(l,d) = sIndices(l,d) - N(d);
            end
            if sIndices(l,d) < 1
                sIndices(l,d) = sIndices(l,d) + N(d);
            end
        end
    end
end

%% external helper functions
        function out=fff(in, dim)
            if nargin==1
                s = size(in);
                f = find(s~=1);
                dim = f(1);
            end
            out=fftshift(fft(ifftshift(in,dim),[],dim),dim);
        end
        function out=ifff(in, dim)
            if nargin==1
                s = size(in);
                f = find(s~=1);
                dim = f(1);
            end 
            out=fftshift(ifft(ifftshift(in,dim),[],dim),dim);
        end
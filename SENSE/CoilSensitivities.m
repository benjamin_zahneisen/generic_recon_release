% Class support for coil sensitivities
%
% This class handles all aspects related to coil sensitivity information.
% Coil sensitivies can be either 2d,3d, or 4d (as in simultaneous multi
% slab imaging)
% 
% Author: Benjamin Zahneisen
% benjamin.zahneisen@gmail.com
% Last modified: 01 May 2016

classdef CoilSensitivities
    properties
        N       = []; %resolution
        Nc      = []; %number of coils
        fov     = []; %field-of-view in [mm]
        dim     = []; %dimensionality
        method  = 'adaptive';
        recon   = [];
        sos     = [];
        mask    = [];
        support = [];
        supportMask = [];
        smaps   = []; %masked sense maps with pixels (air/noise) set to zero
        smapsOrg  = []; %original sense maps (various sizes)
        mask_thresh = 0.9; %which pixels are set to zero
        gccmtx  = [];
        Ncc     = [];
        id      = []; %scan id of underlying scan
        type    = []; %what kind of mr sequence
    end
    
    methods
        function S=CoilSensitivities(cmaps,method,var)
            
            
            S.dim = length(size(cmaps))-1;
            tmp = size(cmaps);
            S.N = tmp(1:S.dim);
            S.Nc = tmp(S.dim+1);
            S.Ncc = S.Nc;
            
            %in any case a sum of squares combination seems useful
            S.sos = sqrt(sum(abs(cmaps).^2,S.dim+1));
            
            if nargin > 1
                S.method = method;
            end
            if((nargin < 3) && (strcmp(S.method,'espirit')))
                var = 0.02; %first threshold for espirit
            end
            
            
            
            %% adaptive combination
            if strcmp(S.method,'adaptive') || strcmp(S.method,'adapt')
            
                if S.dim == 2
                    [S.recon, S.smaps] = adapt(permute(cmaps,[S.dim+1 1:S.dim]));
                    S.smaps = ipermute(S.smaps,[S.dim+1 1:cI-1]);
                else
                    if(S.N(2) < 64)
                        display('smaller y-kernel adapt');
                        [S.recon, S.smaps, S.support] = adapt3D(cmaps,1,[6 4 4]);
                    else    
                        [S.recon, S.smaps, S.support] = adapt3D(cmaps,1,[6 6 4]);
                    end
                    
                    %use histogram to distinguish between noise and image
                    [nelements,centers]= hist(log(S.support(:)),50);
                    [pks,locs] = findpeaks(nelements);
                    %plot(centers,nelements); hold on
                    %plot(centers(locs),nelements(locs),'ro')
                    brain=centers(locs(1));
                    noise=centers(locs(end));
                    S.mask_thresh = exp((noise+brain)*0.5);
                    S.supportMask = S.support > S.mask_thresh;                   
                    
                    S.mask = abs(S.sos);
                    S.smapsOrg = S.smaps;

                end
            %% ESPiRIT coil maps
            elseif strcmp(method,'espirit')
            
                cdata = fff2d(cmaps);
                s = zeros(size(cmaps));
                sup = zeros(size(cmaps,1),size(cmaps,2),size(cmaps,3));
                supmask = zeros(size(cmaps,1),size(cmaps,2),size(cmaps,3));
                parfor n=1:size(cmaps,3)
                   % n
                    %[S.smaps(:,:,n,:), S.support(:,:,n), S.supportMask(:,:,n)] = get_ESPIRiT_maps(squeeze(cdata(:,:,n,:)),[24 16],[4 4],0.03);
                    %[S.smaps(:,:,n,:), S.support(:,:,n), S.supportMask(:,:,n)] = get_ESPIRiT_maps(squeeze(cdata(:,:,n,:)),[24 24],[6 6],var);
                    [s(:,:,n,:), sup(:,:,n), supmask(:,:,n)] = get_ESPIRiT_maps(squeeze(cdata(:,:,n,:)),[24 24],[6 6],var);
                    
                end
                espiritThreshold = 0.97;
                S.smaps = s;
                S.support = sup;
                S.supportMask = supmask;
                
                %fix random slice phase
                cidx = round(size(S.smaps,3)/2);
                for n=1:size(cmaps,3)
                    %jointMask = logical((S.support(:,:,n)>0.98)) & logical((S.support(:,:,cidx)>0.98));
                    %dphi = S.smaps(:,:,n,1)./S.smaps(:,:,cidx,1);
                    %phi(n) = mean(abs(angle(dphi(jointMask))));
                end
                for n=1:size(cmaps,3)
                    %if(phi(n)>1)
                    %    S.smaps(:,:,n,:)=S.smaps(:,:,n,:).*exp(1i*pi);
                    %end
                end
                
                S.mask_thresh = espiritThreshold;
                S.supportMask = S.support > espiritThreshold;
                S.recon = S.sos;
                S.mask = abs(S.sos);
                S.smapsOrg = S.smaps;
            
            elseif strcmp(method,'espirit3d')
                %add some dummy slices
                reconZPad = zpad(cmaps,S.N(1),S.N(2),S.N(3)+8,S.Nc);
                cdata = fff3d(reconZPad);
                [maps, support, supportMask] = get_ESPIRiT_maps3D(cdata,[6 6 6]);
                S.smaps = crop(maps,S.N(1),S.N(2),S.N(3),S.Nc);
                S.support = crop(support,S.N(1),S.N(2),S.N(3));
                S.supportMask = crop(supportMask,S.N(1),S.N(2),S.N(3));
                
                espiritThreshold = 0.94;
                S.mask_thresh = espiritThreshold;
                S.supportMask = S.support > espiritThreshold;
                
                S.recon = S.sos;
                S.mask = abs(S.sos);
                
                
            %% sos combination
            elseif strcmp(method,'sos')

                if S.dim==2
                    for k=1:size(cmaps,3)
                        cmaps(:,:,k) = imfilter(cmaps(:,:,k),fspecial('gauss',[5 5], 2));
                    end
                else
                    for k=1:size(cmaps,4)
                        cmaps(:,:,:,k) = imfilter(cmaps(:,:,:,k),fspecial3D('gauss',[5 5 5], 2));
                    end
                end
                
                S.sos = sqrt(sum(abs(cmaps).^2,S.dim+1));
                S.recon = S.sos;
                sos = repmat(S.sos,[ones(1,S.dim) S.Nc]);
                %sos = reshape(sos, sc);
         
                S.smaps = cmaps ./ sos;
    
                S.mask = (abs(sos)>S.mask_thresh*max(abs(sos(:))));
                S.smaps = S.smaps .* S.mask;
                
                %make mask nd
                S.mask = S.mask(:,:,:,1);
            
                %S.recon = sos;
            
            elseif strcmp(method,'dummy')
                S.smaps = ones(size(cmaps));
                
            end
        end
        
        %% resample coil sensitivities
        function S=resize(S,N,FOV,method)
            %FOV in [mm]
           
            if nargin < 3
                fFOV = [1 1 1];
                FOV = S.fov;
            else
                %get fractional fov
                fFOV = FOV./S.fov;
            end
            if nargin < 4
                method ='linear';
            end
            
            if length(N) == S.dim
                S.smaps = imresize4D(S.smaps,N,fFOV,method);
                S.smapsOrg = imresize4D(S.smapsOrg,N,fFOV,method); 
                S.N = N;
                S.fov = FOV;
                S.mask = imresize3D(single(S.mask),N,fFOV,method);
                S.mask = S.mask > 0.5;
                S.sos = imresize3D(S.sos,N,fFOV,method);
                S.recon = imresize3D(S.recon,N,fFOV,method);
                S.support = imresize3D(S.support,N,fFOV,'linear');
                if(strcmp(S.method,'espirit') || strcmp(S.method,'espirit3d'))
                    S.supportMask = S.support > S.mask_thresh;
                
                elseif(~isempty(S.supportMask))
                    S.supportMask = imresize3D(single(S.supportMask),N,fFOV,'linear');
                    S.supportMask = S.supportMask > S.mask_thresh;
                    display('dont go there')
                end
            else 
                display('wrong dimensions')
            end
            
        end
        
        
       %% apply mask to sensitivites
       function S=applyMask(S,threshold)
           
           if nargin < 2
               threshold = S.mask_thresh;
           end
           
           S.smaps = S.smapsOrg.*repmat((S.support > threshold),[ones(1,S.dim) S.Ncc]);
           S.supportMask = (S.support > threshold);
                     
       end
       
       
        
       %% reshape coil sensitivities to be 4-dimensional
       function S=make4D(S,z1,z2)
           %Rz2 = slab acceleration factor (how many slabs are excited
           %simultaneously
            
           if nargin < 3
               z2 = S.N(3)/z1;
           end
           
           %sanity check
           if z1*z2 == S.N(3)
               S.smaps = reshape(S.smaps,[S.N(1) S.N(2) z1 z2 S.Nc]);
               S.mask = reshape(S.mask,[S.N(1) S.N(2) z1 z2]);
               S.sos = reshape(S.sos,[S.N(1) S.N(2) z1 z2]);
               S.recon = reshape(S.recon,[S.N(1) S.N(2) z1 z2]);
                  
               %change dimensions
               S.N(3) = z1;
               S.N(4) = z2;
               S.dim =4;
           else
               display('Slab dimensions inconsistent!')
           end
       end
       
       %% returns a non-contigous subset of the last dimension (z in case of
       %simultaneous multi slice, z2 in case of multi slab imaging)
       function S = getSubset(S,zidx,Ndummy)
           
           if nargin < 3
               Ndummy = 0;
           end
           
           if S.dim == 4
            S.smaps = S.smaps(:,:,:,zidx,:);
            S.recon = S.recon(:,:,:,zidx);
            S.sos = S.sos(:,:,:,zidx);
            S.mask = S.mask(:,:,:,zidx);
            S.N(4) = length(zidx);
           elseif S.dim ==3
            S.smaps = squeeze(S.smaps(:,:,zidx,:));
            S.recon = squeeze(S.recon(:,:,zidx));
            S.sos = squeeze(S.sos(:,:,zidx));
            S.mask = squeeze(S.mask(:,:,zidx));
            S.N(3) = length(zidx);
            if length(zidx)==1
                S.dim = S.dim-1;
                S.N(3) = [];
            end
            if Ndummy > 0;
                S.smaps(:,:,length(zidx)+Ndummy,:)=0;
                S.smaps = circshift(S.smaps,[0 0 1 0]);
                S.N(3) = S.N(3) + Ndummy;
            end
           end
       end
       
       %% geometric coil compression
       function S = compress(S,Ncnew,gccmtx)
           %perform coil compression along readout dimension (1st dim)
            if nargin < 2
                Ncnew = S.Nc/2;
            end
            
            dimx = 1;
            KDATA = fff3d(S.smaps);
            if nargin < 3
                %calculate compression
                %calib = crop(DATA,[24,200,8]);
                gccmtx =  calcGCCMtx(KDATA,dimx,1); %#ok<*PROP>
                gccmtx = gccmtx(:,1:Ncnew,:);
                S.gccmtx = alignCCMtx(gccmtx);
            else
                %use existing compression
                S.gccmtx = gccmtx;
                display('use input compression matrix')
            end
            
            CCDATA = CC(KDATA,S.gccmtx, dimx);
            S.smaps = ifff3d(CCDATA);
            S.smapsOrg = ifff3d(CC(fff3d(S.smapsOrg),S.gccmtx,dimx));
            S.Ncc = Ncnew;
       
       end
    end
end

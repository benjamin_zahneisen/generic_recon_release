%% ESPIRiT Maps Demo
% This is a demo on how to generate ESPIRiT maps. It is based on the paper
% Uecker et. al, MRM 2013 DOI 10.1002/mrm.24751. ESPIRiT is a method that
% finds the subspace of multi-coil data from a calibration region in
% k-space using a series of eigen-value decompositions in k-space and image
% space. 

%%
% Set parameters

function [maps, support, supportMask] = get_ESPIRiT_maps3D(CDATA,ksize)



[sx,sy,sz,Nc] = size(CDATA);
ncalib = 24; % use 24 calibration lines to compute compression
if nargin < 2
    ksize = [6,6,6]; % kernel size
end


% Threshold for picking singular vercors of the calibration matrix
% (relative to largest singlular value).

eigThresh_1 = 0.02; %the original value is 0.02

% threshold of eigen vector decomposition in image space. 0.95 in demo code
eigThresh_2 = 0.95;

% crop a calibration area
%calib = crop(CDATA,[ncalib,ncalib,2*ncalib,Nc]);
calib = crop(CDATA,[ncalib,ncalib,sz,Nc]);

%%
% Display coil images: 
%im = ifft3c(CDATA);


%% Compute ESPIRiT EigenVectors
% Here we perform calibration in k-space followed by an eigen-decomposition
% in image space to produce the EigenMaps. 


% compute Calibration matrix, perform 1st SVD and convert singular vectors
% into k-space kernels

[k,S] = bz_dat2Kernel3D(calib,ksize);
idx = max(find(S >= S(1)*eigThresh_1));

%% 
% This shows that the calibration matrix has a null space as shown in the
% paper. 




%%
% crop kernels and compute eigen-value decomposition in image space to get
% maps
[M,W] = kernelEig3D(k(:,:,:,:,1:idx),[sx,sy,sz]);

W=W*sqrt(sx*sy*sz);


%%
% project onto the eigenvectors. This shows that all the signal energy
% lives in the subspace spanned by the eigenvectors with eigenvalue 1.
% These look like sensitivity maps. 


% alternate way to compute projection is:
% ESP = ESPIRiT(M);
% P = ESP'*im;
%P = sum(repmat(im,[1,1,1,Nc]).*conj(M),3);




%%
% crop sensitivity maps 
%maps = M(:,:,:,:,end).*repmat(W(:,:,:,end)>eigThresh_2,[1,1,1,Nc]);

maps = M(:,:,:,:,end);
support = W(:,:,:,end);
supportMask = W(:,:,:,end)>eigThresh_2;


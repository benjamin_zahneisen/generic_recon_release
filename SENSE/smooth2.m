function Y = smooth2(X,filt,kernel,sd)

switch filt
    case {'gauss','gaussian'}
        q = gauss_kernel(kernel,sd);
end

Y = conv2(X,q,'same');
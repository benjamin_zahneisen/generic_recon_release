function out=fff(in, dim)

if nargin==1
    s = size(in);
    f = find(s~=1);
    dim = f(1);
end

out=fftshift(fft(ifftshift(in,dim),[],dim),dim);
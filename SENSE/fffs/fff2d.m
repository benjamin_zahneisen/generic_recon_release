function Y=fff2d(X)

% X     : multidimensional array

% Y : fourier transform along first two dimensions

Y = fff(X,1);
Y = fff(Y,2);
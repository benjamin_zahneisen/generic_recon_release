function Y=fff3d(X)

% X     : multidimensional array

% Y : fourier transform along first three dimensions

Y = fff(X,1);
Y = fff(Y,2);
Y = fff(Y,3);

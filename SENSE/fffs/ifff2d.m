function Y=ifff2d(X)

% X     : multidimensional array

% Y : fourier transform along first two dimensions

Y = ifff(X,1);
Y = ifff(Y,2);
function Y=ifff3d(X)

% X     : multidimensional array

% Y : fourier transform along first three dimensions

Y = ifff(X,1);
Y = ifff(Y,2);
Y = ifff(Y,3);

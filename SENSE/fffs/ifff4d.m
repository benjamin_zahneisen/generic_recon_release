function Y=ifff4d(X)

% X     : multidimensional array

% Y : fourier transform along first four dimensions

Y = ifff(X,1);
Y = ifff(Y,2);
Y = ifff(Y,3);
Y = ifff(Y,4);

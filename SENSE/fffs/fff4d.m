function Y=fff4d(X)

% X     : multidimensional array

% Y : fourier transform along first four dimensions

Y = fff(X,1);
Y = fff(Y,2);
Y = fff(Y,3);
Y = fff(Y,4);

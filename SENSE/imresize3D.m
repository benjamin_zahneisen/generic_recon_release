function Z = imresize3D(A, dim, fov_ratio, method)

% function Z = imresize3D(A, dim, fov_ratio, method)
%
% for other methods than cubic consult 'doc interp3'
%
% A = image
% dim = new image size
% fov_ratio = fov_new/fov_old, e.g. to crop the
%             resized image to half of the orig. fov use
%             fov_ratio = [0.5 0.5 0.5]


if nargin<=3 || isempty(method)
    method = 'cubic';
end
if nargin<=2 || isempty(fov_ratio)
    fov_ratio = 1;
end
if isempty(dim)
    dim = size(A);
end

if length(fov_ratio)==1
    fov_ratio = repmat(fov_ratio,[1 3]);
end

[Nr, Nc, Ns] = size(A);
Mr = dim(1);
Mc = dim(2);
Ms = dim(3);

%[X, Y, Z] = meshgrid(1:Nc, 1:Nr, 1:Ns);
[X, Y, Z] = meshgrid(-Nc/2+0.5:Nc/2-0.5, -Nr/2+0.5:Nr/2-0.5, -Ns/2+0.5:Ns/2-0.5);

Ir = linspace(0.5*(1+Nr/Mr),Nr+0.5*(1-Nr/Mr),Mr) - (Nr/2+0.5);
Ic = linspace(0.5*(1+Nc/Mc),Nc+0.5*(1-Nc/Mc),Mc) - (Nc/2+0.5);
Is = linspace(0.5*(1+Ns/Ms),Ns+0.5*(1-Ns/Ms),Ms) - (Ns/2+0.5);

Ir = Ir*fov_ratio(1);
Ic = Ic*fov_ratio(2);
Is = Is*fov_ratio(3);

[Xi,Yi,Zi] = meshgrid(Ic,Ir,Is);

if strcmp(method,'spline')
    extrapolate = true;
else
    extrapolate = false;
end


%2d-case
if(Is==0)
    if(extrapolate)
        Z = interp2(X,Y,A,Xi,Yi,method); % extrapolation
    else
        Z = interp2(X,Y,A,Xi,Yi,method,0); % extrapolation
    end
else %3d
    if(extrapolate)
        Z = interp3(X,Y,Z,A,Xi,Yi,Zi,method); % extrapolation
    else
        Z = interp3(X,Y,Z,A,Xi,Yi,Zi,method,0); % extrapolation
    end
end


%% joint_recon_wrapper
%script performs joint blip up/down reconstruction by going through the
%whole recon pipeline using matlab's parallel computing toolbox and invoking FSL 5.0 (must be installed separately)

%set path to fsl installation
fslpath = getenv('FSLDIR');

%start matlab's parallel pool
delete(gcp('nocreate')) %start by deleting existing pools
Ncpu = 8; %number of cpu's to be used
parpool(Ncpu);

%% 1) call regular SENSE recon (create folders 'blipUp' and 'blipDown', store recons as dti.nii.gz)
% this step is performed by 'dosense' option in generic_recon2.m


%% 2) call generic_recon2 => re-load data and store it in hybrid-sense format (as dataHybrid.mat)
disp('calling generic_recon2: ghost correction, data gridding, store raw data in folder [rawdata]')
tic; generic_recon2('dosense',true); toc
%generic_recon2('dosense',true,'ts',1:12); %uncomment this and reconstruct only the first 4 frames for testing purposes


%% 3) run topup for all frames (use parallel computing toolbox, restart parpool manually if previous settings are expired)
    
    %thats basically what call_fsl.m does:
    %cmd =['LD_LIBRARY_PATH=',fslpath,'; fslnvol./',getenv('UP_DIR'),'/dti.nii.gz']; 
    %unix(cmd)
    
    %get number of volumes
    cmd = ['fslnvols ./',getenv('UP_DIR'),'/dti.nii.gz'];    
    [status,output] = call_fsl(cmd); %call_fsl makes sure 'LD_LIBRARY_PATH' is set to correct location. No interference with matlab shared libraries
     
    if ismac 
        numvols = output; %output of call_fsl does not include copy of forwarded unix command
    else  
        tmp = strsplit(output);numvols = tmp(2); %only use the 2nd output    
        disp([numvols,' volumes found']);
    end   
    numvols=str2double(numvols);
        
    
    %split dti.nii.gz into separate volumes (as pre-processing step)
    unix(['mkdir ',getenv('DICO_DIR')]);
    unix([getenv('GENERIC_HOME'),'/fsl/FSL_upDown_prep.sh']);
    for t=0:(numvols-1)
       [status,output] = call_fsl(['fslroi ./',getenv('UP_DIR'),'/dti.nii.gz',' ','./',getenv('DICO_DIR'),'/tmpUp',' ',num2str(t),' 1']);
       [status,output] = call_fsl(['fslroi ./',getenv('DOWN_DIR'),'/dti.nii.gz',' ','./',getenv('DICO_DIR'),'/tmpDown',' ',num2str(t),' 1']);
       [status,output] = call_fsl(['fslmerge -t ./',getenv('DICO_DIR'),'/V_',num2str(t),' ./',getenv('DICO_DIR'),'/tmpDown ./',getenv('DICO_DIR'),'/tmpUp']);
       
       display(['extracting volume ',num2str(t)]);
    end
    
    %parfor automatically starts multiple "workers" (if available)
    tic
    parfor t=0:(numvols-1)
        %unix(['LD_LIBRARY_PATH=',fslpath,';',getenv('GENERIC_HOME'),'/fsl/FSL_upDown_singleFrame.sh ',num2str(t),''],'-echo');
        call_fsl([getenv('GENERIC_HOME'),'/fsl/FSL_upDown_singleFrame.sh ',num2str(t),''])
    end
    toc
        
    %merge all volumes to one timeseries
    for t=1:(numvols-1)
        [P(:,:,:,t),voxSize] = importNIFTIS(['./',getenv('DICO_DIR'),'/Vout_',num2str(t-1),'.nii.gz'],'generic');
    end
    exportNIFTIS(P,['./',getenv('DICO_DIR'),'/dti.nii'],voxSize,'generic');

    
%% 4) run the joint recon and write data to "joint" directory
disp('run the joint recon. Run topup first if fieldmaps are not yet available');
load('S_corr.mat','w_imag');
[rcn,gmap]=runJointUpDown_recon(w_imag); 
save('gmap.mat','gmap');

%% closing the parallel pool
delete(gcp)

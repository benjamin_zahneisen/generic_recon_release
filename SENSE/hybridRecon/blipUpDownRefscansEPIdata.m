function [rcn, f]=blipUpDownRefscansEPIdata(dataUp,dataDown,w,H_up,H_down)
%based on EPI hybrid data

%dataUp = [Nx Ns Ni Nt 1 Nsl Nc]
%dataDown = [Nx Ny Nz Ni Nt Nsl Nc]

cleanUp = true;

[Nx, Ns, Ni, Nt, Necho, Nsl, Nc]  = size(dataUp);

H_up =  H_up.applyPartialFourier;
H_up = H_up.resizeGrid([64 64 1]);
H_down =  H_down.applyPartialFourier;
H_down = H_down.resizeGrid([64 64 1]);

%% initial reconstruction
    %pretend coils are volumes
    dataUp = reshape(dataUp,[Nx Ns Nt Nsl Nc]);
    dataUp = permute(dataUp(H_up.Xidx,:,:,:,:),[1 2 5 4 3]);
    dataUp_pf= zeros(size(dataUp,1),H_up.Nlines+H_up.pf_lines,Nc,Nsl,Nt);
    dataUp_pf(:,H_up.pf_lines+1:end,:,:,:) = dataUp;
    P0 = hybridReconSVD(H_up,dataUp_pf,[],[],[],0.99,0);
    P0_sos = sos(P0);
    
    dataDown = reshape(dataDown,[Nx Ns Nt Nsl Nc]);
    dataDown = permute(dataDown(H_up.Xidx,:,:,:,:),[1 2 5 4 3]);
    dataDown_pf= zeros(size(dataUp,1),H_up.Nlines+H_up.pf_lines,Nc,Nsl,Nt);
    dataDown_pf(:,H_up.pf_lines+1:end,:,:,:) = dataDown;
    P0r = hybridReconSVD(H_down,dataDown_pf,[],[],[],0.99,0);
    P0r_sos = sos(P0r);
    

%% get fieldmap by running topup 
    V1 = smooth3(abs(P0_sos),'gaussian',[3 3 3],0.25);
    V2 = smooth3(abs(P0r_sos),'gaussian',[3 3 3],0.25);
    [f_topup, Vtopup ,movPar]=run_topup(V1,V2,H_up.voxSize,H_up.effRO,'b02b0_2.cnf',0,cleanUp);


    
%% separate up & down reconstruction with individual fieldmaps
    P = hybridReconSVD(H_up,dataUp_pf,[],f_topup,[],0.99,0);
    %P_sos = sos(P);
    Pr = hybridReconSVD(H_down,dataDown_pf,[],f_topup,[],0.99,0);
    %Pr_sos = sos(Pr);
    

    
 %% get MUSE phase map by adding up all coils
    dmapFilt = smooth4D(sum(P./Pr,4),[3 3 3],0.5,'slice');
    dmapFilt(isnan(dmapFilt))=0;
    dmapFilt = exp(1i*angle(dmapFilt)); %keep only phase information, still complex
    
       
    
%% full combined reconstruction  
    Sdummy = CoilSensitivities(ones(H_up.N(1),H_up.N(2),size(P0,3),2),'dummy');
    Sdummy.Nc=1;
    Sdummy.smaps=ones(Sdummy.N);

   rcn = multiplexhybridReconSVD(H_up,H_down,dataUp_pf,dataDown_pf,Sdummy,angle(dmapFilt),f_topup,[],0.99,0); 

%% return fieldmap 
    f=f_topup;
    
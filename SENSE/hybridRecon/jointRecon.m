function [rcn, gmap]=jointRecon(dataUp,dataDown,w,S,H,fmap)
%performs a single volume joint reconstruction
%data must be zero-filled in case of partial Fourier 

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dataUp = [Nx Ns 1 Nsl Nc] ; 5d hybrid-space SENSE data format
%dataDown = [Nx Ns 1 Nsl Nc]
%w = generic_xxx.e data structure
%S = distortion corrected coil sensitivites
%H = array of HybridSenseTrajectories

if nargin < 6
    fmap =[];
end


[Nx, Ns,~, Nsl, Nc]  = size(dataUp);
N(1) = H(1).N(1);
N(2) = H(1).N(2);

      

%% separate eddy current corrected reconstructions
    S = S.applyMask();

    f = fmap.*S.supportMask;
    P_eddy= hybridReconSVD(H(1),dataUp(:,:,1,:,:),S,f,[],0.15,0);
    P_r_eddy = hybridReconSVD(H(2),dataDown(:,:,1,:,:),S,f,[],0.15,0);

    %low resolution 
    %P_eddy2= hybridReconSVD(Hlow,dataUp(Hlow.Xidx,Hlow.Yidx,1,:,:),S,f,[],0.15,0);

    
%% phase difference maps from corrected low-resolution scans
    %only smooth(average) within slices because different slices are
    %acquired at different times (different motions)
    dmapFilt = P_eddy./P_r_eddy; %no filtering seems to be ok here
    %dmapFilt = smooth4D(P_eddy,[3 3 3],2,'slice')./smooth4D(P_r_eddy,[3 3 3],2,'slice');
    dmapFilt(isnan(dmapFilt))=0;
    dmapFilt = exp(1i*angle(dmapFilt));
    dmapFilt = smooth4D(dmapFilt,[3 3 3],1.5,'slice');
    
    %resize phase difference maps to full resolution (easier on complex
    %data wth mag=1)
    %for t=1:Nt
    %   dmap(:,:,:,t) = angle(imresize3D(dmapFilt(:,:,:,t),[N(1) N(2) size(dmapFilt,3)]));
    %end
    dmap = angle(dmapFilt).*S.supportMask;

    
    
%% full combined reconstruction
    if nargout > 1
        [rcn,gmap]=multiplexhybridReconSVD(H(1),H(2),dataUp(:,:,1,:,:),dataDown(:,:,1,:,:),S,dmap,f,[],0.05,0);
    else
        rcn=multiplexhybridReconSVD(H(1),H(2),dataUp(:,:,1,:,:),dataDown(:,:,1,:,:),S,dmap,f,[],0.05,0);
    end

 
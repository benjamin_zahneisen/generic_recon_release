function [rcn, gmap]=runDWI_recon(dataUp,dataDown,w,H)
%prepares data and loops over volumes

%THe script assumes that dynamic fieldmaps are available as nifti-files

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dataUp = [Nx Ns Ni Nt Necho Nsl Nc]
%dataDown = [Nx Ns Ni Nt Necho Nsl Nc]
%H = array of HybridSenseTrajectories

%directory structure
DICO_DIR = 'dico'; %directory with fieldmaps as separate niftis (output from topup)
FMAP_BASE ='fmap_';
OUTDIR = 'joint';

unix(['mkdir ',OUTDIR]);

GENERIC_HOME=getenv('GENERIC_RECON_HOME');

%check if fieldmaps are available
try
    fmap = importNIFTIS(['./',DICO_DIR,'/',FMAP_BASE,num2str(0),'.nii.gz'],'generic');
    display(['using fieldmaps found in ',DICO_DIR])
catch
    display('no fieldmaps found')
    %unix('/home/bzahneisen/Dropbox/fslScripts/FSL_upDown_preprocessing.sh')
    home = getenv('GENERIC_HOME');
    unix([home,'/fsl/FSL_upDown_preprocessing.sh']); %has to be in PATH
    %return
    display('continue with joint recon...')
end
    

%Ns  = w.ny/w.inplane_R;
[Nx, Ns, Ni, Nt, Necho, Nsl, Nc]  = size(dataUp);
N(1) = H(1).N(1);
N(2) = H(1).N(2);

%make data 5d
dataUp = squeeze(dataUp);
dataDown = squeeze(dataDown);

%% partial Fourier extension
    H(1) = H(1).applyPartialFourier;
    dataUp = cat(2,zeros(Nx,H(1).pf_lines,Nt,Nsl,Nc),dataUp);
    H(2) = H(2).applyPartialFourier;
    dataDown = cat(2,zeros(Nx,H(2).pf_lines,Nt,Nsl,Nc),dataDown);
        
    
%% check if we have blip up down ref data
try 
    load('S_corr.mat')
    dataUp = applyCC(dataUp,S);
    dataDown = applyCC(dataDown,S);
catch
    load('dataRefHybrid.mat')
    %[rcnJoint, f_ref] = blipUpDownRefscans(data_ref_central,data_ref_centrald,w_ref_central);
    [rcnJoint, f_ref]=blipUpDownRefscansEPIdata(data_epi_ref,data_epi_ref2,w_ref,H_refUp,H_ref2);
    S = CoilSensitivities(rcnJoint,'espirit',0.02);
    S.fov = [10*w.fovx 10*w.fovy w.slquant*w.slthick];
    S = S.resize([w.nx w.ny w.slquant],[10*w.fovx 10*w.fovy S.fov(3)]);
    S.mask_thresh = 0.985;
    if(S.Nc > 12)
        S = S.compress(12); %compress data to 12 channels 
        dataUp = applyCC(dataUp,S);
        dataDown = applyCC(dataDown,S);
    end
end
save('S_corr.mat','S','f_ref')


%% make sure normal SENSE recon and hybrid-SENSE recon are in the same space
debug = false;
if debug
    Stmp = S;
    load('dataHybrid.mat','S')
    Sup = S; S=Stmp;   
    Sup = Sup.resize([w.nx w.ny w.slquant],[10*w.fovx 10*w.fovy S.fov(3)]);
    Sup = Sup.compress(12,S.gccmtx);
    Sup.applyMask;
    [P0, gmap] = hybridReconSVD(H(1),dataUp(:,:,1,:,:),Sup,[],[],0.015,0);
   BLIPUPDIR = 'blipUp';
   unix('fslroi ./blipUp/dti ./blipUp/tmp 0 1')
   Psense = importNIFTIS(['./',BLIPUPDIR,'/','tmp','.nii.gz']);
    figure; imagesc(abs(P0(:,:,20)))
    figure; imagesc(abs(Psense(:,:,20)))
end
    
    
%% full combined reconstruction
    rcn = zeros([w.nx w.ny w.slquant Nt]);
    for t=1:Nt
        
        try
            display(['reconstructing frame ',num2str(t)])
            %load fieldmap
            fmap = importNIFTIS(['./',DICO_DIR,'/',FMAP_BASE,num2str(t-1),'.nii.gz']);
            fmap = fmap*2*pi;
            %for now we have to flip the sign 
            %fmap = (-1)*fmap;
        
            %call recon
            rcn(:,:,:,t)=jointRecon(dataUp(:,:,t,:,:),dataDown(:,:,t,:,:),w,S,H,fmap);
            exportNIFTIS(abs(rcn(:,:,:,t)),['./',OUTDIR,'/dti_',num2str(t-1),'.nii'],H(1).voxSize);
        catch
           display('no fieldmap available') 
        end
    end
    
%% get g-map (if needed)
    if nargout > 1
        fmap = importNIFTIS(['./',DICO_DIR,'/',FMAP_BASE,num2str(0),'.nii.gz']);
        fmap = fmap*2*pi;
        [~, gmap]=jointRecon(dataUp(:,:,t,:,:),dataDown(:,:,t,:,:),w,S,H,fmap);
    end

    
%% export niftis
    %mask = S.supportMask;
    %cmd =['mkdir',OUTDIR]; unix(cmd); 
    exportNIFTIS(smooth4D(abs(rcn),[3 3 3],0.1),['./',OUTDIR,'/dti.nii'],H(1).voxSize);
    

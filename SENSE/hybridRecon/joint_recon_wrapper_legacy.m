%joint_recon_wrapper

%start matlab's parallel pool
Ncpu = 4; %number of cpu's to be used
parpool(Ncpu);

%% 1) call regular SENSE recon (create folders 'blipUp' and 'blipDown', store recons as dti.nii.gz)
%tic; generic_recon; toc %this step is now also performed by
%generic_recon2.m


%% 2) call generic_recon2 => re-load data and store it in hybrid-sense format (as dataHybrid.mat)
display('calling generic_recon2: ghost correction, data gridding, store raw data in folder [rawdata]')
generic_recon2_salvage('dosense',true,'Ncc',12);
%generic_recon2_salvage('dosense',true,'Ncc',12,'ts',1:60,'yoff',10);
%generic_recon2_salvage('dosense',true,'ts',1:24,'Ncc',32); %uncomment this and reconstruct only the first 4 frames for testing purposes


%% 3) run topup for all frames (use parallel computing toolbox, restart parpool manually if previous settings are expired)
    %get number of volumes
    [status,numvols] = unix(['fslnvols ./',getenv('UP_DIR'),'/dti.nii.gz']);
    display([numvols,' volumes found']);
    numvols=str2double(numvols);
    
    %split dti.nii.gz into separate volumes (as pre-processing step)
    unix(['mkdir ',getenv('DICO_DIR')]);
    unix([getenv('GENERIC_HOME'),'/fsl/FSL_upDown_prep.sh']);
    for t=0:(numvols-1)
       unix(['fslroi ./',getenv('UP_DIR'),'/dti.nii.gz',' ','./',getenv('DICO_DIR'),'/tmpUp',' ',num2str(t),' 1']);
       unix(['fslroi ./',getenv('DOWN_DIR'),'/dti.nii.gz',' ','./',getenv('DICO_DIR'),'/tmpDown',' ',num2str(t),' 1']);
       unix(['fslmerge -t ./',getenv('DICO_DIR'),'/V_',num2str(t),' ./',getenv('DICO_DIR'),'/tmpDown ./',getenv('DICO_DIR'),'/tmpUp']);
       display(['extracting volume ',num2str(t)]);
    end
    
    %parfor automatically starts multiple "workers" (if available)
    tic
    parfor t=0:(numvols-1)
        unix([getenv('GENERIC_HOME'),'/fsl/FSL_upDown_singleFrame.sh ',num2str(t),''],'-echo');
    end
    toc
    
    
    %merge all volumes to one timeseries
    for t=1:(numvols-1)
        [P(:,:,:,t),voxSize] = importNIFTIS(['./',getenv('DICO_DIR'),'/Vout_',num2str(t-1),'.nii.gz'],'generic');
    end
    exportNIFTIS(P,['./',getenv('DICO_DIR'),'/dti.nii'],voxSize,'generic');

    
%% 4) run the joint recon and write data to "joint" directory
display('run the joint recon. Run topup first if fieldmaps are not yet available')
load('S_corr.mat','w_imag');
tic; [rcn,gmap]=runJointUpDown_recon(w_imag); toc
save('gmap.mat','gmap');

%% closing the parallel pool
delete(gcp)

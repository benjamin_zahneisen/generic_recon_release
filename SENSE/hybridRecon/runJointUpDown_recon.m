function [rcn, gmap]=runJointUpDown_recon(w,frames)
%prepares data and loops over volumes

%THe script assumes that dynamic fieldmaps are available as nifti-files

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%no data input

%directory structure
GENERIC_HOME=getenv('GENERIC_RECON_HOME');
DICO_DIR = getenv('DICO_DIR');
FMAP_BASE = getenv('FMAP_BASE');
OUTDIR = getenv('OUTDIR_JOINT');
RAWDIR = getenv('RAWDIR');
RAW_BASE = getenv('RAW_BASE');

unix(['mkdir ',OUTDIR]);

%check if fieldmaps are available
try
    fmap = importNIFTIS(['./',DICO_DIR,'/',FMAP_BASE,num2str(0),'.nii.gz'],'generic');
    display(['using fieldmaps found in ',DICO_DIR])
catch
    display('no fieldmaps found')
    %unix('/home/bzahneisen/Dropbox/fslScripts/FSL_upDown_preprocessing.sh')
    home = getenv('GENERIC_RECON_HOME');
    unix([home,'/fsl/FSL_upDown_preprocessing.sh']); 
    %return
    display('continue with joint recon...')
end
    

%check if fieldmaps are already available
    tmp = load([RAWDIR,'/',RAW_BASE,'_1.mat']);
    names = fieldnames(tmp);
    hybSENSEdat = tmp.(names{1});%dynamic field reference
    if nargin < 2
        Nt = hybSENSEdat.Nt;
        frames = 1:Nt;
    else
        Nt = max(frames);
    end
    if(isempty(hybSENSEdat.fmap))
        for t=frames
            try
                load(['./',RAWDIR,'/',RAW_BASE,'_',num2str(t),'.mat']);
                fmap = importNIFTIS(['./',DICO_DIR,'/',FMAP_BASE,num2str(t-1),'.nii.gz'],'generic');
                fmap = -fmap*2*pi; %-1 if down=-1 and up=1 in acqp.txt
                if(size(fmap,3)-w.slquant == 2)
                   fmap = fmap(:,:,2:end-1); %that means we had two dummy slices 
                end
                hybSENSEdat.fmap = fmap;
                save(['./',RAWDIR,'/',RAW_BASE,'_',num2str(t),'.mat'],'hybSENSEdat');
            catch
                display(['failed to import fieldmap at tp= ',num2str(t)]);
            end
        end
    else
        display('fieldmaps already stored in data')
    end


        
    
%% check if we have blip up down ref data
try 
    load('S_corr.mat')
    %dataUp = applyCC(dataUp,S);
    %dataDown = applyCC(dataDown,S);
    S=S_corr;
catch
    load('dataRefHybrid.mat')
    %[rcnJoint, f_ref] = blipUpDownRefscans(data_ref_central,data_ref_centrald,w_ref_central);
    [rcnJoint, f_ref]=blipUpDownRefscansEPIdata(data_epi_ref,data_epi_ref2,w_ref,H_refUp,H_ref2);
    S = CoilSensitivities(rcnJoint,'espirit',0.02);
    S.fov = [10*w.fovx 10*w.fovy w.slquant*w.slthick];
    S = S.resize([w.nx w.ny w.slquant],[10*w.fovx 10*w.fovy S.fov(3),'nearest']);
    S.mask_thresh = 0.985;
    if(S.Nc > 12)
        %S = S.compress(12); %compress data to 12 channels 
        %dataUp = applyCC(dataUp,S);
        %dataDown = applyCC(dataDown,S);
    end
    save('S_corr.mat','S','f_ref');
end



%% make sure normal SENSE recon and hybrid-SENSE recon are in the same space
debug = false;
if debug
    Stmp = S;
    load('dataHybrid.mat','S')
    Sup = S; S=Stmp;   
    Sup = Sup.resize([w.nx w.ny w.slquant],[10*w.fovx 10*w.fovy S.fov(3)]);
    Sup = Sup.compress(12,S.gccmtx);
    Sup.applyMask;
    [P0, gmap] = hybridReconSVD(H(1),dataUp(:,:,1,:,:),Sup,[],[],0.99,0);
   BLIPUPDIR = 'blipUp';
   unix('fslroi ./blipUp/dti ./blipUp/tmp 0 1')
   Psense = importNIFTIS(['./',BLIPUPDIR,'/','tmp','.nii.gz']);
    figure; imagesc(abs(P0(:,:,20)))
    figure; imagesc(abs(Psense(:,:,20)))
end
    
    
%% full combined reconstruction
    rcn = zeros([w.nx w.ny w.slquant Nt]);
    tic
    %parfor seems to make a lot of sense parfor(8)=840s; 1 frame = 256s
    parfor t=frames     
        try
            display(['reconstructing frame ',num2str(t)])
            rcn(:,:,:,t)=jointRecon_singleFrame(t,S);
            %exportNIFTIS(abs(rcn(:,:,:,t)),['./',OUTDIR,'/dti_',num2str(t-1),'.nii'],hybSENSEdat.H(1).voxSize,'generic');
        catch
           display('no fieldmap available or error during jointRecon_singleFrame(t,S)') 
        end
    end
    toc
    
%% get g-map (if needed)
    if nargout > 1
        [~, gmap]=jointRecon_singleFrame(1,S);
    end

%% export niftis
    %mask = S.supportMask;
    %cmd =['mkdir',OUTDIR]; unix(cmd); 
    exportNIFTIS(smooth4D(abs(rcn),[3 3 3],0.5),['./',OUTDIR,'/dti.nii'],hybSENSEdat.H(1).voxSize,'generic'); %3d smoothing on magnitude data
    

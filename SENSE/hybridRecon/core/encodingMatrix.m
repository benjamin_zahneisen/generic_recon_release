function E = encodingMatrix(N,Kyz)
%returns the encoding matrix (=matrix representation of FT) along the y and z
%dimension

%N = [Ny Nz] dimensions
%Kyz = [2 x Ns]; x,y times number of samples

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Kyz = [2 x Ns]; x,y times number of samples
if nargin < 2
    %return fully sampled encoding matrix
    for n=1:N(2)
        Kyz(1,(n-1)*N(1)+1:n*N(1)) = linspace(-pi,pi-pi/(N(1)/2),N(1));
        Kyz(2,(n-1)*N(1)+1:n*N(1)) = -pi + (2*pi/N(2))*(n-1);
    end
end

%spatial grid
if length(N)==1
    [ygrid, zgrid]=ndgrid(-N(1)/2:N(1)/2-1,0);
else
    [ygrid, zgrid]=ndgrid(-N(1)/2:N(1)/2-1,-N(2)/2:N(2)/2-1);
    %[ygrid, zgrid]=ndgrid(-N(1)/2:N(1)/2-1,[-1.5 0 1]); %try this one
end    
r(:,1) = ygrid(:);
r(:,2) = zgrid(:);


%pure k-space encoding (does not depend on x-location)
E = exp(-1i*(r*Kyz));
E = E.';
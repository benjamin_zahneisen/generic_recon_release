function [rcn,gmap,Xnoise] = hybridReconSVD(H,data,S,wmap,T2s,threshold,debugLevel)
%core function that performs a truncated-SVD-hybrid-SENSE reconstruction

%H = Hybrid-SENSE trajectory object
%data =[Nx Ns Nt Nsl Nc]
%S = CoilSensitivity object
%wmap = fieldmap [rad/s]
%T2s = T2 star map
%threshold = relative
%plot_flag = obvious! 

%Benjamin Zahneisen, Stanford University, 05/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if nargin < 7
    debugLevel = 0;
end
if nargin < 6
    threshold = [];
end
if nargin < 5
    T2s = [];
end
if nargout > 1
    getGmap = true;
else
    getGmap = false;
end

N = H.N;
Ns = size(data,2);
Nc=size(data,5);
Nt = size(data,3);
Nsl_eff = size(data,4);
tvec = H.tvec;

%multi-band slice index
if(~isempty(S))
    mb=S.N(3)/Nsl_eff;
    mbIdx = 1:Nsl_eff:Nsl_eff*mb;
else
    mbIdx =1:N(3);
    mb=N(3);
end

E = encodingMatrix([H.N(2) H.N(3)],H.Kyz_pi);
Rtot = size(E,2)/Ns;

%in case of odd number of bands (e.g mb=3,5,...) do weird phase shift on
%data
if(mb == 3) 
    p = exp(1i*H.Kyz_pi(2,:)/2); 
    p =repmat(p,[N(1) 1 Nt Nsl_eff Nc]);
    data = data.*p;
end

%prepare raw data
hybr = permute(data,[1 2 5 3 4]);
%ifft along x
hybr = ifff(hybr,1);
hybr = reshape(hybr,[N(1) Ns*Nc Nt Nsl_eff]);

rcn = zeros(N(1),N(2),Nsl_eff*mb,Nt);

if getGmap
    Xnoise = zeros(N(1),N(2),Nsl_eff*mb);
    Xfull = zeros(N(1),N(2),Nsl_eff*mb);
end


for sl=1:Nsl_eff
    for x=1:N(1)
        
        %check if all voxels are empty
        if(~isempty(S))
            coilvoxels = col(abs(S.smaps(x,:,mbIdx+sl-1,:)));
        else
            coilvoxels = 1;
        end
            
        if(all(coilvoxels<0.000001))
            rcn(x,:,mbIdx+sl-1,:)=0; %set complete line to zero - no inversion
        else
        
            %coil sensitivites 
            if(~isempty(S))
                Esense = multiCoilEncoding(E,reshape(S.smaps(x,:,mbIdx+sl-1,:),[size(S.smaps,2) mb Nc]));
                F = Esense;
            else
                F = E; 
            end
        
            %fieldmap
            if(~isempty(wmap))
                Theta=fieldmapEncoding(wmap(x,:,mbIdx+sl-1),Ns,tvec);
                Theta = repmat(Theta,[Nc 1]);
                F = F.*Theta;
            end
        
            %T2star
            if(~isempty(T2s))
                Tau=T2starEncoding(N(2)*mb,50/1000,Ns,tvec,Ns/2);
                Tau = repmat(Tau,[Nc 1]);
                F = F.*Tau;
            end
            
            %regularized inversion
            lambda = 2;
            iF = truncatedSVD(F,threshold,lambda,(debugLevel > 1));
            tmp = iF*reshape(hybr(x,:,:,sl),[Ns*Nc Nt]);
            rcn(x,:,mbIdx+sl-1,:) = reshape(tmp,[N(2) N(3) Nt]);
        
            if getGmap
                %calculate noise propagation through reconstruction process
                %Efull = encodingMatrix([H.N(2) H.N(3)]);
                %EfullSense = multiCoilEncoding(Efull,reshape(S.smaps(x,:,mbIdx+sl-1,:),[size(S.smaps,2) mb Nc]));
                %iE = pinv(EfullSense);
                %covFull = iE*iE';
            
                C = multiCoilEncoding([],reshape(S.smaps(x,:,mbIdx+sl-1,:),[size(S.smaps,2) mb Nc]));
                covFull = C'*C; %around 1
                covFull = covFull/(N(2)*N(3));
            
                Xfull(x,:,mbIdx+sl-1) = reshape(diag(covFull),[N(2) N(3)]);
                covX = iF*iF';
                Xnoise(x,:,mbIdx+sl-1) = reshape(diag(covX),[N(2) N(3)]);
            end        
        end
    end
end

if getGmap
    gmap = sqrt(Xnoise./Xfull)./sqrt(Rtot); %that produces the same g-map as in senseOperator.m (theory comes later)
    gmap(isnan(gmap))=0;
    gmap(isinf(gmap))=0;
end

%toc
if debugLevel > 0 
    if size(rcn,3)>1
        figure; imagesc(array2mosaic(abs(rcn(:,:,:,1)))); colormap gray
    else
        figure; imagesc(abs(rcn(:,:,1,1))); colormap gray
    end
end
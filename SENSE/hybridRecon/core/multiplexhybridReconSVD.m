function [rcn,gmap] = multiplexhybridReconSVD(HybridParams,Hyb2,data1,data2,S,phaseMap,wmap,R2star,threshold,debugLevel)

%N = [Nx Ny Nz]
%Kyz = [dim Ns]; k-space locations of acquired samples [-pi pi]
%data =[Nx Ny Nt Nsl Nc]
%S = CoilSensitivity object
%phaseMap = spatially varying phase offset map  (gets incorporated to 2nd
%set of coil sensitivites)
%wmap = fieldmap [rad]
%R2star = R2star map in [ms]

%performs a truncated-SVD-hybrid-SENSE reconstruction with multiplex phase correction for multiple excitations 

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 8
    debugLevel = 0;
end

if nargout > 1
    getGmap = true;
else
    getGmap = false;
end

N=HybridParams.N;
Ns1 = size(HybridParams.Kyz,2) ; %number of samples (within EPI train) (first readout)
Ns2 = size(Hyb2.Kyz,2) ; %number of samples (within EPI train) (2nd readout)
Ns = Ns1 + Ns2;
Nc = size(data1,5);               %number of coils
Nt = size(data1,3);             %number of time points (along TR)
Nsl = size(data1,4);            %number of slices
Nsl_eff = size(data1,4);

tvec1= HybridParams.tvec;
tvec2= Hyb2.tvec;

E1 = encodingMatrix([N(2) N(3)],HybridParams.Kyz_pi);

E2 = encodingMatrix([N(2) N(3)],Hyb2.Kyz_pi);

E = cat(1,E1,E2);

Rtot = size(E,2)/Ns;

%multi-band slice index
mb=S.N(3)/Nsl_eff;
mbIdx = 1:Nsl_eff:Nsl_eff*mb;

%in case of odd number of bands (e.g mb=3,5,...) do weird phase shift on
%data
if(mb == 3) 
    p = exp(1i*HybridParams.Kyz_pi(2,:)/2); 
    p =repmat(p,[N(1) 1 Nt Nsl_eff Nc]);
    data1 = data1.*p;
    p = exp(1i*Hyb2.Kyz_pi(2,:)/2); 
    p =repmat(p,[N(1) 1 Nt Nsl_eff Nc]);
    data2 = data2.*p;
end

%combined data1 and data2
data=cat(2,data1,data2);

%just in case
phaseMap(isnan(phaseMap))=0;

%re-generate 2nd set of coil sensitivities
smaps2 = zeros(size(S.smaps));
for n=1:Nc
    smaps2(:,:,:,n) = S.smaps(:,:,:,n).*conj(exp(1i*phaseMap));
end

%prepare raw data
hybr = permute(data,[1 2 5 3 4]);
%ifft along x
hybr = ifff(hybr,1);
hybr = reshape(hybr,[N(1) size(data,2)*Nc Nt Nsl_eff]);

rcn = zeros(N(1),N(2),Nsl*mb,Nt);

if getGmap
    Xnoise = zeros(N(1),N(2),Nsl_eff*mb);
    Xfull = zeros(N(1),N(2),Nsl_eff*mb);
end


%tic
for sl=1:Nsl_eff;
    for x=1:N(1)
        %check if all voxels are empty
        if(all(col(abs(S.smaps(x,:,mbIdx+sl-1,:)))<0.000001))
            rcn(x,:,mbIdx+sl-1,:)=0;
        else
            if(~isempty(S))
                Esense =  multiCoilMultiplexEncoding(E1,E2,reshape(S.smaps(x,:,mbIdx+sl-1,:),[size(S.smaps,2) mb Nc]),reshape(smaps2(x,:,mbIdx+sl-1,:),[size(S.smaps,2) mb Nc]));
            else
                Esense = E; 
            end
            if(~isempty(wmap))
                Theta1=fieldmapEncoding(wmap(x,:,mbIdx+sl-1),Ns1,tvec1);
                Theta2=fieldmapEncoding(wmap(x,:,mbIdx+sl-1),Ns2,tvec2);
                Theta=cat(1,Theta1,Theta2);
            else
                Theta = ones(size(E1));
                Theta=cat(1,Theta,Theta);
            end
            Theta = repmat(Theta,[Nc 1]);
            
            if(~isempty(R2star))
                t2s = R2star(x,:,mbIdx+sl-1)/1000; %[ms]
                Tau=T2starEncoding(N(2)*mb,t2s,Ns,tvec,Ns/2);
                Tau=cat(1,Tau,Tau);
                Tau = repmat(Tau,[Nc 1]);
            else
                Tau = ones(size(Theta));
            end
        
            lambda = 2;
            iF = truncatedSVD(single(Esense.*Theta.*Tau),threshold,lambda,(debugLevel > 1));
            rcn(x,:,mbIdx+sl-1,:) = reshape(iF*reshape(hybr(x,:,:,sl),[size(data,2)*Nc Nt]),[N(2) N(3) Nt]);
        
            if getGmap
                %calculate noise propagation through reconstruction process
                C = multiCoilEncoding([],reshape(S.smaps(x,:,mbIdx+sl-1,:),[size(S.smaps,2) mb Nc]));
                covFull = C'*C; %around 1
                covFull = covFull/(N(2)*N(3));
                Xfull(x,:,mbIdx+sl-1) = reshape(diag(covFull),[N(2) N(3)]);
                
                covX = iF*iF';
                Xnoise(x,:,mbIdx+sl-1) = reshape(diag(covX),[N(2) N(3)]);
            end
        end 
    end
    %sl;
end


if getGmap
    gmap = sqrt(Xnoise./Xfull)./sqrt(Rtot); %that produces the same g-map as in senseOperator.m (theory comes later)
    gmap(isnan(gmap))=0;
    gmap(isinf(gmap))=0;
end

%toc
if (debugLevel > 0)
    if size(rcn,3)>1
        figure; imagesc(array2mosaic(abs(rcn(:,:,:,1)))); colormap gray
    else
        figure; imagesc(abs(rcn(:,:,1,1))); colormap gray
    end
end
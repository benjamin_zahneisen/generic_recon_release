% Hybrid SENSE trajectory information 
%

% 
% Author: Benjamin Zahneisen
% benjamin.zahneisen@gmail.com
% 01 Feb 2017

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef HybridSenseTrajectory
    properties
        N           = []; %target resolution (=reconstruction grid)
        Ns          = []; % number of samples (=lines) including PF
        Norg        = []; %nominal resolution
        Fov         = []; %nominal fov
        voxSize     = []; %nominal voxel size
        ryz         = []; %voxel coordiantes [mm] 
        Nlines      = []; %how many acquired lines along y (no PF)
        Ry          = []; %in-plane acceleration along y (without kz blips)
        adc_dwell   = 4/(1000*1000); %time between two k-space samples [s]
        grad_time   = 4/(1000*1000); %time between two gradient updates [s]
        totalRO     = []; % total readout time
        effRO       = []; % effective readout time (including partial Fourier)
        lineDwell   = []; % time between two EPI lines 
        tvec        = []; %vector of time points for each line
        Kyz         = []; %ky-kz-k-space vector [rad/mm];
        Kyz_pi      = []; %ky-kz-k-space vector [-pi pi]; (Kyz * r)
        kx          = [];
        kxmax       = [];
        kymax       = [];
        kzmax       = [];
        Yidx        = []; %which samples of Cartesian grid
        Xidx        = []; %
        pf          = []; %partial fourier factor
        pf_lines    = []; %number of skipped partial fourier lines
        pf_flag     = false; %object accounts for pf or not
        evenOdd     = []; %even-odd index for ghost correction
     end
    
    methods
        %% constructor
        function H = HybridSenseTrajectory(w,ilv)
            %w = generic scan info
            %w_epi = structure describes hybrid-space trajectory
            
            %which interleave of w structure
            if nargin < 2
                ilv=1;
            end
            
            H.N = [w.nx w.ny w.epi_multiband_factor];
            H.Norg=[w.nx w.ny w.epi_multiband_factor];
            H.Fov = [w.fovx*10 w.fovy*10 w.epi_multiband_fov];
            %H.Ns = w.ny/w.inplane_R;
            H.voxSize = [w.fovx*10/w.nx w.fovy*10/w.ny w.slthick];
            H.Ry = w.inplane_R;
            H.pf = w.fn;
            
            %spatial grid vector
            if H.N(3)==1
                [ygrid, zgrid]=ndgrid((-H.N(2)/2:H.N(2)/2-1)*H.voxSize(2),0);
            else
                [ygrid, zgrid]=ndgrid((-H.N(2)/2:H.N(2)/2-1)*H.voxSize(2),(-H.N(3)/2:H.N(3)/2-1)*H.voxSize(3));
            end     
            H.ryz(:,1) = ygrid(:);
            H.ryz(:,2) = zgrid(:);
            
            nptsperline=2*w.epi_ramp_res + w.epi_flat_res;
            H.Nlines=(w.gx_end-w.gx_start+1)/nptsperline;
            H.Ns = H.Nlines;
            H.lineDwell = nptsperline*w.dts_adc;
            H.adc_dwell = w.dts_adc;
            
            ky=w.ky_all(w.gx_start+1:w.gx_end+1,:);
            kz=w.kz_all(w.gx_start+1:w.gx_end+1,:);
            %extract plateaus (=ky-kz per line) 
            ky = reshape(ky,[nptsperline H.Nlines w.ni]);
            kz = reshape(kz,[nptsperline H.Nlines w.ni]);
            
            ky = reshape((ky(round(nptsperline/2),:,:)),[H.Nlines w.ni]);
            kz = reshape((kz(round(nptsperline/2),:,:)),[H.Nlines w.ni]);
            H.kx = (-w.nx/2:w.nx/2-1)*2*pi/w.fovx/10 ; %corresponds to the grid after ramp sampling correction
            
            %get kmax and dk
            H.kxmax = w.nx/2*(2*pi/w.fovx/10); %[?]
            H.kymax = w.ny/2*(2*pi/w.fovy/10); %[?]
            H.kzmax = (w.epi_multiband_factor/2)*(2*pi/w.epi_multiband_fov); %[?]
            
            H.Kyz(1,:)=ky(:,ilv);
            H.Kyz(2,:)=kz(:,ilv);
            H.Kyz_pi(1,:) = H.Kyz(1,:)/H.kymax*pi;
            H.Kyz_pi(2,:) = H.Kyz(2,:)/H.kzmax*pi;
            
            %timting
            H.effRO = (H.lineDwell*H.N(2))/H.Ry;
            H.totalRO = H.lineDwell*H.Nlines;
            for n=1:H.Nlines; H.tvec(n) = (n-1)*H.lineDwell;end
            
            H.Xidx = 1:w.nx;
            H.Yidx = 1:H.Ns;

        end

        %% make pf extension
        function H=applyPartialFourier(H)
                
                if(H.pf_flag)
                    display('object already transformed. No action.')
                else
                    %partial fourier %symmetrical around center of k-space
                    %H.pf_lines = (1/H.pf)*H.Nlines - H.Nlines;
                    H.pf_lines = (H.N(2) - (H.Nlines*H.Ry))/H.Ry; 
                    dky = H.Kyz(1,1) - H.Kyz(1,2);
                    ky_pf = (H.pf_lines:-1:1)*dky + H.Kyz(1,1);
                    ky = cat(2,ky_pf,H.Kyz(1,:));
                    %periodicity for blips (left periodic exxtension)
                    kz = wextend('1d','ppd',H.Kyz(2,:),H.pf_lines,'l');                   
                    H.Kyz=zeros(2,H.Nlines + H.pf_lines);
                    H.Kyz(1,:) = ky;
                    H.Kyz(2,:) = kz;
                    H.Kyz_pi=zeros(2,H.Nlines + H.pf_lines);
                    H.Kyz_pi(1,:) = H.Kyz(1,:)/H.kymax*pi;
                    if(~(H.kzmax==0))
                        H.Kyz_pi(2,:) = H.Kyz(2,:)/H.kzmax*pi;
                    end
                    for n=1:H.pf_lines; tmp(n) = -(n)*H.lineDwell + H.tvec(1);end
                    H.tvec = cat(2,tmp(end:-1:1),H.tvec);
                    
                    H.Ns = H.Nlines + H.pf_lines;
                    H.evenOdd = cat(1,zeros(H.pf_lines,1),H.evenOdd);
                    H.Yidx = 1:H.Ns;
                    H.pf_flag = true;
                end
        end
        
        function H = scale(H,ia)
           %corresponds to changing/inverting the instruction amplitude in the psd
           
           if(length(ia)==1)
               ia(2) = 1;
           end
           
           H.Kyz(1,:) = ia(1)*H.Kyz(1,:);
           H.Kyz(2,:) = ia(2)*H.Kyz(2,:);
           H.Kyz_pi(1,:) = H.Kyz(1,:)/H.kymax*pi;
           if(~(H.kzmax == 0) )
                H.Kyz_pi(2,:) = H.Kyz(2,:)/H.kzmax*pi;
           else
               H.Kyz_pi(2,:) = 0;
           end
        end

        %% get subset of samples
        function H=getSubset(H,idx)
            
            %idx = k-space sample index (line index)
            H.tvec = H.tvec(idx);
            H.Kyz = H.Kyz(:,idx);
            H.Kyz_pi = H.Kyz_pi(:,idx);
            H.evenOdd = H.evenOdd(idx);
            H.Nlines = length(idx);
            H.Ns = length(idx);
            
            %timting
            H.effRO = (H.lineDwell*H.N(2))/H.Ry;
            H.totalRO = H.lineDwell*H.Nlines;
                      
            
        end
        
        %% change recon grid(=change voxel size)
        function H=resizeGrid(H,N)
            %N = new resolution [Nx Ny] no change in Nz
            
            if (numel(N)==1)
                N = [N N];
            end                    
            
            H.N(1) = N(1);
            H.N(2) = N(2);
            H.voxSize(1) = H.Fov(1)/H.N(1);
            H.voxSize(2) = H.Fov(2)/H.N(2);
            
            % new spatial grid vector
            if H.N(3)==1
                [ygrid, zgrid]=ndgrid((-H.N(2)/2:H.N(2)/2-1)*H.voxSize(2),0);
            else
                [ygrid, zgrid]=ndgrid((-H.N(2)/2:H.N(2)/2-1)*H.voxSize(2),(-H.N(3)/2:H.N(3)/2-1)*H.voxSize(3));
            end
            tmp(:,1) = ygrid(:);
            tmp(:,2) = zgrid(:);
            H.ryz = tmp;
            
            %get kmax and dk for new H.N
            H.kxmax = H.N(1)/2*(2*pi/H.Fov(1)); %[rad/mm]
            H.kymax = H.N(2)/2*(2*pi/H.Fov(2)); %[rad/mm]
            
            H.Kyz_pi(1,:) = H.Kyz(1,:)/H.kymax*pi;
            if(~(H.kzmax == 0))
            H.Kyz_pi(2,:) = H.Kyz(2,:)/H.kzmax*pi;
            end
            
            H.Xidx = floor(H.Norg(1)/2)+1+ceil(-N(1)/2) : floor(H.Norg(1)/2)+ceil(N(1)/2);
            
        end
        
        %% resize reconstruction grid
         function H=resize(H,N)
             
            %low resolution (i.e 64x64) reconstruction around k-space center
            if nargin < 2
                Nlow = 80;
            else
                Nlow = N;
            end
            sy = Nlow/H.Ry;
            sx = Nlow;
            %Ns = w.ny/w.inplane_R; %number of acquired samples along y
            H.Yidx = floor(H.Ns/2)+1+ceil(-sy/2) : floor(H.Ns/2)+ceil(sy/2);
            H.Xidx = floor(H.Norg(1)/2)+1+ceil(-sx/2) : floor(H.Norg(1)/2)+ceil(sx/2);

            H.tvec = H.tvec(H.Yidx);

            H.Kyz = H.Kyz(:,H.Yidx);
            H.Kyz(1,:)=H.Kyz(1,:)*(H.Norg(2)/Nlow); %let trajectory go from -pi to pi
            %H.Kyz_down = H.Kyz_down(:,H.Yidx);
            %H.Kyz_down(1,:)=H.Kyz_down(1,:)*(H.Norg(2)/Nlow); %let trajectory go from -pi to pi
            H.N = [Nlow Nlow H.Norg(3)];
            H.voxSize = [H.voxSize(1)*H.Norg(1)/H.N(1) H.voxSize(2)*H.Norg(2)/H.N(2) H.voxSize(3)];
            
            H.totalRO = H.totalRO/H.Norg(2)*H.N(2);
            H.effRO = H.effRO/H.Norg(2)*H.N(2);
             
         end
         
         
         %%plot trajectory
         function plot(H)
            figure; plot(H.Kyz(1,:),H.Kyz(2,:),'o')
            hold on
            plot(H.Kyz_down(1,:),H.Kyz_down(2,:),'ro')
         end
    end
    
end
function Theta=fieldmapEncoding(fmap,Ns,tvec)

%tvec = dwell time(s) in [s]

if length(tvec)==1
    %we assume that's the dwell time 
    tvec = (tvec*Ns)*linspace(0,1,Ns);
end

Theta = ones([Ns,length(fmap(:))]);
for t=1:Ns 
    Theta(t,:) = squeeze(exp(1i*fmap(:)*tvec(t)));
end

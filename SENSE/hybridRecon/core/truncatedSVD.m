function iF = truncatedSVD(F,threshold,lambda,debugFlag)
%performs truncated SVD inversion of matrix F

%F          = input rectangular matrix 
%threshold  = cutoff for singular values (in percent relative to sqrt(size(F,2))


%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%threshold is relative to sqrt(size(F,2)
if ((nargin < 2) || isempty(threshold))
    threshold = 0.99;
end
if( (nargin < 3) || (isempty(lambda)) )
    lambda = sqrt(size(F,2))*0.01;
    %lambda = 0.5;
end
if nargin < 4
    debugFlag = false;
end

%in rare cases the svd does not converge. 
try
    [U,S,V] = svd(F,'econ');
    s = diag(S);
catch
    try
        F(end,:)=0;
        [U,S,V] = svd(F(1:end-3,:),'econ');
        U(size(F,1)-2:size(F,1),:)=0;
        s = diag(S);
        display('SVD warning: reduced F')
    catch
    U = 0;
    iF = F';
    iF(:)=0;
    iF(1:size(F,2),1:size(F,2))=0.001;
    display('SVD warning')
    return;
    end
end


%fullySampledEigenvalue = sqrt(size(F,2));
%threshold = threshold*fullySampledEigenvalue; BZ 2017_10_11 switch to area
%under eigenvalue plot method to determine variable threshold

f = cumsum(s)/sum(s);
cutoff_idx =  find(f > threshold,1);
cutoff_idx = cutoff_idx + 1; %BZ I don't know why but moving the cutoff by 1 place seems to make a huge difference
%cutoff = s(cutoff_idx);

%filter
fi = s./(s + lambda);   

%truncate eigenvalues and "filter"
is = (1./s).*fi;
is(cutoff_idx:end) = 0;

%we perform "economy size decomposition" ==> S is n x n 
iF = V*(diag(is))*U';

if debugFlag
    plot(s);hold on; plot(cutoff_idx,s(cutoff_idx),'ro')
    plot(s.*fi,'g')
    %plot(is,'c')
    drawnow
    hold off
end 


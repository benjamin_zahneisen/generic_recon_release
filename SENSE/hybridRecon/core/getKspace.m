function Kyz = getKspace(w)


%find plateaus
dky = abs(diff(w.ky_all(:,1)));

blips = (dky > 0);

firstRamp = find(blips,1);

n=1;
m=1;
k=1;
while(m < length(dky))

    while ((dky(m) == 0) && (m < length(dky)))
        n=n+1;
        m=m+1;
    end
    beginRamp(k) = m;
    while (dky(m) > 0)
        m=m+1;
    end
    endRamp(k)=m;
    k=k+1;
end

plateauLength = beginRamp(1);
for n=1:length(beginRamp)
    Kyz(n,1) = w.ky_all(beginRamp(n)-round(plateauLength/2),1);
    Kyz(n,2) = w.kz_all(beginRamp(n)-round(plateauLength/2),1);
end


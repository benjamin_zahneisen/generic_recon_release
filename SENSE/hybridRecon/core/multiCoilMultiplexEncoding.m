function Esense = multiCoilMultiplexEncoding(E1,E2,C1,C2)
%returns encoding matrix with coil weighting coefficients

%C = Ny x Nz x Nc
Nc = size(C1,length(size(C1)));

C1=reshape(C1,[size(C1,1)*size(C1,2) Nc]);
C2=reshape(C2,[size(C2,1)*size(C2,2) Nc]);

N = size(C1);
Ns1 = size(E1,1);
Ns2 = size(E2,1);

Esense=zeros((Ns1+Ns2)*Nc,size(E1,2));
for c=1:Nc
    tmpC1= repmat(C1(:,c).',[Ns1 1]);
    tmpC2= repmat(C2(:,c).',[Ns2 1]);
    tmp = cat(1,tmpC1,tmpC2).*cat(1,E1,E2);
    %Esense = cat(1,Esense,tmp);
    Esense(1+(c-1)*(Ns1+Ns2):c*(Ns1+Ns2),:)=tmp;
end

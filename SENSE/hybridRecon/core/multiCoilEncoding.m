function Esense = multiCoilEncoding(E,C)
%returns encoding matrix with coil weighting coefficients

% E = [Ns x Nr] = number of k-space samples x number of voxels
% C = [Ny x Nz x Nc]

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Nc = size(C,length(size(C)));

N = size(C);

if (isempty(E))
    Esense = reshape(C,[N(1)*N(2) Nc]); %[N(1) Ngrid 1 Nc]
    Esense = Esense.'; %dims = [Nc Ngrid]
else
    %normal case
    Ns = size(E,1);

    %replicate coefficient matrix
    C = reshape(C,[N(1)*N(2) 1 Nc]); %[N(1) Ngrid 1 Nc]
    C = repmat(C,[1 Ns 1]);
    C = reshape(C,[N(1)*N(2) Ns*Nc]);

    E = repmat(E,[Nc 1]);

    Esense = E.*C.';
end
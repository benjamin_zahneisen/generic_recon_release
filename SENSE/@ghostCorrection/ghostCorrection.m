classdef ghostCorrection
    
    properties (SetAccess = public, GetAccess = public)
        N           = []; 
        dim         = [];
        Ni          = 1;
        Nsl         = [];       % number of slices
        Nc          = [];       % number of coils
        recontype   = 'sense';  % or 'cg'
        paramRaw    = [];       % calculated phase correction (0th/1th/...-order) [order Nc Ns]
        paramInterp = [];       % interpolated phase correction (if slcInc > 1, coil average,...)
        paramMean   = [];       % global ghost correction paramters (mean over slices) 
        isMultiband = false;  % indicated a fully sampled multi-band scan
        orientation = 'axial';  % options: 'sag','cor','oblique';
        method      = 'entropy';% options: 'ref'
        scanId      = [];       % origin of ghost parameters
        oddLines    = [];       % which lines are odd ([N(1) N(2) N(3)] 
        opts        = [];       % parameter set for fminsearch...
        paramInit   = [];    % initial search parameters
        coilMode    = 'avg';    % coil dependent ghost paramters
        sliceMode   = 'slice-by-slice'; %slice dependent ghost paramters
        slcInc      = 1;        % find separate ghost parameters for every n-th slice
        isTrans     = 0;        % set this to one if it was transformed to MB-scan
        debug       = 0;        % store additional debug information
        recon       = [];       % store recons with optimal entropy 
        
    end
    
    methods
        %% Constructor
        function GC=ghostCorrection(N,oddLines)
            if(length(N)==4)
                GC.dim = 4;
            else
                GC.dim = 3;
                N(4) = 1;
            end

            GC.N = N;
            if(GC.N(3)==1)
                GC.dim = 2;
            end
            GC.oddLines = oddLines;
            
            GC.opts = optimset('fminsearch');
            GC.opts.MaxIter = 40;
            GC.opts.Display = 'none'; % try 'iter' for debugging
            GC.opts.TolX=1e-7;
            GC.opts.TolFun=1e-7;
        end
        
        
        %% actual entropy minimization (for a single recon)
        function optParams=findGhostParameters(GC,data,p0)
            if nargin < 3
                p0 = GC.paramInit;
            end
            GC.opts.MaxIter = 35;
            optParams=fminsearch(@(param) entropyCostFunction(param,GC,data),p0,GC.opts);
        end
        
        %% find a good set of start parameters by an extended search 
        function GC=getStartParameters(GC,data)
            %if all(size(data)==GC.N)
            %    display('incorrect data size in getStartParameters');
            %end    
            opts_tmp = GC.opts;
            GC.opts.MaxIter = 65;
            GC.opts.TolX=1e-7;
            GC.opts.TolFun=1e-7;
            data = ifff(data,1);
            
            %before we start the iterative search we will sweep over a
            %range of phase ramps beta
            beta=-1.5*pi:0.5:1.5*pi;
            zeroPhase = -2:0.2:2;
  
            for m=1:length(zeroPhase)
                for n=1:length(beta);
                    e(n,m)= entropyCostFunction([zeroPhase(m) beta(n)],GC,data);
                end
            end
            [~, minIdx]=min(e(:));
            [I,J,K] = ind2sub([length(beta) length(zeroPhase)],minIdx);
            initP = [zeroPhase(J) beta(I)];                     
            

            GC.paramInit =fminsearch(@(param) entropyCostFunction(param,GC,data),initP,GC.opts);
            GC.opts = opts_tmp;
        end
        
        %% get ghost parameters for all sub-reconstructions
        function GC=doGhostCorrection(GC,data)
            
                %check if we have initial values
                if isempty(GC.paramInit)
                    dataTmp = squeeze(sum(data(:,:,:,ceil(size(data,4)/2),:),5));
                    GC = GC.getStartParameters(dataTmp);
                end
                
                %1D-fft along x (hybrid kz-ky-space)
                data = ifff(data,1);
                
                if(strcmp(GC.coilMode,'avg'))
                    GC.Nc = size(data,5);
                    data = sum(data,5);
                end

                %new loop
                GC.Nc = size(data,5);
                slcIdx = 1:GC.slcInc:size(data,4);
                if(strcmp(GC.sliceMode,'central') && (size(data,4) > 2))
                    slcIdx = ceil(size(data,4)/2)-1:ceil(size(data,4)/2)+1;
                    if(size(data,4)==1)
                        slcIdx = 1;
                    end
     
                end
                n=1;
                for sl=slcIdx
                    GC.paramRaw(:,n,1) = fminsearch(@(param) entropyCostFunction(param,GC,reshape(data(:,:,:,sl,:),[GC.N(1:3) GC.Nc])),GC.paramInit,GC.opts);
                        if GC.debug
                           [~,GC.recon{n,1}] = entropyCostFunction(GC.paramRaw(:,n,1),GC,reshape(data(:,:,:,sl,:),[GC.N(1:3) GC.Nc])); 
                        end
                    n=n+1;
                end 
                if(strcmp(GC.sliceMode,'central'))
                    GC.paramRaw = mean(GC.paramRaw,2);
                end
                
                GC.paramMean = mean(GC.paramRaw,2);
                
        end
        
        %% interpolate slice and coil dependent ghost parameters
        function GC=interpolateGhostParams(GC)
            if(strcmp(GC.coilMode,'avg') || strcmp(GC.coilMode,'coil'))
                GC.paramRaw = repmat(GC.paramRaw,[1 1 GC.Nc]);
            end
            
            if(strcmp(GC.sliceMode,'central'))
                GC.paramInterp = repmat(GC.paramRaw,[1 GC.Nsl 1]);         
            else
                GC.paramInterp(1,:,:) = interp1(1:GC.slcInc:GC.Nsl,squeeze(GC.paramRaw(1,:,:)),1:GC.Nsl,'spline');
                GC.paramInterp(2,:,:) = interp1(1:GC.slcInc:GC.Nsl,squeeze(GC.paramRaw(2,:,:)),1:GC.Nsl,'spline');
            end
        end
        
        %% apply ghost parameters to all 7D-data
        function data = applyPhaseCorrection(GC,data,oddLines)          
            
            %odd lines are marked with ones
            GC.oddLines = oddLines;
            GC.N(1) = size(oddLines,1);
            GC.N(2) = size(oddLines,2);
            GC.N(3) = size(oddLines,3);
            
            %forward fft
            data = ifff(data,1);
            
            if(strcmp(GC.sliceMode,'central')) %coil & slice independent ghost parameters
                theta=getGhostPhase(GC,GC.paramInterp(:,1,1));
                theta = repmat(theta,[1 1 1 size(data,4) 1 size(data,6) size(data,7)]);
                %data(:,:,:,k,t,sl,c) = data(:,:,:,k,t,sl,c).*theta;
                for t=1:size(data,5)
                    data(:,:,:,:,t,:,:) = data(:,:,:,:,t,:,:).*theta;
                end
                    
            else % 3d loop over individual ghost parameters
                for k=1:size(data,4)
                    for t=1:size(data,5)
                        for sl=1:size(data,6)
                            %for c=1:size(data,7)
                                if size(GC.paramInterp,1)==3
                                    theta=getGhostPhaseInterleaved(GC,GC.paramInterp(:,sl,1));
                                else
                                    theta=getGhostPhase(GC,GC.paramInterp(:,sl,1));
                                    theta = repmat(theta,[1 1 1 1 1 1 size(data,7)]);
                                end
                                data(:,:,:,k,t,sl,:) = data(:,:,:,k,t,sl,:).*theta;
                            %end
                        end
                    end
                end
            end
            %undo ifft
            data = fff(data,1);
        end
        
    end
end


%local functions with class scope
function [e, recon]=entropyCostFunction(param,GC,data)
    %data must be in hybrid x-ky-kz-space
    
    theta=getGhostPhase(GC,param(1:2));
    theta = repmat(theta,[1 1 1 size(data,4)]);
    
    recon = ifff(data.*theta,2);
    recon = ifff(recon,3);
    
    %sum-of-squares combination of individual channels
    if(size(data,4)>1)
        recon = sos(recon);
    end
    
    %normalize reconstruction
    recon = abs(recon)/max(abs(recon(:)));
    
    if GC.debug
        if (size(recon,3)==1)
            imagesc(abs(recon));colormap gray
        else
            imagesc(abs(array2mosaic(recon)));colormap gray
        end
        drawnow
    end
    
    %calculate entropy
    e = entropy(double(abs(recon)));
end

function theta=getGhostPhase(GC,param)
%returns EPI ghost correction phase as 3d-array (same size as basic
%reconstruction problem)

    %add global phase to data (for odd lines only) = 0th-order phase
    %globalPhase = ones(GC.N(1),GC.N(2),GC.N(3));
    globalPhase = ones(GC.N);
    globalPhase(GC.oddLines) = exp(1i*param(1));
    
    %add phase ramp for odd lines (=1st-order phase)
    [phaseRampND, ~, ~]=ndgrid(linspace(-1,1,GC.N(1)),ones(GC.N(2),1),ones(GC.N(3),1),ones(GC.N(4),1));
    phaseRampND(~GC.oddLines) = 0;
    phaseRampND = exp(1i*phaseRampND*param(2));

    theta = globalPhase.*phaseRampND;
end

function theta=getGhostPhaseInterleaved(GC,param)
%returns EPI ghost correction phase as 3d-array (same size as basic
%reconstruction problem)
    
    %setup EPI interleav correction
    for y=2:2:GC.N(2)
        yprime(y) = floor((y-1)/GC.Ni);
    end
    [~, yramp2d] = ndgrid(ones(GC.N(1),1),yprime);
    
    if GC.dim == 3
    shiftz = true;
    for n=1:GC.Ni
        if shiftz
            yramp(:,:,n) = circshift(yramp2d,[0 n-1]);
        else
            yramp(:,:,n) = yramp2d;
        end
    end
    elseif GC.dim == 2
        yramp = yramp2d;
    else
        asdf
    end
    %yramp(~GC.oddLines)=0;
    yramp = exp(1i*yramp*param);
    theta = yramp;
end
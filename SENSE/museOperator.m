% A meta operator that is composed of sense operators for 
%
%
% Author: Benjamin Zahneisen
% Last modified: 01 Jan 2015

classdef museOperator
    properties (SetAccess = public, GetAccess = public)
        Noperators          = []; 
        imageDim            = []; 
        numCoils            = []; 
        %trajectoryLengths   = []; %[Noperators x 1] array storing each operators number of k-space samples
        %totalLengths        = []; %stores length of k-space times coils vector
        dataVecIdcs         = {}; %
        adjoint             = false;
        %nufftOperators      = nuFTOperator();
        pinvClin            = []; %pseudo-inverse of coil weighting [Nc*Noperators Rtot]
        gmap                = [];
        AliasingLocLinIdx   = [];
    end
    
    methods
        %% CONSTRUCTOR
        function A=museOperator(senseOpArray)
            %ABC can be an array of nufft-operators
            
            A.Noperators = length(senseOpArray);
            
            if isa(nufftArray(1),'senseND_operator') 
                
                %get some information global information
                A.imageDim = getfield(senseOpArray(1),'imageDim');
                A.numCoils = getfield(senseOpArray(1),'numCoils');
                A.RedFOV = senseOpArray(1).RedFOV;
                
                %loop over operators
                %for n=1:A.Noperators
                %    tmp = size(nufftArray(n));
                %    A.trajectoryLengths(n) = tmp(1)./A.numCoils;
                %    A.totalLengths(n) = tmp(1);
                %    A.nufftOperators(n) = nufftArray(n);
                %end
                
                
                %keep track of k-space data vectors
                %A.dataVecIdcs{1} = 1:A.totalLengths(1);
                %for n=2:A.Noperators
                %    A.dataVecIdcs{n} = A.dataVecIdcs{n-1}(end)+1  :  A.dataVecIdcs{n-1}(end)+A.totalLengths(n);
                %end
                
                %invert coil sensitivities
                %reshape linear index locations of aliasing as
            %[pixelInReducedFoV Rtot]
            LocLinIdx = reshape(A.AliasingLocLinIdx,[prod(A.ReducedFoVSize) A.Rtot]);
                
                for l=1:prod(A.RedFOV)
                    A.pinvClin = 
                end
            else
                display('wrong input. Must be a nufft-operator')
            end

        end
        
        
        %% adjoint(ctranspose) operator
        function B = ctranspose(A)

            B = A;
            if B.adjoint==0
                B.adjoint = true;
            else
                B.adjoint = false;
            end
        end
        %end of ctranspose
        
        
        %% return size
        function [s1,s2] = size(A,n)
            
            t1 = [0 1];
            for n=1:A.Noperators
                t1 = t1 + [A.trajectoryLengths(n).*A.numCoils 0];
                %t1 = [A.trajectoryLength1*A.numCoils + A.trajectoryLength2*A.numCoils 1];
            end
            t2 = A.imageDim;

            if A.adjoint
                tmp = t1;
                t1 = t2;
                t2 = tmp;    
            end
            if nargin==1
                s1 = t1;
                s2 = t2;
            elseif nargin==2
                s2 = [];
                if n==1
                    s1 = t1;
                elseif n==2
                    s1 = t2;
                end
            end
        end
        % end of size
        
        
        %% times operator
        function Q = mtimes(A,B)

            if isa(A,'museOperator')
                if A.adjoint
                    
                    %raw data comes as [Nx Ny (Nz) Nc shots]
                    invRecon = A.ifffnd(B,A.dim);
        
                    if A.dim == 2
                        invRecon_lin = invRecon(1:A.RedFOV(1),1:A.RedFOV(2),:);
                    elseif A.dim == 3
                        invRecon_lin = invRecon(1:A.RedFOV(1),1:A.RedFOV(2),1:A.RedFOV(3),:);
                    elseif A.dim ==4
                        invRecon_lin = invRecon(1:A.RedFOV(1),1:A.RedFOV(2),1:A.RedFOV(3),1:A.RedFOV(4),:);
                    end
                    invRecon_lin = reshape(invRecon_lin,[prod(A.RedFOV) A.Nc A.Noperators]);
        
                    %initialize reconstruction as vector
                    Q = zeros(1,prod(A.N));
                    
                    for l=1:prod(A.RedFOV)
                        
                        Q(A.AliasingLocLin)= squeeze(A.pinvClin(l,:,:))*reshape(squeeze(invRecon_lin(l,:,:)),[A.Nc*A.Noperators 1]);
                    end
                else
                    %forward operation = encoding matrix (A) times image
                    %(z)
                    %Q = zeros((A.trajectoryLength1 + A.trajectoryLength2)*A.numCoils, 1);
                    Q = zeros(size(A));
                    for n=1:A.Noperators
                        Q(A.dataVecIdcs{n}) = A.nufftOperators(n)*B;
                    %Q(A.trajectoryLength1*A.numCoils+1:end) = A.A2*B;
                    end
                end
        
   
            % now B is the operator and A is the vector
            else
                Q = mtimes(B',A')';
            end
        end
        % End of times operator
        
    end
    
end
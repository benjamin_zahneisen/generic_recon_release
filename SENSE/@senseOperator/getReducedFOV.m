function R=getReducedFOV(A,psf0)


%get sense dimension
dim = length(size(psf0));



%% 3D-PSF (=4D SENSE)
if dim == 3
    P = perms([1 2 3]);
elseif dim == 2
    P = perms([2 1]);
else
    P = 1;
end

%intitialize 
redVol = ones(dim,1);

for l = 1:size(P,1)
    
    %flip order of dimensions
    psf = permute(psf0,P(l,:));

    %get subvolumes
    for x=1:size(psf,1)
        subVol = psf(1:x,1,1);
    
        if length(find(subVol>0)) >1
            redVol(1) = x;
            break;
        end
        redVol(1) = x;
    end

    if redVol(1) == size(psf,1)
        %do nothing
    else
        redVol(1) = redVol(1)-1;
    end

        
    %only exectue in case of 2d sense
    if dim > 1 
    for y=1:size(psf,2)
        subVol = psf(1:redVol(1),1:y,1);
    
        if length(find(subVol >0)) > 1
            redVol(2) = y;
            break;
        end
        redVol(2) = y;
    end

    if redVol(2) == size(psf,2)
        %do nothing
    else
        redVol(2) = redVol(2)-1;
    end
    
    end %End of loop for 2nd dimension 
    
    if dim > 2
    for z=1:size(psf,3)
        subVol = psf(1:redVol(1),1:redVol(2),1:z);
    
        if length(find(subVol >0)) > 1
            redVol(3) = z;
            break;
        end
        redVol(3) = z;
    end    

    if z == size(psf,3)
        %do nothing
    else
        redVol(3) = redVol(3)-1;
    end
    end
    
    
    %set the final size of the reduced FOV for each permutation
    rFOV(l,P(l,:)) = redVol;
    R(l,P(l,:)) = size(psf)./rFOV(l,P(l,:));
end


%how many indepent combinations
R = unique(R,'rows');
m=[];
for n=1:size(R,1)
    if (prod(R(n,:)) == A.Rtot)
        %everthing is fine
    else
        display('warning: Reduced FOV might be wrong.')
        m = cat(1,m,n);
    end
end
R(m,:) = [];

%pick the one with better distribution
[a ix]=sort(std(R,1,2),'ascend');
R = R(ix(1),:);

end
    
function R=getAccelerationFactors(A,usPattern)

%get sense dimension
if isvector(usPattern)
    dim =1; %1d-SENSE
    if ~iscolumn(usPattern)
            usPattern = usPattern';
    end
else
    dim = length(size(usPattern)); %nd-SENSE
end

%search along first dimension
for n=1:size(usPattern,2)
    ind = find(usPattern(:,n)>eps, 2, 'first');
    if (isempty(ind))
        %do nothing
    else
        Ry = abs(diff(ind));
        R(1) = Ry;
        break;
    end
end

if dim > 1
    %collapse y-dim
    usPattern2 = sum(usPattern,1);

    ind2 = find(usPattern2>eps*size(usPattern,1), 2, 'first');
    if length(ind2)==1
        Rz=size(usPattern,2);
    else
        Rz = abs(diff(ind2));
    end
    R(2) = Rz;
end



% N-dimensional SENSE operator
%
% 
% Author: Benjamin Zahneisen
% benjamin.zahneisen@gmail.com
% Last modified: 01 May 2016

classdef senseOperator
    properties (SetAccess = public, GetAccess = public)
        N                   = [];
        dim                 = 4; % dim=2; for 2d slices; dim=3 for 3D reconstructions
        Nc                  = [];   
        R                   = [1 1]; %acceleration factor along y and z
        Rtot                = 1; %total acceleration factor (=R(1)*R(2))
        Rfov                = []; %size of 3D box around origin containing "unique voxels"
        usPattern           = []; %ND matrix of k-space locations
        SensePatternND      = [];
        SensePatternND_red  = [];%complex aliased psf
        SensePattIdxND      = []; %non-zero entries from SensePattern2D
        SensePattIdxND_red  = []; %non-zero entries from SensePattern2D
        Cidx                = []; %indices of aliased pixels
        origin              = []; %primary voxel coordinates
        AliasedVoxLinIdx    = []; % [prod(ReducedFov) Rtot] = linear index of voxel that fold onto each other
        AliasingLocLinIdx   = []; %linear index of voxels that fold into one voxel of the reduced FoV ([ReducedFovSize x Rtot])
        smaps               = []; %coil sensitivities as [Nx Ny ... Nc]
        C                   = []; %coil sensitivities as [A.Nc A.Rtot prod(A.ReducedFoVSize)]
        gmap                = []; 
        pinvC               = []; %pseudo-inverse of coil weighting
        pinvClin            = []; %same as pinvC but linearily reordered
        sensePhases         = []; %[Rot x 1] vector of psf phases (rad)
        %psf_weighting       = []; %vector of complex valued sense pattern
        intTreshold         = (10^-5); %distinguish between noise floor and psf values
        adjoint             = 0;
        debug               = false;
        Anatom              = [];
        mask                = [];
        mode                = 'fast'; %'full','fast'
        h_fftn              = []; %function handle to n-dimensional fft;
        h_ifftn             = [];
    end
   
    
    methods
        %-----------------------------------------------------------------+
        %% Constructor
        % Returns a SENSE operator A that can be applied to undersampled
        % raw data in the form recon=A'*s, where s is an n-dimensional
        % matrix of k-space data with zeros representing skipped samples.
        %-----------------------------------------------------------------+
        function A=senseOperator(smaps,usPattern,fft_handles)
            %fft_handles = 2x1 cell array of fft and ifft handles
            
            if isa(smaps,'CoilSensitivities')
                
                A.dim = smaps.dim;
                A.N = smaps.N;
                A.Nc = smaps.Ncc; %coil sensitivites might be compressed
                if ~isempty(smaps.mask)
                    A.mask = smaps.mask;
                    A.Anatom = smaps.sos;
                end
                display([num2str(A.dim),'D imaging, ',num2str(A.dim-1),'D SENSE'])
                if A.debug
                   A.smaps = smaps.smaps;
                end
            else
                if (length(size(smaps)) == 3) && (length(size(usPattern)) == 2)
                    A.dim = 2;
                    display('2D imaging, 1D SENSE')
                    %set size
                    [A.N(1), A.N(2), A.Nc] = size(smaps);
      
                %switch between 2d and 3d SENSE
                elseif (length(size(smaps)) == 4) && (length(size(usPattern)) == 3)
                    A.dim = 3;
                    display('3D imaging, 2D SENSE')
                    %set size
                    [A.N(1), A.N(2), A.N(3), A.Nc] = size(smaps);
                    A.smaps = smaps;
                
                elseif (length(size(smaps)) == 5) && (length(size(usPattern)) == 4)
                    A.dim = 4;
                    display('4D imaging, 3D SENSE')
                    %set size
                    [A.N(1), A.N(2), A.N(3), A.N(4), A.Nc] = size(smaps);
                    %A.smaps = smaps;
               
                else
                    warning('inconsistend dimensions !')
                return
                end
            end
            
            if ~all(A.N == size(usPattern))
                    warning('improper size of undersampling pattern!')
                return;
            else
                A.usPattern = usPattern;
            end
            
            %define ffts
            if nargin < 3
                %default ffts
                A.h_fftn = @A.fffnd;
                A.h_ifftn = @A.ifffnd;
            else
                A.h_fftn = fft_handles{1};
                A.h_ifftn = fft_handles{2};
            end
                
            %calculate total acceleration factor
            A.Rtot = numel(usPattern)/sum(usPattern(:));
            display(['Total acceleration factor: ',num2str(A.Rtot)])
        
            % call deriveSensePattern and return A (yields updated class A)
            A=A.deriveSenseIndexing(usPattern);
            if A.dim ==4
                display(['[Ry Rz_intra Rz_slab]= [',num2str(A.R(1)),' ',num2str(A.R(2)),' ',num2str(A.R(3)),']'])
            elseif A.dim ==3
                display(['[Ry Rz]= [',num2str(A.R(1)),' ',num2str(A.R(2)),']'])
            elseif A.dim == 2
                 display(['[Ry]= [',num2str(A.R(1)),']'])
            end
            
            %draw reduced FoV
            if A.debug
                A.plotSensePattern();
                drawnow
            end
            
            %pre-calculate sense matrix inversions
            if strcmp(A.mode,'full')
                A=A.invertCoilSensitivities(); %time consuming
            else
                if isa(smaps,'CoilSensitivities')
                    CLin = reshape(smaps.smaps,[prod(A.N) A.Nc]);
                else
                    CLin = reshape(smaps,[prod(A.N) A.Nc]);
                end                
                
                %A.C=zeros([A.Nc A.Rtot prod(A.Rfov)]);
                %for n=1:size(A.AliasedVoxLinIdx,1)
                %    A.C(:,:,n) = CLin(A.AliasedVoxLinIdx(n,:),:).';
                %    A.C(:,:,n) = A.C(:,:,n).*repmat(conj(A.sensePhases.'),[A.Nc 1]);
                %end
                                
                %faster update using matlab reorder functions
                idx = A.AliasedVoxLinIdx.';
                tmp = CLin(idx(:),:);
                A.C = reshape(tmp.',[A.Nc A.Rtot prod(A.Rfov)]);
                A.C = A.C.*repmat(conj(A.sensePhases.'),[A.Nc 1 size(A.C,3)]);

            end
         
        end
        % END of Constructor
        %/////////////////////////////////////////////////////////////////+
        
        
        %-----------------------------------------------------------------+
        %% deriveSenseIndexing
        % usPattern : undersampling pattern with ones/zeros encoding the
        % sampled k-space locations
        %-----------------------------------------------------------------+
        function A=deriveSenseIndexing(A,usPattern)
                    
            %the origin of the PSF is always at [1,1,...]
            A.origin = ones(length(A.N),1); %this is valid for n-dimensions
                        
            [A.SensePatternND, A.SensePattIdxND, A.SensePatternND_red, A.SensePattIdxND_red] = A.AliasingPatternFromVoxel(usPattern,A.origin);
            
            A.Rfov(1) = A.N(1); %this is always true
            
            %make "SENSE reduced FoV"
            if A.Rtot == 1
                A.Rfov = A.N;
                R = ones(1,A.dim);           
            else
                %calculate the reduced fov based on the us pattern
                %A.R=A.getReducedFOV(A.SensePattIdxND_red);
                A.R =  A.getAccelerationFactors(squeeze(usPattern(1,:,:)));
            end          
            
            %2d hack
            if A.dim == 2
                A.R = A.Rtot;
            end
            
            %get reduced FOV
            A.Rfov(1) = A.N(1);
            for d=2:A.dim
                A.Rfov(d) = A.N(d)/A.R(d-1);
            end
            
            %check if undersampling factors are integer
            if any(~(rem(A.R,1)==0))
                display('Acceleration factors non integer !')
                display(A.R)
                return;
            end
            
            %these are the linear index locations of all voxels folding
            %onto each other length(BasicAlicasLoc) must equal Rtot
            BasicAliasLoc = find(abs(A.SensePattIdxND) > 0);  
            
            %save phases of psf
            A.sensePhases = exp(1i*angle(A.SensePatternND(BasicAliasLoc)));
            
            
            if A.dim ==4
                [Ix,Iy,Iz,Iz2] = ind2sub(A.N,BasicAliasLoc);
                A.AliasingLocLinIdx = zeros([A.Rfov A.Rtot]);
                Indices(:,1) = Ix;
                Indices(:,2) = Iy;
                Indices(:,3) = Iz;
                Indices(:,4) = Iz2;
                for x=1:A.Rfov(1)
                    for y=1:A.Rfov(2)
                        for z=1:A.Rfov(3)
                            for z2=1:A.Rfov(4) 
                                sIndices=cylicIdxShift(Indices,[x-1 y-1 z-1 z2-1],A.N);
                                A.AliasingLocLinIdx(x,y,z,z2,:) = sub2ind(A.N,sIndices(:,1),sIndices(:,2),sIndices(:,3),sIndices(:,4));
                            end
                        end
                    end
                end
                
            elseif A.dim == 3
                [Ix,Iy,Iz] = ind2sub(A.N,BasicAliasLoc);
                
                %Indices is a Rtot x dim array 
                Indices(:,1) = Ix;
                Indices(:,2) = Iy;
                Indices(:,3) = Iz;                         
                
                A.AliasingLocLinIdx = zeros([A.Rfov A.Rtot]);
                %now we cover the reduced FOV starting from one corner
                for x=1:A.Rfov(1)
                    for y=1:A.Rfov(2)
                        for z=1:A.Rfov(3)
                            %[IndXs,IndYs,IndZs]=cylicIdxShift(Ix,Iy,Iz,[x-1 y-1 z-1],A.N);
                            sIndices=cylicIdxShift(Indices,[x-1 y-1 z-1],A.N);
                            A.AliasingLocLinIdx(x,y,z,:) = sub2ind(A.N,sIndices(:,1),sIndices(:,2),sIndices(:,3));
                        end
                    end
                end
                    
            elseif A.dim == 2
                [Ix,Iy] = ind2sub(A.N,BasicAliasLoc);
                
                %Indices is a Rtot x dim array 
                Indices(:,1) = Ix;
                Indices(:,2) = Iy;                        
                
                A.AliasingLocLinIdx = zeros([A.Rfov A.Rtot]);
                %now we cover the reduced FOV starting from one corner
                for x=1:A.Rfov(1)
                    for y=1:A.Rfov(2)
                        sIndices=cylicIdxShift(Indices,[x-1 y-1],A.N);
                        A.AliasingLocLinIdx(x,y,:) = sub2ind(A.N,sIndices(:,1),sIndices(:,2));
                    end
                end
            else
                %what could be next ?
            end
            
            A.AliasedVoxLinIdx = reshape(A.AliasingLocLinIdx,[prod(A.Rfov) A.Rtot]);
             
        end  
        % END of deriveSenseIndexing()
        %/////////////////////////////////////////////////////////////////
        
        
        %-----------------------------------------------------------------
        %% invertCoilSensitivities()
        %
        %-----------------------------------------------------------------
        function A=invertCoilSensitivities(A)
            
            %vectorize  sense coil matrix
            smaps_vec = reshape(A.smaps,[prod(A.N) A.Nc]);
            
            %reshape linear index locations of aliasing as
            %[pixelInReducedFoV Rtot]
            LocLinIdx = reshape(A.AliasingLocLinIdx,[prod(A.ReducedFoVSize) A.Rtot]);
            
            %initialize inverted coil maps
            pinvCLin = zeros(size(LocLinIdx,1),A.Rtot,A.Nc);
            gmapLin = zeros(size(LocLinIdx,1),A.Rtot);
            
            for n=1:size(LocLinIdx,1)
                %pseudo-inverse of coil sensitivities
                pinvCLin(n,:,:) = pinv(smaps_vec(LocLinIdx(n,:),:).');
                s = smaps_vec(LocLinIdx(n,:),:).';
                gmapLin(n,:)= sqrt(abs(diag(pinv(s'*s)).*diag(s'*s)));
            end
            
            %bring back to regular indexing
            A.pinvC = reshape(pinvCLin,[A.ReducedFoVSize A.Rtot A.Nc]);
            A.pinvClin = pinvCLin;
            for n=1:A.Rtot
                A.gmap(LocLinIdx(:,n)) = gmapLin(:,n);
            end
            A.gmap = reshape(A.gmap,A.N);
           
        end
        % END of invertCoilSensitivities()
        %-----------------------------------------------------------------+
        
        
        %-----------------------------------------------------------------+
        %% getgmap()
        % compute gmap if not already performed 
        function gmap=getgmap(A)
            if isempty(A.gmap)
                    
                gmap = zeros(A.N);
            
                gmapLin = zeros(size(A.AliasedVoxLinIdx,1),A.Rtot);
                for n=1:size(A.AliasedVoxLinIdx,1)
                    s = A.C(:,:,n);
                    gmapLin(n,:)= sqrt(abs(diag(pinv(s'*s)).*diag(s'*s)));
                end
                for n=1:A.Rtot
                    gmap(A.AliasedVoxLinIdx(:,n)) = gmapLin(:,n);
                end
                gmap = reshape(gmap,A.N);
            else
                display('gmap already exists.')
            end
        end
        
        
        
        %% update coil sensitivity maps (the rest is unchanged)
        function A=updateCoilSensitivities(A,smaps,invFlag)
            
            if nargin < 3 || strcmp(A.mode,'fast')
                invFlag = false;
            end
            
            if (isa(smaps,'CoilSensitivities'))
               smaps = smaps.smaps; 
            end
            
            if all(size(smaps)==[A.N A.Nc])
                if A.debug
                    A.smaps = smaps;
                end
            else
                warning('coil sensitivity dimension mismatch !')
                return;
            end
            
            %update the pseudo inverse matrices based on new coil data
            if invFlag
                A=A.invertCoilSensitivities();
            else
                A.gmap = []; %gmap is no longer valid in that case
                CLin = reshape(smaps,[prod(A.N) A.Nc]);
                                
                %faster update using matlab reorder functions
                idx = A.AliasedVoxLinIdx.';
                tmp = CLin(idx(:),:);
                A.C = reshape(tmp.',[A.Nc A.Rtot prod(A.Rfov)]);
                A.C = A.C.*repmat(conj(A.sensePhases.'),[A.Nc 1 size(A.C,3)]);
            end
            
        end
        
        %% derive aliasing pattern
        function [SensePatternND, SensePatternIdxND, SensePatternND_red, SensePatternIdxND_red]  = AliasingPatternFromVoxel(A,usKND,voxelPos)
            %SensePatternND is the actual PSF due to undersampling
            %SensePatternIdxND is the corresponding logical indexing arrray
            % xxx_red has (N-1) dimensions          
            
            voxel = zeros(size(usKND));
            %linearInd = sub2ind(arraySize, dim1Sub, dim2Sub, dim3Sub,
            if A.dim == 4
                voxel(voxelPos(1),voxelPos(2),voxelPos(3),voxelPos(4))=1;
                Kfull = A.fff4d(voxel);
                SensePatternND = A.ifff4d(Kfull.*usKND);%voxel wise multiplication with ones and zeros
                SensePatternIdxND = abs(SensePatternND) > A.intTreshold*max(abs(SensePatternND(:)));
                SensePatternND_red = squeeze(SensePatternND(1,:,:,:));
                SensePatternIdxND_red = squeeze(SensePatternIdxND(1,:,:,:));
            elseif A.dim == 3
               %do the same thing in 3d
                voxel(voxelPos(1),voxelPos(2),voxelPos(3))=1;
                Kfull = A.h_fftn(voxel,3);
                SensePatternND = A.h_ifftn(Kfull.*usKND,3);%voxel wise multiplication with ones and zeros
                SensePatternIdxND = abs(SensePatternND) > A.intTreshold*max(abs(SensePatternND(:)));
                SensePatternND_red = squeeze(SensePatternND(1,:,:));
                SensePatternIdxND_red = squeeze(SensePatternIdxND(1,:,:));
            elseif A.dim == 2
                %now for 2d
                voxel(voxelPos(1),voxelPos(2))=1;
                Kfull = A.fffnd(voxel,A.dim);
                SensePatternND = A.ifffnd(Kfull.*usKND,A.dim);%voxel wise multiplication with ones and zeros
                SensePatternIdxND = abs(SensePatternND) > A.intTreshold*max(abs(SensePatternND(:)));
                SensePatternND_red = squeeze(SensePatternND(1,:));
                SensePatternIdxND_red = squeeze(SensePatternIdxND(1,:));
            end
        end
        % END of AliasingPatternFromVoxel()
        
        
        %% return sorting index for undesampled k-space (including coil dimension)
        function idx=kspaceIndex(A)
            idx =zeros([size(A.usPattern) A.Nc]);
            for n=1:A.Nc
                idx(:,:,:,n)=A.usPattern;
            end
            idx = logical(idx);
        end
        
        
        %% return sorting index in case of "fast" sampling direction along z
        function idx=kspaceIndexFastZ(A)
            idx =zeros([size(A.usPattern) A.Nc]);
            for n=1:A.Nc
                idx(:,:,:,n)=A.usPattern;
            end
            idx = logical(idx);
        end
        
        
        %% plot the sense-pattern and the reduced fov
        function plotSensePattern(A)
            
            if A.dim == 4
                %not yet
                figure;
                %plotAliasedPSF3D(A.SensePattIdxND_red)
                hold on
                %drawCube([A.origin(2)-0.5 A.origin(3)-0.5 A.origin(4)-0.5],[A.Rfov(2)+0.5 A.Rfov(3)+0.5 A.Rfov(4)]+0.5)
                %plot3(A.origin(2),A.origin(3),A.origin(4),'o','MarkerFaceColor','r','MarkerSize',10)
                
            elseif A.dim ==3
            
                figure; 
                imagesc(A.SensePattIdxND_red)
                hold on
                %keep in mind image coordiantes are rotated by 90deg
                line([1 1],[1 A.Rfov(2)],'Color','g','LineWidth',2)
            
                line([1 A.Rfov(3)],[1 1],'Color','g','LineWidth',2)
                
                line([1 A.Rfov(3)],[A.Rfov(2) A.Rfov(2)],'Color','g','LineWidth',2)
                
                line([A.Rfov(3) A.Rfov(3)],[1 A.Rfov(2)],'Color','g','LineWidth',2)
            
            end
        end
        %END of plotSensePattern()
        
        
        %% local fft
        function x=fffnd(A,x,nd)
            for d=1:nd
                x=fff(x,d);
            end
        end
        function x=ifffnd(A,x,nd)
            for d=1:nd
                x=ifff(x,d);
            end
        end
        function out=fff3d(A,in)
            out = fff(in,1);
            out = fff(out,2);
            out = fff(out,3);
        end
        function out=ifff3d(A,in)
            out = ifff(in,1);
            out = ifff(out,2);
            out = ifff(out,3);
        end 
        function out=fff4d(A,in)
            out = fff(in,1);
            out = fff(out,2);
            out = fff(out,3);
            out = fff(out,4);
        end
        function out=ifff4d(A,in)
            out = ifff(in,1);
            out = ifff(out,2);
            out = ifff(out,3);
            out = ifff(out,4);
        end
        
     function A = setAnatomImage(A,Anatom)
    
        imSize = size(Anatom);
        if any(~(imSize == A.N))
            display('wrong size of anatomical image!')
        else
            A.Anatom = Anatom;
            
            %mask out zero intensities
            A.mask = Anatom > 1000;
            
        end
    
     end       
      
     function s = getSingleCoil(A,coiln)
         if A.dim == 3 
             s = A.smaps(:,:,:,coiln);
         elseif A.dim == 4
             s = A.smaps(:,:,:,:,coiln);
         elseif A.dim == 2
             s = A.smaps(:,:,coiln);
         end
     end
     
     
    %plot the gmaps
    function plotGmaps(A)
        if A.dim == 3
            if ~isempty(A.Anatom)
                %z= overlay(array2mosaic(A.Anatom),array2mosaic(A.gmap.*A.mask));
                %image(z)
                figure;
                imagesc(array2mosaic(A.gmap.*A.mask))
                
            else
                imagesc(array2mosaic(A.gmap))
            end
        elseif A.dim == 4
            maxG = max(A.gmap(A.mask));
            for n=1:A.N(4)
                figure;
                imagesc(array2mosaic(A.gmap(:,:,:,n).*A.mask(:,:,:,n)),[0 maxG])
            end
            
        end
        
    end
   
    
    end
    %End of methods

end
%END OF CLASSDEF


%% helper functions
function sIndices=cylicIdxShift(Indices,dI,N)
    
    %Indices is an Rtot x Nd matrix where each row stores the Nd pixel
    %locations
    sIndices = zeros(size(Indices));
    
    for l=1:size(Indices,1)
        for d=1:size(Indices,2)
            sIndices(l,d) = Indices(l,d)+dI(d);
            if sIndices(l,d) > N(d)
                sIndices(l,d) = sIndices(l,d) - N(d);
            end
            if sIndices(l,d) < 1
                sIndices(l,d) = sIndices(l,d) + N(d);
            end
        end
    end
end

%% external helper functions
        function out=fff(in, dim)
            if nargin==1
                s = size(in);
                f = find(s~=1);
                dim = f(1);
            end
            out=fftshift(fft(ifftshift(in,dim),[],dim),dim);
        end
        function out=ifff(in, dim)
            if nargin==1
                s = size(in);
                f = find(s~=1);
                dim = f(1);
            end 
            out=fftshift(ifft(ifftshift(in,dim),[],dim),dim);
        end
function a=size(A)

    Nx = A.N(1);
    Ny = A.N(2);
    Nz = A.N(3);
    Nc = A.Nc;
    
    a = [A.N(1) A.N(2) A.N(3) A.Nc];
end
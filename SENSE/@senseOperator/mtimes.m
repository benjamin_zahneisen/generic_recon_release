function Q = mtimes(A,B)


if isa(A,'senseOperator')
    %performs the actual sense reconstruction
    if A.adjoint
        
        %check if dimensions match and get rid of ones
        B = squeeze(B);
        if(~(size(B,A.dim+1) == A.Nc))
            warning('coil dimensions do not match.')
            return;
        end
        tframes = size(B,A.dim+2);
        
        %back to image domain
        invRecon = A.ifffnd(B,A.dim);
        
        %initialize reconstruction as vector
        Q = zeros(prod(A.N),tframes);
        
        if strcmp(A.mode,'fast')
            %aliasingIdx = reshape(A.AliasingLocLinIdx,[prod(A.ReducedFoVSize) A.Rtot]);
            if A.dim==2
                invReconLin = reshape(invRecon(1:A.Rfov(1),1:A.Rfov(2),:,:),[prod(A.Rfov) A.Nc tframes]);
            elseif A.dim==3
                invReconLin = reshape(invRecon(1:A.Rfov(1),1:A.Rfov(2),1:A.Rfov(3),:,:),[prod(A.Rfov) A.Nc tframes]);
            elseif A.dim==4
                invReconLin = reshape(invRecon(1:A.Rfov(1),1:A.Rfov(2),1:A.Rfov(3),1:A.Rfov(4),:,:),[prod(A.Rfov) A.Nc tframes]);
            end
            
%             CLin = reshape(A.smaps,[prod(A.N) A.Nc]);
%             C=zeros([A.Nc A.Rtot prod(A.ReducedFoVSize)]);
%             for n=1:size(A.AliasedVoxLinIdx,1)
%                 C(:,:,n) = CLin(A.AliasedVoxLinIdx(n,:),:).';
%             end
            %display('init recon')
            
            %do/don't perform rank analysis
            rankflag = false;
            
            if rankflag
                %find all voxels where coil has zero entries
                rankC = zeros([prod(A.ReducedFoVSize) 1]);
                for n=1:size(A.AliasedVoxLinIdx,1)
                    C(:,:,n) = CLin(A.AliasedVoxLinIdx(n,:),:).';
                    rankC(n) = rank(C(:,:,n));
                end

                fullRank= find(rankC == A.Rtot);
                defRank = find((rankC < A.Rtot) & (rankC>0));
                display('rank analysis')
            end
            
            %reduced column inversion is the proper way but it is extremely slow
            %we will stick to do the deficient rank inversion (with error
            %warning turned off) and let matlab handle the rank
            %deficiencies. If output is funky we might have to go back.
            % This occurs if we have masked the coil sensitivities with
            % zeros
%             for n=1:length(defRank)
%                 [R,jb] = rref(C(:,:,defRank(n)));
%                 b = reshape(invReconLin(defRank(n),:,:),[A.Nc tframes]);% b = [Nc tframes]
%                 rC = C(:,jb,defRank(n));
%                 x = rC\b;
%                 Q(A.AliasedVoxLinIdx(defRank(n),jb),:) = x;
%             end
           
            %do a dummy inversion
            %C(:,:,defRank(1))\reshape(invReconLin(defRank(1),:,:),[A.Nc tframes]);
            %[~, msgid] = lastwarn;
            msgid = 'MATLAB:rankDeficientMatrix';
            warning('off',msgid);          
           
            if rankflag
                for n=1:length(defRank)
                    b = reshape(invReconLin(defRank(n),:,:),[A.Nc tframes]);% b = [Nc tframes]
                    x = (C(:,:,defRank(n)))\b; % [Rtot tframes]
                    Q(A.AliasedVoxLinIdx(defRank(n),:),:) = x;
                end
                %display('rankdef inversion')
                warning('on',msgid);
            end
            
            if ~rankflag
                fullRank = 1:size(A.AliasedVoxLinIdx,1);
            end
            for n=1:length(fullRank)
                 %C= [Nc Rtot]
                b = reshape(invReconLin(fullRank(n),:,:),[A.Nc tframes]);% b = [Nc tframes]
                Q(A.AliasedVoxLinIdx(fullRank(n),:),:) = A.C(:,:,fullRank(n))\b;% x= [Rtot tframes]
                %regularized inversion
%                 C=A.C(:,:,fullRank(n));
%                 [U,S,V] = svd(C,'econ');
%                 s=diag(S);
%                 is = 1./s;
%                 is(s==0) = 0;
%                 is(s<0.1) = 0;
%                 iSreg = zeros(size(S));
%                 iSreg(1:size(C,2),1:size(C,2)) = diag(is);
%                 iCr = V*(iSreg.')*U'; 
%                 Q(A.AliasedVoxLinIdx(fullRank(n),:),:) = iCr*b;
            end
            %display('full rank inversion')
            
            
        else
        for x=1:A.ReducedFoVSize(1)
            for y=1:A.ReducedFoVSize(2) 
                
                if A.dim == 2
                    Q(squeeze(A.AliasingLocLinIdx(x,y,:)))= reshape(A.pinvC(x,y,:,:),[A.Rtot A.Nc])*squeeze(invRecon(x,y,:));
                else
                    
                for z=1:A.ReducedFoVSize(3)
                    if A.dim == 3
                        Q(squeeze(A.AliasingLocLinIdx(x,y,z,:)))= reshape(A.pinvC(x,y,z,:,:),[A.Rtot A.Nc])*squeeze(invRecon(x,y,z,:));
                    elseif A.dim ==4
                        for z2 = 1:A.ReducedFoVSize(4)
                        %sort unfolded pixels into final reconstruction
                        Q(squeeze(A.AliasingLocLinIdx(x,y,z,z2,:)))= squeeze(A.pinvC(x,y,z,z2,:,:))*squeeze(invRecon(x,y,z,z2,:));
                        end
                    end
                end
                end
            end
        end
        end
        Q = reshape(Q,[A.N tframes]);
  
    else
    %synthesizes undersampled k-space from image
        Q = [];
        if(isempty(A.smaps))
            %restore smaps
            for c=1:A.Nc
                for n=1:size(A.AliasedVoxLinIdx	,1)
                    A.smaps(A.AliasedVoxLinIdx(n,:),c) = A.C(c,:,n);
                end
            end
            A.smaps = reshape(A.smaps,[A.N A.Nc]);
        end
        for n=1:A.Nc
            dataND = A.fffnd(B.*A.getSingleCoil(n),A.dim);
            dataND = dataND.*A.usPattern;
            Q = cat(A.dim +1,Q,dataND);
        end
    end
    
% now B is the operator and A is the vector
else
    Q = mtimes(B',A')';
    
end
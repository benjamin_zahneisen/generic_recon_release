/* ==========================================================================
 * dcmtk_uid.c 
*
 *
 * To build 32bit version:
 * 
 *
 *
 * To build 64bit version:
 * mex -v -I/usr/local/dicom64/include dcmtk_uid.cpp -L/usr/local/dicom64/lib64/ -ldcmdata -ldcmnet -ldcmpstat -ldcmimage -lofstd -lz
 *
 * To allow building of 64bit version, some more system tweaks are needed:
 * get dcmtk, unpack, then run following commands:
 *
 * ./configure CFLAGS="-fPIC" CXXFLAGS="-fPIC" --prefix=/usr/local/dicom64 --libdir=/usr/local/dicom64/lib64
 *
 * w/o compression support and need for libz, which is missing on chmr's blade servers
 * ./configure --without-zlib CFLAGS="-fPIC" CXXFLAGS="-fPIC" --prefix=/usr/local/dicom64 --libdir=/usr/local/dicom64/lib64
 * mex -v -I/usr/local/dicom64/include dcmtk_uid.cpp -L/usr/local/dicom64/lib64/ -ldcmdata -ldcmnet -ldcmpstat -ldcmimage -lofstd
 * 
 * then log as root via "su" and do:
 * make install
 * make install-lib
 * echo "/usr/local/dicom64/lib64" >> /etc/ld.so.conf
 * /sbin/ldconfig
 *
 *
 *==========================================================================*/


#include "mex.h"
#include "matrix.h"
#include "string.h"
#include <string>
#include <sstream>
#include <dcmtk/config/cfunix.h>
//#include <dcmtk/dcmdata/dcdatset.h>
//#include <dcmtk/dcmdata/dcfilefo.h>
//#include <dcmtk/dcmdata/dcdeftag.h>
//#include <dcmtk/dcmdata/dcdict.h>
//#include <dcmtk/dcmdata/dctag.h>
//#include <dcmtk/dcmdata/dchashdi.h>
//#include <dcmtk/dcmdata/dcdicent.h>

#include <dcmtk/config/osconfig.h>
#include <dcmtk/dcmdata/dcfilefo.h>
#include <dcmtk/dcmdata/dcdeftag.h>
#include <dcmtk/dcmdata/dcuid.h>



#define MAXCHARS 16000   /* max length of string contained in each field */
#define mwSize size_t 

/*  the gateway routine.  */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{

    char uid[100];
	dcmGenerateUniqueIdentifier(uid, "1.2.840.113619.2.156");
	
    if(nlhs == 1)
	{
		plhs[0] = mxCreateString(uid);		
	}
	else
	{
		mexErrMsgTxt("Number of output parameters must be 1");	
    }
    
    return;
}

function X = dicomquickread(varargin)
%DICOMQUICKREAD   Read a directory of DICOM files.

if nargin==1
    filespec=varargin{1};
else
    filespec='*.dcm';
end

dirname = get_dirname(filespec);
d = dir(filespec);

% Read the tags and positions for the first file in a directory.
p = 1;
while (d(p).isdir)
    p = p + 1;
end

filename = [dirname d(p).name];
[first_tags, first_pos] = read_first_file(filename);

% Get the metadata values needed to read a DICOM file.
metadata_tags = request_metadata_tags;
[meta_tags, meta_pos] = winnow_tags(first_tags, first_pos, metadata_tags);
meta_tags = cast_tags(meta_tags);

[tags, pos, data] = quick_info(filename, meta_tags, meta_pos);
details = process_info(tags, data);

% Preallocate.
numFiles = numel(d) - p + 1;
[X, datatype] = preallocate_array(details, numFiles);

% Read the DICOM files.
pixel_tags = request_pixel_tags;
[pix_tags, pix_pos] = winnow_tags(first_tags, first_pos, pixel_tags);
pix_tags = cast_tags(pix_tags);

rows = details.Rows;
cols = details.Columns;
spp  = details.SamplesPerPixel;

for q = p:numel(d)

    filename = [dirname d(q).name];
    start_idx = rows * cols * spp * (q - p) + 1;
    end_idx = start_idx + rows * cols * spp - 1;
    X(start_idx:end_idx) = quick_read(filename, pix_tags, pix_pos, datatype);
    
end

% Reshape.
X = permute(X, [2 1 3 4]);



function [tags, pos] = read_first_file(filename)

file = dicom_create_file_struct;
file.Messages = filename;
file.Location = 'Local';
file = dicom_get_msg(file);

file = dicom_open_msg(file, 'r');
info = dicom_create_meta_struct(file);
[tags, pos, info, file] = dicom_get_tags(file, info);
file = dicom_close_msg(file);



function [tags, pos, data] = quick_info(filename, first_tags, first_pos)

% Open file for reading.  Assume Explicit little-endian.
fid = fopen(filename, 'r', 'ieee-le');

if (fid == -1)
    error(sprintf('Couldn''t open file: %s', filename))
end

count = 0;
tags = {};
pos = {};
data = {};

for p = 1:size(first_tags, 1)
    
    % Move to the approximate location in the file where the attribute
    % should be.  Use a window 200 bytes wide.
    fpos_start = max(0, first_pos(p) - 100);
    
    fseek(fid, fpos_start, 'bof');
    buffer = fread(fid, 200, 'uint8=>uint8');
    idx = findNumericPattern(buffer, first_tags(p,:));
    
    if (~isempty(idx))
        
        count = count + 1;
        
        tags{count} = first_tags(p,:);
        pos{count} = fpos_start + idx - 1;
        data{count} = get_attr_data(fid, pos{count}, tags{count});
        
    end
        
end

fclose(fid);



function frame = quick_read(filename, tag, first_pos, datatype)
%QUICK_READ   Read the image pixels from one DICOM file.

% Open file for reading.  Assume Explicit little-endian.
fid = fopen(filename, 'r', 'ieee-le');

if (fid == -1)
    error(sprintf('Couldn''t open file: %s', filename))
end

% Move to the approximate location in the file where the attribute
% should be.  Use a window 200 bytes wide.
fpos_start = max(0, first_pos - 100);

fseek(fid, fpos_start, 'bof');
buffer = fread(fid, 200, 'uint8=>uint8');
idx = findNumericPattern(buffer, tag(:));
    
if (~isempty(idx))

    pos = fpos_start + idx - 1;
    frame = get_attr_data(fid, pos, tag, datatype);

else
    
    error('Missing data in %s', filename);
    
end

fclose(fid);



function tags = request_pixel_tags
%REQUEST_PIXEL_TAGS   Get a list of attributes for reading an image.

req_tags = {'7FE0', '0010'};

tags = repmat(uint16(0), size(req_tags));
for p = 1:numel(req_tags)
    tags(p) = sscanf(req_tags{p}, '%x');
end



function tags = request_metadata_tags
%REQUEST_PIXEL_TAGS   Get a list of attributes for reading an image.

req_tags = {'0002', '0010'
            '0028', '0002'
            '0028', '0010'
            '0028', '0011'
            '0028', '0100'
            '0028', '0103'};

tags = repmat(uint16(0), size(req_tags));
for p = 1:numel(req_tags)
    tags(p) = sscanf(req_tags{p}, '%x');
end



function [tags_out, pos_out] = winnow_tags(tags_in, pos_in, important_tags);
%WINNOW_TAGS   Find the important tags from all of the tags.

count = 0;
tags_out = uint16([]);
pos_out = [];

for p = 1:size(important_tags, 1)
    
    group = (tags_in(:, 1) == important_tags(p, 1));
    element = (tags_in(:, 2) == important_tags(p, 2));
    
    idx = find((group + element) == 2);
    
    if (~isempty(idx))

        count = count + 1;
        tags_out(count, :) = tags_in(idx, :);
        pos_out(count, :) = pos_in(idx, :);
        
    end
    
end



function data = get_attr_data(fid, pos, tag, varargin)
%GET_ATTR_DATA   Get an attribute's data.

% Determine if we're using explicit VR data.
if (tag(1) < 8)
    
    explicit_vr = true;
    
    fseek(fid, pos + 4, 'bof');
    vr = fread(fid, 2, 'char=>char')';
    
else
    
    fseek(fid, pos + 4, 'bof');
    vr = fread(fid, 2, 'char=>char')';
    
    switch (vr)
    case {'AE','AS','CS','DA','DT','LO','LT','PN','SH','ST','TM','UI', ...
          'UT','AT','OW','US','DS','IS','FL','FD','OB','UN','SL','SQ', ...
          'SS','UL'}
        
        explicit_vr = true;
        
    otherwise
    
        fseek(fid, -2, 'cof');
        explicit_vr = false;
        
    end
    
end

% Determine the amount of data for the attribute.
if (explicit_vr)
    
    switch (vr)
    case {'AE','AS','AT','CS','DA','DS','DT','FD','FL','IS','LO','LT', ...
          'PN','SH','SL','SS','ST','TM','UI','UL','US'} 

        len = fread(fid, 1, 'uint16');
        
    otherwise
        
        fseek(fid, 2, 'cof');
        len = fread(fid, 1, 'uint32');
        
    end
        
else
    
    len = fread(fid, 1, 'uint32');
    
end

% Read the data unless it's a sequence.
if (len < uint32(inf))

    if (isequal(tag, [224 127 16 0]))
        
        datatype = [varargin{1} '=>' varargin{1}];
        data = fread(fid, len, datatype);
        
    else
        data = fread(fid, len, 'uint8=>uint8')';
    end
    
else
    
    data = 'SEQUENCE';
    
end



function out = cast_tags(in)
%CAST_TAGS   Convert UINT16 tags to UINT8 stream.

in = in';
out = cast(in(:), 'uint8');
out = reshape(out, [4, numel(out)/4]);
out = out';



function details = process_info(tags, data)
%PROCESS_INFO  Turn the values from quick_info into a structure.

for p = 1:numel(data)
        
    if (isequal(tags{p}, [40 0 2 0]))
        
        details(1).SamplesPerPixel = double(cast(data{p}, 'uint16'));
    
    elseif (isequal(tags{p}, [40 0 16 0]))
        
        details(1).Rows = double(cast(data{p}, 'uint16'));
        
    elseif (isequal(tags{p}, [40 0 17 0]))
        
        details(1).Columns = double(cast(data{p}, 'uint16'));
        
    elseif (isequal(tags{p}, [40 0 0 1]))
        
        details(1).BitsAllocated = double(cast(data{p}, 'uint16'));
        
    elseif (isequal(tags{p}, [40 0 3 1]))
        
        details(1).PixelRepresentation = double(cast(data{p}, 'uint16'));
        
    end
    
end



function [X, datatype] = preallocate_array(details, numFiles)
%PREALLOCATE_ARRAY   Preallocate the output array.

% Determine the output data type.
if (details.BitsAllocated <= 8)
    
    if (details.PixelRepresentation == 0)
        data = uint8(0);
        datatype = 'uint8';
    else
        data = int8(0);
        datatype = 'int8';
    end
    
else
    
    if (details.PixelRepresentation == 0)
        data = uint16(0);
        datatype = 'uint16';
    else
        data = int16(0);
        datatype = 'int16';
    end

end

% Make the matrix.
X(details.Rows, details.Columns, details.SamplesPerPixel, numFiles) = data;



function dirname = get_dirname(filespec)
%GET_DIRNAME   Get the name of the deepest directory listed in a filespec.

if (isdir(filespec))
    
    if (isequal(filespec(end), filesep))
        dirname = filespec;
    else
        dirname = [filespec filesep];
    end
    
    return
end

idx = [find(filespec == '/'), find(filespec == '\')];
if (isempty(idx))
    dirname = '';
else
    dirname = filespec(1:max(idx(:)));
end

function attrs = dicom_update_iod_sc_storage(attrs, index, X, map, txfr, UID)
%DICOM_UPDATE_IOD_SC_STORAGE   Update a previously created SC Storage IOD.
%   OUT = DICOM_UPDATE_IOD_SC_STORAGE(IN, INDEX, X, MAP, TXFR, UID)
%   updates the attribute structure (IN) with new values from the image
%   (X), colormap (MAP), and SOP instance UID.  INDEX is a n-by-2 matrix
%   of group and element values of the attributes in IN.
%
%   See also DICOM_CREATE_IOD_SC_STORAGE, DICOM_TRIM_META_SC_STORAGE.

%   Copyright 1993-2003 The MathWorks, Inc.
%   $Revision: 1.5 $  $Date: 2003/01/17 16:28:29 $

% Update each attribute that might have changed from first frame.
attrs = update_attrs(attrs, index, '0002', '0003', UID);  % MediaStorageSOPInstanceUID
attrs = update_attrs(attrs, index, '0008', '0018', UID);  % SOPInstanceUID
attrs = update_attrs(attrs, index, '0028', '0106', min(X(:)));  % MIN pix val
attrs = update_attrs(attrs, index, '0028', '0107', max(X(:)));  % MAX pix val

tmp = dicom_add_pixel_data(struct([]), '7fe0', '0010',X, map, txfr);
attrs = update_attrs(attrs, index, '7fe0', '0010', tmp.Data);  % Pixel data



function out_str = update_attrs(in_str, index, group, element, data)
%UPDATE_ATTRS   Update an attribute's data.

group = sscanf(group, '%x');
element = sscanf(element, '%x');

% Look for group and element in index.
occurrences = (index(:, 1) == group) + (index(:, 2) == element);
pos = find(occurrences == 2);

if (numel(pos) < 1)
    error('Attribute (%04X,%04X) not found.', group, element);
elseif (numel(pos) > 2)
    error('Attribute (%04X,%04X) occurs too many times.', group, element);
end

out_str = in_str;
out_str(pos).Data = data;

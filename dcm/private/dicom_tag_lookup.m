function tag = dicom_tag_lookup(attr_name)
%DICOM_TAG_LOOKUP  Look up the data dictionary tag from a attribute name.

%   Copyright 1993-2003 The MathWorks, Inc.
%   $Revision: 1.1.2.2 $  $Date: 2003/07/28 18:18:11 $

persistent all_names all_tags groups elements prev_dictionary taghashtable;
mlock
% Rex hacked in the use of a hashtable to store the name->tag mapping.
% This is like 100X faster than using the two "find" commands they
% had been using before.
% to do the old way, just set this to 0:
do_new_method = 1;

if (isempty(all_names))

    [all_names, all_tags, groups, elements] = get_dictionary_info;
    taghashtable = hashtable(15000,1);
    % build up the hash table (this is only done once)
    for i = 1:length(all_names)
        % due to Matlab's crappy string handling, an initial number in a
        % string makes it claim an invalid field, so pre-pend a 'rex' to
        % it.
        if  (~isstrprop(all_names{i}(1),'alpha'))
            my_attr_name = strcat('rex',all_names{i}) ;
        else
            my_attr_name = char(all_names{i}) ;
        end
        
        % if the tag already exists, don't replace it (this is matlab's method)
        try 
            temp = taghashtable.(my_attr_name)  ;
        catch
            taghashtable.(my_attr_name) = [groups(i), elements(i)] ;
        end
	end
end

if (do_new_method)

    if (~isstrprop(attr_name(1),'alpha'))
        try
            temp  = taghashtable.(strcat('rex',attr_name))  ;
        catch
            if (~isempty(strfind(attr_name, 'Private')))
            % It's private.  Parse out the group and element values.
                tag = parse_private_name(attr_name);
            else
                % It's not a DICOM attribute.
                temp = 0;
            %fprintf('%s was not found in the dicom dictionary!\n',attr_name);
            end
        end
    else
        try
            temp = taghashtable.(char(attr_name))  ;
        catch
            if (~isempty(strfind(attr_name, 'Private')))
                % It's private.  Parse out the group and element values.
                tag = parse_private_name(attr_name);                
            else
                % It's not a DICOM attribute.
                 temp = 0;
                %fprintf('%s was not found in the dicom dictionary!\n',attr_name);
            end
        end
    end
    
    if(exist('temp') == 1)                      
        
        if (temp == 0)
            tag = [];
        else
            tag = [temp(1)-1, temp(2)-1] ;
        end
    end

else


% Look for the name among the attributes.
idx = find(strcmp(attr_name, all_names));

if (isempty(idx))
    
    if (~isempty(strfind(attr_name, 'Private')))
        
        % It's private.  Parse out the group and element values.
        tag = parse_private_name(attr_name);
        
    else
        
        % It's not a DICOM attribute.
        tag = [];
        
    end
    
else
    
    if (numel(idx) > 1)
        
        msg = 'Attribute "%s" is multiply defined.';
        dicom_warn(sprintf(msg, attr_name));
        
        idx = idx(1);
        
    end
    
    % Look for the index in the sparse array.
    % (row, column) values are (group + 1, element + 1)
    [group, element] = find(all_tags == idx);
    tag = [group element] - 1;

end % end else doing old method

end 

function [all_names, all_tags, group, element] = get_dictionary_info
%GET_DICTIONARY_INFO  Get necessary details from the data dictionary

dictionary = dicomdict('get_current');

if (findstr('.mat', dictionary))
    
    dict = load(dictionary);
    
    %all_names = {dict.values(:).Name};
    %all_tags = dict.tags;
    
    %all_tags = dict.all_tags;
    all_tags = dict.tags;
    group = dict.group;
    element = dict.element;
    all_names = {dict.values(:).Name};
    
else
    
    [all_tags, values, group, element] = dicom_load_dictionary(dicomdict('get_current'));
    
    all_names = {values(:).Name};
    
end



function tag = parse_private_name(attr_name)
%PARSE_PRIVATE_NAME  Get the group and element from a private attribute.

if (~isempty(strfind(attr_name, 'Creator')))
    
    % (gggg,0010-00ff) are Private Creator Data attributes:
    % "Private_gggg_eexx_Creator"  -->  (gggg,00ee).
    group = sscanf(attr_name(9:12), '%x');
    element = sscanf(attr_name(14:15), '%x');
    
    tag = [group element];
    
elseif (~isempty(strfind(attr_name, 'GroupLength')))

    % Skip Private Group Length attributes.
    group = sscanf(attr_name(9:12), '%x');
    tag = [group 0];
    
else
    
    % Normal private data attributes: "Private_gggg_eeee".
    group = sscanf(attr_name(9:12), '%x');
    element = sscanf(attr_name(14:17), '%x');
    
    tag = [group element];
    
end

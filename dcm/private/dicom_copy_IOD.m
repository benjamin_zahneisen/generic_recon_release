function [all_attrs, msg, status] = dicom_copy_IOD(X, map, metadata, options)
%DICOM_COPY_IOD  Copy attribtes from metadata to an arbitrary IOD.
%   [ATTRS, MSG, STATUS] = DICOM_COPY_IOD(X, MAP, METADATA, OPTIONS) creates
%   a structure array of DICOM attributes for an arbitrary SOP class
%   corresponding to the class contained in the metadata structure.  The
%   value of image pixel attributes are derived from the image X and the
%   colormap MAP.  Non-image attributes are derived from the METADATA
%   struct (typically given by DICOMINFO) and the transfer syntax UID
%   (OPTIONS.txfr).
%
%   NOTE: This routine does not verify that attributes in METADATA belong
%   in the information object.  A risk exists that invalid data passed to
%   this routine will lead to formally correct DICOM files that contain
%   incomplete or nonsensical data.
%
%   See also: DICOMWRITE, DICOM_CREATE_IOD.

%   Copyright 1993-2003 The MathWorks, Inc.
%   $Revision: 1.1.2.1 $  $Date: 2003/07/31 14:32:24 $

all_attrs = [];
msg = '';
status = [];

% A field containing the SOP Class UID is necessary for writing.
MediaStorageUID_name = dicom_name_lookup('0002', '0002');
SOPClassUID_name = dicom_name_lookup('0008', '0016');

if (isfield(metadata, MediaStorageUID_name))
    
    IOD_UID = metadata.(MediaStorageUID_name);
    
elseif (isfield(metadata, SOPClassUID_name))
    
    IOD_UID = metadata.(SOPClassUID_name);
    
else
    
    eid = 'Images:dicom_copy_IOD:missingSOPClassUID';
    msg = sprintf('Missing required attribute (0008,0016) "%s"', ...
                  SOPClassUID_name);
    error(eid, '%s', msg);
    
end

% Update the instance-specific and required metadata.
if (isequal(options.createmode, 'create'))
  metadata = dicom_prep_SOPCommon(metadata, IOD_UID);
end
metadata = dicom_prep_FileMetadata(metadata, IOD_UID, options.txfr);
metadata = dicom_prep_ImagePixel(metadata, X, map, options.txfr);

% Get the metadata fields that need to be processed.
metadata_fields = fieldnames(metadata);
fields_to_write = remove_dicominfo_fields(metadata_fields);

% Process all of the remaining metadata
for p = 1:numel(fields_to_write)
    
    attr_name = fields_to_write{p};
    new_attr = process_field(attr_name, metadata);
    all_attrs = cat(2, all_attrs, new_attr);
    
end



function fields_out = remove_dicominfo_fields(metadata_fields)
%REMOVE_DICOMINFO_FIELDS  Strip DICOMINFO-specific fields from metadata.

dicominfo_fields = get_dicominfo_fields;
fields_out = setdiff(metadata_fields, dicominfo_fields);



function fields = get_dicominfo_fields
%GET_DICOMINFO_FIELDS  Get a cell array of field names specific to DICOMINFO.

fields = {'Filename'
          'FileModDate'
          'FileSize'
          'Format'
          'FormatVersion'
          'Width'
          'Height'
          'BitDepth'
          'ColorType'
          'SelectedFrames'
          'FileStruct'
          'StartOfPixelData'};



function attr = process_field(attr_name, metadata)
%PROCESS_FIELD  Create an attribute struct from metadata.

% Look up the attribute tag.
if (strncmpi(attr_name, 'item_', 5))
    
    data = encode_item(metadata) % This isn't done
    
else
    
    tag = dicom_tag_lookup(attr_name);
    
end


if (isempty(tag))

    attr = [];
    return

end

% Get the VR.
VR = determine_VR(tag, metadata);

% Process struct data - Person Names (PN) and sequences (SQ).
if (isequal(VR, 'PN'))
    
    data = encode_PN(metadata.(attr_name));

elseif (isequal(VR, 'SQ'))
    
    data = encode_SQ(metadata.(attr_name));
    
else
    
    data = metadata.(attr_name);
    
end

%MATUS
if(isempty(data))
       dummy = 1;
end
    

% Add the attribute.
if (isempty(VR))
    attr = dicom_add_attr([], tag(1), tag(2), data);
else
    attr = dicom_add_attr([], tag(1), tag(2), data, VR);
end



function VR = determine_VR(tag, metadata)
%DETERMINE_VR  Find an attribute's value representation (VR).

attr_details = dicom_dict_lookup(tag(1), tag(2));

if (isempty(attr_details))

    VR = [];
    
else
    
    VR = attr_details.VR;
    
    if (iscell(VR))
        VR = VR{1};
    end
    
end



function PN_chars = encode_PN(PN_struct)
%ENCODE_PN  Turn a structure of name info into a formatted string.

PN_chars = '';

for p = 1:numel(PN_struct)

    % Add each of the components to the output string.
    if (isfield(PN_struct, 'FamilyName'))
        PN_chars = [PN_chars PN_struct.FamilyName '^'];
    else
        PN_chars = [PN_chars '^'];
    end
    
    if (isfield(PN_struct, 'GivenName'))
        PN_chars = [PN_chars PN_struct.GivenName '^'];
    else
        PN_chars = [PN_chars '^'];
    end
    
    if (isfield(PN_struct, 'MiddleName'))
        PN_chars = [PN_chars PN_struct.MiddleName '^'];
    else
        PN_chars = [PN_chars '^'];
    end
    
    if (isfield(PN_struct, 'NamePrefix'))
        PN_chars = [PN_chars PN_struct.NamePrefix '^'];
    else
        PN_chars = [PN_chars '^'];
    end
    
    if (isfield(PN_struct, 'NameSuffix'))
        PN_chars = [PN_chars PN_struct.NameSuffix '^'];
    else
        PN_chars = [PN_chars '^'];
    end
    
    % Remove extraneous '^' separators.
    PN_chars = regexprep(PN_chars, '\^*$', '');
    
    % Separate multiple values.
    PN_chars = [PN_chars '\'];
    
end

% Remove extra value delimiter '\'.
PN_chars = regexprep(PN_chars, '\\$', '');



function attrs = encode_SQ(SQ_struct)
%ENCODE_SQ  Turn a structure of sequence data into attributes.

attrs = [];

% Don't worry about encoding rules yet.  Just convert the MATLAB struct
% containing item and data fields into an array of attribute structs.

if(~isstruct(SQ_struct))
    dummy = 1;
end


items = fieldnames(SQ_struct);
for p = 1:numel(items)
    
    data = encode_item(SQ_struct.(items{p}));
    attrs = dicom_add_attr(attrs, 'fffe', 'e000', data);
    
end



function attrs = encode_item(item_struct)
%ENCODE_ITEM  Turn one item of a sequence into attributes.

attrs = [];

attr_names = fieldnames(item_struct);
for p = 1:numel(attr_names)
    
    new_attr = process_field(attr_names{p}, item_struct);
    attrs = cat(1, attrs, new_attr);
    
end

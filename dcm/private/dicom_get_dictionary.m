function dict = dicom_get_dictionary
%DICOM_GET_DICTIONARY  Get the name of the data dictionary.

%   Copyright 1993-2003 The MathWorks, Inc.
%   $Revision $  $Date $

dict = 'dicom-dict.mat';

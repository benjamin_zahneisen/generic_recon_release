function convert_dicomdict(dictionary, funct_name)
%CONVERT_DICOMDICT generates a matlab function from a dicom dictionary
%
% CONVERT_DICOMDICT(DICTIONARY, FUNCT_NAME) makes a matlab function
% called FUNCT_NAME in a file <FUNCT_NAME>.m from a dicom dictionary
% file, DICTIONARY. The default value for FUNCT_NAME is
% 'load_mydicomdict'. The function can then be used to replace the
% 'textread' call in Matlab's dicom_load_dictionary.m.  This
% eliminates having to find and read an external dictionary file.
%
% DB Clayton - v1.0 2005/11/04


% prologue
if (nargin < 2), funct_name = 'load_mydicomdict'; end
mfile = sprintf('%s.m', funct_name);
test = ls(mfile);
if (strcmp(test, mfile))
  error('output file already exists: %s', mfile);
end

% the following code is extracted directly from matlab's dicom_load_dictionary.m
fmt = '(%4c,%4c)\t%s%s%s';
[group, element, vr, name, vm] = textread(dictionary, fmt, ...
                                         'commentstyle', 'shell');

% generate the matlab code
fid = fopen(mfile, 'w');
fprintf(fid, 'function [g, e, vr, n, vm] = %s\n\n', funct_name);
fprintf(fid, '%% this function was generated on %s using:\n', date);
fprintf(fid, '%% convert_dicomdict(''%s'', ''%s'')\n\n', dictionary, funct_name);
for i=1:size(group, 1)
  fprintf(fid, 'g(%d,:)=''%c%c%c%c''; ', i, group(i,:)');
  fprintf(fid, 'e(%d,:)=''%c%c%c%c''; ', i, element(i,:)');
  fprintf(fid, 'vr{%d}=''%s''; ', i, vr{i});
  fprintf(fid, 'n{%d}=''%s''; ', i, name{i});
  fprintf(fid, 'vm{%d}=''%s''; \n', i, vm{i});
end
fclose(fid);

/* ==========================================================================
 * dicomread_v2.c 
 * example attempt at using dcmtk to read dicoms to matlab faster
 *
 * takes in a filename, and returns an image and a dicom header
 *
 * [dcmhdr, dcmimage] = dicomread_v2('filename.dcm');
 *
 * TODO:
 *
 * To build:
 * mex -v -I/usr/local/dicom/include dicomread_v2.cpp -L/usr/local/dicom/lib/ -ldcmdata -ldcmnet -ldcmpstat -ldcmimage -lofstd -lz
 *
 * To build 64bit version:
 * mex -v -I/usr/local/dicom64/include dicomread_v2.cpp -L/usr/local/dicom64/lib64/ -ldcmdata -ldcmnet -ldcmpstat -ldcmimage -lofstd -lz
 *
 * To allow building of 64bit version, some more system tweaks are needed:
 * get dcmtk, unpack, then run following commands:
 *
 * ./configure CFLAGS="-fPIC" CXXFLAGS="-fPIC" --prefix=/usr/local/dicom64 --libdir=/usr/local/dicom64/lib64
 * 
 * then log as root via "su" and do:
 * make install
 * make install-lib
 * echo "/usr/local/dicom64/lib64" >> /etc/ld.so.conf
 * /sbin/ldconfig
 *
 *
 *
 * Orig. version: Matus Straka, 08/2008
 * Version 2: Matus Straka, 10/2008, added 64-bit support
 *==========================================================================*/

#include "mex.h"
#include "matrix.h"
#include "string.h"
#include <string>
#include <sstream>
#include <dcmtk/config/cfunix.h>
#include <dcmtk/dcmdata/dcdatset.h>
#include <dcmtk/dcmdata/dcfilefo.h>
#include <dcmtk/dcmdata/dcmetinf.h>
#include <dcmtk/dcmdata/dcdeftag.h>
#include <dcmtk/dcmdata/dcdict.h>
#include <dcmtk/dcmdata/dctag.h>
#include <dcmtk/dcmdata/dchashdi.h>
#include <dcmtk/dcmdata/dcdicent.h>

#define MAXCHARS 500   /* max length of string contained in each field */
#define mwSize size_t 
#define VERBOSE 0

#include <list>
#include <map>
typedef std::list<std::string> StringList;

typedef std::map< std::string, bool> HeaderTagMap;

// Function checks if DCMTK's main global tag->VR dictionary is loaded
// Returns true if yes, false if no
// If returned value is no, program should end otherwise unpredictable problems can arise
bool isDcmtk_DICOM_VR_Dictionary_loaded()
{
    const DcmDataDictionary& globalDcmtkDict = dcmDataDict.rdlock();
    OFBool isLoaded = globalDcmtkDict.isDictionaryLoaded();
    dcmDataDict.unlock();

    return isLoaded;
}

//! Modified DcmDataDictionary class to resolve problems of taking private/global tags first
/*! This class is an inherited class from DcmDataDictionary. It contains only one
    reimplemented function that solves problem of the original implementation that put's global
	tags before private tags. For processing of GE scanner data, we need to prefer private tags first
*/

class PrivateTagPreferredDcmDataDictionary : public DcmDataDictionary
{
public:
	//! Constructor to instantiate this dictionary
	PrivateTagPreferredDcmDataDictionary(OFBool loadBuiltin, OFBool loadExternal)
	// call constructor of the predecessor class
	: DcmDataDictionary(loadBuiltin, loadExternal)
	{
		// nothing to do in the body
	};
	
	//! modified search method that favours private tags
	const DcmDictEntry* findEntry(const char *name) const;
};

//! modified search method that favours private tags
/*! original DcmDataDictionary::findEntry(const char*) version of this function prefers global tags over private tags
    (if both types of the tags are found, the global tag would be returned).

	Our version prefers private tags over global tags.
*/
const DcmDictEntry*
PrivateTagPreferredDcmDataDictionary::findEntry(const char *name) const
{
    const DcmDictEntry* e = NULL;
    const DcmDictEntry* ePrivate = NULL;

    /* search first in the normal tags dictionary aPrivateTagPreferredDcmDataDictionarynd if not found
     * then search in the repeating tags list.
     */
	
	// MS: seems that the function looks for the LAST tag with the name
    DcmHashDictIterator iter;
	
	// we need to overcome a small limitation: this function is 'const', i.e. its 'this' pointer is const.
	// we need to get iterators via normalBegin(), normalEnd(), repeatingBegin(), repeatingEnd() methods,
	// but these are 'non-const', i.e. they expect that the object on what their are called is not const.
	// So we create another reference to current object which we manually cast to be non-const.
	// Not a nice solution, but works. We just shall not try to change anything.
	PrivateTagPreferredDcmDataDictionary* non_constThis = (PrivateTagPreferredDcmDataDictionary*)this;

    for (iter= non_constThis->normalBegin(); (e==NULL) && (iter!= non_constThis->normalEnd()); ++iter) {
        if ((*iter)->contains(name)) {
            e = *iter;
            if (e->getGroup() % 2) 
            {
                /* tag is a private tag - continue search to be sure to find non-private keys first */
                if (!ePrivate) ePrivate = e;
                e = NULL;
            }
        }
    }

    if (e == NULL) {
        /* search in the repeating tags dictionary */
        OFBool found = OFFalse;
        DcmDictEntryListConstIterator iter2(non_constThis->repeatingBegin());
        DcmDictEntryListConstIterator last(non_constThis->repeatingEnd());
        for (; !found && iter2 != last; ++iter2) {
            if ((*iter2)->contains(name)) {
                found = OFTrue;
                e = *iter2;
            }
        }
    }

	// MS: Original dcmtk implementation prefers global tags
	
    //// if (e == NULL && ePrivate != NULL) {
    ////    /* no standard key found - use the first private key found */
    ////    e = ePrivate;
    ////}
	
	
	// MS: we prefer private tags
	if (ePrivate != NULL) {
        /* MS: if private tag was found for this name, prefer the private over global tag */
        e = ePrivate;
    }

    return e;
}

/*  the gateway routine.  */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    char     filename[MAXCHARS];
    int      iField, nFields;

	double dummy;
	double* pResultCode = &dummy;
	
	/* *****************************************
	 * Input and output checking and parsing
	 * *****************************************/
    if(nlhs != 2)
	{
      mexErrMsgTxt("Output arguments must be [dcmhdr, dcmimg]");
	}
	else
	{		
		plhs[0] = mxCreateLogicalScalar(false);		
		plhs[1] = mxCreateLogicalScalar(false);		
	}
	
	if(nrhs!=1)
		mexErrMsgTxt("one input is required: readdicom_v2('filename')");	
    	
	PrivateTagPreferredDcmDataDictionary dictionary(true, true);
	
	
    if(!dictionary.isDictionaryLoaded())
    {
        mexPrintf("DICOM dictionary is not loaded\n");
    	mexErrMsgTxt("DICOM dictionary is not loaded");
    }    
	
    mxGetString(prhs[0],filename,MAXCHARS-1);
    
	DcmFileFormat fileformat;	

    // load DICOM header from file
    OFCondition status = fileformat.loadFile(filename);
    if(status.bad())
    {
        char errBuf[MAXCHARS];
        mexPrintf("read error: %s (file %s)\n", status.text(), filename);
		sprintf(errBuf, "Can't read %s", filename);
		mexErrMsgTxt(errBuf);
        return;
    }
	
	DcmDataset* dataset = fileformat.getDataset();
	DcmMetaInfo* metaInfo = fileformat.getMetaInfo();
	
	StringList tagNames;
	StringList tagValues;
    
    HeaderTagMap map;
	
	for(int i = 0; i < 2; i++)
	{
		DcmStack stack; 
   		DcmObject *object = NULL; 
   		DcmElement *elem = NULL; 
   		
		OFCondition status;
		
        int level = 0;
        OFBool intoSub = OFTrue;
		
		do
		{ 			
			if(i == 0)
				status = metaInfo->nextObject(stack, intoSub); 
			else if(i == 1)
				status = dataset->nextObject(stack, intoSub); 
            
            if(level == 0)
            {
                level = 1;
                intoSub = OFFalse;
            }

            if(status.bad())
				break;
						
            object = stack.top(); 
						
            elem = (DcmElement *)object; 
						
            DcmTag tag = elem->getTag();
            const char* tagName = tag.getTagName();
			
            if(tag.getEVR() == EVR_na)
            {
            }
            else if(strcmp(tagName, DcmTag_ERROR_TagName) == 0)
            {
            }
            else if(tag.getEVR() == EVR_SQ)
            {
            }
            else if(strcmp(tagName, "PixelData") == 0)
            {
            }
            else
            {
                OFString val;				
                OFCondition status2 = elem->getOFStringArray(val);
                if(status2.good())
                {
                    HeaderTagMap::iterator it = map.find(tagName);
                    if(it == map.end())
                    {                    
                        tagNames.push_back(tagName);
                        tagValues.push_back(val.c_str());
                        map.insert(HeaderTagMap::value_type(tagName, true));

                        #if VERBOSE
                            mexPrintf("%s : %s\n", tagName, val.c_str());
                        #endif
                    }
                }			
            }            
			
		} while(status.good());
	}
	
	int nHeaderElems = tagNames.size();
	
	mxDestroyArray(plhs[0]);
	
	int i;
	const char ** field_names;
	field_names = new const char*[nHeaderElems];
	
	StringList::const_iterator it = tagNames.begin();
	for (i=0; i < nHeaderElems; i++) 
	{
		field_names[i] = (*it).c_str();
		++it;
	}
	
    int dims[2];
	dims[0] = 1;
	dims[1] = 1;
	
	int name_field, value_field;    

	// Create a 1-by-n array of structs. 
    plhs[0] = mxCreateStructArray(2, dims, nHeaderElems, (const char**)field_names);
	
	//delete[] field_names;

    name_field = mxGetFieldNumber(plhs[0],"name");
    value_field = mxGetFieldNumber(plhs[0],"value");
	
	it = tagValues.begin();
	
	for (i=0; i<nHeaderElems; i++) 
	{
		mxSetFieldByNumber(plhs[0],0,i,mxCreateString((*it).c_str()));
		++it;
    }
	
	int rows = 0;
	int columns = 0;
    int colors = 1;
	
    OFString valStr;

    // get nElements in Y-axis
    if(dataset->findAndGetOFString(DCM_Rows, valStr).good())
    {
        rows = atoi(valStr.c_str());        
    }
	
	// get nElements in X-axis
    if(dataset->findAndGetOFString(DCM_Columns, valStr).good())
    {
        columns = atoi(valStr.c_str());        
    }
	
	// try to find how many bits are representing contents of the dataset
    int bits=0;
    if(dataset->findAndGetOFString(DCM_BitsAllocated, valStr).good())
        bits = atoi(valStr.c_str());

    // get minimum and maximum values
    double minval=0.0;
    double maxval=0.0;

    if(dataset->findAndGetOFString(DCM_SmallestImagePixelValue, valStr).good())
        minval = atof(valStr.c_str());
    if(dataset->findAndGetOFString(DCM_LargestImagePixelValue, valStr).good())
        maxval=atof(valStr.c_str());

    // determine if the data in the DICOM files are signed or unsigned
    bool unsigned_vals = false;
    if(minval >= 0.0) unsigned_vals = true;

    unsigned long count = 0;

    const unsigned char* uint8_ptr=0;
    const unsigned short* uint16_ptr=0;
    const short* sint16_ptr=0;
	
	if(dataset->findAndGetOFString(DCM_SamplesPerPixel, valStr).good())
	{
		colors = atoi(valStr.c_str());
		if((colors < 1) || (colors > 3))
        {
			colors = 1;
        }
		
	}		
    
    #if VERBOSE
        mexPrintf("bits: %d\n", bits);
        mexPrintf("colors %d\n", colors);
        mexPrintf("unsigned: %d\n", unsigned_vals);
        mexPrintf("minval: %f\n", minval);
        mexPrintf("maxval: %f\n", maxval);
        mexPrintf("rows: %d\n", rows);
        mexPrintf("columns: %d\n", columns);
    #endif
    
	mxClassID dataType = mxUNKNOWN_CLASS;
	
    // if data are represented as 8 bits and uint8 ptr is valid, keed the ptr
    if((bits == 8) && (dataset->findAndGetUint8Array(DCM_PixelData, uint8_ptr, &count).good()))
		
    {
        // result is store in 'count' and 'uint_ptr' variables in the if() call
		dataType = mxUINT8_CLASS;
    }
    // if data are represented as 16 bits and uint16 ptr is valid and smallest value in file is positive,
    // the keep the uint16 ptr, else use the sint16ptr
    else if((bits == 16) &&
            (dataset->findAndGetUint16Array(DCM_PixelData, uint16_ptr, &count).good()))
    {
        if(!unsigned_vals) {
            sint16_ptr=(const short*)uint16_ptr;
            uint16_ptr=0;
			dataType = mxINT16_CLASS;
        }
		else
			dataType = mxUINT16_CLASS;	
    }
     // if data are represented as 16 bits and sint16 ptr is valid, then keep the ptr
    else if((bits == 16) &&
            (dataset->findAndGetSint16Array(DCM_PixelData, sint16_ptr, &count).good()))
    {
        // result is store in 'count' and 'uint_ptr' variables in if() call
		dataType = mxINT16_CLASS;
    }
    else
    {
        mexPrintf("no image data found\n");
        mexErrMsgTxt("no image data found\n");	
    }

    // check if the data size reflects the X/Y dimensions
    if((rows * columns * colors) < (count-1) )
    {
        mexPrintf("data corrupted %d %d %d %d %d\n", rows, columns, colors, rows * columns * colors, count);
        mexErrMsgTxt("data corrupted\n");	
    }
		
	int x, y, c;
	
	
	if(dataType == mxUINT16_CLASS)
	{
		int dims2[2];
		dims2[0] = rows;
		dims2[1] = columns;
		
        mxDestroyArray(plhs[1]);
		plhs[1] = mxCreateNumericArray(2, dims2, dataType, mxREAL);
		
		unsigned short* matlab_pixelData = (unsigned short *)mxGetData(plhs[1]);
		
		int cntr1 = 0;
		int cntr2 = 0;
		int cntr3 = 0;
		
		for(y = 0; y < rows; y++)
		{
			cntr3 = cntr2;
			for(x = 0; x < columns; x++)
			{
				matlab_pixelData[cntr3] = uint16_ptr[cntr1];
				cntr1++;
				cntr3 += rows;				
			}
			cntr2 += 1;
		}
	}
	else if(dataType == mxINT16_CLASS)
	{
		int dims[2];
		dims[0] = rows;
		dims[1] = columns;
		
        mxDestroyArray(plhs[1]);
		plhs[1] = mxCreateNumericArray(2, dims, dataType, mxREAL);
		
		short* matlab_pixelData = (short *)mxGetData(plhs[1]);
		int cntr1 = 0;
		int cntr2 = 0;
		int cntr3 = 0;
		
		for(y = 0; y < rows; y++)
		{
			cntr3 = cntr2;
			for(x = 0; x < columns; x++)
			{
				matlab_pixelData[cntr3] = sint16_ptr[cntr1];
				cntr1++;
				cntr3 += rows;				
			}
			cntr2 += 1;
		}
	}
	else if(dataType == mxUINT8_CLASS)
	{
		if(colors == 1)
		{
			int dims[2];
			dims[0] = rows;
			dims[1] = columns;
			
            mxDestroyArray(plhs[1]);
			plhs[1] = mxCreateNumericArray(2, dims, dataType, mxREAL);
			
			unsigned char* matlab_pixelData = (unsigned char *)mxGetData(plhs[1]);
			int cntr1 = 0;
			int cntr2 = 0;
			int cntr3 = 0;
			
			for(y = 0; y < rows; y++)
			{
				cntr3 = cntr2;
				for(x = 0; x < columns; x++)
				{
					matlab_pixelData[cntr3] = uint8_ptr[cntr1];
					cntr1++;
					cntr3 += rows;				
				}
				cntr2 += 1;
			}
		}
		else
		{
			int dims[2];
			dims[0] = rows;
			dims[1] = columns;
			dims[2] = colors;
            			
            mxDestroyArray(plhs[1]);
			plhs[1] = mxCreateNumericArray(3, dims, dataType, mxREAL);
			
			unsigned char* matlab_pixelData = (unsigned char *)mxGetData(plhs[1]);
			int cntr1 = 0;
			int cntr2 = 0;
			int cntr3 = 0;
			int cntr4 = 0;
			
			for(y = 0; y < rows; y++)
			{
				cntr3 = cntr2;
				for(x = 0; x < columns; x++)
				{
					cntr4 = cntr3;
					for(c = 0; c < colors; c++)
					{
						matlab_pixelData[cntr4] = uint8_ptr[cntr1];
						cntr1++;
						cntr4 += columns*rows;
					}
					cntr3 += rows;
				}
				cntr2 += 1;
			}
		}
	}

    return;
}

function metadata = dicomquickinfo(dirname)
%DICOMQUICKINFO   Read information quickly from a set of DICOM files

d = dir(dirname);

% Read the tags and positions for the first file in a directory.
p = 1;
while (d(p).isdir)
    p = p + 1;
end

filename = d(p).name;
[first_tags, first_pos] = read_first_file(filename);
metadata = cell((numel(d) - p + 1), 3);

% Only worry about the positions of the required tags.
important_tags = request_important_tags;
[first_tags, first_pos] = winnow_tags(first_tags, first_pos, important_tags);
first_tags = cast_tags(first_tags);

% For remaining files get positions and data of important tags.
for q = p:numel(d)
    
    [tags, pos, data] = quick_info(d(q).name, first_tags, first_pos);
    metadata((q - p + 1), :) = {tags, pos, data};
    
end



function [tags, pos] = read_first_file(filename)

file = dicom_create_file_struct;
file.Messages = filename;
file.Location = 'Local';
file = dicom_get_msg(file);

file = dicom_open_msg(file, 'r');
info = dicom_create_meta_struct(file);
[tags, pos, info, file] = dicom_get_tags(file, info);
file = dicom_close_msg(file);



function [tags, pos, data] = quick_info(filename, first_tags, first_pos)

% Open file for reading.  Assume Explicit little-endian.
fid = fopen(filename, 'r', 'ieee-le');

if (fid == -1)
    error(sprintf('Couldn''t open file: %s', filename))
end

count = 0;
tags = {};
pos = {};
data = {};

for p = 1:size(first_tags, 1)
    
    % Move to the approximate location in the file where the attribute
    % should be.  Use a window 200 bytes wide.
    fpos_start = max(0, first_pos(p) - 100);
    
    fseek(fid, fpos_start, 'bof');
    buffer = fread(fid, 200, 'uint8=>uint8');
    idx = findNumericPattern(buffer, first_tags(p,:));
    
    if (~isempty(idx))
        
        count = count + 1;
        
        tags{count} = first_tags(p,:);
        pos{count} = fpos_start + idx - 1;
        data{count} = get_attr_data(fid, pos{count}, tags{count});
        
    end
        
end

fclose(fid);



function tags = request_important_tags
%REQUEST_IMPORTANT_TAGS   Get a list of important metadata tags.

req_tags = {'0002', '0003',
            '0002', '0010',
            '0008', '0060',
            '0010', '0020',
            '0020', '000D',
            '0020', '000E',
            '0020', '0010',
            '7FE0', '0010'};

tags = repmat(uint16(0), size(req_tags));
for p = 1:numel(req_tags)
    tags(p) = sscanf(req_tags{p}, '%x');
end



function [tags_out, pos_out] = winnow_tags(tags_in, pos_in, important_tags);
%WINNOW_TAGS   Find the important tags from all of the tags.

count = 0;
tags_out = uint16([]);
pos_out = [];

for p = 1:size(important_tags, 1)
    
    group = (tags_in(:, 1) == important_tags(p, 1));
    element = (tags_in(:, 2) == important_tags(p, 2));
    
    idx = find((group + element) == 2);
    
    if (~isempty(idx))

        count = count + 1;
        tags_out(count, :) = tags_in(idx, :);
        pos_out(count, :) = pos_in(idx, :);
        
    end
    
end



function data = get_attr_data(fid, pos, tag)
%GET_ATTR_DATA   Get an attribute's data.

% Determine if we're using explicit VR data.
if (tag(1) < 8)
    
    explicit_vr = true;
    
    fseek(fid, pos + 4, 'bof');
    vr = fread(fid, 2, 'char=>char')';
    
else
    
    fseek(fid, pos + 4, 'bof');
    vr = fread(fid, 2, 'char=>char')';
    
    switch (vr)
    case {'AE','AS','CS','DA','DT','LO','LT','PN','SH','ST','TM','UI', ...
          'UT','AT','OW','US','DS','IS','FL','FD','OB','UN','SL','SQ', ...
          'SS','UL'}
        
        explicit_vr = true;
        
    otherwise
    
        fseek(fid, -2, 'cof');
        explicit_vr = false;
        
    end
    
end

% Determine the amount of data for the attribute.
if (explicit_vr)
    
    switch (vr)
    case {'AE','AS','AT','CS','DA','DS','DT','FD','FL','IS','LO','LT', ...
          'PN','SH','SL','SS','ST','TM','UI','UL','US'} 

        len = fread(fid, 1, 'uint16');
        
    otherwise
        
        fseek(fid, 2, 'cof');
        len = fread(fid, 1, 'uint32');
        
    end
        
else
    
    len = fread(fid, 1, 'uint32');
    
end

% Read the data unless it's a sequence.
if (len < uint32(inf))

    if (isequal(tag, [224 127 16 0]))
        data = fread(fid, len, 'uint16=>uint16');
    else
        data = char(fread(fid, len, 'uint8=>uint8'))';
    end
    
else
    
    data = 'SEQUENCE';
    
end



function out = cast_tags(in)
%CAST_TAGS   Convert UINT16 tags to UINT8 stream.

in = in';
out = cast(in(:), 'uint8');
out = reshape(out, [4, numel(out)/4]);
out = out';

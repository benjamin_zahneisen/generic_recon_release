% MWEB_HASH
% 
% h = mweb_hash( capacity, factor)
%
% Creates a new hashtable with the specified capacity and factor.
%
% Arguments
%  capacity          -  The original capacity of the hashtable
%  factor            -  When the number of items stored > capacity * factor
%                       the capacity of the table is increased
%
% Usage
%
%   h = hashtable
%   h.x = 10;
%   y = h.x;
%
% You can also use dynamic indexing with strings that
% do not map to valid matlab variable names
%
%   h = hashtable
%   h.('a funny key') = 10;
%   y = h.('a funny key');
%
function h = hashtable(capacity, factor)

   if exist('capacity' , 'var')
      buckets = capacity;
   else
      buckets = 10;
      capacity = 10;
   end

   if ~exist('factor', 'var')
      factor = 0.75;
   end

   item_count = 0;
   contents = cell(1, buckets);

   h.put = @put;
   h.get = @get;
   h.display = @display_;

%   h = class(h, 'hashtable');

   function display_
      for i = 1:length(contents)
          e = contents{i};
          if ~isempty(e)
              for j = 1:length(e)
                % Get a textual representation of the value
                val = evalc('disp(e(j).value)');
                % Strip trailing carriage returns
                if val(end) == sprintf('\n')
                    val=val(1:end-1);
                end
                fprintf('%s\t : %s', e(j).key, val );
             end 
          end
      end
   end


   function out = get(key)
      i = hash(key);
      if isempty(contents{i})
         out = [];
         return;
      end
      items = contents{i};
      for j = 1:length(items);item = items(j);
         if isequal(item.key, key)
            out = item.value;
            return;
         end
      end
      out = [];
      return;
   end

   function put(key, value)
      item_count = item_count + 1;
      put_private(key, value);
      if factor * capacity < item_count
         rehash;
      end
   end

   function put_private(key, value)
      entry.key = key;
      entry.value = value;

      i = hash(key);

      if isempty(contents{i})
         contents{i} =  entry ;
      else
         j = find(strcmp(key, {contents{i}.key}));
         if ~isempty(j)
             contents{i}(j) = entry;
         else
             contents{i}(end+1) = entry;
         end
      end

   end
   
 

   function rehash
      % Increase capacity
      buckets = round( buckets / factor );
      capacity = round( buckets / factor );
      old_contents = contents;
      contents = cell(1,buckets);

      for i = 1:length(old_contents)
          e = old_contents{i};
          if ~isempty(e)
              for j = 1:length(e)
                put_private(e(j).key, e(j).value);
             end 
          end
         pass = true;
      end
   end


   function i = hash(string)
      i = rem(hashcode(string),buckets) + 1;
   end

end

function hash = hashcode(str)
    str = uint32(str(:))';
    hash = uint32(5381);
    for c = str
        hash = bitshift(hash, 5) + hash + c;
    end
end




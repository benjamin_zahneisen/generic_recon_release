/* ==========================================================================
 * dicomwrite_v2.c 
 * example attempt at using dcmtk to replace matlab's dicomwrite()
 *
 * takes in a filename, an image, and a dicom header
 *
 * dicomwrite_v2(img,'filename.dcm',dcmhdr);
 *
 * TODO:
 * Check dataset size/type, and set some dicom flags based on it.
 * Much more error checking!
 * Colored outputs.
 *
 * To build 32bit version:
 * mex -v -I/usr/local/dicom/include dicomwrite_v2.cpp -L/usr/local/dicom/lib/ -ldcmdata -ldcmnet -ldcmpstat -ldcmimage -lofstd
 *
 * To build 64bit version:
 * mex -v -I/usr/local/dicom64/include dicomwrite_v2.cpp -L/usr/local/dicom64/lib64/ -ldcmdata -ldcmnet -ldcmpstat -ldcmimage -lofstd -lz
 *
 * To allow building of 64bit version, some more system tweaks are needed:
 * get dcmtk, unpack, then run following commands:
 *
 * ./configure CFLAGS="-fPIC" CXXFLAGS="-fPIC" --prefix=/usr/local/dicom64 --libdir=/usr/local/dicom64/lib64
 *
 * w/o compression support and need for libz, which is missing on chmr's blade servers
 * ./configure --without-zlib CFLAGS="-fPIC" CXXFLAGS="-fPIC" --prefix=/usr/local/dicom64 --libdir=/usr/local/dicom64/lib64
 * mex -v -I/usr/local/dicom64/include dicomwrite_v2.cpp -L/usr/local/dicom64/lib64/ -ldcmdata -ldcmnet -ldcmpstat -ldcmimage -loflog -lofstd
 * ETP added -loflog
 * 
 * then log as root via "su" and do:
 * make install
 * make install-lib
 * echo "/usr/local/dicom64/lib64" >> /etc/ld.so.conf
 * /sbin/ldconfig
 *
 *
 *
 * Orig. version: Rex Newbould, 2006-2007
 * Version 2: Matus Straka, 08/2008
 * Version 3: Matus Straka, 10/2008, added 64-bit support
 *==========================================================================*/


#include "mex.h"
#include "matrix.h"
#include "string.h"
#include <string>
#include <sstream>
#include <dcmtk/config/cfunix.h>
#include <dcmtk/dcmdata/dcdatset.h>
#include <dcmtk/dcmdata/dcfilefo.h>
#include <dcmtk/dcmdata/dcdeftag.h>
#include <dcmtk/dcmdata/dcdict.h>
#include <dcmtk/dcmdata/dctag.h>
#include <dcmtk/dcmdata/dchashdi.h>
#include <dcmtk/dcmdata/dcdicent.h>

#define MAXCHARS 16000   /* max length of string contained in each field */
#define mwSize size_t 
#define VERBOSE 0

// Function checks if DCMTK's main global tag->VR dictionary is loaded
// Returns true if yes, false if no
// If returned value is no, program should end otherwise unpredictable problems can arise
bool isDcmtk_DICOM_VR_Dictionary_loaded()
{
    const DcmDataDictionary& globalDcmtkDict = dcmDataDict.rdlock();
    OFBool isLoaded = globalDcmtkDict.isDictionaryLoaded();
    dcmDataDict.unlock();

    return isLoaded;
}

//! Modified DcmDataDictionary class to resolve problems of taking private/global tags first
/*! This class is an inherited class from DcmDataDictionary. It contains only one
    reimplemented function that solves problem of the original implementation that put's global
	tags before private tags. For processing of GE scanner data, we need to prefer private tags first
*/

class PrivateTagPreferredDcmDataDictionary : public DcmDataDictionary
{
public:
	//! Constructor to instantiate this dictionary
	PrivateTagPreferredDcmDataDictionary(OFBool loadBuiltin, OFBool loadExternal)
	// call constructor of the predecessor class
	: DcmDataDictionary(loadBuiltin, loadExternal)
	{
		// nothing to do in the body
	};
	
	//! modified search method that favours private tags
	const DcmDictEntry* findEntry(const char *name) const;
};

//! modified search method that favours private tags
/*! original DcmDataDictionary::findEntry(const char*) version of this function prefers global tags over private tags
    (if both types of the tags are found, the global tag would be returned).

	Our version prefers private tags over global tags.
*/
const DcmDictEntry*
PrivateTagPreferredDcmDataDictionary::findEntry(const char *name) const
{
    const DcmDictEntry* e = NULL;
    const DcmDictEntry* ePrivate = NULL;

    /* search first in the normal tags dictionary aPrivateTagPreferredDcmDataDictionarynd if not found
     * then search in the repeating tags list.
     */
	
	// MS: seems that the function looks for the LAST tag with the name
    DcmHashDictIterator iter;
	
	// we need to overcome a small limitation: this function is 'const', i.e. its 'this' pointer is const.
	// we need to get iterators via normalBegin(), normalEnd(), repeatingBegin(), repeatingEnd() methods,
	// but these are 'non-const', i.e. they expect that the object on what their are called is not const.
	// So we create another reference to current object which we manually cast to be non-const.
	// Not a nice solution, but works. We just shall not try to change anything.
	PrivateTagPreferredDcmDataDictionary* non_constThis = (PrivateTagPreferredDcmDataDictionary*)this;

    for (iter= non_constThis->normalBegin(); (e==NULL) && (iter!= non_constThis->normalEnd()); ++iter) {
        if ((*iter)->contains(name)) {
            e = *iter;
            if (e->getGroup() % 2) 
            {
                /* tag is a private tag - continue search to be sure to find non-private keys first */
                if (!ePrivate) ePrivate = e;
                e = NULL;
            }
        }
    }

    if (e == NULL) {
        /* search in the repeating tags dictionary */
        OFBool found = OFFalse;
        DcmDictEntryListConstIterator iter2(non_constThis->repeatingBegin());
        DcmDictEntryListConstIterator last(non_constThis->repeatingEnd());
        for (; !found && iter2 != last; ++iter2) {
            if ((*iter2)->contains(name)) {
                found = OFTrue;
                e = *iter2;
            }
        }
    }

	// MS: Original dcmtk implementation prefers global tags
	
    //// if (e == NULL && ePrivate != NULL) {
    ////    /* no standard key found - use the first private key found */
    ////    e = ePrivate;
    ////}
	
	
	// MS: we prefer private tags
	if (ePrivate != NULL) {
        /* MS: if private tag was found for this name, prefer the private over global tag */
        e = ePrivate;
    }

    return e;
}

/*  the gateway routine.  */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    char     filename[MAXCHARS];
    int      iField, nFields;
	
	/* *****************************************
	 * Input and output checking and parsing
	 * *****************************************/
    if(nlhs > 1)
	{
      mexErrMsgTxt("Too many output arguments.");
	}
	else if(nlhs == 1)
	{
		plhs[0] = mxCreateLogicalScalar(false);		
	}
	else
	{
	}
		
	if(nrhs!=3)
		mexErrMsgTxt("Three inputs are required: dicomwrite_v2(img,'filename.dcm',dcmhdr);");	
    if(!mxIsStruct(prhs[2]))
	  	mexErrMsgTxt("Input #3 must be a structure.");
	
	PrivateTagPreferredDcmDataDictionary dictionary(true, true);
	
	if(!dictionary.isDictionaryLoaded())
    	mexErrMsgTxt("DICOM dictionary is not loaded");
	
    mxGetString(prhs[1],filename,MAXCHARS-1);
    
	// get image dimensions/sizes/type. Accept only 2D greyscale or 3D RGB data
	int imageNDims = mxGetNumberOfDimensions(prhs[0]);
	const int* ptrDimSizes = mxGetDimensions(prhs[0]);
	int imgType = mxGetClassID(prhs[0]) ;

	if (imageNDims == 3) {
		if (ptrDimSizes[2] != 3)
			mexErrMsgTxt("Image data is 3D, but for RGB output the 3rd dimension must have size 3") ;
		if (imgType != mxUINT8_CLASS && imgType != mxINT8_CLASS)
			mexErrMsgTxt("Image data is 3D, but for RGB it must be 8 bits per pixel (cast with uint8)") ;
	} else if (imageNDims == 2) {
		if (imgType != mxINT16_CLASS && imgType != mxUINT16_CLASS)
			mexErrMsgTxt("Image data is 2D, but only 16 bits per pixel (greyscale) is supported.") ;
	} else {
		mexErrMsgTxt("Image data should be a 2D (greyscale) or 3D (RGB) array") ;
	}
	
	
	/* *****************************************
	 * Now find the group and element associated
	 * with each field name in the header, and
	 * insert its contents into the dcmtk dataset
	 * *****************************************/	
    DcmFileFormat dcmfile;
    DcmDataset *dataset = dcmfile.getDataset() ;
    DcmTag tag ;   
	char contents[MAXCHARS] ;
    const char * fieldName ;
	int nTagsIgnored = 0;
	int nTagsFaulty = 0;
    /* Loop through the fields in the header, and find their tag/key, and insert into the dataset. */
    nFields = mxGetNumberOfFields(prhs[2]);
    for (iField=0; iField< nFields; iField++)
	{
        fieldName = mxGetFieldNameByNumber(prhs[2],iField);
        
		// OFCondition status = DcmTag::findTagFromName(fname,tag) ;
		DcmTag tag;
		const DcmDictEntry* dicEnt = dictionary.findEntry(fieldName);
		OFCondition status = EC_Normal;
		if(dicEnt != NULL)
			tag.set(dicEnt->getKey());
		else
			status = EC_TagNotFound;
		
	    if (status.bad()) {
				char errBuf[MAXCHARS];
				sprintf(errBuf, "Error when looking up tag '%s', error: %s\n", fieldName, status.text());
				mexPrintf(errBuf);
		}
		else
		{
			#if VERBOSE
				mexPrintf("Fieldname: %s gives tag (%04x,%04x)\n", fieldName,tag.getGTag() ,tag.getETag() ); /*<< tag.getTagName << endl */
			#endif
		}
		
		
		/* The dicom standard, bless its heart, allows a special tag inside a private group 
		which actually defines the first byte of the elements in that private group.  GE always
		uses "10", but when I look up tags by Name, the dcmtk has not parsed this magic tag (gggg,0010)
		which tells it to use "10", so it always uses "00".  So tags like UserData0 are 0019,00A7
		instead of 0019,10A7.  A hackish but working solution is to simply look up the tag that is
		XXXX,10XX after I get the Group/Element, and if the name is the same, you should have been
		using the 10XX version. */
		
		DcmTag priv_tag(tag.getGTag(), (0x1000 | tag.getETag()) );  
		if (strcmp(priv_tag.getTagName(), fieldName) == 0) {
			#if VERBOSE
				 mexPrintf("This should have used 0x10XX!\n") ;
			#endif
			tag = priv_tag ;
		} else {
			priv_tag.set(tag.getGTag(), (0x1100 | tag.getETag()));
			if (strcmp(priv_tag.getTagName(), fieldName) == 0) {
				#if VERBOSE
					mexPrintf("This should have used 0x11XX!\n") ;
				#endif
				tag = priv_tag ;
			}
		}
		
        /* I'm sure there's a better way to do this: */
        mxArray *tmp = mxGetFieldByNumber(prhs[2],0, iField);
        mxClassID classID = mxGetClassID(tmp);
		mxArray* nameField ;
		int nElems = mxGetNumberOfElements(tmp);
		
        switch (classID) {
         case mxUNKNOWN_CLASS:
              mxCELL_CLASS:
              mxLOGICAL_CLASS:
              mxFUNCTION_CLASS:
                  mexPrintf("%s is of an invalid type!\n",fieldName);
                  mexErrMsgTxt("Invalid field in the structure!\n");
                  break;
         case mxSTRUCT_CLASS:
                  nameField = mxGetField(tmp,0,"FamilyName");
                  if (nameField == NULL) {
                      mexPrintf("%s is a struct, but not containing the family name!\n",fieldName);
                      mexErrMsgTxt("Invalid field in the structure!\n");
                  }
                  mxGetString(nameField, contents, MAXCHARS-1) ;
				  break;
         case mxCHAR_CLASS:
                  mxGetString(tmp, contents, MAXCHARS-1) ;
		 		  break ;
		 case mxDOUBLE_CLASS:
		 		{
					double dvalue  = mxGetScalar(tmp);
					sprintf(contents, "%.10g",dvalue);
					if (nElems > 1) {
						#if VERBOSE
							mexPrintf("Multi-doubles (%d) in %s\n",nElems,fieldName) ;
						#endif
						int ctr = 1 ;
						double *dataptr = (double *) mxGetData(tmp) ;
						while (ctr < nElems) {
							dvalue = dataptr[ctr++] ;
							sprintf(contents, "%s\\%.10g",contents,dvalue);		
						}
					}
				}
				break;
         default:
             /* mxSINGLE_CLASS, mxINT8_CLASS,
        		mxUINT8_CLASS, mxINT16_CLASS,
        		mxUINT16_CLASS, mxINT32_CLASS,
        		mxUINT32_CLASS, mxINT64_CLASS,
        		mxUINT64_CLASS */
		     	int nElems =  mxGetNumberOfElements(tmp);
		 	 	if (nElems == 1 ) {
             		double value  = mxGetScalar(tmp);
             		sprintf(contents, "%lf",value);
			 	} else {
					 mexPrintf("Unimplemented multi-element class.\n") ;
			 	}
        };
        
		// We need to look the VR in dictionary
		tag.lookupVRinDictionary();
		DcmEVR evr = tag.getEVR();
		
		// check if EVR is unknown after dictioary lookup;
		if(evr != EVR_UNKNOWN)
		{
			// try to insert the tag, check if successfull
			OFCondition status;           
            
            #if VERBOSE
				mexPrintf("inserting tag '%s' with value '%s'\n", fieldName, contents);
			#endif
            
            if(evr == EVR_xs)
            {
                unsigned short value = atoi(contents);
                status = dataset->putAndInsertUint16(tag, value);
            }
            else
            {
                status = dataset->putAndInsertString(tag, contents);
            }
			
			if(status.bad())
			{
				char errBuf[MAXCHARS];
				sprintf(errBuf, "Error when inserting tag '%s' with value '%s', error: %s\n", fieldName, contents, status.text());
				mexPrintf(errBuf);
				nTagsFaulty += 1;
			}
		}
		else
			nTagsIgnored += 1;
    }    
	
	
	if(nTagsIgnored > 0)
	{
		// if there were some tags with unrecognized EVR (EVR_UNKNOWN), inform the user
		mexPrintf("Number of ignored tags: %d\n", nTagsIgnored);
	}
	if(nTagsFaulty > 0)
	{
		// if there were some tags which could not be inserted, inform the user
		mexPrintf("Number of faulty tags: %d\n", nTagsFaulty);
	}	
	

  	/* *****************************************
	 * Insert the 2D or 3D data into the file,
	 * and set some tags based on which it is.
	 * *****************************************/
     
	if(((imgType == mxINT16_CLASS) || (imgType == mxUINT16_CLASS)) && (imageNDims == 2))  /* Must be 2D greyscale */
	{
		Uint16 *matlab_imagePixels = (Uint16*)mxGetData(prhs[0]) ;
		
		/* Since matlab is column major, and C is row-major, we have to swap X and Y */
		Uint16 *imagePixels = (Uint16 *) calloc(ptrDimSizes[0] * ptrDimSizes[1], sizeof(Uint16));
		int subs[2], matsubs[2] ;

		for (int y=0; y < ptrDimSizes[0];y++)// rows
		{
			matsubs[0] = y ;
			for (int x=0; x < ptrDimSizes[1];x++) // cols
			{
				matsubs[1] = x ;
				imagePixels[x + y*ptrDimSizes[1]] = (Uint16) matlab_imagePixels[ mxCalcSingleSubscript(prhs[0], 2, matsubs) ];
			}
		}
	
		if(!dataset->putAndInsertUint16(DCM_SamplesPerPixel, 1).good())
			mexErrMsgTxt("DCM header build: DCM_SamplesPerPixel insert failed");
		if(!dataset->putAndInsertUint16(DCM_PixelRepresentation,0).good())
			mexErrMsgTxt("DCM header build: DCM_PixelRepresentation insert failed");
		if(!dataset->putAndInsertString(DCM_PhotometricInterpretation, "MONOCHROME2").good())
			mexErrMsgTxt("DCM header build: DCM_PhotometricInterpretation insert failed");
		if(!dataset->putAndInsertUint16(DCM_BitsAllocated, 16).good())
			mexErrMsgTxt("DCM header build: DCM_BitsAllocatedmexErrMsgTxt( insert failed");
		if(!dataset->putAndInsertUint16(DCM_BitsStored, 16).good())
			mexErrMsgTxt("DCM header build: DCM_BitsStored insert failed");
		if(!dataset->putAndInsertUint16(DCM_HighBit, 15).good())
			mexErrMsgTxt("DCM header build: DCM_HighBit insert failed");
		if(!dataset->putAndInsertUint16Array(DCM_PixelData, imagePixels, ptrDimSizes[0] * ptrDimSizes[1]).good())
			mexErrMsgTxt("DCM header build: DCM_PixelData insert failed");
		
		//free(imagePixels);			
			
     } else {	 /* By elimination this must be a 3D 8-bit RGB image */
			Uint8 *matlab_imagePixels = (Uint8*)mxGetData(prhs[0]) ;
		 
		 	/* The X and Y swap here is automatic in the mxCalcSingleSubscript vs ctr++ indexing,
			but the pixels go: (0,0) R, G, B; (0,1) R, G, B, so we have some resorting to do. */
		 
		 	Uint8 *imagePixels = (Uint8 *) calloc(ptrDimSizes[0] * ptrDimSizes[1]* ptrDimSizes[2], sizeof(Uint8));

			//if (ptrDimSizes[0] != ptrDimSizes[1]){
			//		mexPrintf("dicom_write_v2.cpp: WARNING - non-square colorized dicom not validated. ABORTING\n");
			//		return;
			//}
      
      		int ctr=0, matsubs[3] ;
		 	for (int x=0; x < ptrDimSizes[0];x++) 
			{
				matsubs[0] = x ;
				for (int y=0; y < ptrDimSizes[1];y++) 
				{
					matsubs[1] = y ;
				  	for (int c=0; c<ptrDimSizes[2];c++) 
					{
						matsubs[2] = c ;					
						imagePixels[ ctr++ ] = matlab_imagePixels[ mxCalcSingleSubscript(prhs[0], 3, matsubs) ];
				  	}
				}
			}
			
			if(!dataset->putAndInsertUint16(DCM_SamplesPerPixel, 3).good())
        		mexErrMsgTxt("DCM header build: DCM_SamplesPerPixel insert failed");
		    if(!dataset->putAndInsertUint16(DCM_PixelRepresentation,0).good())
		        mexErrMsgTxt("DCM header build: DCM_PixelRepresentation insert failed");
			if(!dataset->putAndInsertUint16(DCM_PlanarConfiguration,0).good())
		        mexErrMsgTxt("DCM header build: DCM_PlanarConfiguration insert failed");
    		if(!dataset->putAndInsertString(DCM_PhotometricInterpretation, "RGB").good())
        		mexErrMsgTxt("DCM header build: DCM_PhotometricInterpretation insert failed");
    		if(!dataset->putAndInsertUint16(DCM_BitsAllocated, 8).good())
        		mexErrMsgTxt("DCM header build: DCM_BitsAllocatedmexErrMsgTxt insert failed");
    		if(!dataset->putAndInsertUint16(DCM_BitsStored, 8).good())
        		mexErrMsgTxt("DCM header build: DCM_BitsStored insert failed");
    		if(!dataset->putAndInsertUint16(DCM_HighBit, 7).good())
        		mexErrMsgTxt("DCM header build: DCM_HighBit insert failed");
			
			/* Since the VR is OW for the pixel data, we have to use a 16-bit pointer, and put in half
			as many values. */
			OFCondition status = dataset->putAndInsertUint16Array(DCM_PixelData, (Uint16 *) imagePixels, (ptrDimSizes[0] * ptrDimSizes[1] * ptrDimSizes[2])/2);
    		if(!status.good())
        		mexPrintf("DCM header build: DCM_PixelData insert failed with %s", status.text() );
	}
	
	if(!dataset->putAndInsertUint16(DCM_Rows,(Uint16)ptrDimSizes[0]).good())
   		mexErrMsgTxt("DCM header build: DCM_Rows failed");
    if(!dataset->putAndInsertUint16(DCM_Columns,(Uint16)ptrDimSizes[1]).good())
   		mexErrMsgTxt("DCM header build: DCM_Columns insert failed");
			
	// Write the file:
	OFCondition status = dcmfile.saveFile(filename, EXS_LittleEndianExplicit, EET_UndefinedLength, EGL_withGL);
	if(status.bad()) {
		mexPrintf("File %s had problems: %s\n", filename, status.text());
	}
	else
	{
		// everything was fine, so return 1 to caller if they wanted to
        if(nlhs > 0)
        {
            mxLogical* resultFlag = (mxLogical*)mxGetData(plhs[0]);
            *resultFlag = true;
        }
	}
	
    return;
}

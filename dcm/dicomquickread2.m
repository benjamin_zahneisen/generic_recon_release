% function [images] = dicomquickread2(path)
% function [images headers] = dicomquickread2(path)
% function [images headers] = dicomquickread2
%
% reads all the dicom images in a given path. the original dicomquickread
% function did not work well for some datasets. This function reads the
% dicom files according to the ordering of the filenames only.

function [images varargout] = dicomquickread2(varargin)


nout=max(nargout,1)-1;

if numel(varargin) == 1
    fname=varargin{1};
else
    fname='.';
end


[pathstr name ext] = fileparts(fname);

%if fname is a path
if exist(fname,'dir') == 7
    pathstr=fname;
    name='*';
    ext='dcm';
end

if isempty(pathstr)
    pathstr='.';
end

if isempty(name)
    name='*';
end

if isempty(ext)
    ext='.dcm';
end

fname=fullfile(pathstr,[name ext]);

files=dir(fname);

if isempty(files)
    return;
end


%headers are needed
%if nout ==1

% setting dicom dictonary
dicomdict('set',which('gems-dicom-dict.txt'));
D=dicomdict('get');

%end


len=numel(files);
if nout ==1
    header=cell(len,1);
end
files2=cell(len,1);
for i=1:len
    files2{i}=files(i).name;
end
files=sort_nat(files2);
%[hdrdum im]=dicomread_v2(fullfile(pathstr,files{1}));
im=dicomread(fullfile(pathstr,files{1}));
images=zeros([size(im) len]);
for filei=1:len
    %[hdrdum images(:,:,filei)]=dicomread_v2(fullfile(pathstr,files{filei}));
    images(:,:,filei)=dicomread(fullfile(pathstr,files{filei}));
    if nout == 1
        header{filei}=dicominfo(fullfile(pathstr,files{filei}),'dictionary',D);
    end
end

if nout == 1
    varargout{1}=header;
end
end
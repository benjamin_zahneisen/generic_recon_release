function dh = dicomsliceloc(dh, slice)
%DICOMSLICELOC Modifies DICOM metadata for the goemetry of a given slice
% 
%  D2 = DICOMSLICELOC(D1) returns the DICOM header structure, D2, that
%  contains the correct slice location information for the slice
%  number given by D1.InstanceNumber in the original DICOM header
%  structure, D1.
%  
%  It is assumed that D1.ImagePositionPatient is the position vector
%  of the first slice.  From this vector, D1.SpacingBetweenSlices,
%  D1.RASLetterOfImageLocation, and D1.ImageOrientationPatient, new
%  values for the following fields are determined:
%  
%     (0020,0032) D2.ImagePositionPatient
%     (0027,1040) D2.RASLetterOfImageLocation 
%     (0020,1041) D2.SliceLocation
%     (0027,1041) D2.ImageLocation = D2.SliceLocation
% 
%  D2 = DICOMSLICELOC(D1, SLICE) calculates the new location fields
%  assuming the slice number is SLICE, rather than D1.InstanceNumber.
% 
%  DB Clayton - v1.1 2005/11/07


% some other useful fields not currently used here
%   - dcm.FirstScanRAS	       - dcm.LastScanRAS
%   - dcm.FirstScanLocation    - dcm.LastScanLocation


if (nargin < 2), slice = dh.InstanceNumber; end

gap = dh.SpacingBetweenSlices;
ras = dh.RASLetterOfImageLocation;
orient = dh.ImageOrientationPatient;
pvec = dh.ImagePositionPatient;  % position vector for first slice

%ETP added
if dh.FirstScanLocation<dh.LastScanLocation %if the scan progresses from inferior to superior (original option)
    nvec = cross(orient(1:3), orient(4:6));  % unit vector normal to slice plane (original line!)
else %if the scan progresses from superior to inferior
    nvec = cross(orient(4:6),orient(1:3)); % unit vector normal to slice plane, opposite sign from above
end
%end ETP add

pvec = ((slice-1) * gap * nvec) + pvec;  % new position vector

switch ras
  case {'R', 'L'}
    loc = pvec(1);
    if (loc >= 0), ras='R'; else ras='L'; end
  case {'A', 'P'}
    loc = pvec(2);
    if (loc >= 0), ras='A'; else ras='P'; end
  case {'S', 'I'}
    loc = pvec(3);
    if (loc >= 0), ras='S'; else ras='I'; end
  otherwise
    error('unknown RASLetterOfImageLocation: %s', ras);
end

dh.RASLetterOfImageLocation = ras;
dh.ImagePositionPatient = pvec;
dh.SliceLocation = loc;
dh.ImageLocation = loc;

function genuids(varargin)

if nargin == 0
   h = read_gehdr(list_pfiles);
   calmatfile = sprintf('P%05d_%d.mat',h.rdb.run_int,h.rdb.user3);
else
   calmatfile = varargin{1};   
end

if exist(calmatfile,'file') && ~isempty(whos('-file',calmatfile,'seriesuid'))
  load(calmatfile,'seriesuid'); % will read in 'seriesuid' (if it exist) from 'calmatfile'
else
  for s = 1:20
    seriesuid{s} = uid_gedcm;
  end
end

if exist(calmatfile,'file') && ~isempty(whos('-file',calmatfile,'sopinstanceuid'))
  load(calmatfile,'sopinstanceuid'); % will read in 'sopinstanceuid' (if it exist) from 'calmatfile'
else
  sopinstanceuid = 0;
end

save(calmatfile,'sopinstanceuid', 'seriesuid','-V6')


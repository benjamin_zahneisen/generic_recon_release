
1. if you are running the reconstruction for the first time call setup.m

2. For the reconstruction of a p-file (acquired with a generic flavor sequence) do the following:
    - switch to the directly containing a single p-file
    - call rcn = generic_recon2();

    The code should perform the reconstruction and the complex output rcn is returned. Additionaly, a nifti-folder (and a dicom) folder 
    is generated and the reconstructed is exportet to a nifti file.

3. The joint up/down reconstruction pipeline can be executed by calling joint_recon_wrapper.m
    

function GC = generic_ghostCorrection(data,Kidx_odd)

%data = fully sampled dataset (5d = [Nx Ny Nz Ns Nc]

    %calculate ghost correction parameters from fully sampled references
    %scans
    GC=ghostCorrection([size(data,1) size(data,2) size(data,3)],Kidx_odd);
    %GC.Nsl = size(data,6);
    GC.coilMode = 'coil';
    GC.sliceMode = 'central'; %'central'|slice-by-slice
    GC.sliceMode = 'slice-by-slice'; %'central'|slice-by-slice
    GC.debug = 1;
    GC.Nc = size(data,5);
    GC.Nsl = size(data,4);
    
    %let ghost correction handle sum-of-squares combination
    dataTmp = reshape(data(:,:,:,ceil(size(data,4)/2),:),[GC.N(1) GC.N(2) GC.N(3) GC.Nc]);
    
    %get inital values for fminsearch
    GC=GC.getStartParameters(dataTmp);

    GC=GC.doGhostCorrection(data);
        
    GC = GC.interpolateGhostParams();
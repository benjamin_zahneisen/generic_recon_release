function [data_grid, Kidx,Kidx_evenOdd,w]=generic_sortGrid(data,w,H,kzero)
%sort EPI data into 3d k-space for a SENSE type reconstruction

%the dummy slice part for sense recon is a total mess !!

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 4
    kzero = false;
end

%define full k-space while paying attention that the undersampling is
%periodic
Nx = w.nx;
Ny = w.ny;
Nz = ceil(w.epi_multiband_R)*(w.epi_multiband_factor/w.epi_multiband_R);
% (w.epi_multiband_factor/w.epi_multiband_R) should be integer because it corresponds to the blip pattern
w.epi_multiband_factor_sense = Nz;
w.epi_multiband_R_sense = ceil(w.epi_multiband_R);
w.epi_multiband_fov_sense = w.epi_multiband_factor_sense*(w.epi_multiband_fov/w.epi_multiband_factor); %update fov for dummy slices
effRy = (w.epi_multiband_factor_sense/w.epi_multiband_R_sense)*w.inplane_R;
w.ny_sense = ceil(w.ny/effRy)*effRy;
Ny = w.ny_sense;

Nt = size(data,4);
data_grid = zeros([Nx Ny Nz Nt 1 w.slquant_eff size(data,7)]);
Kidx = zeros([Nx Ny Nz]);
Kidx_evenOdd = zeros([Nx Ny Nz]);

%y-direction
kymax = Ny/2*(2*pi/w.fovy/10);
dky = 2*pi/w.fovy/10;
kyfull = -kymax+dky/2:dky:kymax;
if(kzero)
    kyfull = kyfull + dky/2;
end

%z-direction (caipi blip direction)
if(w.epi_multiband_factor>1)
    dkz = 2*pi/w.epi_multiband_fov_sense;
    kzmax = (Nz/2)*(2*pi/w.epi_multiband_fov_sense);
    kzfull = -kzmax:dkz:(kzmax-dkz);
    if(w.epi_multiband_factor == 2)
        kzfull = kzfull + dkz/2;
    elseif(w.epi_multiband_factor == 3) 
        %kzfull = kzfull + dkz;
    else
       kzfull = kzfull + dkz;
       display('check if kzfull + dkz is ok in generic_sortGrid') 
    end
else
    kzfull=0;
end

[ky,kz]=ndgrid(kyfull,kzfull);

Kfull(:,1) = ky(:);
Kfull(:,2) = kz(:);

%no interleaves any more (w_epi describes a single shot. Period.)
%for i=1:w.ni/w.inplane_R
i=1;
    for n=1:H.Nlines
        if(abs(H.Kyz(1,n)) > max(abs(ky(:))))
           display('kymax!')
           break;
        end
        d = sqrt((ky(:)-H.Kyz(1,n)).^2 + (kz(:)-H.Kyz(2,n)).^2);
        [mind(n) ind(n)]=min(d); %linear index
        [I(n),J(n)] = ind2sub([Ny Nz],ind(n));
        data_grid(:,I(n),J(n),:,:,:,:)=data(:,n,i,:,:,:,:);
        Kidx(:,I(n),J(n))=1;
        %if(mod(n,2))
            Kidx_evenOdd(:,I(n),J(n))=H.evenOdd(n);
        %end
    end
%end
    
Kidx = logical(Kidx);
Kidx_evenOdd = logical(Kidx_evenOdd);

end

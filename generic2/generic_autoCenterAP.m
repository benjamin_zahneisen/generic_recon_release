function [dy_mm,dy_pixel] = generic_autoCenterAP(data_img,w_imag)

%re-center the calibration images after ghost correction

%data_img is a [Nx,Ny,Nz] reconstructed volume

%% image domain shifts
Ny = size(data_img,2);
Nx= size(data_img,1);
[X,Y] = ndgrid(linspace(0,1,Nx),linspace(-1,1,Ny));
Y = repmat(Y,[1 1 size(data_img,3)]);
e=zeros(Ny,1);
for dy = 1:Ny
    tmp = circshift(data_img(:,:,:),[0, dy,0]).*(Y(:,:,:).^2);
    e(dy)= sum(tmp(:));
    %figure; imagesc(circshift(data_img(:,:,32),[0, dx]).*(Y(:,:,1).^2))
end

[~,dy_min] = min(e);

dy_pixel = dy_min;
dy_mm = (dy_min/Ny)*(w_imag.fovy*10);


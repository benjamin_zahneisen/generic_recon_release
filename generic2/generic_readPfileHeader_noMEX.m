function [w_imag, header]=generic_readPfileHeader_noMEX(pfilename) 

%corresponds to header for generic_DPFG.e (2017_04_12)
% 07.07.2018: add support for DV26/27 p-file header
% July 2020: no more mex files, standalone compiled binaries + text files
% instead

%getting header variables
%ADD_NEW_RO_TYPE

%% distinguish between <26 and >26
hdr = read_MR_headers(pfilename,'all');

%% jsonencode only supported from R2016b on
% w_imag=generic_ro_struct_mex; no mext call
% store json instead
[filepath,name,ext] = fileparts(pfilename);
fname_json = fullfile(filepath,'pheader.json');

ver = version;
ver = str2double(ver(1:3));
if ver < 9.1
    json_string = jdataencode(hdr);
    json=savejson('',hdr,fname_json);
else
    json_string = jsonencode(hdr);
    fileID = fopen(fname_json,'w'); 
    fwrite(fileID,json_string,'char'); 
    fclose(fileID);
end

% call c-binary function
out_folder = strcat(filepath,'/');
if ismac
    binary_full_path = strcat(getenv('GENERIC_HOME'),'/generic2/generic_ro_standalone/generic_ro_osx');
elseif isunix
    binary_full_path = strcat(getenv('GENERIC_HOME'),'/generic2/generic_ro_standalone/generic_ro');
else
    disp('os not supported')
end
    command = sprintf('%s %s %s',binary_full_path,fname_json,out_folder);
%disp(command)
[status,output] = system(command);
if status ~= 0
   disp('error when calling generic_ro binary') 
end

% import w_imag, grads and k from text files
w_filename = fullfile(filepath,'w.txt');
k_filename = fullfile(filepath,'k.txt');
k_all_filename = fullfile(filepath,'k_all.txt');
g_filename = fullfile(filepath,'g.txt');

w_imag = generic_import_w_from_files(w_filename,k_filename,k_all_filename,g_filename);

% re-use it for now to be consistent
if(strcmp(hdr.format,'pfile24'))
    hdr=read_gehdr(pfilename);
    header.format = 'pfile24';
else
    disp('pfile DV27 header')
    hdr.rdb = hdr.rdb_hdr; %to be consistent with old format
    header.format = 'pfile27';
end

%% this is from the obsolete read p-file header mex-file approach 
%w_imag.ro_type=hdr.rdb.user0;

%w_imag.fovx=hdr.image.dfov*0.1; % lets hope hdr.image.dfov*0.1 = hdr.rdb.user1
%w_imag.fovy=hdr.image.dfov_rect*0.1;
%w_imag.fovz=hdr.rdb.user14; % fovz can be different than excited slice thickness. 
                            % Keep as user14 and do not use hdr.image.slthick
%w_imag.ni=hdr.rdb.user2;
%w_imag.nx=hdr.rdb.user3;
%w_imag.ny=hdr.rdb.user4;
%w_imag.nz=hdr.rdb.user5;
%w_imag.dts=hdr.rdb.user6;
%w_imag.dts_adc=hdr.rdb.user9;
%w_imag.gmax=hdr.rdb.user7;
%w_imag.gslew=hdr.rdb.user8;
%w_imag.Tmax=hdr.rdb.user9;
%w_imag.slabthk=hdr.rdb.user14;
%w_imag.fn=hdr.image.nex;
%w_imag.fn=hdr.rdb.user34;
%w_imag.fn2=hdr.rdb.user38;
%w_imag.epi_rampsamp=hdr.rdb.user21;
%w_imag.turbine_radial_nspokes=hdr.rdb.user22;
%w_imag.rewind_readout=hdr.rdb.user23;
%w_imag.nav_type=hdr.rdb.user24;
%w_imag.golden_angle=hdr.rdb.user25;
%w_imag.reverse_readout=hdr.rdb.user26;
%w_imag.R=max(1,hdr.rdb.user38); %!!!!this is a dirty hack
%w_imag.R_skip=w_imag.ni; %!!!!
%w_imag.permute = hdr.rdb.user36;
%w_imag.epi_blip_up_down = hdr.rdb.user42;
%w_imag.epi_multiband_factor = hdr.rdb.user29;
%w_imag.epi_multiband_fov = hdr.rdb.user30;
%w_imag.epi_multiband_R = hdr.rdb.user32;
%w_imag.add_epi_ref = hdr.rdb.user16; not yet supported
%w_imag.epi_blip_up_down = hdr.rdb.user44;

%call mex-file (no longer)
%w_imag=generic_gen_ro_mex(w_imag);


%% extra header information after binary (previously mex) call
w_imag.slquant              = hdr.image.slquant;
w_imag.slquant_eff         = hdr.image.slquant/w_imag.epi_multiband_factor;
w_imag.slthick                = hdr.image.slthick;
w_imag.scanspacing      = hdr.image.scanspacing;
w_imag.R_skip                = w_imag.ni;
w_imag.inplane_R           = hdr.rdb.user48;
w_imag.pompCalib          = 0;
w_imag.Rtot                     = w_imag.epi_multiband_factor * w_imag.inplane_R;

%% get pfile-header information
header.b0map               = hdr.rdb.b0map;
header.numdifdirs          = hdr.rdb.user17;  %opdifnumdirs
header.numB0               = hdr.rdb.user18;  %num_B0 
header.blipUpDown          = hdr.rdb.user44; 
header.singleBandRef       = (hdr.rdb.user47 > 0);
header.singleBandRefDown   = (hdr.rdb.user47 > 1);
header.numecho             = hdr.image.numecho;
header.nav_pos             = hdr.rdb.user27;
header.frsize              = hdr.rdb.frame_size;
header.nd                  = hdr.rdb.user12;
if(strcmp(header.format,'pfile27'))
    header.nc               = (hdr.rdb_hdr.dab(2)-hdr.rdb_hdr.dab(1))+1 ;  
else
    header.nc               = hdr.rdb.dab_stop_rcv(1)-hdr.rdb.dab_start_rcv(1)+1;
end          
header.nsl                 = hdr.image.slquant;
header.refscan             = hdr.rdb.user31;
header.sliceOrderMode      = hdr.rdb.user15;
header.scanspacing         = hdr.image.scanspacing;

header.xoff         = 0; %always zero because we account for shifts with theta-wave during readout
header.yoff         = hdr.rdb.yoff;
%header.yoff         = w_imag.ny*(hdr.rdb.yoff/(w_imag.fovy*10))
%uncommented as of 07/13/2018. See also evernote note "progress log"

header.te           = hdr.rdb.te;
header.tr            = hdr.image.tr;
header.bw           = hdr.rdb.bw;

header.fovx         = w_imag.fovx;
header.fovy         = w_imag.fovy;
header.fovz         = w_imag.fovz;
header.slthick      = w_imag.slthick;

%take care of ADC sampling rate
header.decimation = hdr.rdb.frame_size/w_imag.frsize;

header.se_epi = 1; %1 = spin-echo epi ; 0 = GRE-EPI
%save original header
header.hdr = hdr;


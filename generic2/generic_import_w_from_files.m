 function w_imag = generic_import_w_from_files(w_filename,k_filename,k_all_filename,g_filename)

% create empty structure
w_imag = struct();

%% import w-structure file
w_file = fopen(w_filename,'r');

tline = fgetl(w_file);
while ischar(tline)
    tline = fgetl(w_file);
    formatSpec = '%s %f';
    if isempty(tline)
        break
    else
        C = textscan(tline,formatSpec);
        w_imag=setfield(w_imag,C{1}{1},C{2});
    end
       
end
fclose(w_file);

%% get k-space file
fileID = fopen(k_filename,'r');
formatSpec = '%f %f %f';
sizeA = [3 Inf];
A = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
w_imag.kx = A(1,:)';
w_imag.ky = A(2,:)';
w_imag.kz = A(3,:)';

%% get k_all files
fileID = fopen(k_all_filename,'r');
formatSpec = '%f %f %f';
sizeA = [3 Inf];
A = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
k_all_length = size(A,2)/w_imag.ni;
w_imag.kx_all = reshape(A(1,:),[k_all_length,w_imag.ni]);
w_imag.ky_all = reshape(A(2,:),[k_all_length,w_imag.ni]);
w_imag.kz_all = reshape(A(3,:),[k_all_length,w_imag.ni]);

%% get gradient file
fileID = fopen(g_filename,'r');
formatSpec = '%f %f %f';
sizeA = [3 Inf];
A = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
w_imag.gx = A(1,:)';
w_imag.gy = A(2,:)';
w_imag.gz = A(3,:)';
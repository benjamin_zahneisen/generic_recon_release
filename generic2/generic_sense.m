function [rcn,gmap]=generic_sense(data_grid,Kidx,w,H,S)
%performs a sense reconstruction

% data_grid = [Nx Ny Nz Nt 1 Nsl Nc] array of cartesian undersampled raw data
% Kidx = undersampling pattern
% w = generic readout structure
% H = HybridSenseTrajectory object (not used )
% Coil Sensitivity object

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if nargout > 1
    getGmap = true;
else
    getGmap = false;
end


%get reference scan and adjust it in case of zero-filling, sense adjustements, etc
S = S.resize([w.nx w.ny_sense w.slquant],[10*w.fovx 10*w.fovy S.fov(3)],'nearest');
S = S.applyMask(); 

mb = w.epi_multiband_factor;
Ntot = w.slquant;
if (mod(mb,2)==1)
    %odd multiband factor
    sliceInc = Ntot/mb;
    slcIdx = 1:sliceInc:Ntot;
else
    sliceInc = Ntot/mb;
    slcIdx = 1:sliceInc:Ntot;
end

%filter k-space to avoid ringing (only 2d)
k_filter = generic_make_mask([w.nx w.ny_sense],'type','arctan');
k_filter = repmat(k_filter,[1 1 size(data_grid,3) size(data_grid,4) size(data_grid,5) size(data_grid,6) size(data_grid,7)]);
data_grid = data_grid.*k_filter;


NdummySlices = w.epi_multiband_factor_sense-w.epi_multiband_factor;
A = senseOperator(S.getSubset(slcIdx,NdummySlices),Kidx);

display('reconstructing all slices and time frames')
rcn = single(zeros([w.nx w.ny_sense w.slquant size(data_grid,4) 1])); 
for sl=1:size(data_grid,6)
    B(sl) = A.updateCoilSensitivities(S.getSubset(slcIdx+sl-1,NdummySlices),0);
end
rcnTmp = single(zeros([w.nx w.ny_sense mb+NdummySlices size(data_grid,6) size(data_grid,4) 1]));
dim=A.dim;
for sl=1:size(data_grid,6)
    if (dim == 2)
        rcn(:,:,slcIdx+sl-1,:,1) = B(sl)'*permute(squeeze(data_grid(:,:,1,:,1,sl,:)),[1 2 4 3]);
        if(getGmap)
           gmap(:,:,slcIdx+sl-1,:,1) = B(sl).getgmap;
        end
    else
        rcnTmp(:,:,:,sl,:) = B(sl)'*permute(squeeze(data_grid(:,:,:,:,1,sl,:)),[1 2 3 5 4]);
        
        if(getGmap)
           gmap(:,:,slcIdx+sl-1,:,1) = B(sl).getgmap;
        end
    end
end

%re-order multiband slice groups
if mb>1
    for sl=1:size(data_grid,6)
        rcn(:,:,slcIdx+sl-1,:,1) = rcnTmp(:,:,NdummySlices+1:end,sl,:);
    end
end

if(getGmap)
    save('gmap.mat','gmap')
end
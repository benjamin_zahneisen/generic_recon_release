function [data_epi, w_epi]=generic_reOrderAndGrid(data,w)
%input data = [nk x ni x nt x necho x nsl x nc]

%output data_epi = [kx nlines ni nd 1 nsl nc]
    
nd=size(data,3);
nsl=size(data,5);
nc=size(data,6);
ni=size(data,2);
nptsperline=2*w.epi_ramp_res + w.epi_flat_res;
nlines=(w.gx_end-w.gx_start+1)/nptsperline;

w_epi.nlines = nlines;
ky=w.ky_all(w.gx_start+1:w.gx_end+1,:);
kz=w.kz_all(w.gx_start+1:w.gx_end+1,:);
%extract plateaus (=ky-kz per line) 
ky = reshape(ky,[nptsperline nlines w.ni]);
kz = reshape(kz,[nptsperline nlines w.ni]);

w_epi.ky = reshape((ky(round(nptsperline/2),:,:)),[nlines w.ni]);
w_epi.kz = reshape((kz(round(nptsperline/2),:,:)),[nlines w.ni]);
w_epi.kx = (-w.nx/2:w.nx/2-1)*2*pi/w.fovx/10 ; %corresponds to the grid after ramp sampling correction

%get kmax and dk
w_epi.kxmax = w.nx/2*(2*pi/w.fovx/10); %[?]
w_epi.kymax = w.ny/2*(2*pi/w.fovy/10); %[?]
w_epi.kzmax = (w.epi_multiband_factor/2)*(2*pi/w.epi_multiband_fov); %[?]

%k-space in range [-pi pi]
w_epi.kx_pi = (w_epi.kx/w_epi.kxmax)*pi;
w_epi.ky_pi = (w_epi.ky/w_epi.kymax)*pi;
w_epi.kz_pi = (w_epi.kz/w_epi.kzmax)*pi;

% reshape epi data (extract readout lines)
data_epi = data;

%echo index is faster since it divides the data in a single TR
data_epi=permute(data_epi,[1 4 2 3 5 6]);
    
%making data size nx x ny1 x ny2 x 1 x nd x nsl x nc by dividing each acquisition into individual lines
data_epi=reshape(data_epi,nptsperline,nlines,ni,nd,1,nsl,nc);

%reverting every other line
data_epi(:,2:2:end,:,:,:,:,:)=data_epi(end:-1:1,2:2:end,:,:,:,:,:);
w_epi.evenOdd = zeros(nlines,1);
w_epi.evenOdd(2:2:end) = 1;

%ramp sampling
data_epi = generic_rampsamp_corr(data_epi,w);

end

%% ramp sampling correction
function data=generic_rampsamp_corr(data,w)

    nptsperline=2*w.epi_ramp_res + w.epi_flat_res;

    if w.epi_rampsamp == 0
        data=data(w.epi_ramp_res+1:w.epi_ramp_res +w.epi_flat_res,:,:,:,:,:,:);
    elseif w.epi_rampsamp==1
        ind_k=(w.gx_start+1+w.epi_blip_res/2):(w.gx_start+nptsperline);
        ind_data=(1+w.epi_blip_res/2):(nptsperline);
        data=interp1(w.kx(ind_k),...
            data(ind_data,:,:,:,:,:,:),....
            (-w.nx/2:w.nx/2-1)*2*pi/w.fovx/10,...
            'linear',...
            0);
    end

end
function [data_epi, H]=generic_reOrderAndGridRO(data,w)
%input data = [nk x ni x nt x necho x nsl x nc]
% w = generic trajectory structure 

%output data_epi = [kx nlines ni nd 1 nsl nc]
%it is assumed that the data starts with a ramp

%Benjamin Zahneisen, Stanford University, 05/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nd=size(data,3);
nsl=size(data,5);
nc=size(data,6);
ni=size(data,2);
nptsperline=2*w.epi_ramp_res + w.epi_flat_res;
nlines=(w.gx_end-w.gx_start+1)/nptsperline;

%get trajectory(ies)
if(w.epi_blip_up_down)
    if(w.ni == 2)
        ilvIdx = [1 2];
    elseif(w.ni == 4)
       ilvIdx = [1 4]; 
    else
        display('what?')
    end
    acquiredIlvs = 2;
else
    acquiredIlvs = 1;
    ilvIdx = 1;
end

%% get a different trajectory structure for each interleave
for n=1:acquiredIlvs
    H(n) = HybridSenseTrajectory(w,ilvIdx(n));
end


% reshape epi data (extract readout lines)
data_epi = data;

%echo index is faster since it divides the data in a single TR
data_epi=permute(data_epi,[1 4 2 3 5 6]);
    
%making data size nx x ny1 x ny2 x 1 x nd x nsl x nc by dividing each acquisition into individual lines
data_epi=reshape(data_epi,nptsperline,nlines,ni,nd,1,nsl,nc);

%reverting every other line
data_epi(:,2:2:end,:,:,:,:,:)=data_epi(end:-1:1,2:2:end,:,:,:,:,:);
for n=1:length(H)
    H(n).evenOdd = zeros(H(n).Nlines,1);
    H(n).evenOdd(2:2:end) = 1;
end

%ramp sampling
data_epi = generic_rampsamp_corr(data_epi,w);

end

%% ramp sampling correction
function data=generic_rampsamp_corr(data,w)

    nptsperline=2*w.epi_ramp_res + w.epi_flat_res;

    if w.epi_rampsamp == 0
        data=data(w.epi_ramp_res+1:w.epi_ramp_res +w.epi_flat_res,:,:,:,:,:,:);
    elseif w.epi_rampsamp==1
        ind_k=(w.gx_start+1+floor(w.epi_blip_res/2)):(w.gx_start+nptsperline);
        ind_data=(1+floor(w.epi_blip_res/2)):(nptsperline);
        data=interp1(w.kx(ind_k),...
            data(ind_data,:,:,:,:,:,:),....
            (-w.nx/2:w.nx/2-1)*2*pi/w.fovx/10,...
            'linear',...
            0);
    end

end
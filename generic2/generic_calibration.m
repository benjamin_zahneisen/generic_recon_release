function S  = generic_calibration(data,header,espirit_thresh1)
%data is 7D [Nx Ny Nz 1 1 Nsl Nc]

%Benjamin Zahneisen, Stanford University, 05/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 3
    espirit_thresh1 = 0.03;
end

%2d gaussian filter
q = gauss_kernel([size(data,1) size(data,2)],9);
q = q/max(q(:));
q = repmat(q,[1 1 1 1 1 size(data,6) size(data,7)]);
data = data.*q;

%crop data to be symmetric around center

recon = zeros([size(data,1) size(data,2) header.nsl size(data,7)]);
%low resolution sensitivity maps
for sl=1:size(data,6)
    for c=1:size(data,7)
        %recon(:,:,sl,c) = ifff3d(crop(squeeze(data(:,:,:,1,1,sl,c)),[calibSize]));
        recon(:,:,sl,c) = ifff3d(squeeze(data(:,:,:,1,1,sl,c)));
    end
end

%make calibration low res
recon = imresize4D(recon,[48 48 size(recon,3)]);
     
S=CoilSensitivities(recon,'espirit',espirit_thresh1);
%S=CoilSensitivities(single(recon),'adaptive');
S.fov = [10*header.fovx 10*header.fovy header.nsl*header.slthick];
S.type = 'epi';
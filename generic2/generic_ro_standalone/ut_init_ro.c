//
//  call generic_ro.c
//  
//
//  Created by Benjamin A. Zahneisen on 6/27/20.
//

#include <stdio.h>
#include <math.h>
#include "./epic_interface/generic_readouts/generic_ro.h"
#include "./epic_interface/generic_readouts/generic_ro.c"
#include "./epic_interface/generic_readouts/gen_base_ro.c"





int main(int argc, char *argv[] )
{
    // input arguments:
    // 2) output location for kspace.txt
    printf("Program name %s\n", argv[0]);

	//create w readout    
    generic_ro * w = (generic_ro*)malloc(sizeof(generic_ro));
    
    // initialize readout-structure
    init_ro2(w);
    
    // generates the base readout waveform
    gen_base_ro(w);
    
    // generates all interleaves from the base waveform
    gen_all_ro(w);

    write_log_3(w);
    
    char output_folder[256];
    char w_out_filename[256];
    char k_out_filename[256];
    char k_all_out_filename[256];
    char g_out_filename[256];
    
    if(argc == 2){
        strncpy (k_out_filename, argv[1], sizeof(k_out_filename) );
        strcat(k_out_filename,"k.txt");

        // file for k(x,y,z,)_all
        strncpy(k_all_out_filename,argv[1],sizeof(k_all_out_filename));
        strcat(k_all_out_filename,"k_all.txt");
        
        // file for g(x,y,z,)
        strncpy(g_out_filename,argv[1],sizeof(g_out_filename));
        strcat(g_out_filename,"g.txt");
        
        // full file for w structure
        strncpy(w_out_filename,argv[1],sizeof(w_out_filename));
        strcat(w_out_filename,"w_init.txt");
    }
    else{
        strncpy (output_folder,"./kspace.txt",sizeof("./kspace.txt"));
    }
    printf("Writing k-space to %s\n", k_out_filename);
    write_kspace2(w, k_out_filename);
    
    printf("Writing k-all to %s\n", k_all_out_filename);
    write_k_all(w,k_all_out_filename);
    
    printf("Writing f to %s\n", g_out_filename);
    write_g(w,g_out_filename);
    
    //export w structure to text
    printf("Writing w to %s\n",w_out_filename);
    write_log_2(w,w_out_filename);
    
    // garbage collection
    free_w(w);
  
    
return 0;
}

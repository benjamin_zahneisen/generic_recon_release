#!/bin/bash

echo "compiling generic_ro_standalone for $OSTYPE"

# separate compilations for linux and osx

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # compile on linux
    # the -lm flag is necessary to link the correct math.h library
    gcc -o generic_ro call_generic_ro.c -lm

    gcc -o ut_init_w ut_init_ro.c -lm

elif [[ "$OSTYPE" == "darwin"* ]]; then
    # Mac OSX
    # call osx compiler
    clang call_generic_ro.c -o generic_ro_osx
else
    # Unknown.
    echo "os not supported"
fi

#------------

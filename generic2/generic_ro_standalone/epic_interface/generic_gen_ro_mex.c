#include <string.h>
#include "./generic_readouts/generic_ro.c"


void mexFunction(int nlhs, mxArray *plhs[ ],int nrhs, const mxArray *prhs[ ]) 
{    
    generic_ro  * w_in=ro_matlab_to_c(prhs[0]);    
    generic_ro * w_out = (generic_ro *)malloc(sizeof(generic_ro)); 
    init_ro(w_out);    
    copy_w(w_in,w_out);    
    /*mexPrintf("readout type = %d\n",w_out->ro_type);*/
    gen_base_ro(w_out);  
/*
    
    add_ramp_prewinder_to_g(w_out);
    add_nav_before_g(w_out);
    
*/
    
    gen_all_ro(w_out);
    plhs[0]=ro_c_to_matlab(w_out);
    free_w(w_out);    
    free_w(w_in);    
};

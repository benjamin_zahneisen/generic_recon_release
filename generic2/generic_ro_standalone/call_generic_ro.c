//
//  call generic_ro.c
//  
//
//  Created by Benjamin A. Zahneisen on 6/27/20.
//

#include <stdio.h>
#include <math.h>
#include "./epic_interface/generic_readouts/generic_ro.h"
#include "./epic_interface/generic_readouts/generic_ro.c"
#include "./epic_interface/generic_readouts/gen_base_ro.c"

// json support
#include "./cJSON-master/cJSON.h"
#include "./cJSON-master/cJSON.c"

// extract integer value from cjson structure
int get_int_value(cJSON * cjson_struct, char  * field_name){
    
    cJSON *cjson_field = NULL;
    int cjson_value;
    cjson_field = cJSON_GetObjectItemCaseSensitive(cjson_struct, field_name);
    
    if (cJSON_IsNumber(cjson_field)){
        //if(print_val){
        printf("%s = \"%f\"\n", field_name, cjson_field->valuedouble);
        //}
    }
    
    cjson_value = (int) cjson_field->valuedouble;
    
    return cjson_value;
}

// extract double value from cjson structure
double get_double_value(cJSON * cjson_struct, char  *field_name){
    
    cJSON * cjson_field = NULL;
    double cjson_value;
    cjson_field = cJSON_GetObjectItemCaseSensitive(cjson_struct, field_name);
    
    if (cJSON_IsNumber(cjson_field)){
        printf("%s = \"%f\"\n", field_name, cjson_field->valuedouble);
    }
    
    cjson_value = (double) cjson_field->valuedouble;
    
    return cjson_value;
}


int main(int argc, char *argv[] )
{
    // input arguments:
    // 1) pheader.json file path
    // 2) output location for kspace.txt
    printf("Program name %s\n", argv[0]);

    
    // load json p-file header
    /* declare a file pointer */
    FILE    *infile;
    char    *buffer;
    long    numbytes;
    
    /* open an existing file for reading */
    //char*
    //filepath = argv[1]; //pheader.json
    printf("Reading pfile header from  %s\n", argv[1]);
    infile = fopen(argv[1], "r");
    
    /* quit if the file does not exist */
    if(infile == NULL)
        return 1;
    
    /* Get the number of bytes */
    fseek(infile, 0L, SEEK_END);
    numbytes = ftell(infile);
    
    /* reset the file position indicator to the beginning of the file */
    fseek(infile, 0L, SEEK_SET);
    
    /* grab sufficient memory for the buffer to hold the text */
    buffer = (char*)calloc(numbytes, sizeof(char));
    
    /* memory error */
    if(buffer == NULL)
        return 1;
    
    /* copy all the text into the buffer */
    fread(buffer, sizeof(char), numbytes, infile);
    fclose(infile);
    
       /* confirm we have read the file by
        outputing it to the console */
       //printf("The file called test.dat contains this text\n\n%s", buffer);
    
       /* free the memory we used for the buffer */
       //free(buffer);
    
    int buffer_length;
    buffer_length = numbytes;
    cJSON *json = cJSON_ParseWithLength(buffer, buffer_length);
    
    //char *string = cJSON_Print(json);
    //printf("content of cJSON tree \n\n%s", string);
    
    // this extracts a string value
    const cJSON *format = NULL;
    format = cJSON_GetObjectItemCaseSensitive(json, "format");
    if (cJSON_IsString(format) && (format->valuestring != NULL))
    {
        printf("p-file format \"%s\"\n", format->valuestring);
    }

    
    // get dab structure with user parameters
    const cJSON *rdb_hdr = NULL;
    rdb_hdr = cJSON_GetObjectItemCaseSensitive(json, "rdb_hdr");
    
    // get image structure with fov, etc.
    const cJSON *image = NULL;
    image = cJSON_GetObjectItemCaseSensitive(json, "image");
    
    if(!cJSON_IsObject(rdb_hdr)){
        printf(" is a cJSON object");
    }
        
        const cJSON *user0 = NULL;
        user0 = cJSON_GetObjectItemCaseSensitive(rdb_hdr, "user0");
        int user0_int_val;
    
        //printf("user0 \"%s\"\n", user0->valuestring);
        //if (cJSON_IsNumber(user0)){
         //   printf("user0 \"%f\"\n", user0->valuedouble);
        //}
    //user0_int_val = (int) user0->valuedouble;
        //printf("user0-value \"%d\"\n", user0_int_val);
    
    //int a;
    //a = get_int_value(rdb_hdr,"user0");
    //printf("user0-value from function \"%d\"\n", a);
    
        //const cJSON *user1 = NULL;
        //user1 = cJSON_GetObjectItemCaseSensitive(dab, "user1");
        //if (cJSON_IsNumber(user1)){
            //printf("user1 \"%f\"\n", user1->valuedouble);
    //}
    
    generic_ro * w = (generic_ro*)malloc(sizeof(generic_ro));
    
    // initialize readout-structure
    init_ro(w);
    
    // popule readout_structure fields
    w->ro_type = get_int_value(rdb_hdr,"user0"); // readout type, e.g. EPI

    //fov is read from image-field
    w->fovx= get_double_value(image,"dfov")*0.1; //hdr.image.dfov*0.1; //% let's hope hdr.image.dfov*0.1 = hdr.rdb.user1
    w->fovy = get_double_value(image,"dfov_rect")*0.1; //hdr.image.dfov_rect*0.1;
    w->fovz= get_double_value(rdb_hdr,"user14");// hdr.rdb.user14; % fovz can be different than excited slice thickness. //% Keep as user14 and do not use hdr.image.slthick
    
    // resolution block
    w->ni = get_int_value(rdb_hdr,"user2"); // number of interleaves
    w->nx = get_int_value(rdb_hdr,"user3"); // in-plane resolution Nx
    w->ny = get_int_value(rdb_hdr,"user4"); // in-plane resolution Ny
    w->nz = get_int_value(rdb_hdr,"user5"); // Nz resolution

    // gradient system parameters
    w->dts = get_double_value(rdb_hdr,"user6"); // dwell time hdr.rdb.user6;
    w->dts_adc = get_double_value(rdb_hdr,"user9"); // dwell time adc hdr.rdb.user9;
    w->gmax = get_double_value(rdb_hdr,"user7"); // hdr.rdb.user7;
    w->gslew = get_double_value(rdb_hdr,"user8"); //hdr.rdb.user8;
    //%w->Tmax=hdr.rdb.user9;
    //w->slabthk = get_double_value(rdb_hdr,"user14"); // hdr.rdb.user14;

    // less specific parameters
    w->turbine_radial_nspokes = get_int_value(rdb_hdr,"user22"); //hdr.rdb.user22;
    w->rewind_readout = get_int_value(rdb_hdr,"user23"); //hdr.rdb.user23;
    //w->nav_type = get_int_value(rdb_hdr,"user24"); //hdr.rdb.user24;
    w->golden_angle = get_int_value(rdb_hdr,"user25"); //hdr.rdb.user25;
    w->reverse_readout = get_int_value(rdb_hdr,"user26"); //hdr.rdb.user26;
    
    //acceleration
    int Rtmp;
    Rtmp = get_int_value(rdb_hdr,"user38");
    w->R = Rtmp; //max(1,Rtmp); //!!!!this is a dirty hack
    w->R_skip = w->ni; //!!!!
    w->fn = (float) get_double_value(rdb_hdr,"user34");// fractional nex hdr.rdb.user34;
    w->fn2= get_double_value(rdb_hdr,"user38"); //hdr.rdb.user38;
    w->permute = get_int_value(rdb_hdr,"user36"); //hdr.rdb.user36;
    
    // epi parameters
    w->epi_rampsamp = get_int_value(rdb_hdr,"user21"); //hdr.rdb.user21;
    w->epi_multiband_factor = get_int_value(rdb_hdr,"user29"); //hdr.rdb.user29;
    w->epi_multiband_fov = get_double_value(rdb_hdr,"user30"); //hdr.rdb.user30;
    w->epi_multiband_R = get_int_value(rdb_hdr,"user32"); //hdr.rdb.user32;
    //%w->add_epi_ref = hdr.rdb.user16; not yet supported
    
    // that's how its currently implemented in MATLAB
    w->epi_blip_up_down = get_int_value(rdb_hdr,"user42"); // hdr.rdb.user42;
    w->epi_blip_up_down = get_int_value(rdb_hdr,"user44"); // hdr.rdb.user44;

    //%call mex-file (that's what would happen in MATLAB but NOT here)
    //w_imag=generic_gen_ro(w_imag);
    
    // generates the base readout waveform
    gen_base_ro(w);
    
    // generates all interleaves from the base waveform
    gen_all_ro(w);
    
    // print w information to console
    //write_log_3(w);

    /*%extra header information after mex call
    w->slquant              = hdr.image.slquant;
    w->slquant_eff         = hdr.image.slquant/w->epi_multiband_factor;
    w->slthick                = hdr.image.slthick;
    w->scanspacing      = hdr.image.scanspacing;
    w->R_skip                = w->ni;
    w->inplane_R           = hdr.rdb.user48;
    w->pompCalib          = 0;
    w->Rtot                     = w->epi_multiband_factor * w->inplane_R;
    
    */
    char output_folder[256];
    char w_out_filename[256];
    char k_out_filename[256];
    char k_all_out_filename[256];
    char g_out_filename[256];
    
    if(argc == 3){
        strncpy (k_out_filename, argv[2], sizeof(k_out_filename) );
        strcat(k_out_filename,"k.txt");

        // file for k(x,y,z,)_all
        strncpy(k_all_out_filename,argv[2],sizeof(k_all_out_filename));
        strcat(k_all_out_filename,"k_all.txt");
        
        // file for g(x,y,z,)
        strncpy(g_out_filename,argv[2],sizeof(g_out_filename));
        strcat(g_out_filename,"g.txt");
        
        // full file for w structure
        strncpy(w_out_filename,argv[2],sizeof(w_out_filename));
        strcat(w_out_filename,"w.txt");
    }
    else{
        strncpy (output_folder,"./kspace.txt",sizeof("./kspace.txt"));
    }
    printf("Writing k-space to %s\n", k_out_filename);
    write_kspace2(w, k_out_filename);
    
    printf("Writing k-all to %s\n", k_all_out_filename);
    write_k_all(w,k_all_out_filename);
    
    printf("Writing f to %s\n", g_out_filename);
    write_g(w,g_out_filename);
    
    //export w structure to text
    printf("Writing w to %s\n",w_out_filename);
    write_log_2(w,w_out_filename);
    
    // garbage collection
    free_w(w);
    //cJSON_Delete(rdb_hdr);
    cJSON_Delete(json);
    //cJSON_Delete(rdb_hdr);
  
    
return 0;
}

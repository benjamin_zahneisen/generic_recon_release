function [w, frsize_adc_new] = generic_oversampling(w,ratio,frsize_adc)

%ratio = ADC_dwell/GRAD_UPDATE_TIME

%get # of readouts
nlines = w.frsize/(w.epi_flat_res + 2*w.epi_ramp_res); %this is integer by definition
lineLengthNew =  floor(frsize_adc/nlines);
frsize_adc_new = lineLengthNew*nlines; %upsample data to that length 

newRatio = (w.epi_flat_res + 2*w.epi_ramp_res)/lineLengthNew;

if(ratio < 1)
    %upsample w_imag
    idxOrg = 1:size(w.gx);
    r=newRatio;
    f = 1/newRatio;
    idxNew = 1:r:size(w.gx)+r;
    w.gx = interp1(idxOrg,w.gx,idxNew,'linear');
    w.gy = interp1(idxOrg,w.gy,idxNew,'linear');
    w.gz = interp1(idxOrg,w.gz,idxNew,'linear');
    w.gx_start = round(f*(w.gx_start-1));
    w.gx_end = round(f*w.gx_end);
    w.epi_blip_res = round(f*w.epi_blip_res);
    w.epi_flat_res = round(f*w.epi_flat_res);
    w.epi_ramp_res = round(f*w.epi_ramp_res);
    w.epi_flat_res = lineLengthNew - 2*w.epi_ramp_res; %flat res is arbitrary at this point
    idxOrg = 1:size(w.kx);
    idxNew = 1:r:size(w.kx)+r;
    %w.kx = g2k(w.gx,4*ratio);
    w.kx = interp1(idxOrg,w.kx_all,idxNew,'linear');
    w.kx_all = interp1(idxOrg,w.kx_all,idxNew,'linear');
    w.ky_all = interp1(idxOrg,w.ky_all,idxNew,'linear');
    w.kz_all = interp1(idxOrg,w.kz_all,idxNew,'linear');
    
    % this must also match (w.gx_end-w.gx_start+1)/nptsperline = nlines
    w.gx_start = w.gx_end - frsize_adc_new +1;
end

%acccount for oversampling
w.os = newRatio;

end

function k=g2k(g,dts)
    
    %dts is either 2 or 4
    M_PI = pi;
    gamma = 2.0*M_PI*4.257e3; %/* rad * Hz / G */
    k(1)=0;
    for i=1:length(g)
    %for (i=0;i<gres;i++)
        k(i+1)=k(i)+ g(i)*gamma*(dts*10^-6)*0.1;
    end

end
function hdr = generic_export_pheader(pfilename,outdir)

%% distinguish between <26 and >26
hdr = read_MR_headers(pfilename,'all');
if(strcmp(hdr.format,'pfile24'))
    hdr=read_gehdr(pfilename);
    header.format = 'pfile24';
else
    disp('pfile DV27 header')
    hdr.rdb = hdr.rdb_hdr; %to be consistent with old format
    header.format = 'pfile27';
end

json_string = jsonencode(hdr);

full_file = strcat(outdir);
fileID = fopen('pheader.json','w'); 
fwrite(fileID,json_string,'char'); 
fclose(fileID);


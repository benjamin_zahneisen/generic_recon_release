function [rcn, rcn_nav, w_imag] = generic_recon_upDown(varargin)

%reconstruction for self-navigated up/down-EPI readout (ro_type=15)


%get filename
[pfilename, opts_start]=generic_getpfilename(varargin);

%load header and call readout mex file
[w_imag, header]=generic_readPfileHeader(pfilename);

%make sure this is UD-EPI
if(~(w_imag.ro_type == 15))
    display('wrong recon pipeline')
    return;
end

%load data
data=generic_loadData(pfilename,header,w_imag);

% save some memory and remove empty interleaves
data = single(data(:,1:w_imag.inplane_R:end,:,:,:,:));

%shifting data relative to central fov by applying a linear phase (depends
%on ky-sampling up,down,calibration, ...)
display(['inplane shifts = [',num2str(header.xoff),',',num2str(header.yoff),']'])

%% hack some parameters
%header.blipUpDown = 1;
%header.singleBandRefDown =0;
%header.sliceOrderMode =0;
%header.yoff = 0;
GhostRefLines = false;

    

%% slice re-ordering (linear/interleaved)
if(header.sliceOrderMode == 1)
    slcIdxFull = 1:2:w_imag.slquant;
    slcIdxFull = [slcIdxFull (2:2:w_imag.slquant)];
    [~, slcIdxFull]=sort(slcIdxFull);
    slcIdx = 1:2:w_imag.slquant_eff;
    slcIdx = [slcIdx (2:2:w_imag.slquant_eff)];
    [~, slcIdx]=sort(slcIdx);
else
    slcIdxFull = 1:w_imag.slquant;
    slcIdx = 1:w_imag.slquant_eff;
end


%% split ref and normal epi
if(w_imag.add_epi_ref)
     [w_imag, w_ref,data,data_refLines]=generic_strip_ref(w_imag,data);
     [pc, pc_fit, data_epi_refLines] = generic_getGhostPhase(data_refLines,w_ref);
     
     %average over coils
     %tmp = reshape(pc_fit,[size(pc_fit,1) size(pc_fit,2) size(pc_fit,3)*size(pc_fit,4)]);
     %tmp = pc_fit;
     %tmp = mean(tmp,4);
     %tmp = repmat(tmp,[1 1 1 size(pc_fit,4)]);
     %pc_fit = tmp;
     %pc_fit = reshape(tmp,[size(pc_fit,1) size(pc_fit,2) size(pc_fit,3) size(pc_fit,4)]);
     
     pc_fit = pc_fit(:,:,slcIdx,:);
end


%% grid and separate readout dimension
[data_epi, H]=generic_reOrderAndGridRO(data,w_imag);


%% split host and nav EPI for ud-EPI

    linesNav = ((w_imag.ny/2)*w_imag.fn)/(w_imag.R_skip);
    linesImag = (w_imag.ny*w_imag.fn)/(w_imag.R_skip);
    
    if (~((linesNav + linesImag)==H.Nlines))
        display('wrong navigator lines')
    end
    
    data_epi_nav = data_epi(:,1:linesNav,:,:,:,:,:);
    data_epi = data_epi(:,linesNav+1:end,:,:,:,:,:);
    
    H_nav = H.getSubset(1:linesNav);
    H_epi = H.getSubset(linesNav+1:H.Nlines);
    Hfull =H;
    H(1) = H_epi;
    H(2) = H_nav;


%% single band, central region k-space in case of interleaved EPI(=inplane acceleration >1)
if(header.singleBandRef ==1)
    display('single band calibration')
    
    %this function must match generic_dti.e
    [data_epi_ref,w_ref, H_refUp]=generic_singleBandCalibration(data_epi,w_imag,H(1));
    
    %undo interleaved slice ordering
    data_epi_ref = data_epi_ref(:,:,:,:,:,slcIdxFull,:);
    
    %apply in-plane shifts
    data_epi_ref = generic_inplaneShifts(data_epi_ref,header.xoff,header.yoff,H_refUp);
    
    %sort into kx-ky-(kz=1)-grid
    [data_grid, Kidx, Kidx_odd]=generic_sortGrid(data_epi_ref,w_ref,H_refUp,true);
    

    %NEW ghost correction from ref lines
    if(GhostRefLines)
        data_grid = generic_applyGhostPhase(data_grid,Kidx_odd,pc_fit(:,1,:));
        GC = generic_ghostCorrection(reshape(data_grid(:,:,:,1,1,:,:),[size(data_grid,1) size(data_grid,2) size(data_grid,3) w_imag.slquant size(data_grid,7)]),Kidx_odd); 
    else
        GC = generic_ghostCorrection(reshape(data_grid(:,:,:,1,1,:,:),[size(data_grid,1) size(data_grid,2) size(data_grid,3) w_imag.slquant size(data_grid,7)]),Kidx_odd);      
    
        %make sure the ghost correction matches the actual data
        data_grid = GC.applyPhaseCorrection(data_grid,Kidx_odd);
    end
    
    %ghost correction for hybrid-EPI data
    Kidx_dummy = zeros(size(data_epi_ref,1),size(data_epi_ref,2),1);
    Kidx_dummy(:,logical(H_refUp.evenOdd),:)=1;
    data_epi_ref = GC.applyPhaseCorrection(data_epi_ref,logical(Kidx_dummy));  
    
    %calculate sensitivity maps
    S = generic_calibration(data_grid,header,0.03);
    
    %remove ref-data from data_imag
    data_epi = data_epi(:,:,:,w_imag.epi_multiband_factor+1:end,:,:,:);


        %reconstruct ref lines nav
        [data_epi_ref_nav,w_ref_nav,H_refNav1]=generic_singleBandCalibration(data_epi_nav,w_imag,H(2));
        data_epi_ref_nav = data_epi_ref_nav(:,:,:,:,:,slcIdxFull,:);
        data_epi_ref_nav = generic_inplaneShifts(data_epi_ref_nav,header.xoff,header.yoff,H_refNav1);
        [data_grid_nav, Kidx_nav, Kidx_odd_nav]=generic_sortGrid(data_epi_ref_nav,w_ref_nav,H_refNav1);

        data_grid_nav = GC.applyPhaseCorrection(data_grid_nav,Kidx_odd_nav);
        Snav = generic_calibration(data_grid_nav,header,0.03);
        
        %remove calibration scans
        data_epi_nav = data_epi_nav(:,:,:,w_imag.epi_multiband_factor+1:end,:,:,:);
    
    
end

%% blip down single band scan
if(header.singleBandRefDown ==1)
    
    [data_epi_ref2,w_ref2, H_ref2]=generic_singleBandCalibration(data_epi,w_imag,H(1));    
    
    data_epi_ref2 = data_epi_ref2(:,:,:,:,:,slcIdxFull,:);%undo interleaved slice ordering
    
    H_ref2 = H_ref2.scale([-1 1]); %because we changed the instruction amplitude in generic.e
    
    %apply in-plane shifts
    data_epi_ref2 = generic_inplaneShifts(data_epi_ref2,header.xoff,header.yoff,H_ref2);
    
    [data_grid2, Kidx, Kidx_odd]=generic_sortGrid(data_epi_ref2,w_ref2,H_ref2);

    GC = generic_ghostCorrection(reshape(data_grid2(:,:,:,1,1,:,:),[size(data_grid,1) size(data_grid,2) size(data_grid,3) w_imag.slquant size(data_grid,7)]),Kidx_odd);
    
    data_grid2 = GC.applyPhaseCorrection(data_grid2,Kidx_odd);
    
    
    %ghost correction for hybrid-EPI data
    Kidx_dummy = zeros(size(data_epi_ref,1),size(data_epi_ref,2),1);
    Kidx_dummy(:,logical(H_ref2.evenOdd),:)=1;
    data_epi_ref2 = GC.applyPhaseCorrection(data_epi_ref2,logical(Kidx_dummy));

    Sdown = generic_calibration(data_grid2,header,0.03);
    
    %remove ref-data from data_imag
    data_epi = data_epi(:,:,:,w_imag.epi_multiband_factor+1:end,:,:,:);

   
        [data_epi_ref_nav2,w_ref_nav2,HrefNav2]=generic_singleBandCalibration(data_epi_nav,w_imag,H(2));
        HrefNav2 = HrefNav2.scale([-1 1]); %because we changed the instruction amplitude in generic.e
        data_epi_ref_nav2 = data_epi_ref_nav2(:,:,:,:,:,slcIdxFull,:);
        data_epi_ref_nav2 = generic_inplaneShifts(data_epi_ref_nav2,header.xoff,header.yoff,HrefNav2);
        [data_grid_nav2, Kidx_nav, Kidx_odd_nav]=generic_sortGrid(data_epi_ref_nav2,w_ref_nav2,HrefNav2);

        data_grid_nav2 = GC.applyPhaseCorrection(data_grid_nav2,Kidx_odd_nav);
        Snavdown = generic_calibration(data_grid_nav2,header,0.03);
        
        %remove calibration scans
        data_epi_nav = data_epi_nav(:,:,:,w_imag.epi_multiband_factor+1:end,:,:,:);

else
    Sdown = S;
    data_epi_ref2 = data_epi_ref;
    H_ref2 = H_refUp;
end




%% re-order slices prior to multi-band slice offset correction
    data_epi = data_epi(:,:,:,:,:,slcIdx,:);
    data_epi_nav = data_epi_nav(:,:,:,:,:,slcIdx,:);

%% slice offset correction
    if(w_imag.epi_multiband_factor > 1)
        data_epi=generic_sliceOffsetCorrection(data_epi,w_imag,H(1),0);
        data_epi_nav=generic_sliceOffsetCorrection(data_epi_nav,w_imag,H(2),0); 
    end

%% ghost correction on epi-data (prior to sorting into 3d k-space)
    Kidx_dummy = zeros(size(data_epi,1),size(data_epi,2),size(data_epi,3));
    Kidx_dummy(:,logical(H(1).evenOdd),:)=1;
    
    if(GhostRefLines)
        data_epi = generic_applyGhostPhase(data_epi,Kidx_dummy,pc_fit(:,3:end,:));
     else        
        data_epi = GC.applyPhaseCorrection(data_epi,logical(Kidx_dummy));
    end
    
    Kidx_dummy = zeros(size(data_epi_nav,1),size(data_epi_nav,2),size(data_epi_nav,3));
    Kidx_dummy(:,logical(H(2).evenOdd),:)=1;
    data_epi_nav = GC.applyPhaseCorrection(data_epi_nav,logical(Kidx_dummy));


%% in-plane shifts
    data_epi = generic_inplaneShifts(data_epi,header.xoff,header.yoff,H(1));

    data_epi_nav = generic_inplaneShifts(data_epi_nav,header.xoff,header.yoff,H(2));   

    
    
%% export to hybrid sense
if(w_imag.ro_type == 15)
    save('dataHybrid.mat','data_epi','data_epi_nav','S','Snav','Sdown','Snavdown','w_imag','H','-v7.3')
    save('dataRefHybrid.mat','data_epi_ref','data_epi_ref2','w_ref','H_refUp','H_ref2')
elseif(header.blipUpDownIlv)
    save('dataHybrid.mat','data_epi','data_epi_down','S','Sdown','H','w_imag','-v7.3')
    save('dataRefHybrid.mat','data_epi_ref','data_epi_ref2','data_grid_nav','data_grid_nav2','w_ref','H_refUp','H_ref2')
end

%% full sense reconstruction
    %sort into 3d-grid 
    [data_grid, Kidx, Kidx_odd]=generic_sortGrid(data_epi,w_imag,H(1));

 
    [data_grid_nav, Kidx_nav, Kidx_odd_nav]=generic_sortGrid(data_epi_nav,w_imag,H(2));

        
    %fix partial fourier indexing
    Kidx = generic_partialFourier(Kidx);

    Kidx_nav = generic_partialFourier(Kidx_nav);


    w_imag.ny_sense = w_imag.ny;
    [rcn,gmap]=generic_sense(data_grid,Kidx,w_imag,H(1),S);
    figure;  imagesc(array2mosaic(abs(rcn(:,:,:,1))));colormap gray


     rcn_nav=generic_sense(data_grid_nav,Kidx_nav,w_imag,H(2),Sdown);
     figure;  imagesc(array2mosaic(abs(rcn_nav(:,:,:,1))));colormap gray


    
end

%% BZ multi band slice re-ordering
function slcIdx=getSliceIndex(Ntot,mb_factor,mode)

    if nargin < 3
        mode = 'sense';
    end

    if (mod(mb_factor,2)==1)
        %odd multiband factor
        sliceInc = Ntot/mb_factor;
        slcIdx = 1:sliceInc:Ntot;
    else
        sliceInc = Ntot/mb_factor;
        slcIdx = 1:sliceInc:Ntot;
        %if (strcmp(mode,'cg'))
        %    slcIdx = slcIdx(end:-1:1);
        %end
    end    
end
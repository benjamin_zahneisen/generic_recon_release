function [data, slcOffset]=generic_sliceOffsetCorrection(data,w_imag,H,header)

%data = 7d =[Nx Nxy Ni ...]
%w_imag
%H = trajectory class
%p-file header info

d=0; %fudge the slice offset (not used for now)
nsl = w_imag.slquant;

coverage=(w_imag.slthick + header.scanspacing)*w_imag.slquant;
if(mod(w_imag.slquant,2)==0)
    slcPos1 = -coverage/2 + (w_imag.slthick + header.scanspacing)/2;
    slcPos = slcPos1:(w_imag.slthick + header.scanspacing):(coverage/2-w_imag.slthick/2);
        
    if(mod(w_imag.epi_multiband_factor,2)==0); %mb=2,4,6,...
        lowestSliceGroup = 1:(w_imag.slquant/w_imag.epi_multiband_factor):w_imag.slquant;
        lowestSliceOffset = slcPos(lowestSliceGroup(w_imag.epi_multiband_factor/2 + 1));
    else %3,5,...
        lowestSliceGroup = 1:(w_imag.slquant/w_imag.epi_multiband_factor):w_imag.slquant;
        lowestSliceOffset = slcPos(lowestSliceGroup(ceil(w_imag.epi_multiband_factor/2 )) );  
    end
        
else
    display('not yet implemented.')
end

%add kz-dependent phase
for sl=1:(nsl/w_imag.epi_multiband_factor)
    slcOffset(sl) = lowestSliceOffset + (sl-1)*(w_imag.slthick + header.scanspacing);
    phaseOffset(:,:,sl) = exp(1i*(H.Kyz(2,:)*(slcOffset(sl)+d)));
end
phaseOffset = reshape(phaseOffset,[1 size(data,2) size(data,3) 1 1 size(data,6) 1]);
phaseOffset = repmat(phaseOffset,[size(data,1) 1 1 size(data,4) size(data,5) 1 size(data,7) ]);
    
data = data.*phaseOffset;

function [rcn, rcnDown] = generic_recon2(varargin)
%call full EPI recon pipeline for generic p-files
%store raw data for subsequent blip up/down processing 

%options are passed as 'name'|'value' pairs

    %'dosense' = true | [false] : perform sense recon or not

    %'Ncc' = [8] : number of virtual coils after compression

    %'mask_thresh' = [0.98] : threshold for espirit coil mask
    
    %'ts' = [1:tframes] : which time frames to load from p-file
    
    %'gmap' = true | [false] : return g-factor penalty map 
    
    %'savedata' = [true] | false : export rawdata for hybrid-SENSE recon
    
    %'centerAP' = [true] | false : automatically shift image to center
    %along AP


%Benjamin Zahneisen, Stanford University, 07/2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% get filename
[pfilename, opts_start]=generic_getpfilename(varargin);

%% load header and call readout mex file
[w_imag, header]=generic_readPfileHeader_noMEX(pfilename);

%% parse recon options
opts_def.ts=1:header.nd;
opts_def.sls=[1:header.nsl/w_imag.epi_multiband_factor];
opts_def.epi_ref=[];
opts_def.epi_ref_use_entropy=0;
opts_def.epi_ref_entropy_per_slice=0;
opts_def.outpath=pwd;
opts_def.reg=1;
opts_def.flavor ='sense';
opts_def.dosense = false;
opts_def.mask_thresh = 0.98;
opts_def.Ncc = 8;
opts_def.precision = 'single';
opts_def.gmap = false;
opts_def.savedata = true;
opts_def.scanArchive = 'false';
opts_def.centerAP = 'true';
opts=get_opts({varargin{opts_start:end}},opts_def);

header.nd = length(opts.ts);

%% load the raw data with downward compatiliblty for DV25 p-file
if(strcmp(header.format,'pfile27'))
    [data, header2, ec] = read_MR_rawdata(pfilename);
    data = permute(data,[2 3 1 4 5 6]);
elseif(strcmp(header.format,'scanArchive'))
    data=generic_loadScanArchive(scanArchiveName,header);   
else
    data=generic_loadData(pfilename,header,w_imag);
    if(strcmp(opts.precision,'single'))
        data = single(data);
    end
end

if(strcmp(header.hdr.image.psd_iname,'generic_dPFG'))
    generic_exportHeader(header.hdr);
end

%% align gradient trajectory and ADC sampling by stretching w_imag.gx...
%downward compatibility (temp)
%disp('be careful: manually setting w_imag.dts_adc = w_imag.dts/2')
%disp('should be removed in future versions')
%w_imag.dts_adc = w_imag.dts/2;

if isfield(w_imag,'dts_adc')
    [w_imag, frsizeNew] = generic_oversampling(w_imag,w_imag.dts_adc/w_imag.dts,size(data,1));
    data = interp1(linspace(-1,1,size(data,1)),data,linspace(-1,1,frsizeNew),'linear');
else
    w_imag.dts_adc = w_imag.dts;
end
%% split ref and normal epi (not yet activated 2017_04_11)
%if(w_imag.add_epi_ref)
%     [w_imag, w_ref,data,data_ref]=generic_strip_ref(w_imag,data);
%end


%% slice re-ordering (linear/interleaved)
if(header.sliceOrderMode == 1)
    slcIdxFull = 1:2:w_imag.slquant;
    slcIdxFull = [slcIdxFull (2:2:w_imag.slquant)];
    [~, slcIdxFull]=sort(slcIdxFull);
    slcIdx = 1:2:w_imag.slquant_eff;
    slcIdx = [slcIdx (2:2:w_imag.slquant_eff)];
    [~, slcIdx]=sort(slcIdx);
else
    slcIdxFull = 1:w_imag.slquant;
    slcIdx = 1:w_imag.slquant_eff;
end


%% grid and separate readout dimension
[data_epi, H]=generic_reOrderAndGridRO(data,w_imag);


%% get idea how to split host and nav EPI for ud-EPI
if(w_imag.ro_type == 15)

    linesNav = ((w_imag.ny/2)*w_imag.fn)/(w_imag.R_skip);
    linesImag = (w_imag.ny*w_imag.fn)/(w_imag.R_skip);
    
    if (~((linesNav + linesImag)==H.Nlines))
        disp('wrong navigator lines')
    end
end

%% single band, central region k-space in case of interleaved EPI(=inplane acceleration >1)
if(header.singleBandRef ==1)
    disp('single band calibration')
    
    %this function must match generic_dti.e 
    [data_epi_ref,w_ref, H_refUp]=generic_singleBandCalibration(data_epi,w_imag,H(1));
    
    %undo interleaved slice ordering
    data_epi_ref = data_epi_ref(:,:,:,:,:,slcIdxFull,:);
    
    %apply in-plane shifts
    data_epi_ref = generic_inplaneShifts(data_epi_ref,header.xoff,header.yoff,H_refUp);
    
    %sort into kx-ky-(kz=1)-grid
    [data_grid, ~, Kidx_odd]=generic_sortGrid(data_epi_ref,w_ref,H_refUp);
    
    GC = generic_ghostCorrection(reshape(data_grid(:,:,:,1,1,:,:),[size(data_grid,1) size(data_grid,2) size(data_grid,3) w_imag.slquant size(data_grid,7)]),Kidx_odd);  
    
    %make sure the ghost correction matches the actual data
    data_grid = GC.applyPhaseCorrection(data_grid,Kidx_odd);
    
    %do sos recon and autocenter fov
    if opts.centerAP    
        vol = sos(ifff2d(squeeze(data_grid)));
        [dy_mm,~] = generic_autoCenterAP(vol,w_imag);
        data_epi_ref = generic_inplaneShifts(data_epi_ref,0,-dy_mm,H_refUp);
        [data_grid, ~, Kidx_odd]=generic_sortGrid(data_epi_ref,w_ref,H_refUp);
        data_grid = GC.applyPhaseCorrection(data_grid,Kidx_odd);
    end
        
    %ghost correction for hybrid-EPI data
    Kidx_dummy = zeros(size(data_epi_ref,1),size(data_epi_ref,2),1);
    Kidx_dummy(:,logical(H_refUp.evenOdd),:)=1;
    data_epi_ref = GC.applyPhaseCorrection(data_epi_ref,logical(Kidx_dummy));  
    
    %calculate sensitivity maps
    disp('calculating coil sensitivities from blip-up ref scan')
    S = generic_calibration(data_grid,header,0.03);
    S.mask_thresh = opts.mask_thresh;
    
    %remove ref-data from data_imag
    data_epi = data_epi(:,:,:,w_imag.epi_multiband_factor+1:end,:,:,:);
    
end

%% blip down single band scan
if(header.singleBandRefDown ==1)
    
    [data_epi_ref2,w_ref2, H_ref2]=generic_singleBandCalibration(data_epi,w_imag,H(1));    
    
    data_epi_ref2 = data_epi_ref2(:,:,:,:,:,slcIdxFull,:);%undo interleaved slice ordering
    
    H_ref2 = H_ref2.scale([-1 1]); %because we changed the instruction amplitude in generic.e
    
    %apply in-plane shifts
    data_epi_ref2 = generic_inplaneShifts(data_epi_ref2,header.xoff,header.yoff,H_ref2);
    
    [data_grid2, ~, Kidx_odd]=generic_sortGrid(data_epi_ref2,w_ref2,H_ref2);

    %get the ghost parameters only once from bip up reference
    %GC = generic_ghostCorrection(reshape(data_grid2(:,:,:,1,1,:,:),[size(data_grid,1) size(data_grid,2) size(data_grid,3) w_imag.slquant size(data_grid,7)]),Kidx_odd);
    
    data_grid2 = GC.applyPhaseCorrection(data_grid2,Kidx_odd);
    
    %apply shifts from blip up auto-centering
    if opts.centerAP
        data_epi_ref2 = generic_inplaneShifts(data_epi_ref2,0,-dy_mm,H_ref2);
        [data_grid2, ~, Kidx_odd]=generic_sortGrid(data_epi_ref2,w_ref2,H_ref2);
        data_grid2 = GC.applyPhaseCorrection(data_grid2,Kidx_odd);
    end
       
    %ghost correction for hybrid-EPI data
    Kidx_dummy = zeros(size(data_epi_ref,1),size(data_epi_ref,2),1);
    Kidx_dummy(:,logical(H_ref2.evenOdd),:)=1;
    data_epi_ref2 = GC.applyPhaseCorrection(data_epi_ref2,logical(Kidx_dummy));

    disp('calculating coil sensitivities from blip-down ref scan')
    Sdown = generic_calibration(data_grid2,header,0.03);
    Sdown.mask_thresh = opts.mask_thresh;
    
    %remove ref-data from data_imag
    data_epi = data_epi(:,:,:,w_imag.epi_multiband_factor+1:end,:,:,:);

else
    Sdown = S;
    data_epi_ref2 = data_epi_ref;
    H_ref2 = H_refUp;
end


%% split data into nav and imaging part (after calibration scans)
if(w_imag.ro_type == 15)

    linesNav = ((w_imag.ny/2)*w_imag.fn)/(w_imag.R_skip);
    linesImag = (w_imag.ny*w_imag.fn)/(w_imag.R_skip);
    
    if (~((linesNav + linesImag)==H.Nlines))
        disp('wrong navigator lines')
    end

    data_epi_nav = data_epi(:,1:linesNav,:,:,:,:,:);
    data_epi = data_epi(:,linesNav+1:end,:,:,:,:,:);
    
    H_nav = H.getSubset(1:linesNav);
    H_epi = H.getSubset(linesNav+1:H.Nlines);
    Hfull =H;
    H(1) = H_epi;
    H(2) = H_nav;


elseif((w_imag.ro_type == 2) && header.blipUpDown)
   data_epi_down = data_epi(:,:,2,:,:,:,:); %blip up/down is always realized as interleaves
   data_epi = data_epi(:,:,1,:,:,:,:);   
   
else
    data_epi = data_epi(:,:,1:w_imag.inplane_R:end,:,:,:,:);
    data_epi_down = data_epi(:,:,1:w_imag.inplane_R:end,2,:,:,:);
    %invert y-ampliutde for 2nd readout (blip down)
    H(2) = H(1);
    H(2) = H(2).scale([-1 1]);
    header.blipUpDownIlv = 1;
end


%% re-order slices prior to multi-band slice offset correction
    data_epi = data_epi(:,:,:,:,:,slcIdx,:);
    if(header.blipUpDown)
        data_epi_down = data_epi_down(:,:,:,:,:,slcIdx,:);
    end
    if(w_imag.ro_type == 15)
        data_epi_nav = data_epi_nav(:,:,:,:,:,slcIdx,:);
    end

%% slice offset correction
    if(w_imag.epi_multiband_factor > 1)
        data_epi=generic_sliceOffsetCorrection(data_epi,w_imag,H(1),header);
        if(header.blipUpDown)
            data_epi_down = generic_sliceOffsetCorrection(data_epi_down,w_imag,H(2),header);
        end
        if(w_imag.ro_type == 15)
            data_epi_nav=generic_sliceOffsetCorrection(data_epi_nav,w_imag,H(2),header); 
        end
    end

%% ghost correction on epi-data (prior to sorting into 3d k-space)
    Kidx_dummy = zeros(size(data_epi,1),size(data_epi,2),size(data_epi,3));
    Kidx_dummy(:,logical(H(1).evenOdd),:)=1;
    data_epi = GC.applyPhaseCorrection(data_epi,logical(Kidx_dummy));
    if(header.blipUpDown)
        data_epi_down = GC.applyPhaseCorrection(data_epi_down,logical(Kidx_dummy));
    end
    if(w_imag.ro_type == 15)
        Kidx_dummy = zeros(size(data_epi_nav,1),size(data_epi_nav,2),size(data_epi_nav,3));
        Kidx_dummy(:,logical(H(2).evenOdd),:)=1;
        data_epi_nav = GC.applyPhaseCorrection(data_epi_nav,logical(Kidx_dummy));
    end

%% in-plane shifts
    if opts.centerAP
        %dy = (dy_mm + dy2_mm)/2; %mean of up-down shift [mm]
        dy = dy_mm; %only shift from blip up
        header.yoff = header.yoff - dy;
    end
    data_epi = generic_inplaneShifts(data_epi,header.xoff,header.yoff,H(1));
    if(header.blipUpDown)
        data_epi_down = generic_inplaneShifts(data_epi_down,header.xoff,header.yoff,H(2));
    end
    if(w_imag.ro_type == 15)
        data_epi_nav = generic_inplaneShifts(data_epi_nav,header.xoff,header.yoff,H(2));   
    end
    
%% distortion corrected sense-maps
if (header.blipUpDown && opts.savedata)
    [rcnJoint, f_ref]=blipUpDownRefscansEPIdata(data_epi_ref,data_epi_ref2,w_ref,H_refUp,H_ref2);
    disp('calculating coil sensitivities from topup corrected refscans')
    S_corr = CoilSensitivities(rcnJoint,'espirit',0.02);
    S_corr.fov = [10*w_imag.fovx 10*w_imag.fovy w_imag.slquant*w_imag.slthick];
    %S_corr = S_corr.resize([w_imag.nx w_imag.ny w_imag.slquant],[10*w_imag.fovx 10*w_imag.fovy S_corr.fov(3)]);
    S_corr.mask_thresh = opts.mask_thresh;
      
    %save('S_corr.mat','S_corr')
    
end

%% compress coil sensitivities and data (read directions must have equal lengths)
if(S.Nc > opts.Ncc)
    %resize coils first
    w_imag.ny_sense = w_imag.ny;
    S = S.resize([w_imag.nx w_imag.ny_sense w_imag.slquant],[10*w_imag.fovx 10*w_imag.fovy S.fov(3)],'nearest');
    if(w_imag.Rtot >= opts.Ncc)
        opts.Ncc = opts.Ncc*1.5; 
        display(['decreasing compression factor by 50% to ',num2str(opts.Ncc)])
    end
    S = S.compress(opts.Ncc); %compress data to 12 channels
    Sdown = Sdown.resize([w_imag.nx w_imag.ny_sense w_imag.slquant],[10*w_imag.fovx 10*w_imag.fovy S.fov(3)],'nearest');
    Sdown = Sdown.compress(opts.Ncc,S.gccmtx);
    if(opts.savedata && header.blipUpDown)
        S_corr = S_corr.resize([w_imag.nx w_imag.ny_sense w_imag.slquant],[10*w_imag.fovx 10*w_imag.fovy S.fov(3)],'nearest');
        S_corr = S_corr.compress(opts.Ncc,S.gccmtx);
    end
    data_epi = applyCC(data_epi,S); %
    data_epi_down = applyCC(data_epi_down,S);
else
    %re-sizing S_corr only
    w_imag.ny_sense = w_imag.ny;
    S = S.resize([w_imag.nx w_imag.ny_sense w_imag.slquant],[10*w_imag.fovx 10*w_imag.fovy S.fov(3)],'nearest');
    Sdown = Sdown.resize([w_imag.nx w_imag.ny_sense w_imag.slquant],[10*w_imag.fovx 10*w_imag.fovy S.fov(3)],'nearest');
    if(exist('S_corr','var'))
        S_corr = S_corr.resize([w_imag.nx w_imag.ny_sense w_imag.slquant],[10*w_imag.fovx 10*w_imag.fovy S.fov(3)],'nearest');
    end 
end


%% export to hybrid sense
if(opts.savedata)
if(w_imag.ro_type == 15)
    save('dataHybrid.mat','data_epi','data_epi_nav','S','Sdown','w_imag','H','-v7.3')
    save('dataRefHybrid.mat','data_epi_ref','data_epi_ref2','w_ref','H_refUp','H_ref2')
elseif(header.blipUpDown)
    %save('dataHybrid.mat','data_epi','data_epi_down','S','Sdown','H','w_imag','-v7.3');
    %save('dataRefHybrid.mat','data_epi_ref','data_epi_ref2','w_ref','H_refUp','H_ref2');
    RAW_BASE = getenv('RAW_BASE');
    RAWDIR = getenv('RAWDIR');
    unix(['mkdir ',RAWDIR]);
    [Nx, Ns, Ni, Nt, Necho, Nsl, Nc]  = size(data_epi);
    for t=1:size(data_epi,4)
        hybSENSEdat = hSENSEdata(single(reshape(data_epi(:,:,1,t,:,:,:),[Nx Ns 1 Nsl Nc])),single(reshape(data_epi_down(:,:,1,t,:,:,:),[Nx Ns 1 Nsl Nc])));
        hybSENSEdat.SMSfactor = length(slcIdx);
        hybSENSEdat.SMSidx = slcIdx;
        hybSENSEdat.tframe = t;
        hybSENSEdat.Nt = Nt;
        hybSENSEdat.H = H;
        save(['./',RAWDIR,'/',RAW_BASE,'_',num2str(t),'.mat'],'hybSENSEdat');
    end
    Sup = S;
    save(['./',RAWDIR,'/','S_up.mat'],'Sup');
    save(['./',RAWDIR,'/','S_down.mat'],'Sdown');
    save('S_corr.mat','S_corr','w_imag','header');
end
end

%% full sense reconstruction
if(opts.dosense)
    
    % 1. sort into 3d-grid 
    [data_grid, Kidx, Kidx_odd,w_imag]=generic_sortGrid(data_epi,w_imag,H(1));
    if(header.blipUpDown)
        [data_grid_down, KidxDown, Kidx_odd]=generic_sortGrid(data_epi_down,w_imag,H(2));
    end
    if(w_imag.ro_type == 15)
        [data_grid_nav, Kidx_nav, Kidx_odd_nav]=generic_sortGrid(data_epi_nav,w_imag,H(2));
    end
        
    %2. fix partial fourier indexing 
    [Kidx,w_imag] = generic_partialFourier(w_imag,Kidx);
    if(header.blipUpDown)
        KidxDown = generic_partialFourier(w_imag,KidxDown);
    end
    if(w_imag.ro_type == 15)
        Kidx_nav = generic_partialFourier(Kidx_nav);
    end
    
    %3. sense recon, calculate g-map only if required
    if(opts.gmap)
        [rcnUp,gmap]=generic_sense(data_grid,Kidx,w_imag,H(1),S);
        save('gmap.mat','gmap');
    else
        rcnUp=generic_sense(data_grid,Kidx,w_imag,H(1),S);
    end
    figure;  imagesc(array2mosaic(abs(rcnUp(:,:,:,1))));colormap gray
    
    
    if(header.blipUpDown)
        rcnDown=generic_sense(data_grid_down,KidxDown,w_imag,H(2),Sdown);
        figure;  imagesc(array2mosaic(abs(rcnDown(:,:,:,1))));colormap gray
    end
    if(w_imag.ro_type == 15)
        rcn_nav=generic_sense(data_grid_nav,Kidx_nav,w_imag,H(2),Sdown);
        figure;  imagesc(array2mosaic(abs(rcn_nav(:,:,:,1))));colormap gray
    end
    
    %return output
    rcn = rcnUp;
else
   rcn =0; 
end


%% export everthing
    time=clock;
    fprintf('%.2d %.2d %.2d: Reconstruction complete. Writing output files...\n', time(4), time(5),time(6));
    if(opts.dosense)
        
        if(w_imag.ny_sense > w_imag.ny)
            rcnUp = imresize4D(abs(rcnUp),[w_imag.nx w_imag.ny size(rcnUp,3)]);
            if(header.blipUpDown)
                rcnDown = imresize4D(abs(rcnDown),[w_imag.nx w_imag.ny size(rcnUp,3)]);
            end
        end
        
        %flip x-direction to match gradient files 
        if(header.blipUpDown)
            generic_write_nii(smooth4D(abs(rcnUp),[3 3 3],0.5,'slice'),header.hdr,w_imag,'blipUp',1);
            generic_write_nii(smooth4D(abs(rcnDown),[3 3 3],0.5,'slice'),header.hdr,w_imag,'blipDown',1);
        else
            generic_write_nii(smooth4D(abs(rcnUp),[3 3 3],0.5,'slice'),header.hdr,w_imag,'nii');
        end
    else
        rcn = [];
        rcnDown = [];
    end
    
end

%% BZ multi band slice re-ordering
function slcIdx=getSliceIndex(Ntot,mb_factor,mode)

    if nargin < 3
        mode = 'sense';
    end

    if (mod(mb_factor,2)==1)
        %odd multiband factor
        sliceInc = Ntot/mb_factor;
        slcIdx = 1:sliceInc:Ntot;
    else
        sliceInc = Ntot/mb_factor;
        slcIdx = 1:sliceInc:Ntot;
    end    
end
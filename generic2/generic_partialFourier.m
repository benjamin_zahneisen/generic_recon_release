function [Kidx_pf,w] = generic_partialFourier(w,Kidx)

%extend the sampling pattern into "empty" partial fourier regions


%check for mb-acceleration
% zInc = ceil(w.epi_multiband_R);
% w.nmb_sense = w.epi_multiband_factor;
% if(w.epi_multiband_factor >1)
%     if(zInc ~= w.epi_multiband_R)
%         for l=1:w.epi_multiband_factor
%             w.nmb_sense = w.epi_multiband_factor + l;
%             w.nmbR_sense = zInc;
%             if (mod(w.nmb_sense,w.nmbR_sense) ==0)
%                 break;
%             end
%         end
%     end
% end
    
%check for in-plane acceleration
% w.ny_sense = w.ny;
% effRy = (w.nmb_sense/w.epi_multiband_R)*w.inplane_R;
% if(mod(w.ny,effRy) ~= 0)
%     for l=1:w.nmb_sense
%         if (mod(w.ny+l,effRy) ~=0)
%             w.ny_sense = w.ny + l;
%             break;
%         end
%     end
% end

%
% Kidx_pf_sense = Kidx;
% dny = w.ny_sense - w.ny;
% dnz = w.nmb
% Kidx_pf_sense(:,w.ny+dny:w.ny_sense,

%get a blurred psf 
psf = ifff3d(Kidx);
maxima = find(abs(psf) > 0.93*max(abs(psf(:))));
psf2 = zeros(size(Kidx));
psf2(maxima)=1/length(maxima);
psf2(maxima) = psf2(maxima).*exp(1i*angle(psf(maxima)));
Kidx_pf = abs(fff3d(psf2));
Kidx_pf = Kidx_pf > 2*eps;


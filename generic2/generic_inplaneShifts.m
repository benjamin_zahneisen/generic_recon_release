function data = generic_inplaneShifts(data,dx,dy,H)

%data is [Nx Nlines Ni Nt 1 Nsl Nc]
% H = HybridSenseTrajectory object

%each interleave needs a tracetory object
if length(H) == size(data,3)

    %theta = exp(-sqrt(-1)*(w.kx_all(w.gx_start+1:w.gx_end+1,1:ni)*xoff  -ypolarity*w.ky_all(w.gy_start+1:w.gy_end+1,1:ni)*yoff ));
    
    thetaX(:,1) = exp(-1i*H.kx*dx);
    for n=1:size(data,3)
        thetaY(1,:,n) = exp(1i*H(n).Kyz(1,:)*dy);
    end
    
    data = data.*repmat(thetaX,[1 size(data,2) size(data,3) size(data,4) size(data,5) size(data,6) size(data,7)]);
    data = data.*repmat(thetaY,[size(data,1) 1 1 size(data,4) size(data,5) size(data,6) size(data,7)]);
    
else
    display('not enough trajectory objects for all interleaves')
end
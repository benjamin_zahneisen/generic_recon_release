function [data_epi_ref,w_ref, H_refUp]=generic_singleBandCalibration(data_epi,w_imag,H_up)
%this function must match the relevant section in generic_xxx.e
%the basic idea is to acquire fully sampled, single band, single shot
%images as reference scans


%Benjamin Zahneisen, Stanford University, 05/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    
    
    %single band coverage now spreads over multiple TRs
    data_epi_ref = data_epi(:,:,1,1:w_imag.epi_multiband_factor,1,:,:);
    data_epi_ref = permute(data_epi_ref,[1 2 3 5 6 4 7]);
    data_epi_ref = reshape(data_epi_ref,[size(data_epi,1) size(data_epi,2) 1 1 1 w_imag.slquant size(data_epi_ref,7)]);
    
    %decrease gradient amplitude (corresponds to setiamp())
    H_refUp = H_up.scale([1/w_imag.inplane_R 0]); %in-plane acceleration can be different from interleaves
    H_refUp.N(3) = 1; %it's a single band acquisition
    H_refUp.kzmax = 0;
    
    w_ref = w_imag;
    w_ref.ni = 1;
    w_ref.ny = w_imag.ny/w_imag.inplane_R;
    w_ref.epi_multiband_factor = 1;
    w_ref.epi_multiband_R = 1;
    w_ref.slquant_eff = w_imag.slquant;
    w_ref.R_skip = 1; %we do no longer skip samples
    w_ref.inplane_R=1;
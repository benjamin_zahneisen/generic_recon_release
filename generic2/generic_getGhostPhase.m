function [pc, pc_fit, data_epi_refLines] = generic_getGhostPhase(data_refLines,w_ref)

%ghost correction from ref lines

%start +2 might be because add_prewinder adds a node with length 2 in
%gen_ud_epi.c

w_ref.gx_start = w_ref.gx_start + 2;
w_ref.gx_end = w_ref.gx_end + 2;
%w_ref.gy_start = w_ref.gy_start + 3;
%w_ref.gz_start = w_ref.gz_start + 3;
%w_ref.gx_end = w_ref.gx_end + 3;

data_ref = data_refLines(w_ref.gx_start+1:w_ref.gx_end+1,:,:,:,:,:);
[data_epi_refLines, H_refLines]=generic_reOrderAndGridRO(data_ref,w_ref);

cdata = sum(data_epi_refLines,7);
lines = ifff(cdata);
% 
% lines1 = ifff(squeeze(data_epi_refLines(:,2,1,:,1,:,:)));
% lines2 = ifff(squeeze(data_epi_refLines(:,2,1,:,1,:,:)));
% lines3 = ifff(squeeze(data_epi_refLines(:,3,1,:,1,:,:)));
%
lines_odd = lines(:,1,:,:,:,:); %maybe average later
lines_even = lines(:,2,:,:,:,:);
lines_odd2 = lines(:,3,:,:,:,:);

pc = angle(exp(1i*angle(lines_odd./lines_even)));
pc2 = angle(exp(1i*angle(lines_odd2./lines_even)));

%pc = (pc + pc2)/2;

%figure; plot(squeeze(pc(:,1,1,1,1,:)),'r')
%hold on; plot(squeeze(pc2(:,1,1,1,1,:)),'c')

%1st order fit to ghost phase
A(1:size(lines,1),1)=ones(size(lines,1),1);
A(1:size(lines,1),2)=linspace(-1,1,size(lines,1));
for t=1:size(pc,4)
     for sl=1:size(pc,6)
         %for c=1:size(pc,7)
             w=abs(lines_odd(:,1,1,t,1,sl))/max(abs(lines_odd(:,1,1,t,1,sl)));
             W=diag(w);
             iA = (A'*W*A)\(A'*W);
             b(:,t,sl)= iA*pc(:,1,1,t,1,sl);
             pc_fit(:,t,sl)=A*b(:,t,sl);
         %end
     end
 end
%figure; plot(squeeze(pc_fit(:,1,:,1)),'r'); hold on; plot(squeeze(pc(:,1,1,1,1,:,1)))
 
%forming the ghost correction matrix
% d_ref=d(:,size(d,2)-iref*2+1:end,:,:,:);
% d_ref=reshape(d_ref,size(d_ref,1),size(d_ref,2),nd,nsl,nc);
% %!! test
% d_ref(:,2:2:end,:,:,:)=d_ref(end:-1:1,2:2:end,:,:,:);
% d_ref=ifftnc(d_ref,1);
% d_ref_odd=sum(d_ref(:,1:2:end,:,:,:),2);
% d_ref_even=sum(d_ref(:,2:2:end,:,:,:),2);
% %!!!test!!!
% %d_ref_odd=d_ref(:,1,:,:,:);
% %d_ref_even=d_ref(:,2,:,:,:);
% d_ref_dif=exp(-sqrt(-1)*angle(d_ref_odd.*conj(d_ref_even)));
% d_ref_dif=cat(2,d_ref_dif,ones(size(d_ref_dif,1),1,nd,nsl,nc));
% d_ref_dif=repmat(d_ref_dif,[1 (size(d,2)-iref*2)/2 1 1 1]);
% for n=2:30
%     %d_ref_dif(:,:,n,:,:) = d_ref_dif(:,:,1,:,:);
% end


%contructing epi image
% d2=d(:,1:size(d,2)-iref*2,:,:,:);
% d2=reshape(d2,size(d2,1),size(d2,2),nd,nsl,nc);
% d2=ifftnc(d2,1);
% d2=d2.*d_ref_dif;
% d3 = zeros(242,242,30,14,8);
% d3(:,101:152,:,:,:) = d2;
% im=ifftnc(d3,2);
% im=sqrt(sum(abs(im).^2,5));
% im = im(58:174,:,:,:);
% im = permute(im,[1 2 4 3]);
% im = circshift(im,[0 116 0 0]);
% im = imresize(im,[256 256]);
% figure;montage(reshape(im,size(im,1),size(im,2),1,size(im,3)*size(im,4)),'Size',[size(im,4),size(im,3)],'DisplayRange',[0 6]) 
 
 
 
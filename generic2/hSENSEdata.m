%class to store data for a single time frame for hybrid-space SENSE joint
%up/down recon

%5d data form [Nx Ns 1 Nsl Nc]

%Benjamin Zahneisen, Stanford University, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hSENSEdata
    properties (SetAccess = public, GetAccess = public)
        dataUp          = []; %[Nx Ns 1 Nsl Nc]
        dataDown        = [];
        fmap            = []; %[Hz]
        PF_zerofilled   = false;
        Nx              = [];
        Ns              = [];
        Nsl             = [];
        Nc              = [];
        Nt              = []; %total number of time frames in dataset
        SMSfactor       = []; %multi-band factor
        SMSidx          = []; %slice ordering index
        tframe          = []; %time frame index
        H               = []; %array of hybrid-space sense trajectories
    end
    
    methods
        function hS = hSENSEdata(dataUp,dataDown)
            if ~(length(size(dataUp))==5)
               display('wrong data format.') 
            end
            [hS.Nx, hS.Ns, ~, hS.Nsl, hS.Nc]  = size(dataUp);
            hS.dataUp = dataUp;
            hS.dataDown = dataDown;
            
            
        end
    end
end
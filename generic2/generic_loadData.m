function data=generic_loadData(pfilename,header,w)

%output data = [nk x ni x nt x necho(=1) x nsl x nc]

%reading data from GE p-file
%Benjamin Zahneisen, Stanford University, 07/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

frames_to_read=[];
%for i=1:numel(opts.ts)
%    frames_to_read=[frames_to_read (opts.ts(i)-1)*w_imag.ni+1:opts.ts(i)*w_imag.ni];
%end

pfile_list = dir('P*.7');
nr_pfiles = length(pfile_list);

frames_to_read = 1:header.nd*(w.ni/w.inplane_R); %(w.ni/w.inplane_R) because we no longer store empty lines in p-file
%data is size nk x ni x necho x nsl x nc
data=read_pfile_2(pfilename,'frames',frames_to_read, 'nr_pfiles', nr_pfiles);

%append 0's to the data to make frame sizes between traj and data equal
%data(end:w.frsize,:,:,:,:)=0.0;

%check frsize
if(~(w.frsize == size(data,1)))
    display('trajectory and data length do not match.')
end

% putting in the number of interleaves
% nk x ni x nd x 1 x nsl x nc
s=size(data);
data=reshape(data,[s(1) (w.ni/w.inplane_R) header.nd s(3:end)]);

function data = generic_applyGhostPhase(data,OddIdx,pc)

data = ifff(data,1);

nt = size(data,4);

for t=1:nt
    for sl=1:size(pc,3)
        for c=1:size(data,7)
            phase = repmat(pc(:,t,sl),[1 size(data,2) size(data,3)]);
            phase(~logical(OddIdx)) = 0;
            data(:,:,:,t,1,sl,c) = data(:,:,:,t,1,sl,c).*exp(1i*phase);
        end
    end
end

data = fff(data,1);
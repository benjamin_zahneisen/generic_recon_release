---------------------------------------------------------------------------

This package includes Matlab functions to perform geometric-decomposition coil compression (GCC). 

It accompanies the journal article:
Zhang T, Pauly JM, Vasanawala SS, Lustig M.
"Coil Compression for Accelerated Imaging with Cartesian Sampling" 
Magnetic Resonance in Medicine, 2012, in press.

Please send any feedback to Tao Zhang at tao@mrsrl.stanford.edu.
This package is available online at
http://www.stanford.edu/~tzhang08/software.html

(c) Tao Zhang, Stanford University, 2011

----------------------------------------------------------------------------

A GE resolution phantom dataset is given as example. 
It was acquired at a 1.5T GE scanner with an 8ch cardiac coil (2x2 array on both posterior and anterior sides).

The coil compression is written as a function (CoilCompression.m).
Another function (crop.m, provided by Dr. Michael Lustig) is also called within CoilCompression.m.

To run the demo, simply run demo_gcc.m.

----------------------------------------------------------------------------


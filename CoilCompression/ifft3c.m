function res = ifft3c(x)
%
%
% res = ifft3c(x)
% 
% orthonormal centered 3D ifft
%
% (c) Tao Zhang 2010

% res = sqrt(length(x(:)))*ifftshift(ifft2(fftshift(x)));

tmp = fftshift(fftshift(x,1),2);

tmp = ifft(ifft(ifft(tmp,[],1),[],2),[],3);
res = fftshift(fftshift(tmp,1),2);


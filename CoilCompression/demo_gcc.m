% This is a demo of geometric-decomposition coil compression for 3D Cartesian MRI.
% The original dataset is a fully sampled GE resolution phantom dataset 
% The dataset was acquired with 8ch cardiac coil (2x2 array in both posterior and anterior sides).
% (c) Tao Zhang, Stanford University, 2011

clear all;close all;

load kspace.mat;
[fe,pe,se,coils] = size(kspace);
numVrec = 3; % set the number of virtual coils
calsize = [20,20]; % set the fully sampled center k-space size
correction = 1; % turn on the alignment of the coil compression matrices
tic
kspace_new = CoilCompression(kspace, numVrec , calsize,correction);
toc

% comparing the results
im_original = ifft3c(kspace);
im_gcc = ifft3c(kspace_new);
slice = round(fe/2); 
im_original_sos = sqrt(sum(abs(squeeze(im_original(slice,:,:,:))).^2,3));
im_gcc_sos = sqrt(sum(abs(squeeze(im_gcc(slice,:,:,:))).^2,3));
error_gcc = abs(im_original_sos-im_gcc_sos);
figure,imshow(cat(2,im_original_sos,im_gcc_sos,5*error_gcc),[]),title('8 original coils, 3 virtual coils, 5x diff')

